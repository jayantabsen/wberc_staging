//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.SourceF1_6aVO;
import in.gov.wberc.vo.SourceListF1_6aVO;


public class SourceForComboDAO {

	public SourceListF1_6aVO getSourcesForCombo(String utilid) throws MyException {
		
		String query1 = QueryConstants.QUERY_SOURCE_FOR_COMBO;
		
		PreparedStatement prstmt1 = null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		
		// Getting Type IDs + Type Names for Stations
		SourceListF1_6aVO slf16avo = null;
		ArrayList<SourceF1_6aVO> sf16aAl = null;
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(utilid));
			
			rs1=prstmt1.executeQuery();
			if ( rs1 != null ) {
				sf16aAl = new ArrayList<SourceF1_6aVO>();
				while ( rs1.next() ) {
					SourceF1_6aVO sf16a = new SourceF1_6aVO();
					sf16a.setId(rs1.getString("epsid"));
					sf16a.setName(rs1.getString("source"));
					sf16aAl.add(sf16a);
					System.out.println("list from db:"+sf16aAl);
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
						
		}
		slf16avo = new SourceListF1_6aVO();
		slf16avo.setSources(sf16aAl);
		return slf16avo;
		
	}
	
}

