//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17dJExcelDataVO;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;

public class Form1_17dJExcelDAO {
	
	public Form1_17dJExcelDataVO getF1_17dJExcelData( String form_id, String py_id, String user_id ) 
			throws MyException, SQLException {
		Form1_17dJExcelDataVO f117djxldvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> row = null;
		ArrayList<ArrayList<String>> rowAl = new ArrayList<ArrayList<String>>();

		String query1 = QueryConstants.QUERY_GET_PHID;
		String query2 = QueryConstants.QUERY_GET_F1__17d_DATA;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(py_id);
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			/*
			select loan_desc, repayable_loan, repayment_rate, drawal_rate from t_arrdf1_17d where form_instance_id=
			(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) and py_id=?
			*/
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, int_ph_id);
			prstmt1.setInt(3, int_py_id);
			
			System.out.println(prstmt1);
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				rowAl = new ArrayList<ArrayList<String>>();
				while (rs1.next()) {
					row = new ArrayList<String>();
					row.add(rs1.getString("loan_desc"));
					row.add(rs1.getString("repayable_loan"));
					row.add(rs1.getString("repayment_rate"));
					row.add(rs1.getString("drawal_rate"));
					rowAl.add(row);
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		f117djxldvo = new Form1_17dJExcelDataVO();
		f117djxldvo.setPy_id(py_id);
		f117djxldvo.setForm_id(form_id);
		f117djxldvo.setUser_id(user_id);
		f117djxldvo.setVals(rowAl);

		return f117djxldvo;
	}
}
