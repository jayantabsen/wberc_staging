package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.From1_1JExcelDataVO;
import in.gov.wberc.vo.StationRowVO;

public class From1_1JExcelDAO {
	
	public From1_1JExcelDataVO getF1_1JExcelData(String ph_id, String util_id) throws MyException {

		From1_1JExcelDataVO f11jxlvo = null;
		
		PreparedStatement prstmt1 = null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
		
		ArrayList<String> cols = null;
		ArrayList<String> rows = null;
		ArrayList<StationRowVO> vals = null;
		StationRowVO stn_rowvo = null;
		ArrayList<ArrayList<String>> valForStationType = null;
		ArrayList<String> stn_row = null;
		
		try {
			conn1=DBUtil.getConnection();
			String query1 = QueryConstants.QUERY_GET_PYID;
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					cols.add(rs1.getString("py_id"));
				}
			}
			
			rows = new ArrayList<String>();
			vals = new ArrayList<StationRowVO>();
			
			int int_util_id = Integer.parseInt(util_id);
			String query2 = "";
			if (int_util_id != 0) {
				query2 = QueryConstants.QUERY_GET_UTILITY_STATION_TYPE_V1;
			} else {
				query2 = QueryConstants.QUERY_GET_UTILITY_STATION_TYPE_V2;
			}
			prstmt1 = conn1.prepareStatement(query2);
			
			if (int_util_id != 0) {
				prstmt1.setInt(1, Integer.parseInt(util_id));
			}
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				while(rs1.next()) {
					stn_rowvo = new StationRowVO();
					int stypeid = Integer.parseInt(rs1.getString("stype_id"));
					stn_rowvo.setStation_type(rs1.getString("stype_desc"));
					
					String query3 = "";
					if (int_util_id != 0) {
						query3 = QueryConstants.QUERY_GET_UTILITY_STATION_V1;
					} else {
						query3 = QueryConstants.QUERY_GET_UTILITY_STATION_V2;
					}
					prstmt1 = conn1.prepareStatement(query3);
					if (int_util_id != 0) {
						prstmt1.setInt(1, stypeid);
						prstmt1.setInt(2, Integer.parseInt(util_id));
					} else {
						prstmt1.setInt(1, stypeid);
					}
					
					rs2=prstmt1.executeQuery();
					
					if(rs2!=null) {
						valForStationType = new ArrayList<ArrayList<String>>();
						while(rs2.next()) {
							stn_row = new ArrayList<String>();
							stn_row.add(rs2.getString("stn_desc"));
							String stnid = rs2.getString("stn_id");
							rows.add(stnid);
							int int_stnid = Integer.parseInt(stnid);
							
							for ( int i = 0; i<cols.size(); i++ ) {
								String get_f1_1_data_val = QueryConstants.QUERY_GET_F1_1_DATA_VAL;
								prstmt1 = conn1.prepareStatement(get_f1_1_data_val);
								prstmt1.setInt(1, int_stnid);
								prstmt1.setInt(2, Integer.parseInt(cols.get(i)));
								rs3=prstmt1.executeQuery();
								if(rs3!=null) {
									if(rs3.next()) {
										stn_row.add(rs3.getString("val"));
									}
								}
							}
							
							valForStationType.add(stn_row);
						}
						stn_rowvo.setValForStationType(valForStationType);
					}
					vals.add(stn_rowvo);
				}
			}
			
			f11jxlvo = new From1_1JExcelDataVO();
			f11jxlvo.setRows(rows);
			f11jxlvo.setCols(cols);
			f11jxlvo.setVals(vals);
			
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		} finally {
			try {
				if(prstmt1!=null) {
					prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
				if(rs2!=null) {
					rs2.close();
				}
				if(rs3!=null) {
					rs3.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			}
		}
		
		return f11jxlvo;
	}
	
}
