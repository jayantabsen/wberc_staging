package in.gov.wberc.dao;

import java.util.ArrayList;

import in.gov.wberc.vo.Form1_18JExcelDataVO_1;
import in.gov.wberc.vo.Form1_19aJExcelDataVO;
import in.gov.wberc.vo.Form1_19aObjectVO;

public class FrontendFormula {

	private static final String[] col_header = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", 
			"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	private static final String[] row_header = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", 
			"13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"};
	
	public static ArrayList<ArrayList<String>> appendFormula_row_1_18_1(ArrayList<String> cols, ArrayList<String> rows1, 
			ArrayList<ArrayList<String>> station_vals) {
		
		ArrayList<String> formula_row = new ArrayList<String>();
		int start_row_index = 0;
		int start_col_index = 0;
		formula_row.add("Total");
		int r = start_row_index + rows1.size();
		int c = start_col_index + cols.size();
		// Exm. =SUM(C13:C14)
		for (int i = start_col_index; i<c; i++) {
			formula_row.add("=SUM("+col_header[i]+row_header[start_row_index]+":"+col_header[i]+row_header[r-1]+")");
		}
		//System.out.println(formula_row_s);
		station_vals.add(formula_row);
		
		return station_vals;
		
	}
	
	public static Form1_19aJExcelDataVO appendFormula_row_1_19a(Form1_19aJExcelDataVO form1_19aRef) {
		
		ArrayList<String> cols = form1_19aRef.getCols();
		ArrayList<String> rows = form1_19aRef.getStn_ids();
		Form1_19aObjectVO data_obj = new Form1_19aObjectVO();
		data_obj = form1_19aRef.getVals();
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		values = data_obj.getVals_for_stations();
		
		ArrayList<String> formula_row = new ArrayList<String>();
		int start_row_index = 0;
		int start_col_index = 0;
		formula_row.add("Total");
		int r = start_row_index + rows.size();
		int c = start_col_index + cols.size();
		// Exm. =SUM(C13:C14)
		for (int i = start_col_index; i<c; i++) {
			formula_row.add("=SUM("+col_header[i]+row_header[start_row_index]+":"+col_header[i]+row_header[r-1]+")");
		}
		values.add(formula_row);
		
		return form1_19aRef;
		
	}
	
}
