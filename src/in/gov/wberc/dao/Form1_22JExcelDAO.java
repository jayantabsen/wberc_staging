//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_20JExcelDataVO;
import in.gov.wberc.vo.Form1_22JExcelDataVO;

public class Form1_22JExcelDAO {
	
	public Form1_22JExcelDataVO getF1_22JExcelData( String form_id, String util_id, String ph_id, String user_id ) 
			throws MyException, SQLException {
		Form1_22JExcelDataVO f122jxldvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> col = null;	
		ArrayList<String> miscids = null;
		ArrayList<String> miscnms = null;
		ArrayList<String> miscrow = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();

		String query1 = QueryConstants.QUERY_GET_PYID;
		String query2 = QueryConstants.QUERY_GET_MISCIDS;
		String query3 = QueryConstants.QUERY_GET_F1_22_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids = new ArrayList<String>();
				miscnms = new ArrayList<String>();
				while(rs1.next()) {
					miscids.add(rs1.getString("misc_form_label_id"));
					miscnms.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			int int_ph_id = Integer.parseInt(ph_id);
			valAl = new ArrayList<ArrayList<String>>();
			
			miscrow = FormCalculationDAO.getComputationResultFrom_F1_20a(ph_id, util_id, "47", user_id); // form_id of 1.20(a) is 47
			valAl.add(miscrow);
			
			prstmt1 = conn1.prepareStatement(query3);
			
			for (int i = 0; i < miscids.size(); i++) {
				miscrow =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids.get(i));
				miscrow.add(miscnms.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					
					/*
				 	SELECT val FROM t_arrdf1_22 where py_id=? and misc_form_label_id=? and form_instance_id= 
				 	(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, int_ph_id);
					
					//System.out.println(prstmt1);
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow.add(rs1.getString("val"));
						}
					}
				}
				valAl.add(miscrow);
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		f122jxldvo = new Form1_22JExcelDataVO();
		f122jxldvo.setCols(col);
		f122jxldvo.setMiscids(miscids);
		f122jxldvo.setVals(valAl);

		return f122jxldvo;
	}

}
