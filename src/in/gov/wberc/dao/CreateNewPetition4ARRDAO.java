package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import in.gov.wberc.constants.JavaConstants;
import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.dms.CreateFolderOnly;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.CapitalExpTypeVO;
import in.gov.wberc.vo.CreateNewPetition4ARRVO;
import in.gov.wberc.vo.DepreciationRateVO;
import in.gov.wberc.vo.MiscFormLabelVO;
import in.gov.wberc.vo.SeasonPeriodVO;

public class CreateNewPetition4ARRDAO {

	public CreateNewPetition4ARRVO createNewPetition4ARR(String base_year, int int_prev_year, int int_ensuing_year, 
			String utilid, String userid, String module) throws MyException, SQLException {
		
		ArrayList<String> prev_years = new ArrayList<String>();
		int int_y1 = Integer.parseInt(base_year.substring(0, 4)); //2017
		for (int i = int_prev_year - 1; i>=0; i--) {
			prev_years.add(String.valueOf(int_y1 - (i + 1)) + "-" + String.valueOf(int_y1 - i).substring(2));
		}
		ArrayList<String> ensu_years = new ArrayList<String>();
		for (int i = 0; i<int_ensuing_year; i++) {
			ensu_years.add(String.valueOf(int_y1 + (i + 1)) + "-" 
					+ String.valueOf(int_y1 + (i + 2)).substring(2));
		}
		
		String query1 = QueryConstants.QUERY_INSERT_t_petition_header;
		String query2 = QueryConstants.QUERY_INSERT_t_petition_year;
		String query3 = QueryConstants.QUERY_GET_UTILITY_ABBREVIATION_BY_ID;
		String setPititionInactive =  QueryConstants.QUERY_SET_PITITION_INACTIVE;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		//String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String currentTimestamp = sdf.format(timestamp);
		
		//System.out.println(sdf.format(timestamp));
		//System.out.println(new java.sql.Timestamp(System.currentTimeMillis()));
		
		int phid;
		String entity = ""; 
		boolean insertTableForPitition = false; //Priyanka 29.01.2018
		CreateNewPetition4ARRVO ref = new CreateNewPetition4ARRVO();//Priyanka 29.01.2018
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1, Statement.RETURN_GENERATED_KEYS);
			
			prstmt1.setInt(1, Integer.parseInt(utilid));
			prstmt1.setString(2, base_year);
			prstmt1.setTimestamp(3, timestamp);
			prstmt1.setInt(4, Integer.parseInt(userid));
			prstmt1.setTimestamp(5, timestamp);
			prstmt1.setInt(6, Integer.parseInt(userid));
			prstmt1.setBoolean(7, true);
			prstmt1.setInt(8, 1);
			prstmt1.setInt(9, 1);
			
			prstmt1.executeUpdate();
			
			rs1 = prstmt1.getGeneratedKeys();
			rs1.next();
			phid = rs1.getInt(1);
			
			prstmt1 = conn1.prepareStatement(query2);
			
			int i = prev_years.size() * (-1);
			for ( String py : prev_years ) {
				prstmt1.setString(1, py);
				prstmt1.setInt(2, i++);
				prstmt1.setBoolean(3, true);
				prstmt1.setInt(4, phid);
				prstmt1.addBatch();
			}
			prstmt1.setString(1, base_year);
			prstmt1.setInt(2, i++);
			prstmt1.setBoolean(3, true);
			prstmt1.setInt(4, phid);
			prstmt1.addBatch();
			for ( String ey : ensu_years ) {
				prstmt1.setString(1, ey);
				prstmt1.setInt(2, i++);
				prstmt1.setBoolean(3, true);
				prstmt1.setInt(4, phid);
				prstmt1.addBatch();
			}
			
			prstmt1.executeBatch();
			
			// Priyanka 16.01.2018 @Start
			int int_utilid = Integer.parseInt(utilid);
			if ( int_utilid !=0 ) {
				prstmt1 = conn1.prepareStatement(query3);
				prstmt1.setInt(1, int_utilid);
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					if(rs1.next()) {
						entity = rs1.getString("util_code");
					}
				}
			}else {
				entity = "WBERC";
			}
			// Priyanka 16.01.2018 @End
			
			conn1.commit();
			
			// Priyanka 29.01.2018 @Start
			insertTableForPitition = insertARRTable(Integer.parseInt(utilid), phid, Integer.parseInt(userid)); 
			
	        if (insertTableForPitition == true) {
	        	CreateFolderOnly.createPetitionFolderStructure(base_year, entity, module);
	    		ref.setPhid(String.valueOf(phid));
	    		ref.setUtilid(utilid);
	        }else {
	        	ref = null;
	    		prstmt1 = conn1.prepareStatement(setPititionInactive);
	    		prstmt1.setInt(1, phid);
	    		prstmt1.executeUpdate();
	    		conn1.commit();
	        }
	     // Priyanka 29.01.2018 @End
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
			throw new MyException();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		/*if (!insertARRTable(Integer.parseInt(utilid), phid, Integer.parseInt(userid))) {
			return null;
		}*/
     
        return ref;
	}
	
private boolean insertARRTable(int utilid, int phid, int userid) throws MyException, SQLException {
		
	boolean insertTableForPitition = false;
 		String formList = QueryConstants.ANNEXURE_FORM_LIST_BY_PITITION_HEADER_ID;
		String insert_t_form_instance = QueryConstants.QUERY_INSERT_t_form_instance;
		String getStationIds = QueryConstants.QUERY_UTILWISE_STATIONS;
		String getPititionYear = QueryConstants.QUERY_PHIDWISE_PYID;
		String unitAndStation = QueryConstants.QUERY_GET_STATION_UNIT_FOR_UTILITY;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		int arrformid;
		
		List<SeasonPeriodVO> seasonPeriodList = null;
		List<MiscFormLabelVO> miscidList = new ArrayList<MiscFormLabelVO>();
		List<Integer> monthidList = new ArrayList<Integer>();
		List<Integer> pumpStationList = new ArrayList<Integer>();
		List<Integer> ensuingPyidList = new ArrayList<Integer>();
		List<DepreciationRateVO> depreciationRateList = new ArrayList<DepreciationRateVO>();
		List<CapitalExpTypeVO> capitalExpList = new ArrayList<CapitalExpTypeVO>();
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(formList);
			prstmt1.setInt(1, phid);
			prstmt1.setInt(2, utilid);
			
			rs1=prstmt1.executeQuery();
			ArrayList<Integer> formidList = new ArrayList<Integer>();
			if(rs1!=null) {
				while(rs1.next()) {
					formidList.add(rs1.getInt("form_id"));
				}
			} 
			
			prstmt1 = conn1.prepareStatement(insert_t_form_instance,Statement.RETURN_GENERATED_KEYS);
			ArrayList<Integer> arrformidAl = new ArrayList<Integer>();
			
			for ( int i = 0; i<formidList.size(); i++ ) {
				prstmt1.setInt(1, formidList.get(i));
				prstmt1.setInt(2, utilid);
				prstmt1.setInt(3, phid);
				prstmt1.setInt(4, 0);
				prstmt1.setTimestamp(5, timestamp);
				prstmt1.setInt(6, userid);
				prstmt1.setTimestamp(7, timestamp);
				prstmt1.setInt(8, userid);
				prstmt1.setBoolean(9, true);
				
				prstmt1.executeUpdate();
				rs1 = prstmt1.getGeneratedKeys();
				rs1.next();
				arrformid = rs1.getInt(1);
				arrformidAl.add(arrformid);
			}
			
			// Getting stnid-s
			prstmt1 = conn1.prepareStatement(getStationIds);
			prstmt1.setInt(1, utilid);
			rs1=prstmt1.executeQuery();
			ArrayList<String> stnidAl = null;
			if(rs1!=null) {
				stnidAl = new ArrayList<String>();
				while(rs1.next()) {
					stnidAl.add(rs1.getString("stn_id"));
				}
			}
			
			// Getting pyid-s
			prstmt1 = conn1.prepareStatement(getPititionYear);
			prstmt1.setInt(1, phid);
			rs1=prstmt1.executeQuery();
			ArrayList<String> pyidAl = null;
			if(rs1!=null) {
				pyidAl = new ArrayList<String>();
				while(rs1.next()) {
					pyidAl.add(rs1.getString("py_id"));
				}
			}
			
			// Getting stnid+unitid-s
			prstmt1 = conn1.prepareStatement(unitAndStation);
			prstmt1.setInt(1, utilid);
			rs1=prstmt1.executeQuery();
			ArrayList<String> stnid_unitidAl = null;
			if(rs1!=null) {
				stnid_unitidAl = new ArrayList<String>();
				while(rs1.next()) {
					stnid_unitidAl.add(rs1.getInt("stn_id")+"~"+rs1.getString("unit_id"));
				}
			}
			
			
			for(int l= 0;l<formidList.size();l++) {
				int formid = formidList.get(l);
				int specific_arrformid = arrformidAl.get(l);
				switch(formid) { 
			    case 1: // For form1.1
			    	String insertForm1_1  = QueryConstants.QUERY_INSERT_t_arrdf1_1;
					prstmt1 = conn1.prepareStatement(insertForm1_1);
					
					for ( int i = 0; i<stnidAl.size(); i++ ) {
						int stnid = Integer.parseInt(stnidAl.get(i));
						for ( int j = 0; j<pyidAl.size(); j++  ) {
							int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, stnid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
						}
					}
			        break; 
			    case 2: // For form1.1a
			    	String insertForm1_1a = QueryConstants.QUERY_INSERT_t_arrdf1_1a;
					prstmt1 = conn1.prepareStatement(insertForm1_1a);
					
					for ( int i = 0; i<stnid_unitidAl.size(); i++ ) {
						String x[] = stnid_unitidAl.get(i).split("~");
						int unitid = Integer.parseInt(x[1]);
						for ( int j = 0; j<pyidAl.size(); j++  ) {
							int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, unitid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
						}
					}
			        break; 
			    case 3: // For form1.2
			    	String insertForm1_2 = QueryConstants.QUERY_INSERT_t_arrdf1_2;
			    	prstmt1 = conn1.prepareStatement(insertForm1_2);
					for ( int i = 0; i<stnidAl.size(); i++ ) {
						int stnid = Integer.parseInt(stnidAl.get(i));
						
						for ( int j = 0; j<pyidAl.size(); j++  ) {
							int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, stnid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
						}
					}
			        break; 
			    case 4: // For form1.2a
			    	String insertForm1_2a = QueryConstants.QUERY_INSERT_t_arrdf1_2a;	
					prstmt1 = conn1.prepareStatement(insertForm1_2a);
					
					for ( int i = 0; i<stnid_unitidAl.size(); i++ ) {
						String x[] = stnid_unitidAl.get(i).split("~");
						int unitid = Integer.parseInt(x[1]);
						
						for ( int j = 0; j<pyidAl.size(); j++  ) {
							int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, unitid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
						}
					}
			        break; 
			    case 5: // For form1.3
			    	seasonPeriodList = getSeasonPeriodList();
			    	String insertForm1_3 = QueryConstants.QUERY_INSERT_t_arrdf1_3;
			    	prstmt1 = conn1.prepareStatement(insertForm1_3);
			    	
			    	if(seasonPeriodList!=null) {
			    		for (SeasonPeriodVO sp : seasonPeriodList) {
							int periodid = sp.getPeriodid();
							
							for ( int i = 0; i<stnidAl.size(); i++ ) {
								int stnid = Integer.parseInt(stnidAl.get(i));
							
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, periodid);
									prstmt1.setInt(4, pyid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
							}
			    		}
					}
			    	break;
			    case 6: // For form1.4a
			    	String insertForm1_4a = QueryConstants.QUERY_INSERT_t_arrdf1_4a;
			    	prstmt1 = conn1.prepareStatement(insertForm1_4a);
			    	
			    	if(seasonPeriodList==null) {
			    		seasonPeriodList = getSeasonPeriodList();
			    	}
			    	if(seasonPeriodList!=null) {
			    		for (SeasonPeriodVO sp : seasonPeriodList) {
							int seasonid = sp.getSeasonid();
							
							for ( int i = 0; i<stnidAl.size(); i++ ) {
								int stnid = Integer.parseInt(stnidAl.get(i));
							
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, seasonid);
									prstmt1.setInt(4, pyid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
							}
			    		}
					}
			    	break;
			    case 7: // For form1.4b
			    	if(seasonPeriodList==null) {
			    		seasonPeriodList = getSeasonPeriodList();
			    	}
			    	pumpStationList = getPumpStationList(utilid);
			    	
			    	String insertForm1_4b = QueryConstants.QUERY_INSERT_t_arrdf1_4b;
			    	prstmt1 = conn1.prepareStatement(insertForm1_4b);
			    	
			    	if(seasonPeriodList!=null) {
			    		for (SeasonPeriodVO sp : seasonPeriodList) {
							//int seasonid = sp.getSeasonid();
							int periodid = sp.getPeriodid();
							
							for ( int i = 0; i<pumpStationList.size(); i++ ) {
								int pumpStnid =  pumpStationList.get(i);
								
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, pumpStnid);
									prstmt1.setInt(3, periodid);
									prstmt1.setInt(4, pyid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
							}
						}
					}
			    	break;
			    case 8: // For form1.5
			    	if(seasonPeriodList==null) {
			    		seasonPeriodList = getSeasonPeriodList();
			    	}
			    	String insertForm1_5 = QueryConstants.QUERY_INSERT_t_arrdf1_5;
			    	prstmt1 = conn1.prepareStatement(insertForm1_5);
			    	
			    	if(seasonPeriodList!=null) {
			    		for (SeasonPeriodVO sp : seasonPeriodList) {
							//int seasonid = sp.getSeasonid();
							int periodid = sp.getPeriodid();
							
							for ( int i = 0; i<stnidAl.size(); i++ ) {
								int stnid = Integer.parseInt(stnidAl.get(i));
								
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, periodid);
									prstmt1.setInt(4, pyid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
								
									prstmt1.executeUpdate();
								}
							}
						}
					}
			    	break;
			    case 9: // For form1.17(b)
			    	/*miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17b = QueryConstants.QUERY_INSERT_t_arrdf1_17b;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17b);
			    	
			    	if(miscidList!=null) {
			    		for(Integer list : miscidList) {
			    			int miscid = list;		
			    			  
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}*/
			    	break;
			    case 10: // For form1.17(c)
			    	/*miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17c = QueryConstants.QUERY_INSERT_t_arrdf1_17c;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17c);
			    	
			    	if(miscidList!=null) {
			    		for(Integer list : miscidList) {
			    			int miscid = list;		
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}*/
			    	break;
			    case 11: // For form1.6(c)
			    	miscidList = getMiscidList(formid);
			    	monthidList = getMonthidList();
			    	
			    	String insertForm1_6c = QueryConstants.QUERY_INSERT_t_arrdf1_6c;
			    	prstmt1 = conn1.prepareStatement(insertForm1_6c);
			    	
			    	if(monthidList!=null && miscidList!=null) {
			    		for ( int j = 0; j<pyidAl.size(); j++  ) {
			    			int pyid = Integer.parseInt(pyidAl.get(j));
			    			for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();	
			    				
			    				for(Integer month : monthidList) {
			    					int monthid = month;
			    					//prstmt1.setInt(1, ++arrdf16cid);
			    					prstmt1.setInt(1, specific_arrformid);
			    					prstmt1.setInt(2, miscid);
			    					prstmt1.setInt(3, pyid);
			    					prstmt1.setDouble(4, 0.0);
			    					prstmt1.setInt(5, monthid);
			    					prstmt1.setTimestamp(6, timestamp);
			    					prstmt1.setInt(7, userid);
			    					prstmt1.setTimestamp(8, timestamp);
			    					prstmt1.setInt(9, userid);
								
			    					prstmt1.executeUpdate();
			    				}
			    			}
			    		}
			    	}
			    	break;
			    case 12: // For form1.17(e)
			    /*	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17e = QueryConstants.QUERY_INSERT_t_arrdf1_17e;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17e);
			    	
			    	if(miscidList!=null) {
			    		for(Integer list : miscidList) {
			    			int miscid = list;		  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}*/
			    	break;
			    case 13: // For form1.11
			    /*	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_11 = QueryConstants.QUERY_INSERT_t_arrdf1_11;
			    	prstmt1 = conn1.prepareStatement(insertForm1_11);
			    	
			    	if(miscidList!=null) {
			    		for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
			    			
							for(Integer list : miscidList) {
								int miscid = list;		
			    			
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, miscid);
									prstmt1.setInt(3, pyid);
									prstmt1.setInt(4, stnid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
							}
			    		}
			    	}*/
			    	break;
			    case 14: // For form1.12
			    	/*miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_12 = QueryConstants.QUERY_INSERT_t_arrdf1_12;
			    	prstmt1 = conn1.prepareStatement(insertForm1_12);
			    	
			    	if(miscidList!=null) {
			    		for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
							
				    		for(Integer list : miscidList) {
				    			int miscid = list;		
				    			
				    			for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, miscid);
									prstmt1.setInt(3, pyid);
									prstmt1.setInt(4, stnid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
				    		}
			    		}
			    	}*/
			    	break;
			  case 15: // For form1.6(a)
				  /* 	if(seasonPeriodList==null) {
			    		seasonPeriodList = getSeasonPeriodList();
			    	}
			    	String insertForm1_6a = QueryConstants.QUERY_INSERT_t_arrdf1_6a;
			    	prstmt1 = conn1.prepareStatement(insertForm1_6a);
			    	
			    	if(seasonPeriodList!=null) {
			    		for (SeasonPeriodVO sp : seasonPeriodList) {
							int seasonid = sp.getSeasonid();
							int periodid = sp.getPeriodid();
							
							for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								//prstmt1.setInt(1, ++arrdf16aid);
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, seasonid);
								prstmt1.setInt(3, periodid);
								prstmt1.setInt(4, pyid);
								prstmt1.setDouble(5, 0.0);
								prstmt1.setTimestamp(6, timestamp);
								prstmt1.setInt(7, userid);
								prstmt1.setTimestamp(8, timestamp);
								prstmt1.setInt(9, userid);
								
								//prstmt1.executeUpdate();
							}
						}
					}*/
			    	break;
			    case 16: // For form1.6(b)
			    	break;
			    case 17: // For form1.7
			    	break;
			    case 18: // For form1.8
			    	break;
			    case 19: // For form1.9
			    	break;
			    case 20: // For form
			    	break;
			    case 21: // For form 1.11
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_11 = QueryConstants.QUERY_INSERT_t_arrdf1_11;
			    	prstmt1 = conn1.prepareStatement(insertForm1_11);
			    	
			    	if(miscidList!=null) {
			    		for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
			    			
			    			for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();			
			    			
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, miscid);
									prstmt1.setInt(3, pyid);
									prstmt1.setInt(4, stnid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
							}
			    		}
			    	}
			    	break;
			    case 22: // For form 1.12
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_12 = QueryConstants.QUERY_INSERT_t_arrdf1_12;
			    	prstmt1 = conn1.prepareStatement(insertForm1_12);
			    	
			    	if(miscidList!=null) {
			    		for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
							
			    			for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();			
				    			
				    			for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, miscid);
									prstmt1.setInt(3, pyid);
									prstmt1.setInt(4, stnid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									
									prstmt1.executeUpdate();
								}
				    		}
			    		}
			    	}
			    	break;
			    case 23: // For form
			    	break;
			    case 24: // For form
			    	break;
			    case 25: // For form
			    	break;
			    case 26: // For form
			    	break;
			    case 27: // For form
			    	break;
			    case 28: // For form
			    	break;
			    case 29: // For form 1.17b
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17b = QueryConstants.QUERY_INSERT_t_arrdf1_17b;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17b);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			
			    			  
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 30: // For form 1.17(c)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17c = QueryConstants.QUERY_INSERT_t_arrdf1_17c;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17c);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();		
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 31: // For form 1.17(d)
			    	ensuingPyidList = getEnsuingPyidList(phid);
			    	String insertForm1_17d = QueryConstants.QUERY_INSERT_t_arrdf1_17d;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17d);
			    	
			    	for ( Integer pyid : ensuingPyidList) {
						prstmt1.setInt(1, specific_arrformid);
						prstmt1.setInt(2, pyid);
						prstmt1.setString(3,null );
						prstmt1.setDouble(4, 0.0);
						prstmt1.setDouble(5, 0.0);
						prstmt1.setDouble(6, 0.0);
						prstmt1.setTimestamp(7, timestamp);
						prstmt1.setInt(8, userid);
						prstmt1.setTimestamp(9, timestamp);
						prstmt1.setInt(10, userid);
						
						prstmt1.executeUpdate();
			    	}
			    	break;
			    case 32: // For form 1.17(e)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17e = QueryConstants.QUERY_INSERT_t_arrdf1_17e;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17e);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 33: // For form 1.17(f)
				    String insertForm1_17f = QueryConstants.QUERY_INSERT_t_arrdf1_17f;
				    prstmt1 = conn1.prepareStatement(insertForm1_17f);
				    	
				    for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, pyid);
							prstmt1.setString(3,null );
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
				    	}
			    	break;
			    case 34: // For form 1.17(g)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_17g = QueryConstants.QUERY_INSERT_t_arrdf1_17g;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17g);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();		  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 35: // For form 1.17(h)
			    	String insertForm1_17h = QueryConstants.QUERY_INSERT_t_arrdf1_17h;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17h);
			    	
			    	if(ensuingPyidList == null) {
			    		ensuingPyidList = getEnsuingPyidList(phid);
			    	}
			    	for ( Integer pyid : ensuingPyidList) {
						prstmt1.setInt(1, specific_arrformid);
						prstmt1.setInt(2, pyid);
						prstmt1.setDouble(3, 0.0);
						prstmt1.setDouble(4, 0.0);
						prstmt1.setDouble(5, 0.0);
						prstmt1.setDouble(6, 0.0);
						prstmt1.setDouble(7, 0.0);
						prstmt1.setDouble(8, 0.0);
						prstmt1.setDouble(9, 0.0);
						prstmt1.setDouble(10, 0.0);
						prstmt1.setDouble(11, 0.0);
						prstmt1.setDouble(12, 0.0);
						prstmt1.setDouble(13, 0.0);
						prstmt1.setDouble(14, 0.0);
						prstmt1.setDouble(15, 0.0);
						prstmt1.setDouble(16, 0.0);
						prstmt1.setDouble(17, 0.0);
						prstmt1.setDouble(18, 0.0);
						prstmt1.setDouble(19, 0.0);
						prstmt1.setDouble(20, 0.0);
						prstmt1.setDouble(21, 0.0);
						prstmt1.setDouble(22, 0.0);
						prstmt1.setDouble(23, 0.0);
						prstmt1.setDouble(24, 0.0);
						prstmt1.setDouble(25, 0.0);
						prstmt1.setDouble(26, 0.0);
						prstmt1.setDouble(27, 0.0);
						prstmt1.setDouble(28, 0.0);
						prstmt1.setDouble(29, 0.0);
						prstmt1.setDouble(30, 0.0);
						prstmt1.setDouble(31, 0.0);
						prstmt1.setDouble(32, 0.0);
						prstmt1.setTimestamp(33, timestamp);
						prstmt1.setInt(34, userid);
						prstmt1.setTimestamp(35, timestamp);
						prstmt1.setInt(36, userid);
							
						prstmt1.executeUpdate();
					}
			    	break;
			    case 36: // For form 1.17(i)
			    	String insertForm1_17i = QueryConstants.QUERY_INSERT_t_arrdf1_17i;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17i);
			    	
			    	if(ensuingPyidList == null) {
			    		ensuingPyidList = getEnsuingPyidList(phid);
			    	}
			    	for ( Integer pyid : ensuingPyidList) {
						prstmt1.setInt(1, specific_arrformid);
						prstmt1.setInt(2, pyid);
						prstmt1.setDouble(3, 0.0);
						prstmt1.setDouble(4, 0.0);
						prstmt1.setDouble(5, 0.0);
						prstmt1.setDouble(6, 0.0);
						prstmt1.setDouble(7, 0.0);
						prstmt1.setDouble(8, 0.0);
						prstmt1.setDouble(9, 0.0);
						prstmt1.setDouble(10, 0.0);
						prstmt1.setDouble(11, 0.0);
						prstmt1.setDouble(12, 0.0);
						prstmt1.setDouble(13, 0.0);
						prstmt1.setDouble(14, 0.0);
						prstmt1.setDouble(15, 0.0);
						prstmt1.setDouble(16, 0.0);
						prstmt1.setDouble(17, 0.0);
						prstmt1.setTimestamp(18, timestamp);
						prstmt1.setInt(19, userid);
						prstmt1.setTimestamp(20, timestamp);
						prstmt1.setInt(21, userid);
							
						prstmt1.executeUpdate();
					}
			    	break;
			    case 37: // For form 1.17(j)
			    	miscidList = getMiscidList(formid);
			    	if(ensuingPyidList == null) {
			    		ensuingPyidList = getEnsuingPyidList(phid);
			    	}
			    
			    	String insertForm1_17j = QueryConstants.QUERY_INSERT_t_arrdf1_17j;
			    	prstmt1 = conn1.prepareStatement(insertForm1_17j);
			    	
			    	if(miscidList!=null) {
			    		List<Integer> list1 = new ArrayList<>() ;
			    		List<Integer> list2 = new ArrayList<>() ;
			    		for (MiscFormLabelVO misc : miscidList ) {
			    			if(misc.isIscolumn() == true ) {
			    				list1.add(misc.getMisc_form_label_id());
			    			}else {
			    				list2.add(misc.getMisc_form_label_id());
			    			}
			    		}
			    		for(Integer l1:list1)	{
			    			int miscid1 = l1;
			    			
			    			for(Integer l2:list2)	{
				    			int miscid2 = l2;
				    			
				    			for ( Integer pyid : ensuingPyidList) {
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, miscid1);
									prstmt1.setInt(3, miscid2);
									prstmt1.setInt(4, pyid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
										
									prstmt1.executeUpdate();
								}
			    			}
			    		}
			    	}
				    break;
			    case 38: // For form
				    break;
			    case 39: // For form 1.18
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_18_1 = QueryConstants.QUERY_INSERT_t_arrdf1_18_1;
			    	String insertForm1_18_2 = QueryConstants.QUERY_INSERT_t_arrdf1_18_2;
			    	
			    	for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
						
						for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
			    			prstmt1 = conn1.prepareStatement(insertForm1_18_1);
			    			
			    			prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, stnid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
						}
						if(miscidList!=null) {
				    		for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();			  
								prstmt1 = conn1.prepareStatement(insertForm1_18_2);
									
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, pyid);
								prstmt1.setInt(3, miscid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
									
								prstmt1.executeUpdate();
				    		}
				    	}
			    	}
				    break;
			    case 40: // For form 1.18(a)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_18a = QueryConstants.QUERY_INSERT_t_arrdf1_18a;
			    	prstmt1 = conn1.prepareStatement(insertForm1_18a);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
				    break;
			    case 41: // For form 1.18(b)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_18b = QueryConstants.QUERY_INSERT_t_arrdf1_18b;
			    	prstmt1 = conn1.prepareStatement(insertForm1_18b);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
				    break;
			    case 42: // For form 1.18(c)i
			    	String insertForm1_18c1 = QueryConstants.QUERY_INSERT_t_arrdf1_18c1;
				    prstmt1 = conn1.prepareStatement(insertForm1_18c1);
				   
				    for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, pyid);
							prstmt1.setString(3,null );
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
				    	}
			    	break;
			    case 43: // For form 1.18(c)ii
			    	String insertForm1_18c2 = QueryConstants.QUERY_INSERT_t_arrdf1_18c2;
				    prstmt1 = conn1.prepareStatement(insertForm1_18c2);
				    	
				    for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, pyid);
							prstmt1.setString(3,null );
							prstmt1.setDouble(4, 0.0);
							prstmt1.setTimestamp(5, timestamp);
							prstmt1.setInt(6, userid);
							prstmt1.setTimestamp(7, timestamp);
							prstmt1.setInt(8, userid);
							
							prstmt1.executeUpdate();
				    	}
			    	break;
			    case 44: // For form 1.19a
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_19a1 = QueryConstants.QUERY_INSERT_t_arrdf1_19a1;
			    	String insertForm1_19a2 = QueryConstants.QUERY_INSERT_t_arrdf1_19a2;
			    	String insertForm1_19a3 = QueryConstants.QUERY_INSERT_t_arrdf1_19a3;
			    	
			    	for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
						
						for ( int i = 0; i<stnidAl.size(); i++ ) {
			    			int stnid = Integer.parseInt(stnidAl.get(i));
			    			prstmt1 = conn1.prepareStatement(insertForm1_19a1);
			    			
			    			prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, stnid);
							prstmt1.setInt(3, pyid);
							prstmt1.setDouble(4, 0.0);
							prstmt1.setDouble(5, 0.0);
							prstmt1.setTimestamp(6, timestamp);
							prstmt1.setInt(7, userid);
							prstmt1.setTimestamp(8, timestamp);
							prstmt1.setInt(9, userid);
							
							prstmt1.executeUpdate();
						}
						if(miscidList!=null) {
				    		for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();	
			    				int sequence = misc.getSequence();
			    				
			    				if(sequence <=2) {
			    					prstmt1 = conn1.prepareStatement(insertForm1_19a2);
			    				}else {
			    					prstmt1 = conn1.prepareStatement(insertForm1_19a3);
			    				}
								
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setDouble(5, 0.0);
								prstmt1.setTimestamp(6, timestamp);
								prstmt1.setInt(7, userid);
								prstmt1.setTimestamp(8, timestamp);
								prstmt1.setInt(9, userid);
									
								prstmt1.executeUpdate();
				    		}
				    	}
			    	}
			    	break;
			    case 45: // For form 1.19b
			    	miscidList = getMiscidList(formid);
			    	capitalExpList = getcapitalExpList();
			    	
			    	String insertForm1_19b1 = QueryConstants.QUERY_INSERT_t_arrdf1_19b1;
			    	String insertForm1_19b2 = QueryConstants.QUERY_INSERT_t_arrdf1_19b2;
			    	String insertForm1_19b3 = QueryConstants.QUERY_INSERT_t_arrdf1_19b3;
			    	
			    	
						/*for (CapitalExpTypeVO expType : capitalExpList) {
							int expTypeId = expType.getCapital_exp_type_id();*/
			    	for (int k = 0; k < 1; k++) {
			    		//CapitalExpTypeVO expType = capitalExpList.get(k);
						int expTypeId = capitalExpList.get(k).getCapital_exp_type_id();
							//Station wise
							for (int i = 0; i < 1; i++) {
								int stnid = Integer.parseInt(stnidAl.get(i));
								prstmt1 = conn1.prepareStatement(insertForm1_19b1);
								
								if (miscidList != null) {
									for (MiscFormLabelVO misc : miscidList) {
										int miscid = misc.getMisc_form_label_id();

										prstmt1.setInt(1, specific_arrformid);
										prstmt1.setInt(2, stnid);
										prstmt1.setInt(3, miscid);
										prstmt1.setInt(4, 0);
										prstmt1.setInt(5, expTypeId);
										prstmt1.setDouble(6, 0.0);
										prstmt1.setDouble(7, 0.0);
										prstmt1.setTimestamp(8, timestamp);
										prstmt1.setInt(9, userid);
										prstmt1.setTimestamp(10, timestamp);
										prstmt1.setInt(11, userid);

										prstmt1.executeUpdate();
									}
								}
								for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
								
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, 0);
									prstmt1.setInt(4, pyid);
									prstmt1.setInt(5, expTypeId);
									prstmt1.setDouble(6, 0.0);
									prstmt1.setDouble(7, 0.0);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									prstmt1.setTimestamp(10, timestamp);
									prstmt1.setInt(11, userid);

									prstmt1.executeUpdate();
								}
							}
							//for completed project 
							String projectDesc = null;
							if (miscidList != null) {
								for (MiscFormLabelVO misc : miscidList) {
									int miscid = misc.getMisc_form_label_id();
									prstmt1 = conn1.prepareStatement(insertForm1_19b2);
									
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setString(2, projectDesc);
									prstmt1.setInt(3, expTypeId);
									prstmt1.setInt(4, 0);
									prstmt1.setInt(5, miscid);
									prstmt1.setDouble(6, 0.0);
									prstmt1.setDouble(7, 0.0);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									prstmt1.setTimestamp(10, timestamp);
									prstmt1.setInt(11, userid);
									
									prstmt1.executeUpdate();
								}
							}
							for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1 = conn1.prepareStatement(insertForm1_19b2);
								
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setString(2, projectDesc);
								prstmt1.setInt(3, expTypeId);
								prstmt1.setInt(4, pyid);
								prstmt1.setInt(5, 0);
								prstmt1.setDouble(6, 0.0);
								prstmt1.setDouble(7, 0.0);
								prstmt1.setTimestamp(8, timestamp);
								prstmt1.setInt(9, userid);
								prstmt1.setTimestamp(10, timestamp);
								prstmt1.setInt(11, userid);
								
								prstmt1.executeUpdate();
							}
							
							//for approval of project
							//String projectDesc ="";
							if (miscidList != null) {
								for (MiscFormLabelVO misc : miscidList) {
									int miscid = misc.getMisc_form_label_id();
									prstmt1 = conn1.prepareStatement(insertForm1_19b3);
									
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setString(2, projectDesc);
									prstmt1.setInt(3, expTypeId);
									prstmt1.setInt(4, 0);
									prstmt1.setInt(5, miscid);
									prstmt1.setDouble(6, 0.0);
									prstmt1.setDouble(7, 0.0);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									prstmt1.setTimestamp(10, timestamp);
									prstmt1.setInt(11, userid);
									
									prstmt1.executeUpdate();
								}
							}
							for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1 = conn1.prepareStatement(insertForm1_19b3);
								
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setString(2, projectDesc);
								prstmt1.setInt(3, expTypeId);
								prstmt1.setInt(4, pyid);
								prstmt1.setInt(5, 0);
								prstmt1.setDouble(6, 0.0);
								prstmt1.setDouble(7, 0.0);
								prstmt1.setTimestamp(8, timestamp);
								prstmt1.setInt(9, userid);
								prstmt1.setTimestamp(10, timestamp);
								prstmt1.setInt(11, userid);
								
								prstmt1.executeUpdate();
							}
						}	
			    	break;
			    case 46: // For form 
			    	break;
			    case 47: // For form 1.20(a)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_20a = QueryConstants.QUERY_INSERT_t_arrdf1_20a;
			    	prstmt1 = conn1.prepareStatement(insertForm1_20a);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 48: // For form 1.20(b)
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_20b = QueryConstants.QUERY_INSERT_t_arrdf1_20b;
			    	prstmt1 = conn1.prepareStatement(insertForm1_20b);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 49: // For form 1.21
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_21 = QueryConstants.QUERY_INSERT_t_arrdf1_21;
			    	prstmt1 = conn1.prepareStatement(insertForm1_21);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;	
			    case 50: // For form 1.22
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_22 = QueryConstants.QUERY_INSERT_t_arrdf1_22;
			    	prstmt1 = conn1.prepareStatement(insertForm1_22);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 51: // For form 1.23
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_23_1 = QueryConstants.QUERY_INSERT_t_arrdf1_23_1;
			    	String insertForm1_23_2 = QueryConstants.QUERY_INSERT_t_arrdf1_23_2;
			    	String insertForm1_23_3 = QueryConstants.QUERY_INSERT_t_arrdf1_23_3;
			    	
			    	for ( int i = 0; i<stnidAl.size(); i++ ) {
		    			int stnid = Integer.parseInt(stnidAl.get(i));
		    									    
		    			if(miscidList!=null) {
					    	for (MiscFormLabelVO misc : miscidList ) {
				    			int miscid = misc.getMisc_form_label_id();
						        int sequence = misc.getSequence();
						        
						        if(sequence <= 4) {
			    					prstmt1 = conn1.prepareStatement(insertForm1_23_1);
			    				}else if(sequence > 4 && sequence <= 9){
			    					prstmt1 = conn1.prepareStatement(insertForm1_23_2);
			    				}else {
			    					prstmt1 = conn1.prepareStatement(insertForm1_23_3);
			    				}
						        
						        for ( int j = 0; j<pyidAl.size(); j++  ) {
									int pyid = Integer.parseInt(pyidAl.get(j));
									
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, pyid);
									prstmt1.setInt(4, miscid);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
										
									prstmt1.executeUpdate();
								}
					    	}
		    			}
			    	}
			    	break;
			    case 52: // For form 1.24
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm1_24 = QueryConstants.QUERY_INSERT_t_arrdf1_24;
			    	prstmt1 = conn1.prepareStatement(insertForm1_24);
			    	
			    	if(miscidList!=null) {
			    		for (MiscFormLabelVO misc : miscidList ) {
		    				int miscid = misc.getMisc_form_label_id();			  
			    			
			    			for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, miscid);
								prstmt1.setInt(3, pyid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
							}
			    		}
			    	}
			    	break;
			    case 57: // For form A
			    	String insertForm_a = QueryConstants.QUERY_INSERT_t_arrdfa;
			    	prstmt1 = conn1.prepareStatement(insertForm_a);
			    	
			    	for (String station : stnidAl ) {
			    		int stnid = Integer.parseInt(station);			  
			    		  
		    			for ( int j = 0; j<pyidAl.size(); j++  ) {
							int pyid = Integer.parseInt(pyidAl.get(j));
							prstmt1.setInt(1, specific_arrformid);
							prstmt1.setInt(2, pyid);
							prstmt1.setInt(3, stnid);
							prstmt1.setString(4, null);
							prstmt1.setTimestamp(5, null);
							prstmt1.setTimestamp(6, null);
							prstmt1.setString(7, null);
							prstmt1.setString(8, null);
							prstmt1.setString(9, null);
							prstmt1.setString(10, null);
							prstmt1.setString(11, null);
							prstmt1.setString(12, null);
							prstmt1.setString(13, null);
							prstmt1.setTimestamp(14, timestamp);
							prstmt1.setInt(15, userid);
							prstmt1.setTimestamp(16, timestamp);
							prstmt1.setInt(17, userid);
							
							prstmt1.executeUpdate();
						}
			    	}
			    	break;
			    case 58: // For form B
			    	miscidList = getMiscidList(formid);
			    	depreciationRateList = getDepreciationRateList();
			    			
			    	String insertForm_b = QueryConstants.QUERY_INSERT_t_arrdfb;
			    	prstmt1 = conn1.prepareStatement(insertForm_b);
			    	
		    		for ( int j = 0; j<pyidAl.size(); j++  ) {
						int pyid = Integer.parseInt(pyidAl.get(j));
						  
						if(miscidList!=null) {
				    		for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();	
			    				prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, pyid);
								prstmt1.setDouble(3, 0.0);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setDouble(5, 0.0);
								prstmt1.setDouble(6, 0.0);
								prstmt1.setDouble(7, 0.0);
								prstmt1.setInt(8, miscid);
								prstmt1.setInt(9, 0);
								prstmt1.setTimestamp(10, timestamp);
								prstmt1.setInt(11, userid);
								prstmt1.setTimestamp(12, timestamp);
								prstmt1.setInt(13, userid);
									
								prstmt1.executeUpdate();
				    		}
						}
						if(depreciationRateList!=null) {
				    		for (DepreciationRateVO depRate : depreciationRateList ) {
			    				int rateid = depRate.getDepreciation_rate_id();
			    				prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, pyid);
								prstmt1.setDouble(3, 0.0);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setDouble(5, 0.0);
								prstmt1.setDouble(6, 0.0);
								prstmt1.setDouble(7, 0.0);
								prstmt1.setInt(8, 0);
								prstmt1.setInt(9, rateid);
								prstmt1.setTimestamp(10, timestamp);
								prstmt1.setInt(11, userid);
								prstmt1.setTimestamp(12, timestamp);
								prstmt1.setInt(13, userid);
									
								prstmt1.executeUpdate();
				    		}
						}
			    	}
			    	break;
			    case 61: // For form D1
			    	List<Integer> utilCoalFieldIds = getCoalFieldIds(utilid);
			    	List<Integer> coalgradeIds = getCoalgradeIds();
			    	
			    	String insertForm_D1 = QueryConstants.QUERY_INSERT_t_arrdfd1;
			    	prstmt1 = conn1.prepareStatement(insertForm_D1);
			    	
			    	if(utilCoalFieldIds!=null && coalgradeIds!=null) {
			    		for (String station : stnidAl ) {
				    		int stnid = Integer.parseInt(station);	
				    		for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
								for(Integer utilCoalFieldId : utilCoalFieldIds) {
									for(Integer coalgradeId : coalgradeIds) {
										prstmt1.setInt(1, specific_arrformid);
										prstmt1.setInt(2, stnid);
										prstmt1.setInt(3, pyid);
										prstmt1.setInt(4, utilCoalFieldId);
										prstmt1.setInt(5, coalgradeId);
										prstmt1.setDouble(6, 0.0);
										prstmt1.setTimestamp(7, timestamp);
										prstmt1.setInt(8, userid);
										prstmt1.setTimestamp(9, timestamp);
										prstmt1.setInt(10, userid);
										
										prstmt1.executeUpdate();
									}
								}
				    		}
			    		}
			    	}
			    	break;
			    case 62: // For form D2
			    	miscidList = getMiscidList(formid);
			    	String insertForm_D2 = QueryConstants.QUERY_INSERT_t_arrdfd2;
			    	prstmt1 = conn1.prepareStatement(insertForm_D2);
			    	
			    	for (String station : stnidAl ) {
			    		int stnid = Integer.parseInt(station);			  
			    		  
			    		if(miscidList!=null) {
				    		for (MiscFormLabelVO misc : miscidList ) {
			    				int miscid = misc.getMisc_form_label_id();			  
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, stnid);
								prstmt1.setInt(3, miscid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
								
								prstmt1.executeUpdate();
				    		}
						}
			    	}
			    	break;
			    case 124: // For form 6
			    	miscidList = getMiscidList(formid);
			    	
			    	String insertForm6_income = QueryConstants.QUERY_INSERT_t_arrdf6_income;
			    	String insertForm6_expense = QueryConstants.QUERY_INSERT_t_arrdf6_expense;
			    	String insertForm6_asset = QueryConstants.QUERY_INSERT_t_arrdf6_asset;
			    	String insertForm6_add_fund = QueryConstants.QUERY_INSERT_t_arrdf6_add_fund;
			    	String insertForm6_utilise_fund = QueryConstants.QUERY_INSERT_t_arrdf6_utilise_fund;
			    	
		    		if(miscidList!=null) {
					    for (MiscFormLabelVO misc : miscidList ) {
				    		int miscid = misc.getMisc_form_label_id();
						    int sequence = misc.getSequence();
						        
						    if(sequence <= 3) {
			    				prstmt1 = conn1.prepareStatement(insertForm6_income);
			    			}else if(sequence > 3 && sequence <= 5){
			    				prstmt1 = conn1.prepareStatement(insertForm6_expense);
			    			}else if(sequence > 5 && sequence <= 10){
			    				prstmt1 = conn1.prepareStatement(insertForm6_asset);
			    			}else if(sequence > 10 && sequence <= 16){
			    				prstmt1 = conn1.prepareStatement(insertForm6_add_fund);
			    			}else {
			    				prstmt1 = conn1.prepareStatement(insertForm6_utilise_fund);
			    			}
						        
						    for ( int j = 0; j<pyidAl.size(); j++  ) {
								int pyid = Integer.parseInt(pyidAl.get(j));
									
								prstmt1.setInt(1, specific_arrformid);
								prstmt1.setInt(2, pyid);
								prstmt1.setInt(3, miscid);
								prstmt1.setDouble(4, 0.0);
								prstmt1.setTimestamp(5, timestamp);
								prstmt1.setInt(6, userid);
								prstmt1.setTimestamp(7, timestamp);
								prstmt1.setInt(8, userid);
										
								prstmt1.executeUpdate();
							}
					    }
		    		}
			    	break;
			    case 129: // For form P(D1)
			    	miscidList = getMiscidList(formid);
			    	String thermal_stn_type = JavaConstants.THERMAL_POWER_STATION;
			    	List<Integer> thermalStationList = getSpecificTypeStation(thermal_stn_type);
			    	
			    	String insertForm_PD1 = QueryConstants.QUERY_INSERT_t_arrdfp_d1;
			    	prstmt1 = conn1.prepareStatement(insertForm_PD1);
			    	
			    	if(thermalStationList!=null) {
				    	for (Integer stnid : thermalStationList ) {
				    		if(miscidList!=null) {
					    		for (MiscFormLabelVO misc : miscidList ) {
				    				int miscid = misc.getMisc_form_label_id();			  
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, miscid);
									prstmt1.setDouble(4, 0.0);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setDouble(6, 0.0);
									prstmt1.setTimestamp(7, timestamp);
									prstmt1.setInt(8, userid);
									prstmt1.setTimestamp(9, timestamp);
									prstmt1.setInt(10, userid);
									
									prstmt1.executeUpdate();
					    		}
							}
				    	}
			    	}
			    	break;
			    case 130: // For form P(D2)
			    	miscidList = getMiscidList(formid);
			    	String hydro_stn_type = JavaConstants.HYDEL_POWER_STATION;
			    	List<Integer> hydroStationList = getSpecificTypeStation(hydro_stn_type);
			    	
			    	String insertForm_PD2 = QueryConstants.QUERY_INSERT_t_arrdfp_d2;
			    	prstmt1 = conn1.prepareStatement(insertForm_PD2);
			    	
			    	if(hydroStationList!=null) {
				    	for (Integer stnid : hydroStationList ) {
				    		if(miscidList!=null) {
					    		for (MiscFormLabelVO misc : miscidList ) {
				    				int miscid = misc.getMisc_form_label_id();			  
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, miscid);
									prstmt1.setDouble(4, 0.0);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setDouble(6, 0.0);
									prstmt1.setDouble(7, 0.0);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
									prstmt1.setTimestamp(10, timestamp);
									prstmt1.setInt(11, userid);
									
									prstmt1.executeUpdate();
					    		}
							}
				    	}
			    	}
			    	break;
			    case 132: // For form P(D4)  
			    	miscidList = getMiscidList(formid);
			    
			    	String insertForm_PD4 = QueryConstants.QUERY_INSERT_t_arrdfp_d4;
			    	prstmt1 = conn1.prepareStatement(insertForm_PD4);
			    	
			    	if(miscidList!=null) {
			    		List<Integer> list1 = new ArrayList<>() ;
			    		List<Integer> list2 = new ArrayList<>() ;
			    		for (MiscFormLabelVO misc : miscidList ) {
			    			if(misc.isIscolumn() == true ) {
			    				list1.add(misc.getMisc_form_label_id());
			    			}else {
			    				list2.add(misc.getMisc_form_label_id());
			    			}
			    		}
			    		
			    		for (String station : stnidAl ) {
				    		int stnid = Integer.parseInt(station);		
				    		
				    		for(Integer l1:list1)	{
				    			int miscid1 = l1;
				    			
				    			for(Integer l2:list2)	{
					    			int miscid2 = l2;
									prstmt1.setInt(1, specific_arrformid);
									prstmt1.setInt(2, stnid);
									prstmt1.setInt(3, miscid1);
									prstmt1.setInt(4, miscid2);
									prstmt1.setDouble(5, 0.0);
									prstmt1.setTimestamp(6, timestamp);
									prstmt1.setInt(7, userid);
									prstmt1.setTimestamp(8, timestamp);
									prstmt1.setInt(9, userid);
											
									prstmt1.executeUpdate();
				    			}
				    		}
			    		}
			    	}
			    	break;
			    case 133: //Form P(E)
			    	String insertForm_PE = QueryConstants.QUERY_INSERT_t_arrdfp_e_genco;
			    	prstmt1 = conn1.prepareStatement(insertForm_PE);
			    	
			    	for (String station : stnidAl ) {
			    		int stnid = Integer.parseInt(station);	
			    		prstmt1.setInt(1, specific_arrformid);
						prstmt1.setInt(2, stnid);
						prstmt1.setString(3, null);
						prstmt1.setString(4, null);
						prstmt1.setString(5, null);
						prstmt1.setString(6, null);
						prstmt1.setTimestamp(7, timestamp);
						prstmt1.setTimestamp(8, timestamp);
						prstmt1.setTimestamp(9, timestamp);
						prstmt1.setString(10, null);
						prstmt1.setString(11, null);
						prstmt1.setString(12, null);
						prstmt1.setDouble(13, 0.0);
						prstmt1.setDouble(14, 0.0);
						prstmt1.setTimestamp(15, timestamp);
						prstmt1.setInt(16, userid);
						prstmt1.setTimestamp(17, timestamp);
						prstmt1.setInt(18, userid);
						
						prstmt1.executeUpdate();
			    	}
			    	break;
				}
				
				String insertComment  = QueryConstants.QUERY_INSERT_t_arr_comment;
				prstmt1 = conn1.prepareStatement(insertComment);
				
				prstmt1.setInt(1, specific_arrformid);
				prstmt1.setString(2, null);
				prstmt1.setBoolean(3, false);
				prstmt1.setTimestamp(4, timestamp);
				prstmt1.setInt(5, userid);
				prstmt1.setTimestamp(6, timestamp);
				prstmt1.setInt(7, userid);
						
				prstmt1.executeUpdate();
			}
			
			conn1.commit();
			insertTableForPitition = true;
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
			return insertTableForPitition;
			//throw new MyException();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		return insertTableForPitition;
	}

private List<SeasonPeriodVO> getSeasonPeriodList() throws SQLException, MyException {
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getSeasonPeriod = QueryConstants.QUERY_GET_LIST_OF_SEASONS_AND_PERIODS;
	List<SeasonPeriodVO> seasonPeriodList = new ArrayList<SeasonPeriodVO>();
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getSeasonPeriod);
		rs=prstmt.executeQuery();

		while(rs.next()) {
			SeasonPeriodVO season = new SeasonPeriodVO();
			season.setSeasonid(rs.getInt("season_id"));
			season.setSeason_nm(rs.getString("season_desc"));
			season.setPeriodid(rs.getInt("period_id"));
			season.setPeriod_nm(rs.getString("period_desc"));
			seasonPeriodList.add(season);
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return seasonPeriodList;
}

private List<MiscFormLabelVO> getMiscidList(int formid) throws SQLException, MyException {
	List<MiscFormLabelVO> miscidList = new ArrayList<MiscFormLabelVO>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getMiscid = QueryConstants.QUERY_GET_LIST_OF_MISCID;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getMiscid);
		prstmt.setInt(1, formid);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			MiscFormLabelVO miscids = new MiscFormLabelVO();
			miscids.setMisc_form_label_id(rs.getInt("misc_form_label_id"));
			miscids.setIscolumn(rs.getBoolean("iscolumn"));
			miscids.setSequence(rs.getInt("sequence"));
			miscidList.add(miscids);
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return miscidList;
}

private List<Integer> getMonthidList() throws SQLException, MyException  {
	List<Integer> monthids = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getMonthid = QueryConstants.QUERY_GET_LIST_OF_MONTH;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getMonthid);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			monthids.add(rs.getInt("month_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return monthids;
}	

private List<Integer> getPumpStationList(int utilid) throws SQLException, MyException {
	List<Integer> pumpStationIds = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getPumpStnid = QueryConstants.QUERY_GET_LIST_OF_PUMPSTATION;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getPumpStnid);
		prstmt.setInt(1, utilid);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			pumpStationIds.add(rs.getInt("pump_stn_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return pumpStationIds;
}

private List<Integer> getEnsuingPyidList(int phid) throws SQLException, MyException {
	List<Integer> ensuingPyids = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getEnsuingPyid = QueryConstants.QUERY_GET_ENSUING_PETITION_YEAR;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getEnsuingPyid);
		prstmt.setInt(1, phid);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			ensuingPyids.add(rs.getInt("py_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return ensuingPyids;
}

private List<Integer> getCoalFieldIds(int utilid) throws SQLException, MyException {
	List<Integer> utilCoalFieldIds = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getutilCoalFieldId = QueryConstants.QUERY_GET_LIST_OF_UTIL_COALFIELD_ID;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getutilCoalFieldId);
		prstmt.setInt(1, utilid);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			utilCoalFieldIds.add(rs.getInt("util_coal_field_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return utilCoalFieldIds;
}

private List<Integer> getCoalgradeIds() throws SQLException, MyException {
	List<Integer> coalgradeIds = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getCoalgradeId = QueryConstants.QUERY_GET_LIST_OF_COALGRADEID;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getCoalgradeId);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			coalgradeIds.add(rs.getInt("coalgrade_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return coalgradeIds;
	
	
}

private List<DepreciationRateVO> getDepreciationRateList() throws SQLException, MyException {
	List<DepreciationRateVO> depreciationRateList = new ArrayList<DepreciationRateVO>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getDepreciationRate = QueryConstants.QUERY_GET_LIST_OF_DEPRECIATION_RATE;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getDepreciationRate);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			DepreciationRateVO rateids = new DepreciationRateVO();
			rateids.setDepreciation_rate_id(rs.getInt("depreciation_rate_id"));
			depreciationRateList.add(rateids);
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return depreciationRateList;
}

private List<CapitalExpTypeVO> getcapitalExpList() throws SQLException, MyException {
	List<CapitalExpTypeVO> expTypeList = new ArrayList<CapitalExpTypeVO>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getCapitalExpType = QueryConstants.QUERY_GET_LIST_OF_CAPITAL_EXP_TYPE;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getCapitalExpType);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			CapitalExpTypeVO expTypeids = new CapitalExpTypeVO();
			expTypeids.setCapital_exp_type_id(rs.getInt("capital_exp_type_id"));
			expTypeList.add(expTypeids);
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return expTypeList;
}

private List<Integer> getSpecificTypeStation(String stn_type) throws SQLException, MyException {
	List<Integer> specificTypeStationIds = new ArrayList<Integer>();
	Connection conn=null;
	PreparedStatement prstmt = null;
	ResultSet rs = null;
	String getspecificTypeStnid = QueryConstants.QUERY_GET_LIST_OF_SPECIFIC_TYPE_STATION;
	
	try {
		conn=DBUtil.getConnection();
		prstmt = conn.prepareStatement(getspecificTypeStnid);
		prstmt.setString(1, stn_type);
		rs=prstmt.executeQuery();
		
		while(rs.next()) {
			specificTypeStationIds.add(rs.getInt("stn_id"));
		}
		conn.commit();
	}catch(Exception e) {
		e.printStackTrace();
		conn.rollback();
		throw new MyException();
	} finally {
		if(conn!=null) {
			conn.close();
		}
		if(prstmt!=null) {
			prstmt.close();
		}
		if(rs!=null) {
			rs.close();
		}
	}
	return specificTypeStationIds;
}

}

