/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_23JExcelDataVO;

public class Form_1_23JExcelDAO {

	@SuppressWarnings("resource")
	public Form1_23JExcelDataVO getF_1_23JExcelData(String stn_id, String ph_id, String form_id, String user_id, String util_id) throws SQLException, MyException {
		Form1_23JExcelDataVO form1_23Ref = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		// Misc ids for groups
		ArrayList<String> miscids_for_generation = null;
		ArrayList<String> miscids_for_hydropower = null;
		ArrayList<String> miscids_for_transmission = null;

		ArrayList<String> miscnms_for_generation = null;
		ArrayList<String> miscnms_for_hydropower = null;
		ArrayList<String> miscnms_for_transmission = null;

		ArrayList<String> miscrow_for_generation = null;
		ArrayList<String> miscrow_for_hydropower = null;
		ArrayList<String> miscrow_for_transmission = null;
		
		ArrayList<ArrayList<String>> valAl_for_generation = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_hydropower = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_transmission = new ArrayList<ArrayList<String>>();
		
		ArrayList<String> col = null;	
		
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		String getMiscIds_for_generation = QueryConstants.QUERY_GET_F1_23_1_MISCID_N_NAME;
		String get1_23_1Val = QueryConstants.QUERY_GET_F1_23_1_VAL;
		
		String getMiscIds_for_hydropower = QueryConstants.QUERY_GET_F1_23_2_MISCID_N_NAME;
		String get1_23_2Val = QueryConstants.QUERY_GET_F1_23_2_VAL;
		
		String getMiscIds_for_transmission = QueryConstants.QUERY_GET_F1_23_3_MISCID_N_NAME;
		String get1_23_3Val = QueryConstants.QUERY_GET_F1_23_3_VAL;

		try {
			conn1=DBUtil.getConnection();
			// Fetch Petition year
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			// Fetch For Generation
			prstmt1 = conn1.prepareStatement(getMiscIds_for_generation);
			prstmt1.setInt(1, Integer.parseInt(stn_id));
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, Integer.parseInt(ph_id));

			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids_for_generation = new ArrayList<String>();
				miscnms_for_generation= new ArrayList<String>();
				while(rs1.next()) {
					miscids_for_generation.add(rs1.getString("misc_form_label_id"));
					miscnms_for_generation.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			int int_stn_id = Integer.parseInt(stn_id);
			valAl_for_generation= new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(get1_23_1Val);
			
			for (int i = 0; i < miscids_for_generation.size(); i++) {
				miscrow_for_generation =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_generation.get(i));
				miscrow_for_generation.add(miscnms_for_generation.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_misc_form_label_id);
					prstmt1.setInt(4, Integer.parseInt(form_id));
					prstmt1.setInt(5, Integer.parseInt(ph_id));
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow_for_generation.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_generation.add(miscrow_for_generation);
			}
			
			// Fetch For Hydropower
			prstmt1 = conn1.prepareStatement(getMiscIds_for_hydropower);
			prstmt1.setInt(1, Integer.parseInt(stn_id));
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids_for_hydropower = new ArrayList<String>();
				miscnms_for_hydropower = new ArrayList<String>();
				while(rs1.next()) {
					miscids_for_hydropower.add(rs1.getString("misc_form_label_id"));
					miscnms_for_hydropower.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			valAl_for_hydropower = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(get1_23_2Val);
			
			for (int i = 0; i < miscids_for_hydropower.size(); i++) {
				miscrow_for_hydropower =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_hydropower.get(i));
				miscrow_for_hydropower.add(miscnms_for_hydropower.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_misc_form_label_id);
					prstmt1.setInt(4, Integer.parseInt(form_id));
					prstmt1.setInt(5, Integer.parseInt(ph_id));
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow_for_hydropower.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_hydropower.add(miscrow_for_hydropower);
			}
			
			
			
			// Fetch For Transmission
			prstmt1 = conn1.prepareStatement(getMiscIds_for_transmission);
			prstmt1.setInt(1, Integer.parseInt(stn_id));
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, Integer.parseInt(ph_id));			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids_for_transmission = new ArrayList<String>();
				miscnms_for_transmission = new ArrayList<String>();
				while(rs1.next()) {
					miscids_for_transmission.add(rs1.getString("misc_form_label_id"));
					miscnms_for_transmission.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			valAl_for_transmission = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(get1_23_3Val);
			
			for (int i = 0; i < miscids_for_transmission.size(); i++) {
				miscrow_for_transmission =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_transmission.get(i));
				miscrow_for_transmission.add(miscnms_for_transmission.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_misc_form_label_id);
					prstmt1.setInt(4, Integer.parseInt(form_id));
					prstmt1.setInt(5, Integer.parseInt(ph_id));
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow_for_transmission.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_transmission.add(miscrow_for_transmission);
			}
			// Getting Equity value from 1.20a
			miscrow_for_transmission = FormCalculationDAO.getComputationResultFrom_F1_20a(ph_id, "47", user_id, util_id); // form_id of 1.20(a) is 47
			miscrow_for_transmission.set(0, "Equity");
			valAl_for_transmission.add(miscrow_for_transmission);
			
			conn1.commit();
		}
		catch(SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		} finally {						
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		form1_23Ref = new Form1_23JExcelDataVO();
		form1_23Ref.setForm_id(form_id);
		form1_23Ref.setPh_id(ph_id);
		form1_23Ref.setStn_id(stn_id);
		form1_23Ref.setUser_id(user_id);
		form1_23Ref.setCols(col);
		// Generation
		form1_23Ref.setMiscid_for_Incentive_for_Generation(miscids_for_generation);
		form1_23Ref.setVals_for_Incentive_for_Generation(valAl_for_generation);
		// Hydropower
		form1_23Ref.setMiscid_for_Incentive_for_Hydropower(miscids_for_hydropower);
		form1_23Ref.setVals_for_Incentive_for_Hydropower(valAl_for_hydropower);
		// Transmission
		form1_23Ref.setMiscid_for_Incentive_for_Transmission(miscids_for_transmission);
		form1_23Ref.setVals_for_Incentive_for_Transmission(valAl_for_transmission);
		
		return form1_23Ref;
	}

	@SuppressWarnings("resource")
	public boolean updateF1_23(Form1_23JExcelDataVO data)
			throws MyException, SQLException {
		// Get petition years
		ArrayList<String> cols = data.getCols();
		// Get misc ids for groups
		ArrayList<String> miscids_for_generation = data.getMiscid_for_Incentive_for_Generation();
		ArrayList<String> miscids_for_hydropower = data.getMiscid_for_Incentive_for_Hydropower();
		ArrayList<String> miscids_for_transmission = data.getMiscid_for_Incentive_for_Transmission();
		// Get values for groups
		ArrayList<ArrayList<String>> values_for_generation = data.getVals_for_Incentive_for_Generation();
		ArrayList<ArrayList<String>> values_for_hydropower = data.getVals_for_Incentive_for_Hydropower();
		ArrayList<ArrayList<String>> values_for_transmission = data.getVals_for_Incentive_for_Transmission();
		// Get station id
		int stationid = Integer.parseInt(data.getStn_id());
		// Get form id
		int formid = Integer.parseInt(data.getForm_id());
		// Get user id
		int userid = Integer.parseInt(data.getUser_id());
		// Get petition id
		int phid = Integer.parseInt(data.getPh_id());

		String updateQuery_f1_23_1 = QueryConstants.QUERY_UPDATE_F1_23_1;
		String updateQuery_f1_23_2 = QueryConstants.QUERY_UPDATE_F1_23_2;
		String updateQuery_f1_23_3 = QueryConstants.QUERY_UPDATE_F1_23_3;

		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		// Get current time
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			// Update for group 1
			prstmt1 = conn1.prepareStatement(updateQuery_f1_23_1);
			for ( int i = 0; i < miscids_for_generation.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_generation.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_generation.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, stationid);
					prstmt1.setInt(5, pyid);
					prstmt1.setInt(6, miscid);
					prstmt1.setInt(7, formid);
					prstmt1.setInt(8, phid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			// Update for group 2
			prstmt1 = conn1.prepareStatement(updateQuery_f1_23_2);
			for ( int i = 0; i < miscids_for_hydropower.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_hydropower.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_hydropower.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, stationid);
					prstmt1.setInt(5, pyid);
					prstmt1.setInt(6, miscid);
					prstmt1.setInt(7, formid);
					prstmt1.setInt(8, phid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			// Update for group 3
			prstmt1 = conn1.prepareStatement(updateQuery_f1_23_3);
			for ( int i = 0; i < miscids_for_transmission.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_transmission.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_transmission.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, stationid);
					prstmt1.setInt(5, pyid);
					prstmt1.setInt(6, miscid);
					prstmt1.setInt(7, formid);
					prstmt1.setInt(8, phid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}



}
