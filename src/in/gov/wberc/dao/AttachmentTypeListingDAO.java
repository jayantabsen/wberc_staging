package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.AttachmentTypeListingVO;
import in.gov.wberc.vo.AttachmentTypeVO2;

public class AttachmentTypeListingDAO {

	public AttachmentTypeListingVO getAttachmentTypeListing(String form_id) throws MyException, SQLException {
		
		AttachmentTypeListingVO attypelstvo = null;
		AttachmentTypeVO2 attyvo2 = null;
		ArrayList<AttachmentTypeVO2> attyvo2Al = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_ATTACHMENT_TYPE_LIST;
		
		int int_form_id = Integer.parseInt(form_id);
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_form_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				attyvo2Al = new ArrayList<AttachmentTypeVO2>();
				while (rs1.next()) {
					// SELECT attachment_type_id, attachment_type_desc FROM m_attachment_type WHERE form_id=? ORDER BY attachment_type_id
					attyvo2 = new AttachmentTypeVO2();
					attyvo2.setAttachment_type_id(rs1.getString("attachment_type_id"));
					attyvo2.setAttachment_type_desc(rs1.getString("attachment_type_desc"));
					attyvo2Al.add(attyvo2);
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
		}
		
		attypelstvo = new AttachmentTypeListingVO();
		attypelstvo.setVals(attyvo2Al);
		
		return attypelstvo;
		
	}
	
}
