/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_21JExcelDataVO;

public class Form1_21JExcelDAO {
	@SuppressWarnings("resource")
	public Form1_21JExcelDataVO getF1_21JExcelData( String form_id, String ph_id ) 
			throws MyException, SQLException {
		Form1_21JExcelDataVO f121jxlvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> col = null;	
		ArrayList<String> miscids = null;
		ArrayList<String> miscnms = null;
		ArrayList<String> miscrow = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();
		
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		String getMiscIds = QueryConstants.QUERY_GET_MISCIDS;
		String getF121Val = QueryConstants.QUERY_GET_F1_21_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(getMiscIds);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids = new ArrayList<String>();
				miscnms = new ArrayList<String>();
				while(rs1.next()) {
					miscids.add(rs1.getString("misc_form_label_id"));
					miscnms.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			int int_ph_id = Integer.parseInt(ph_id);
			valAl = new ArrayList<ArrayList<String>>();
			
			prstmt1 = conn1.prepareStatement(getF121Val);
			
			for (int i = 0; i < miscids.size(); i++) {
				miscrow =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids.get(i));
				miscrow.add(miscnms.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, int_ph_id);
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow.add(rs1.getString("val"));
						}
					}
				}
				valAl.add(miscrow);
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		f121jxlvo = new Form1_21JExcelDataVO();
		f121jxlvo.setCols(col);
		f121jxlvo.setMiscids(miscids);
		f121jxlvo.setVals(valAl);
		return f121jxlvo;
		
	}

	
	public boolean updateF1_21(Form1_21JExcelDataVO data)
			throws MyException, SQLException {
		
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();

		String query1 = QueryConstants.QUERY_UPDATE_F1_21;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i < miscids.size(); i++) {
				int miscid = Integer.parseInt(miscids.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, pyid);
					prstmt1.setInt(3, miscid);
					
					prstmt1.addBatch();
				}
			}
			
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
	
}
