package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.DashboardPetitionVO;

public class DashboardPetitionDAO {

	public DashboardPetitionVO getDashboardPetition(String user_id) throws MyException, SQLException {
		
		DashboardPetitionVO dpvo = null;
		int int_user_id = Integer.parseInt(user_id);
		
		Connection conn1 = null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		ArrayList<String> petition = null;
		ArrayList<ArrayList<String>> petitionAl = null;
		String query1 = QueryConstants.QUERY_GET_PETITION_DETAILS;
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_user_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				petitionAl = new ArrayList<ArrayList<String>>();
				while(rs1.next()) {
					petition = new ArrayList<String>();
					/*
					SELECT t_petition_header.ph_id as ph_id, t_petition_header.base_year as base_year, module_type_desc, 
					TO_CHAR(t_petition_header.created_on, 'YYYY-MM-DD HH24:MI:SS') as created_on, m_user.user_fnm as fnm, 
					m_user.user_mnm as mnm, m_user.user_lnm as lnm FROM t_petition_header, m_module_type, m_user where 
					t_petition_header.module_type_id=m_module_type.module_type_id and t_petition_header.created_by=
					m_user.user_id and petition_state_id=1 and t_petition_header.isactive ='TRUE' and 
					t_petition_header.created_by=? order by ph_id
					*/
					petition.add(rs1.getString("ph_id"));
					petition.add(rs1.getString("base_year"));
					petition.add(rs1.getString("module_type_desc"));
					petition.add(rs1.getString("created_on").toString());
					String fnm = rs1.getString("fnm");
					String mnm = rs1.getString("mnm");
					String lnm = rs1.getString("lnm");
					String fullnm = null;
					if (mnm != null && mnm.length() != 0) {
						fullnm = fnm+" "+mnm+" "+lnm;
					} else {
						fullnm = fnm+" "+lnm;
					}
					petition.add(fullnm);
					
					petitionAl.add(petition);
				}
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
				conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
			
		}
		dpvo = new DashboardPetitionVO();
		dpvo.setPetitions(petitionAl);
		
		return dpvo;
		
	}
	
}
