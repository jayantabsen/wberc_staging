//*** By JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17gJExcelDataVO;
import in.gov.wberc.vo.Form1_22JExcelDataVO;

public class Form1_22UpdateDAO {

	public boolean updateF1_22Update(Form1_22JExcelDataVO data)
			throws MyException, SQLException {
		
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();

		String query1 = QueryConstants.QUERY_UPDATE_F1_22;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i < miscids.size(); i++) {
				int miscid = Integer.parseInt(miscids.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values.get(i + 1).get(j + 1));
					
					//update t_arrdf1_22 set val=? where py_id=? and misc_form_label_id=?
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, pyid);
					prstmt1.setInt(3, miscid);
					
					prstmt1.addBatch();
				}
			}
			
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
	
}
