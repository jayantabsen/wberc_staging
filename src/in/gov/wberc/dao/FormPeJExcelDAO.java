//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17hJExcelDataVO;
import in.gov.wberc.vo.Form1_17iJExcelDataVO;
import in.gov.wberc.vo.FormPeDataVO;
import in.gov.wberc.vo.FormPeJExcelDataVO;

public class FormPeJExcelDAO {
	
	public FormPeJExcelDataVO getFPeJExcelData( String form_id, String stn_id, String ph_id, String user_id ) 
			throws MyException, SQLException {
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query2 = QueryConstants.QUERY_GET_FPe_GENCO_VAL;
		
		FormPeJExcelDataVO fdejxldvo = new FormPeJExcelDataVO();
		fdejxldvo.setForm_id(form_id);
		fdejxldvo.setStn_id(stn_id);
		fdejxldvo.setPh_id(ph_id);
		fdejxldvo.setUser_id(user_id);
		
		FormPeDataVO fpe_datavo = null;
		ArrayList<FormPeDataVO> fpe_datavoAl = null;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			int int_ph_id = Integer.parseInt(ph_id);
			
			// Getting data
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, Integer.parseInt(stn_id));
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, int_ph_id);
			
			System.out.println(prstmt1);
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				fpe_datavoAl = new ArrayList<FormPeDataVO>();
				while (rs1.next()) {
					fpe_datavo = new FormPeDataVO();
					/*
					select package_name, work_scope, awarded_by, bid_recv_no, award_date, work_start_date, work_end_date, 
					award_value, firm_escalated, actual_expenditure, tax, cost form t_arrdfp_e_genco where stn_id=? and 
					form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					fpe_datavo.setPackage_name(rs1.getString("package_name"));
					fpe_datavo.setWork_scope(rs1.getString("work_scope"));
					fpe_datavo.setAwarded_by(rs1.getString("awarded_by"));
					fpe_datavo.setBid_recv_no(rs1.getString("bid_recv_no"));
					String dt = rs1.getString("award_date").toString();
					//fpe_datavo.setAward_date(dt.substring(0, dt.lastIndexOf('.')));
					fpe_datavo.setAward_date(dt);
					dt = rs1.getString("work_start_date").toString();
					//fpe_datavo.setWork_start_date(dt.substring(0, dt.lastIndexOf('.')));
					fpe_datavo.setWork_start_date(dt);
					dt = rs1.getString("work_end_date").toString();
					//fpe_datavo.setWork_end_date(dt.substring(0, dt.lastIndexOf('.')));
					fpe_datavo.setWork_end_date(dt);
					fpe_datavo.setAward_value(rs1.getString("award_value"));
					fpe_datavo.setFirm_escalated(rs1.getString("firm_escalated"));
					fpe_datavo.setActual_expenditure(rs1.getString("actual_expenditure"));
					fpe_datavo.setTax(rs1.getString("tax"));
					fpe_datavo.setCost(rs1.getString("cost"));

					fpe_datavoAl.add(fpe_datavo);
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		fdejxldvo.setVal(fpe_datavoAl);
	
		return fdejxldvo;
	}
	
	public boolean updateFPeMatrixToDatabase(FormPeJExcelDataVO data)
			throws MyException, SQLException {
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			String query2 = QueryConstants.QUERY_FPe_GENCO_UPDATE;
			
			int int_stn_id = Integer.parseInt(data.getStn_id());
			int int_form_id = Integer.parseInt(data.getForm_id());
			int int_ph_id = Integer.parseInt(data.getPh_id());
			ArrayList<FormPeDataVO> val = data.getVal();
			
			SimpleDateFormat dateFormatted = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String dt = null;
			Date dt_date = null;
			Calendar cal_dt = Calendar.getInstance();
			Timestamp dtInTimeStamp = null;
			
			/*
			update t_arrdfp_e_genco set package_name=?, work_scope=?, awarded_by=?, bid_recv_no=?, award_date=?, 
			work_start_date=?, work_end_date=?, award_value=?, firm_escalated=?, actual_expenditure=?, tax=?, cost=?, 
			updated_on=?, updated_by=? where stn_id=? and form_instance_id=(select form_instance_id from 
			t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1 = conn1.prepareStatement(query2);
			
			for (int i = 0; i<val.size(); i++) {
				FormPeDataVO row  = val.get(i);
				prstmt1.setString(1, row.getPackage_name());
				prstmt1.setString(2, row.getWork_scope());
				prstmt1.setString(3, row.getAwarded_by());
				prstmt1.setString(4, row.getBid_recv_no());
				
				dt = row.getAward_date();
				dt_date = dateFormatted.parse(dt);
				cal_dt.setTime(dt_date);
				dtInTimeStamp = new Timestamp(cal_dt.getTimeInMillis());
				prstmt1.setTimestamp(5, dtInTimeStamp);
				
				dt = row.getWork_start_date();
				dt_date = dateFormatted.parse(dt);
				cal_dt.setTime(dt_date);
				dtInTimeStamp = new Timestamp(cal_dt.getTimeInMillis());
				prstmt1.setTimestamp(6, dtInTimeStamp);
				
				dt = row.getWork_end_date();
				dt_date = dateFormatted.parse(dt);
				cal_dt.setTime(dt_date);
				dtInTimeStamp = new Timestamp(cal_dt.getTimeInMillis());
				prstmt1.setTimestamp(7, dtInTimeStamp);
				
				prstmt1.setString(8, row.getAward_value());
				prstmt1.setString(9, row.getFirm_escalated());
				prstmt1.setString(10, row.getActual_expenditure());
				prstmt1.setDouble(11, Double.parseDouble(row.getTax()));
				prstmt1.setDouble(12, Double.parseDouble(row.getCost()));
				
				prstmt1.setTimestamp(13, timestamp);
				prstmt1.setInt(14, Integer.parseInt(data.getUser_id()));
				prstmt1.setInt(15, int_stn_id);
				prstmt1.setInt(16, Integer.parseInt(data.getForm_id()));
				prstmt1.setInt(17, int_ph_id);
				
				prstmt1.addBatch();
			}
			
			prstmt1.executeBatch();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} catch(ParseException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
		
	}
	
}
