//*** By Najma


package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_5SeasonUsageVO;
import in.gov.wberc.vo.Form1_5GrossJExcelDataVO;

public class Form1_5GrossJExcelDAO {

	public Form1_5GrossJExcelDataVO getF1_5GrossJExcelData(String ph_id, String form_id, String util_id) 
			throws MyException, SQLException {
		
		Form1_5GrossJExcelDataVO f15gjxldvo = null;
		F1_5SeasonUsageVO f15suvo1 = null;

		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
		
		// Getting PYIDs
		ArrayList<ArrayList<String>> usageAl2 = null;

		ArrayList<String> col = null;
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		String getSeasonIds = QueryConstants.QUERY_GET_SEASONIDS;
		String getSeasonName = QueryConstants.QUERY_GET_SEASONNM;
		String getSumVal = QueryConstants.QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_5;
		String getPeriodNamePerSeason = QueryConstants.QUERY_GET_PERIOD_NAME_SEASON_WISE;
		
		ArrayList<String> seasons = null;
		ArrayList<ArrayList<String>> alal = new ArrayList<ArrayList<String>>();
		ArrayList<F1_5SeasonUsageVO> f15suvoAl1 = new ArrayList<F1_5SeasonUsageVO>();

		ArrayList<String> al = null;

		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(getSeasonIds);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			
			for (String season : seasons ) { //***************** Season
				f15suvo1 = new F1_5SeasonUsageVO();
				int int_seasonid = Integer.parseInt(season);
				prstmt1 = conn1.prepareStatement(getSeasonName);
				prstmt1.setInt(1, int_seasonid);
				rs1=prstmt1.executeQuery();
				if(rs1!=null) {
					while(rs1.next()) {
						String all_season_name = rs1.getString("season_desc");
						all_season_name = all_season_name.substring(0,1).toUpperCase() + all_season_name.substring(1).toLowerCase();
						f15suvo1.setSeason("Total " + all_season_name);
						usageAl2 = new ArrayList<ArrayList<String>>();
						prstmt1 = conn1.prepareStatement(getPeriodNamePerSeason);
						prstmt1.setInt(1, int_seasonid);
						rs2 = prstmt1.executeQuery();
						if(rs2!=null) {
							while(rs2.next()) {
								
								al = new ArrayList<String>();
								
								String period_name = rs2.getString("period_desc");
								String period_id = rs2.getString("period_id");
								al.add(period_name + ": ");

								for (String pyid : col ) {
									int pyear = Integer.parseInt(pyid);
									prstmt1 = conn1.prepareStatement(getSumVal);
									prstmt1.setInt(1, Integer.parseInt(period_id));
									prstmt1.setInt(2, pyear);
									prstmt1.setInt(3, Integer.parseInt(form_id));
									prstmt1.setInt(4, Integer.parseInt(ph_id));
									System.out.println("prst"+prstmt1);
									rs3 = prstmt1.executeQuery();
									if (rs3!= null) {
										while(rs3.next()) {
											String sum = rs3.getString("sumval");
											System.out.println("sum"+sum);
											al.add(sum);
										}
									}
								}
								usageAl2.add(al);
							}
							}
						
						

					}
				}
				
		/*		for (String pyid : col ) { //****************** Petition Year
					int int_pyid = Integer.parseInt(pyid);
					prstmt1 = conn1.prepareStatement(getSumVal);
					
					prstmt1.setInt(1, int_pyid);
//					prstmt1.setInt(2, int_seasonid);
					rs1=prstmt1.executeQuery();
					if(rs1!=null) {
						if(rs1.next()) {
							al.add( rs1.getString("sumval") );
						}
					}
					usageAl2.add(al);

				}*/
				f15suvo1.setUsage(usageAl2);
				f15suvoAl1.add(f15suvo1);

			}
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(prstmt1!=null) {
				prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}
		
		f15gjxldvo = new Form1_5GrossJExcelDataVO();
		f15gjxldvo.setCols(col);
		f15gjxldvo.setSeasons(seasons);
		f15gjxldvo.setGross(f15suvoAl1);
		
		return f15gjxldvo;
	}
	
}
