//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;
import in.gov.wberc.vo.FormD1JExcelDataVO;

public class FormD1JExcelDAO {
	
	public FormD1JExcelDataVO getFD1JExcelData( String form_id, String util_id, String stn_id, String py_id ) 
			throws MyException, SQLException {
		
		FormD1JExcelDataVO fd1jxldvo = null;
		/*
		private String stn_id;
		private String py_id;
		private ArrayList<String> cols; // coalgrade_id(s)
		private ArrayList<String> rows; // util_coal_field_id
		private ArrayList<ArrayList<String>> vals;
		*/
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> cols = null; // coalgrade_id(s)
		ArrayList<String> rows_util_coal_field_id = null; // util_coal_field_id(s)
		ArrayList<String> coal_field_desc = null;
		ArrayList<String> val = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();

		String query1 = QueryConstants.QUERY_GET_COALGRADE_IDs;
		String query2 = QueryConstants.QUERY_GET_UTIL_COAL_FIELD;
		String query3 = QueryConstants.QUERY_GET_PHID;
		String query4 = QueryConstants.QUERY_GET_FD1_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			// select coalgrade_id from m_coalgrade order by coalgrade_id
			prstmt1 = conn1.prepareStatement(query1);

			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					cols.add(rs1.getString("coalgrade_id"));
				}
			}
			
			/*
			select util_coal_field_id, coal_field_desc from m_coal_field, mp_util_coal_field where 
			m_coal_field.coal_field_id=mp_util_coal_field.coal_field_id and util_id=? order by 
			m_coal_field.coal_field_id
			*/
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, Integer.parseInt(util_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				rows_util_coal_field_id = new ArrayList<String>();
				coal_field_desc = new ArrayList<String>();
				while(rs1.next()) {
					rows_util_coal_field_id.add(rs1.getString("util_coal_field_id"));
					coal_field_desc.add(rs1.getString("coal_field_desc"));
				}
			}
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(py_id);
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query3);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			int int_stn_id = Integer.parseInt(stn_id);
			valAl = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(query4);
			for (int i = 0; i < rows_util_coal_field_id.size(); i++) {
				val =  new ArrayList<String>();
				int int_util_coal_field_id = Integer.parseInt(rows_util_coal_field_id.get(i));
				val.add(coal_field_desc.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int int_coalgrade_id = Integer.parseInt(cols.get(j));
					
					/*
					SELECT val FROM t_arrdfd1 where stn_id=? and py_id=? and util_coal_field_id=? and 
					coalgrade_id=? form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_util_coal_field_id);
					prstmt1.setInt(4, int_coalgrade_id);
					prstmt1.setInt(5, Integer.parseInt(form_id));
					prstmt1.setInt(6, int_ph_id);
					
					System.out.println(prstmt1);
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							val.add(rs1.getString("val"));
						}
					}
				}
				valAl.add(val);
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		fd1jxldvo = new FormD1JExcelDataVO();
		fd1jxldvo.setStn_id(stn_id);
		fd1jxldvo.setPy_id(py_id);
		fd1jxldvo.setForm_id(form_id);
		fd1jxldvo.setCols(cols);
		fd1jxldvo.setRows(rows_util_coal_field_id);
		fd1jxldvo.setVals(valAl);

		return fd1jxldvo;
	}
}
