package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_3SeasonUsageVO1;
import in.gov.wberc.vo.F1_4bSeasonUsageVO;
import in.gov.wberc.vo.Form1_3JExcelDataVO;
import in.gov.wberc.vo.Form1_4bJExcelDataVO;

public class Form1_4bJExcelDAO {

	public Form1_4bJExcelDataVO getF1_4bJExcelData(String pdid, String formid, 
			String utilid, String pump_stationid) throws MyException, SQLException {
		
		Form1_4bJExcelDataVO f14bjxldvo1 = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		
		ArrayList<String> col = null;
		F1_4bSeasonUsageVO f13suvo1 = null;
		ArrayList<String> usageAl1 = null;
		ArrayList<String> usage = null;
		ArrayList<ArrayList<String>> usageAl2 = null;
		ArrayList<ArrayList<String>> usageList = new ArrayList<ArrayList<String>>();
		ArrayList<F1_4bSeasonUsageVO> f13suvoAl1 = new ArrayList<F1_4bSeasonUsageVO>();
		
		
		String query2 = QueryConstants.QUERY_GET_PYID;
		String query3 = QueryConstants.QUERY_GET_SEASONIDS;
		String query5 = QueryConstants.QUERY_GET_SEASONNM;
		String query6 = QueryConstants.QUERY_GET_PERIOD_NAME;
		String query7 = QueryConstants.QUERY_GET_F1_4bUSAGEWISE_VALUE;
		String query8= QueryConstants.QUERY_GET_PERIOD_SEASON_WISE;
		
		ArrayList<String> seasons = null;
		
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, Integer.parseInt(pdid));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			prstmt1 = conn1.prepareStatement(query3);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String seasonid : seasons) {
				prstmt1 = conn1.prepareStatement(query8);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					usage = new ArrayList<String>();
					while(rs1.next()) {
						usage.add(rs1.getString("period_id"));
					}
				}
				usageList.add(usage);
				f13suvo1 = new F1_4bSeasonUsageVO();
				conn1=DBUtil.getConnection();
				prstmt1 = conn1.prepareStatement(query5);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					if(rs1.next()) {
						f13suvo1.setSeason(rs1.getString("season_desc"));
						usageAl2 = new ArrayList<ArrayList<String>>();
						ArrayList<String> periodPerSeason = null;
						
						prstmt1 = conn1.prepareStatement(query8);
						prstmt1.setInt(1, Integer.parseInt(seasonid));
						rs2=prstmt1.executeQuery();
						if(rs2!=null) {
							periodPerSeason = new ArrayList<String>();
							while(rs2.next()) {
								periodPerSeason.add(rs2.getString("period_id"));
							}
						}
						
						try {
							if(rs2!=null) {
								rs2.close();
							}
						}catch(SQLException e) {
							e.printStackTrace();
							throw new MyException();
						
						}
						

						for (String todid : periodPerSeason) {
							prstmt1 = conn1.prepareStatement(query6);
							prstmt1.setInt(1, Integer.parseInt(todid));
							usageAl1 = new ArrayList<String>();
							
							rs2=prstmt1.executeQuery();
							if ( rs2 != null ) {
								if(rs2.next()) {
									usageAl1.add(rs2.getString("period_desc"));
								}
							}
							try {
								if(rs2!=null) {
									rs2.close();
								}
							}catch(SQLException e) {
								e.printStackTrace();
								throw new MyException();
							
							}
							prstmt1 = conn1.prepareStatement(query7);
							for(int m=0;m<col.size();m++) {
								prstmt1.setInt(1, Integer.parseInt(pump_stationid));
								prstmt1.setInt(2, Integer.parseInt(todid));
								prstmt1.setInt(3, Integer.parseInt(col.get(m)));
								rs2=prstmt1.executeQuery();
								if ( rs2 != null ) {
									while (rs2.next()) {
										usageAl1.add(rs2.getString("val"));
									}
								}
							}
							usageAl2.add(usageAl1);
							}
					}
					f13suvo1.setUsage(usageAl2);
					f13suvoAl1.add(f13suvo1);
				}
			}
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
			}
		f14bjxldvo1 = new Form1_4bJExcelDataVO();
		f14bjxldvo1.setPump_stationid(pump_stationid);
		f14bjxldvo1.setCols(col);
		f14bjxldvo1.setSeasons(seasons);
		f14bjxldvo1.setUsage(usageList);
		f14bjxldvo1.setVals(f13suvoAl1);
		
		return f14bjxldvo1;
		
	}

	
}
