package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.CommonUtils;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.utils.GetMaxID;

public class AuthenticatorDAO {

	public String fetchUserPass(String loginID, String password) throws MyException, SQLException {
		String pwCompareStr="";
		String query = QueryConstants.QUERY_LOGIN_AUTH;

		PreparedStatement prstmt=null;
		Connection conn=null;
		ResultSet rs=null;

		int count=1;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setString(count++, loginID);

			rs=prstmt.executeQuery();
			if(rs!=null) {
				while(rs.next()) {
					pwCompareStr = rs.getString("pwd");
				}
			}
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null) {
				prstmt.close();
				}
				if(rs!=null) {
					rs.close();
				}
		}
		return pwCompareStr;
	}

	public boolean authenticate(String hashedPass, String pwCompareStr) {

		if(hashedPass.equals(pwCompareStr))
			return true;
		else 
			return false;
	}

	public boolean checkUserINTokenDatabase(String loginid) throws SQLException {
		String query = QueryConstants.QUERY_FIRST_TIME_USER_CHECK;
		String user="";
		PreparedStatement prstmt=null;
		Connection conn=null;
		ResultSet rs=null;

		int count=1;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setString(count++, loginid);

			rs=prstmt.executeQuery();
			if(rs!=null) {
				while(rs.next()) {
					user = rs.getString("login_id");
				}

			}

			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
			if(conn!=null) {
				conn.close();
			}
			if(prstmt!=null) {
			prstmt.close();
			}
			if(rs!=null) {
				rs.close();
			}
			
		}
		if(user.equals(loginid))
			return true;
		else 
			return false;
	}

	public String newUser(String loginID) throws MyException, SQLException {
		String query = QueryConstants.QUERY_INSERT_USER;
		String token = CommonUtils.issueToken();
		
		int mid = Integer.parseInt(GetMaxID.getMaxID("t_user_token", "utoken_id"));
		mid++;

		PreparedStatement prstmt=null;
		Connection conn=null;
		int count=1;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setInt(count++, mid);
			prstmt.setString(count++, loginID);
			prstmt.setString(count++, token);
			
			prstmt.executeUpdate();
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null) {
				prstmt.close();
				}
				
		}
		return token;

	}

	public boolean checkTokenValidity(String token) throws SQLException {
		String query = QueryConstants.QUERY_TOKEN_VALIDITY;
		String userToken="";
		ResultSet rs=null;

		PreparedStatement prstmt=null;
		Connection conn=null;

		int count=1;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setString(count++, token);

			rs=prstmt.executeQuery();

			if(rs!=null) {
				while(rs.next()) {
					userToken = rs.getString("token");
				}
			}
			conn.commit();
		}	catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
			if(conn!=null) {
				conn.close();
			}
			if(prstmt!=null) {
			prstmt.close();
			}
			if(rs!=null) {
				rs.close();
			}
		}
		if(userToken.equals(token))
			return true;
		else
			return false;
	}

	public String updateCurrentUserTokenValidity(String loginID) throws SQLException {
		String query = QueryConstants.QUERY_UPDATE_USER_TOKEN_VALIDITY;
		String query1 = QueryConstants.QUERY_GET_TOKEN_FROM_USER;
		String token = "";

		PreparedStatement prstmt=null;
		PreparedStatement prstmt1=null;
		Connection conn=null;
		ResultSet rs=null;
		int updated=0;
		int count=1;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setString(count++, loginID);
			
			updated = prstmt.executeUpdate();
			conn.commit();
			
			if(updated==1) {

				prstmt1 = conn.prepareStatement(query1);
				prstmt1.setString(1, loginID);
				
				rs=prstmt1.executeQuery();
				
				if(rs!=null) {
					while(rs.next()) {
						token = rs.getString("token");
					}
				}
			}
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null || prstmt1!=null) {
				prstmt.close();
				prstmt1.close();
				}
				if(rs!=null) {
					rs.close();
				}
			
		}
		return token;
	}
	
	public String getToken(String loginID) throws SQLException {
		String token=null;
		String query1 = QueryConstants.QUERY_GET_TOKEN_FROM_USER;
		
		PreparedStatement prstmt1=null;
		Connection conn=null;
		ResultSet rs=null;
		
		try {
			conn=DBUtil.getConnection();
			prstmt1 = conn.prepareStatement(query1);
			prstmt1.setString(1, loginID);
			
			rs=prstmt1.executeQuery();

			if(rs!=null) {
				while(rs.next()) {
					token = rs.getString("token");
				}
			}
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs!=null) {
					rs.close();
				}
			
		}
		return token;
	}
	
	public void logOutUser(String loginID, String token) throws MyException, SQLException {
		String query = QueryConstants.QUERY_DELETE_TOKEN_ON_LOGOUT;
		PreparedStatement prstmt=null;
		Connection conn=null;
		ResultSet rs=null;
		int updated = 0;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);
			
			prstmt.setString(1, loginID);
			prstmt.setString(2, token);
			
			updated = prstmt.executeUpdate();
			System.out.println(updated + " in AutheinticatorDAO");
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null) {
				prstmt.close();
				}
				if(rs!=null) {
					rs.close();
				}
			
		}
		//return updated;
	}

}
