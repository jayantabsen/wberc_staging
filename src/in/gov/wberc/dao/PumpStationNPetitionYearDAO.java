package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.PumpStationNPetitionYearVO;
import in.gov.wberc.vo.YearListVO;
import in.gov.wberc.vo.YearVO;

public class PumpStationNPetitionYearDAO {

	public PumpStationNPetitionYearVO getPumpStationNPetitionYear(String form_id, String pump_station_id, 
			String ph_id) throws MyException, SQLException {
		
		PumpStationNPetitionYearVO psnpyvo = null;
		YearVO year = null;
		YearListVO prev_ylvo = null;
		YearListVO base_ylvo = null;
		YearListVO ensu_ylvo = null;
		
		String query1 = QueryConstants.QUERY_PUMP_STATION_DETAILS;
		String query2 = QueryConstants.QUERY_GET_PETITION_YEARS;
		String query3 = QueryConstants.GET_MEASURING_UNIT;

		
		PreparedStatement prstmt1 = null;	
		Connection conn1=null;	
		ResultSet rs1=null;
		
		// Getting stationid, stationnm, capacity and measuring_unit
		String pump_stationid_from_db = null;
		String pump_stationnm = null;
		String capacity = null;
		String measuring_unit = null;
		
		ArrayList<YearVO> prev_yearsAl = null;
		ArrayList<YearVO> base_yearsAl = null;
		ArrayList<YearVO> ensu_yearsAl = null;
		ArrayList<YearListVO> labeledYearsAl = null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query3);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if ( rs1 != null ) {
				if ( rs1.next() ) {
					measuring_unit = rs1.getString("unit_of_measurement");
				}
			}
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(pump_station_id));
			rs1=prstmt1.executeQuery();
			
			if ( rs1 != null ) {
				if ( rs1.next() ) {
					pump_stationid_from_db = rs1.getString("pump_stn_id");
					pump_stationnm = rs1.getString("pump_stn_desc");
					capacity = rs1.getString("capacity");
				}
			}
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {				
				prev_yearsAl = new ArrayList<YearVO>();
				base_yearsAl = new ArrayList<YearVO>();
				ensu_yearsAl = new ArrayList<YearVO>();
				prev_ylvo = new YearListVO();
				base_ylvo = new YearListVO();
				ensu_ylvo = new YearListVO();
				while(rs1.next()) {
					year = new YearVO();
					year.setYearval(rs1.getString("year_nm"));
					int seq = Integer.parseInt(rs1.getString("seq"));
					if ( seq < 0 ) {
						prev_yearsAl.add(year);
					} else {
						if ( seq > 0 ) {
							ensu_yearsAl.add(year);
						} else {
							base_yearsAl.add(year);
						}
					}
				}
				prev_ylvo.setLabel("Previous Year");
				prev_ylvo.setYearsAl(prev_yearsAl);
				base_ylvo.setLabel("Base Year");
				base_ylvo.setYearsAl(base_yearsAl);
				ensu_ylvo.setLabel("Ensuing Year");
				ensu_ylvo.setYearsAl(ensu_yearsAl);

				labeledYearsAl = new ArrayList<YearListVO>();
				labeledYearsAl.add(prev_ylvo);
				labeledYearsAl.add(base_ylvo);
				labeledYearsAl.add(ensu_ylvo);
			}
			
			conn1.commit();	
		}catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		}finally{
				if(conn1!=null) {
				conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
		}
		psnpyvo = new PumpStationNPetitionYearVO();
		psnpyvo.setPump_stationid(pump_stationid_from_db);
		psnpyvo.setPump_stationnm(pump_stationnm);
		psnpyvo.setCapacity(capacity);
		psnpyvo.setUnit_of_calc(measuring_unit);
		psnpyvo.setLabeledYearsAl(labeledYearsAl);
		
		return psnpyvo;
		
	}
	
}
