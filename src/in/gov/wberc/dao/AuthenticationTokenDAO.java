package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;

public class AuthenticationTokenDAO {
	
	public boolean checkTokenValidity(String token) {
		String query = QueryConstants.QUERY_TOKEN_VALIDITY;
		String userToken="";
		ResultSet rs=null;

		PreparedStatement prstmt=null;
		Connection conn=null;

		int count=1;
		try {
			conn = DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);

			prstmt.setString(count++, token);

			rs=prstmt.executeQuery();

			if(rs!=null) {
				while(rs.next()) {
					userToken = rs.getString("token");
				}
			}
		}	catch(SQLException e) {
			e.printStackTrace();
		}finally{
			if(prstmt!=null) {
				try {
					prstmt.close();
					if(conn!=null) {
						conn.close();
					}
					if(rs!=null) {
						rs.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
		if(userToken.equals(token))
			return true;
		else
			return false;
	}

	
	public void logOutUser(String loginID, String token) throws MyException, SQLException {
		String query = QueryConstants.QUERY_DELETE_TOKEN_ON_LOGOUT;
		PreparedStatement prstmt=null;
		Connection conn=null;
		ResultSet rs=null;
		int updated = 0;
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);
			
			prstmt.setString(1, loginID);
			prstmt.setString(2, token);
			
			updated = prstmt.executeUpdate();
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null) {
				prstmt.close();
				}
				
				if(rs!=null) {
					rs.close();
				}
			
		}
		//return updated;
	}
	
}
