//*** By Najma Updated by JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17JExcelDataVO;


public class Form1_6cDataUpdateDAO {

	public boolean updateF1_6cMatrixToDatabase(Form1_17JExcelDataVO data)
			throws MyException, SQLException {
		
		int int_py_id = Integer.parseInt(data.getPy_id());
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		String query1 = QueryConstants.QUERY_UPDATE_F1_6c;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		//Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			for (int i = 0; i < miscids.size(); i++) {
				int int_misc_id = Integer.parseInt(miscids.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int int_month_id = Integer.parseInt(cols.get(j));
					
					double val = Double.parseDouble(values.get(i).get(j+1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_month_id);
					prstmt1.setInt(4, int_misc_id);
					prstmt1.executeUpdate();
				}
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(conn1!=null) {
				conn1.close();
			}
		}
		
		return true;
		
	}
	
}
