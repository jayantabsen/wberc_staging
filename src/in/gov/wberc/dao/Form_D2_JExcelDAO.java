/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form_D2_JExcelDataVO;

public class Form_D2_JExcelDAO {

	@SuppressWarnings("resource")
	public Form_D2_JExcelDataVO getF_D2_JExcelData(String stn_id, String ph_id, String form_id) throws SQLException, MyException {
		
		Form_D2_JExcelDataVO formD2Ref = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> miscids = null;
		ArrayList<String> miscnms = null;
		ArrayList<String> miscrow = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();
		
		String getMiscIds = QueryConstants.QUERY_GET_MISCIDS;
		String getD2Val = QueryConstants.QUERY_GET_F_D2_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getMiscIds);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids = new ArrayList<String>();
				miscnms = new ArrayList<String>();
				while(rs1.next()) {
					miscids.add(rs1.getString("misc_form_label_id"));
					miscnms.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			int int_stn_id = Integer.parseInt(stn_id);
			valAl = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getD2Val);
			
			for (int i = 0; i < miscids.size(); i++) {
				miscrow = new ArrayList<String>();
				int int_misc_id = Integer.parseInt(miscids.get(i));
				miscrow.add(miscnms.get(i));
				prstmt1.setInt(1, int_stn_id);
				prstmt1.setInt(2, int_misc_id);
				prstmt1.setInt(3, Integer.parseInt(form_id));
				prstmt1.setInt(4, Integer.parseInt(ph_id));

				rs1 = prstmt1.executeQuery();

				if (rs1 != null) {
					while (rs1.next()) {
						miscrow.add(rs1.getString("cost"));
					}
				}
				valAl.add(miscrow);
			}
			conn1.commit();
		}
		catch(SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		} finally {						
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		formD2Ref = new Form_D2_JExcelDataVO();
		formD2Ref.setStationid(stn_id);
		formD2Ref.setFormid(form_id);
		formD2Ref.setPhid(ph_id);
		formD2Ref.setMiscids(miscids);
		formD2Ref.setVals(valAl);
		
		return formD2Ref;
	}
	
	
	
//	Data Update
	public boolean updateF_D2_JExcelData(Form_D2_JExcelDataVO data)
			throws MyException, SQLException {
		
		int int_stn_id = Integer.parseInt(data.getStationid());
		int form_id = Integer.parseInt(data.getFormid());
		int ph_id = Integer.parseInt(data.getPhid());

		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();

		String updateFormD2 = QueryConstants.QUERY_UPDATE_F_D2;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(updateFormD2);
			for (int i = 0; i < miscids.size(); i++) {
				int int_misc_id = Integer.parseInt(miscids.get(i));
				
					double val = Double.parseDouble(values.get(i).get(1));
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_stn_id);
					prstmt1.setInt(3, int_misc_id);
					prstmt1.setInt(4, form_id);
					prstmt1.setInt(5, ph_id);
					prstmt1.executeUpdate();
				
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
}
