//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_4bGrossJExcelDataVO;

public class Form1_4bGrossJExcelDAO {

	public Form1_4bGrossJExcelDataVO getF1_4bGrossJExcelData(String pdid, String formid, String utilid) 
			throws MyException {
		
		Form1_4bGrossJExcelDataVO f14bgjxldvo = null;
		
		Connection conn1=null;
		
		PreparedStatement prstmt1 = null;
		
		ResultSet rs1=null;
		
		// Getting PYIDs
		ArrayList<String> col = null;
		String query1 = QueryConstants.QUERY_GET_PYID;
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(pdid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("pyid"));
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
			}
		
		
		// Getting Season IDs
		ArrayList<String> seasons = null;
		String query2 = QueryConstants.QUERY_GET_SEASONIDS;
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query2);
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("seasonid"));
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
			}
		
		
		String query3 = QueryConstants.QUERY_GET_SEASONNM;
		String query4 = QueryConstants.QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_4b;
		ArrayList<ArrayList<String>> alal = new ArrayList<ArrayList<String>>();
		ArrayList<String> al = null;
		for (String season : seasons ) { //***************** Season
			int int_seasonid = Integer.parseInt(season);
			al = new ArrayList<String>();
			try {
				conn1=DBUtil.getConnection();
				prstmt1 = conn1.prepareStatement(query3);
				
				prstmt1.setInt(1, int_seasonid);
				
				rs1=prstmt1.executeQuery();
				if(rs1!=null) {
					if(rs1.next()) {
						al.add("Total " + rs1.getString("season_nm") + ": ");
					}
				}
				
					try {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
						if(rs1!=null) {
							rs1.close();
						}
					}catch(SQLException e) {
						e.printStackTrace();
						throw new MyException();
					}
			
				
				for (String pyid : col ) { //****************** Petition Year
					int int_pyid = Integer.parseInt(pyid);
					
					conn1=DBUtil.getConnection();
					prstmt1 = conn1.prepareStatement(query4);
					
					prstmt1.setInt(1, int_pyid);
					prstmt1.setInt(2, int_seasonid);
					
					//System.out.println("Query for Seasonid="+season+" and "+"Pyid="+pyid+" -- "+prstmt1);
					
					rs1=prstmt1.executeQuery();
					if(rs1!=null) {
						if(rs1.next()) {
							al.add( rs1.getString("sumval") );
						}
					}
				}
				
					try {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
						if(rs1!=null) {
							rs1.close();
						}
					}catch(SQLException e) {
						e.printStackTrace();
						throw new MyException();
					}
				
				alal.add(al);
				
			} catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			} finally {
				
					try {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
						if(rs1!=null) {
							rs1.close();
						}
					}catch(SQLException e) {
						e.printStackTrace();
						throw new MyException();
					}
				}
			}
		
		f14bgjxldvo = new Form1_4bGrossJExcelDataVO();
		f14bgjxldvo.setCols(col);
		f14bgjxldvo.setSeasons(seasons);
		f14bgjxldvo.setGross(alal);
		
		return f14bgjxldvo;
	}
	
}
