package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.AttachmentTypeVO;

public class AttachmentTypeDAO {

	public AttachmentTypeVO getAttachmentType(String attachment_type_id) throws MyException, SQLException {
		
		AttachmentTypeVO attyoevo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		
		String attachment_type_desc = "";
		
		String query1 = QueryConstants.QUERY_GET_ATTACHMENT_TYPE;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(attachment_type_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					attachment_type_desc = rs1.getString("attachment_type_desc");
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}
			if (rs1 != null) {
				rs1.close();
			}
		}
		
		attyoevo = new AttachmentTypeVO();
		attyoevo.setAttachment_type_desc(attachment_type_desc);
		
		return attyoevo;
		
	}
	
}
