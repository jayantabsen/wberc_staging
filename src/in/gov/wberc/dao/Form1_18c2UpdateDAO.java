//*** By JBS
package in.gov.wberc.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_18c2JExcelDataVO;

public class Form1_18c2UpdateDAO {

	public boolean updateF1_18c2Update(Form1_18c2JExcelDataVO data)
			throws MyException, SQLException {
		
		int int_ph_id = Integer.parseInt(data.getPh_id());
		int int_form_id = Integer.parseInt(data.getForm_id());
		int int_user_id = Integer.parseInt(data.getUser_id());
		ArrayList<String> cols = data.getCols(); // py_id(s)
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String query1 = QueryConstants.QUERY_GET_FORM_INSTANCE_ID;
		String query2 = QueryConstants.QUERY_DELETE_F1__18c2;
		String query3 = QueryConstants.QUERY_INSERT_t_arrdf1_18c2;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			
			// Getting form_instance_id
			prstmt1 = conn1.prepareStatement(query1);
			/*
			select form_instance_id from t_form_instance where form_id=? and ph_id=?
			*/
			prstmt1.setInt(1, int_form_id);
			prstmt1.setInt(2, int_ph_id);
			rs1=prstmt1.executeQuery();
			int int_form_instance_id = 0;
			if(rs1!=null) {
				if(rs1.next()) {
					int_form_instance_id = Integer.parseInt(rs1.getString("form_instance_id"));
				}
			}
			
			// Getting insurance_desc count
			Object[] s = cols.toArray();
			Array array = conn1.createArrayOf("INTEGER", s);
			prstmt1 = conn1.prepareStatement(query2);
			/*
			Delete => delete from t_arrdf1_18c2 where py_id = ANY(?) and form_instance_id=?
			*/
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setArray(1, array);
			prstmt1.setInt(2, int_form_instance_id);
			prstmt1.executeUpdate();
			
			/*
			Insert => INSERT INTO t_arrdf1_18c2 (form_instance_id, py_id, income_desc, val, 
			created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)
			*/
			prstmt1 = conn1.prepareStatement(query3);
			for ( int i = 0; i<vals.size(); i++ ) {
				ArrayList<String> tableRow = vals.get(i);
				String income_desc = tableRow.get(0);
				for ( int j = 0; j<cols.size(); j++  ) {
					int py_id = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(tableRow.get(j+1));
					
					prstmt1.setInt(1, int_form_instance_id);
					prstmt1.setInt(2, py_id);
					prstmt1.setString(3, income_desc);
					prstmt1.setDouble(4, val);
					prstmt1.setTimestamp(5, timestamp);
					prstmt1.setInt(6, int_user_id);
					prstmt1.setTimestamp(7, timestamp);
					prstmt1.setInt(8, int_user_id);
					
					prstmt1.executeUpdate();
				}
			}
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
	}
	
}
