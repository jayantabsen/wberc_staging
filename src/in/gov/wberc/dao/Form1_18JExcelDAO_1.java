//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_18JExcelDataVO_1;
import in.gov.wberc.vo.Form1_18JExcelDataVO_2;

public class Form1_18JExcelDAO_1 {
	
	public Form1_18JExcelDataVO_1 getF1_18Data_1( String form_id, String util_id, String ph_id, String user_id ) 
			throws MyException, SQLException {
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PYID;
		String query2 = QueryConstants.QUERY_GET_UTILWISE_STATIONS;
		String query3 = QueryConstants.QUERY_GET_F1_18VAL_1;
		
		String query4 = QueryConstants.QUERY_GET_MISCIDS;
		String query5 = QueryConstants.QUERY_GET_F1_18VAL_2;
		
		Form1_18JExcelDataVO_1 fdatajxldvo = new Form1_18JExcelDataVO_1();
		fdatajxldvo.setForm_id(form_id);
		fdatajxldvo.setUtil_id(util_id);
		fdatajxldvo.setPh_id(ph_id);
		fdatajxldvo.setUser_id(user_id);
		
		ArrayList<String> cols = null; // py_id(s)
		ArrayList<String> rows1 = null; // stn_id(s) + formula
		ArrayList<String> rows2 = null; // misc_form_label_id(s)
		ArrayList<String> station_names = null;
		ArrayList<String> misc_labels = null;
		ArrayList<String> row = null;
		Form1_18JExcelDataVO_2 val = null;
		ArrayList<ArrayList<String>> station_vals = null;
		ArrayList<ArrayList<String>> misc_vals = null;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			int int_ph_id = Integer.parseInt(ph_id);
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_ph_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					cols.add(rs1.getString("py_id"));
				}
			}
			prstmt1.close();
			rs1.close();
			
			int int_util_id = Integer.parseInt(util_id);
			
			prstmt1 = conn1.prepareStatement(query2);
			// select stn_id, stn_desc from m_util_stn where util_id=? order by stn_id
			prstmt1.setInt(1, int_util_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				rows1 = new ArrayList<String>();
				station_names = new ArrayList<String>();
				while(rs1.next()) {
					rows1.add(rs1.getString("stn_id"));
					station_names.add(rs1.getString("stn_desc"));
				}
			}
			prstmt1.close();
			rs1.close();
			
			int int_form_id = Integer.parseInt(form_id);
			
			prstmt1 = conn1.prepareStatement(query4);
			// SELECT misc_form_label_id, misc_form_label_desc, remarks FROM m_misc_form_labels where form_id=? order by misc_form_label_id
			prstmt1.setInt(1, int_form_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				rows2 = new ArrayList<String>();
				misc_labels = new ArrayList<String>();
				while(rs1.next()) {
					rows2.add(rs1.getString("misc_form_label_id"));
					misc_labels.add(rs1.getString("misc_form_label_desc"));
				}
			}
			prstmt1.close();
			rs1.close();
			
			// Getting data
			prstmt1 = conn1.prepareStatement(query3);
			/*
			select val from t_arrdf1_18_1 where stn_id=? and py_id=? and form_instance_id=
			(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			station_vals = new ArrayList<ArrayList<String>>();
			for ( int i = 0; i<rows1.size(); i++ ) {
				int int_stn_id = Integer.parseInt(rows1.get(i));
				row = new ArrayList<String>();
				row.add(station_names.get(i));
				for ( int j = 0; j<cols.size(); j++ ) {
					int int_py_id = Integer.parseInt(cols.get(j));
					
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_form_id);
					prstmt1.setInt(4, int_ph_id);
					
					//System.out.println(prstmt1);
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						if (rs1.next()) {
							row.add(rs1.getString("val"));
						}
					}
				}
				station_vals.add(row);
			}
			prstmt1.close();
			rs1.close();
			station_vals = FrontendFormula.appendFormula_row_1_18_1(cols, rows1, station_vals);
			
			prstmt1 = conn1.prepareStatement(query5);
			/*
			select val from t_arrdf1_18_2 where misc_form_label_id=? and py_id=? and form_instance_id=
			(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			misc_vals = new ArrayList<ArrayList<String>>();
			for ( int i = 0; i<rows2.size(); i++ ) {
				int int_misc_form_label_id = Integer.parseInt(rows2.get(i));
				row = new ArrayList<String>();
				row.add(misc_labels.get(i));
				for ( int j = 0; j<cols.size(); j++ ) {
					int int_py_id = Integer.parseInt(cols.get(j));
					
					prstmt1.setInt(1, int_misc_form_label_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_form_id);
					prstmt1.setInt(4, int_ph_id);
					
					//System.out.println(prstmt1);
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						if (rs1.next()) {
							row.add(rs1.getString("val"));
						}
					}
				}
				misc_vals.add(row);
			}

			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		fdatajxldvo.setCols(cols);
		fdatajxldvo.setRows1(rows1);
		fdatajxldvo.setRows2(rows2);
		val = new Form1_18JExcelDataVO_2();
		val.setStation_vals(station_vals);
		val.setMisc_vals(misc_vals);
		fdatajxldvo.setVal(val);
	
		return fdatajxldvo;
	}
	
	public boolean updateF1_18Data_1(Form1_18JExcelDataVO_1 data)
			throws MyException, SQLException {
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		int int_form_id = Integer.parseInt(data.getForm_id());
		int int_ph_id = Integer.parseInt(data.getPh_id());
		int int_user_id = Integer.parseInt(data.getUser_id());
		ArrayList<String> cols = data.getCols(); // py_id(s)
		ArrayList<String> rows1 = data.getRows1(); // stn_id(s) + formula
		ArrayList<String> rows2 = data.getRows2(); // misc_form_label_id(s)
		Form1_18JExcelDataVO_2 voval = data.getVal();
		ArrayList<ArrayList<String>> station_vals = voval.getStation_vals();
		ArrayList<ArrayList<String>> misc_vals = voval.getMisc_vals();
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			String query1 = QueryConstants.QUERY_UPDATE_F1_18VAL_1;
			int int_stn_id = -1;
			int int_py_id = -1;
			prstmt1 = conn1.prepareStatement(query1);
			for (int i = 0; i < rows1.size(); i++) {
				int_stn_id = Integer.parseInt(rows1.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int_py_id = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(station_vals.get(i).get(j+1));
					
					/*
					update t_arrdf1_18_1 set val=?, updated_on=?, updated_by=? where stn_id=? and py_id=? and 
					form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, int_user_id);
					prstmt1.setInt(4, int_stn_id);
					prstmt1.setInt(5, int_py_id);
					prstmt1.setInt(6, int_form_id);
					prstmt1.setInt(7, int_ph_id);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			prstmt1.close();
			
			String query2 = QueryConstants.QUERY_UPDATE_F1_18VAL_2;
			int int_misc_form_label_id = -1;
			int_py_id = -1;
			prstmt1 = conn1.prepareStatement(query2);
			for (int i = 0; i < rows2.size(); i++) {
				int_misc_form_label_id = Integer.parseInt(rows2.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int_py_id = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(misc_vals.get(i).get(j+1));
					
					/*
					update t_arrdf1_18_2 set val=?, updated_on=?, updated_by=? where misc_form_label_id=? and py_id=? and 
					form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, int_user_id);
					prstmt1.setInt(4, int_misc_form_label_id);
					prstmt1.setInt(5, int_py_id);
					prstmt1.setInt(6, int_form_id);
					prstmt1.setInt(7, int_ph_id);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
		
	}
	
}
