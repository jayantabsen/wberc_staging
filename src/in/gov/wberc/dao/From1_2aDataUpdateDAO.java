package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.StationNUnitValVO;

public class From1_2aDataUpdateDAO {

	ArrayList<String> unitidAl;
	ArrayList<String> pyidAl;
	ArrayList<StationNUnitValVO> urvoAl;
	ArrayList<ArrayList<String>> unitPyidVal = null;
	String formid = "";
	
	public From1_2aDataUpdateDAO(ArrayList<String> unitidAl, ArrayList<String> pyidAl, 
			ArrayList<StationNUnitValVO> urvoAl, String formid) {
		
		this.unitidAl = unitidAl;
		this.pyidAl = pyidAl;
		this.urvoAl = urvoAl;
		this.formid = formid;
		
		unitPyidVal = new ArrayList<ArrayList<String>>();
		for ( StationNUnitValVO srvo : urvoAl ) {
			unitPyidVal.addAll(srvo.getUnitValForStation());
		}
		
	}
	
	public boolean updateF1_2aMatrixToDatabase() throws MyException, SQLException {
		
		String query1 = QueryConstants.QUERY_UPDATE_FORM12a_MATRIX; 
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		int formId = Integer.parseInt(formid);
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			int int_unitid = -1;
			int int_pyid = -1;
			for (int i = 0; i < unitidAl.size(); i++) {
				int_unitid = Integer.parseInt(unitidAl.get(i));
				for (int j = 0; j < pyidAl.size(); j++) {
					int_pyid = Integer.parseInt(pyidAl.get(j));
					double val = Double.parseDouble(unitPyidVal.get(i).get(j+1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_unitid);
					prstmt1.setInt(3, int_pyid);
					prstmt1.setInt(4, formId);
					prstmt1.setInt(5, int_pyid);
					System.out.println(prstmt1);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();

		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
                prstmt1.close();
                }
			
		}
		
		return true;
	}
	
}
