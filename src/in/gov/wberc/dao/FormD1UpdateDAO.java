//*** By JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.FormD1JExcelDataVO;

public class FormD1UpdateDAO {

	public boolean updateFD1Update(FormD1JExcelDataVO data)
			throws MyException, SQLException {
		
		int int_stn_id = Integer.parseInt(data.getStn_id());
		int int_py_id = Integer.parseInt(data.getPy_id());
		int int_form_id = Integer.parseInt(data.getForm_id());
		ArrayList<String> cols = data.getCols();
		ArrayList<String> rows_util_coal_field_id = data.getRows();
		ArrayList<ArrayList<String>> valAl = data.getVals();

		String query1 = QueryConstants.QUERY_GET_PHID;
		String query2 = QueryConstants.QUERY_UPDATE_FD1_VAL;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			
			// Getting ph_id form py_id
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			for (int i = 0; i < rows_util_coal_field_id.size(); i++) {
				int int_util_coal_field_id = Integer.parseInt(rows_util_coal_field_id.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int int_coalgrade_id = Integer.parseInt(cols.get(j));
					
					double val = Double.parseDouble(valAl.get(i).get(j + 1));
					/*
					update t_arrdfd1 set val=? where stn_id=? and py_id=? and util_coal_field_id=? and coalgrade_id=? 
					and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_stn_id);
					prstmt1.setInt(3, int_py_id);
					prstmt1.setInt(4, int_util_coal_field_id);
					prstmt1.setInt(5, int_coalgrade_id);
					prstmt1.setInt(6, int_form_id);
					prstmt1.setInt(7, int_ph_id);
					
					// System.out.println(prstmt1);
					
					prstmt1.addBatch();
				}
			}
			
			prstmt1.executeBatch();
			conn1.commit();
		
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
	}
	
}
