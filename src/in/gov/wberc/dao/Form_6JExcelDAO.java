/*** @author Najma on 31-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form_6_JExcelDataVO;

public class Form_6JExcelDAO {
	
	public Form_6_JExcelDataVO getF_6JExcelData( String ph_id, String form_id, String user_id) throws SQLException, MyException {
		
		Form_6_JExcelDataVO form6Ref = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		// Misc ids for groups
		ArrayList<String> miscids_for_incomes = null;
		ArrayList<String> miscids_for_expenses = null;
		ArrayList<String> miscids_for_assets = null;
		ArrayList<String> miscids_for_utilisation_add_fund = null;
		ArrayList<String> miscids_for_utilisation_capital_fund = null;

		ArrayList<String> miscnms_for_incomes = null;
		ArrayList<String> miscnms_for_expenses = null;
		ArrayList<String> miscnms_for_assets = null;
		ArrayList<String> miscnms_for_utilisation_add_fund = null;
		ArrayList<String> miscnms_for_utilisation_capital_fund = null;

		ArrayList<String> miscrow_for_incomes = null;
		ArrayList<String> miscrow_for_expenses = null;
		ArrayList<String> miscrow_for_assets = null;
		ArrayList<String> miscrow_for_utilisation_add_fund = null;
		ArrayList<String> miscrow_for_utilisation_capital_fund = null;
		
		ArrayList<ArrayList<String>> valAl_for_incomes = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_expenses = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_assets = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_utilisation_add_fund = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_utilisation_capital_fund = new ArrayList<ArrayList<String>>();

		
		ArrayList<String> col = null;
		
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		
		// Incomes
		String getF6MiscIds_for_incomes= QueryConstants.QUERY_GET_F6_INCOMES_MISCID_N_NAME;
		String getF6Incomes1Val = QueryConstants.QUERY_GET_F6_INCOMES_VAL;
		
		// Expenses
		String getF6MiscIds_for_expenses = QueryConstants.QUERY_GET_F6_EXPENSES_MISCID_N_NAME;
		String getF6ExpensesVal = QueryConstants.QUERY_GET_F6_EXPENSES_VAL;
		
		//Assets
		String getF6MiscIds_for_assets = QueryConstants.QUERY_GET_F6_ASSETS_MISCID_N_NAME;
		String getF6AssetsVal = QueryConstants.QUERY_GET_F6_ASSETS_VAL;
		
		// Utilisation Add Fund
		String getF6MiscIds_for_utilisation_add_fund = QueryConstants.QUERY_GET_F6_UTILISATION_ADD_MISCID_N_NAME;
		String getF6UtilisationAddFundVal = QueryConstants.QUERY_GET_F6_UTILISATION_ADD_VAL;
		
		// Utilisation Capital Fund
		String getF6MiscIds_for_utilisation_capital_fund = QueryConstants.QUERY_GET_F6_UTILISATION_CAPITAL_MISCID_N_NAME;
		String getF6UtilisationCapitalFundVal = QueryConstants.QUERY_GET_F6_UTILISATION_CAPITAL_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			// Fetch Petition year
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			prstmt1.setInt(1, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				col = new ArrayList<String>();
				while (rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			// Fetch For Incomes
			prstmt1 = conn1.prepareStatement(getF6MiscIds_for_incomes);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids_for_incomes = new ArrayList<String>();
				miscnms_for_incomes = new ArrayList<String>();
				while(rs1.next()) {
					miscids_for_incomes.add(rs1.getString("misc_form_label_id"));
					miscnms_for_incomes.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			valAl_for_incomes = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getF6Incomes1Val);
			
			for (int i = 0; i < miscids_for_incomes.size(); i++) {
				miscrow_for_incomes =  new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_incomes.get(i));
				miscrow_for_incomes.add(miscnms_for_incomes.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow_for_incomes.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_incomes.add(miscrow_for_incomes);
			}
			
			
			// Fetch For Expenses
			prstmt1 = conn1.prepareStatement(getF6MiscIds_for_expenses);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_expenses = new ArrayList<String>();
				miscnms_for_expenses = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_expenses.add(rs1.getString("misc_form_label_id"));
					miscnms_for_expenses.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_expenses = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getF6ExpensesVal);

			for (int i = 0; i < miscids_for_expenses.size(); i++) {
				miscrow_for_expenses = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_expenses.get(i));
				miscrow_for_expenses.add(miscnms_for_expenses.get(i));
				for (int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1 = prstmt1.executeQuery();

					if (rs1 != null) {
						while (rs1.next()) {
							miscrow_for_expenses.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_expenses.add(miscrow_for_expenses);
			}
			
			// Fetch For Assets
			prstmt1 = conn1.prepareStatement(getF6MiscIds_for_assets);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_assets = new ArrayList<String>();
				miscnms_for_assets = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_assets.add(rs1.getString("misc_form_label_id"));
					miscnms_for_assets.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_assets = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getF6AssetsVal);

			for (int i = 0; i < miscids_for_assets.size(); i++) {
				miscrow_for_assets = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_assets.get(i));
				miscrow_for_assets.add(miscnms_for_assets.get(i));
				for (int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1 = prstmt1.executeQuery();

					if (rs1 != null) {
						while (rs1.next()) {
							miscrow_for_assets.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_assets.add(miscrow_for_assets);
			}
			
			// Fetch For Utilisation Add Fund
			prstmt1 = conn1.prepareStatement(getF6MiscIds_for_utilisation_add_fund);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_utilisation_add_fund = new ArrayList<String>();
				miscnms_for_utilisation_add_fund = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_utilisation_add_fund.add(rs1.getString("misc_form_label_id"));
					miscnms_for_utilisation_add_fund.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_utilisation_add_fund = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getF6UtilisationAddFundVal);

			for (int i = 0; i < miscids_for_utilisation_add_fund.size(); i++) {
				miscrow_for_utilisation_add_fund = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_utilisation_add_fund.get(i));
				miscrow_for_utilisation_add_fund.add(miscnms_for_utilisation_add_fund.get(i));
				for (int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1 = prstmt1.executeQuery();

					if (rs1 != null) {
						while (rs1.next()) {
							miscrow_for_utilisation_add_fund.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_utilisation_add_fund.add(miscrow_for_utilisation_add_fund);
			}
			
			
			// Fetch For Utilisation Capital Fund
			prstmt1 = conn1.prepareStatement(getF6MiscIds_for_utilisation_capital_fund);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_utilisation_capital_fund = new ArrayList<String>();
				miscnms_for_utilisation_capital_fund = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_utilisation_capital_fund.add(rs1.getString("misc_form_label_id"));
					miscnms_for_utilisation_capital_fund.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_utilisation_capital_fund = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getF6UtilisationCapitalFundVal);

			for (int i = 0; i < miscids_for_utilisation_capital_fund.size(); i++) {
				miscrow_for_utilisation_capital_fund = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_utilisation_capital_fund.get(i));
				miscrow_for_utilisation_capital_fund.add(miscnms_for_utilisation_capital_fund.get(i));
				for (int j = 0; j < col.size(); j++) {
					int int_py_id = Integer.parseInt(col.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1 = prstmt1.executeQuery();

					if (rs1 != null) {
						while (rs1.next()) {
							miscrow_for_utilisation_capital_fund.add(rs1.getString("val"));
						}
					}
				}
				valAl_for_utilisation_capital_fund.add(miscrow_for_utilisation_capital_fund);
			}

			conn1.commit();
		}
		catch(SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		} finally {						
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		form6Ref = new Form_6_JExcelDataVO();
		form6Ref.setForm_id(form_id);
		form6Ref.setPh_id(ph_id);
		form6Ref.setUser_id(user_id);
		form6Ref.setCols(col);
		// Set Value for Incomes
		form6Ref.setMiscid_for_Incomes(miscids_for_incomes);
		form6Ref.setVals_for_Incomes(valAl_for_incomes);
		// Set Value for Expenses
		form6Ref.setMiscid_for_Expenses(miscids_for_expenses);
		form6Ref.setVals_for_Expenses(valAl_for_expenses);
		// Set Value for Assets
		form6Ref.setMiscid_for_Assets(miscids_for_assets);
		form6Ref.setVals_for_Assets(valAl_for_assets);
		// Set Value for Utilisation add fund
		form6Ref.setMiscid_for_UtilisationAddFund(miscids_for_utilisation_add_fund);
		form6Ref.setVals_for_UtilisationAddFund(valAl_for_utilisation_add_fund);
		// Set Value for Utilisation capital fund
		form6Ref.setMiscid_for_UtilisationCapitalFund(miscids_for_utilisation_capital_fund);
		form6Ref.setVals_for_UtilisationCapitalFund(valAl_for_utilisation_capital_fund);
		
		return form6Ref;
	}

	@SuppressWarnings("resource")
	public boolean updateF6(Form_6_JExcelDataVO data)
			throws MyException, SQLException {
		// Get petition years
		ArrayList<String> cols = data.getCols();
		// Get misc ids for groups
		ArrayList<String> miscids_for_incomes = data.getMiscid_for_Incomes();
		ArrayList<String> miscids_for_expenses = data.getMiscid_for_Expenses();
		ArrayList<String> miscids_for_assets = data.getMiscid_for_Assets();
		ArrayList<String> miscids_for_add_fund = data.getMiscid_for_UtilisationAddFund();
		ArrayList<String> miscids_for_capital_fund = data.getMiscid_for_UtilisationCapitalFund();


		// Get values for groups
		ArrayList<ArrayList<String>> values_for_incomes = data.getVals_for_Incomes();
		ArrayList<ArrayList<String>> values_for_expenses = data.getVals_for_Expenses();
		ArrayList<ArrayList<String>> values_for_assets = data.getVals_for_Assets();
		ArrayList<ArrayList<String>> values_for_add_fund = data.getVals_for_UtilisationAddFund();
		ArrayList<ArrayList<String>> values_for_capital_fund = data.getVals_for_UtilisationCapitalFund();

		// Get form id
		int formid = Integer.parseInt(data.getForm_id());
		// Get user id
		int userid = Integer.parseInt(data.getUser_id());
		// Get petition id
		int phid = Integer.parseInt(data.getPh_id());

		String updateQuery_f6_incomes = QueryConstants.QUERY_UPDATE_F6_INCOMES_VAL;
		String updateQuery_f6_expenses = QueryConstants.QUERY_UPDATE_F6_EXPENSES_VAL;
		String updateQuery_f6_assets = QueryConstants.QUERY_UPDATE_F6_ASSETS_VAL;
		String updateQuery_f6_add_fund = QueryConstants.QUERY_UPDATE_F6_UTILISATION_ADD_VAL;
		String updateQuery_f6_capital_fund = QueryConstants.QUERY_UPDATE_F6_UTILISATION_CAPITAL_VAL;

		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		// Get current time
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			// Update for incomes
			prstmt1 = conn1.prepareStatement(updateQuery_f6_incomes);
			for ( int i = 0; i < miscids_for_incomes.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_incomes.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_incomes.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			// Update for expenses
			prstmt1 = conn1.prepareStatement(updateQuery_f6_expenses);
			for (int i = 0; i < miscids_for_expenses.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_expenses.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_expenses.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			// Update for assets
			prstmt1 = conn1.prepareStatement(updateQuery_f6_assets);
			for (int i = 0; i < miscids_for_assets.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_assets.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_assets.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			// Update for utilisation add fund
			prstmt1 = conn1.prepareStatement(updateQuery_f6_add_fund);
			for (int i = 0; i < miscids_for_add_fund.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_add_fund.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_add_fund.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();

			// Update for utilisation capital fund
			prstmt1 = conn1.prepareStatement(updateQuery_f6_capital_fund);
			for (int i = 0; i < miscids_for_capital_fund.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_capital_fund.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_capital_fund.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}




}
