//** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_5SeasonUsageVO;
import in.gov.wberc.vo.From1_5JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_5JExcelDAO implements TransInterfaceVO {

	public  From1_5JExcelDataVO getF1_5JExcelData(String ph_id, String stn_id) throws MyException, SQLException {
		
		From1_5JExcelDataVO f15jxldvo1 = null;
		F1_5SeasonUsageVO f15suvo1 = null;

		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		ResultSet rs2=null;
	
		ArrayList<String> col = null;
		ArrayList<String> seasons = null;
		ArrayList<String> usageAl1 = null;
		ArrayList<String> usage = null;
		ArrayList<ArrayList<String>> usageAl2 = null;
		ArrayList<ArrayList<String>> usageList = new ArrayList<ArrayList<String>>();
		ArrayList<F1_5SeasonUsageVO> f15suvoAl1 = new ArrayList<F1_5SeasonUsageVO>();
		
		String getPetitionYears = QueryConstants.QUERY_GET_PYID;
		String getSeasonIds = QueryConstants.QUERY_GET_SEASONIDS;
		String getSeasonName = QueryConstants.QUERY_GET_SEASONNM;
		String getPeriodName = QueryConstants.QUERY_GET_PERIOD_NAME;
		String getValPerStation = QueryConstants.QUERY_GET_F1_5_DATA_VAL;
		String getPeriodPerSeason= QueryConstants.QUERY_GET_PERIOD_SEASON_WISE;
				
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYears);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(getSeasonIds);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String seasonid : seasons) {
				prstmt1 = conn1.prepareStatement(getPeriodPerSeason);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					usage = new ArrayList<String>();
					while(rs1.next()) {
						usage.add(rs1.getString("period_id"));
					}
				}
				usageList.add(usage);
				f15suvo1 = new F1_5SeasonUsageVO();
				prstmt1 = conn1.prepareStatement(getSeasonName);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					
					if(rs1.next()) {
						
						f15suvo1.setSeason(rs1.getString("season_desc"));
						usageAl2 = new ArrayList<ArrayList<String>>();
						ArrayList<String> periodPerSeason = null;
						prstmt1 = conn1.prepareStatement(getPeriodPerSeason);
						prstmt1.setInt(1, Integer.parseInt(seasonid));
						rs2=prstmt1.executeQuery();
						
						if(rs2!=null) {
							periodPerSeason = new ArrayList<String>();
							while(rs2.next()) {
								periodPerSeason.add(rs2.getString("period_id"));
							}
						}
						try {
							if (rs2 != null) {
								rs2.close();
							}
						} catch (SQLException e) {
							e.printStackTrace();
							throw new MyException();
						}

						for (String period_id : periodPerSeason) {
							
							prstmt1 = conn1.prepareStatement(getPeriodName);
							prstmt1.setInt(1, Integer.parseInt(period_id));
							usageAl1 = new ArrayList<String>();
							rs2=prstmt1.executeQuery();
							if ( rs2 != null ) {
								if(rs2.next()) {
									usageAl1.add(rs2.getString("period_desc"));
								}
							}
							
							try {
								if (rs2 != null) {
									rs2.close();
								}
							} catch (SQLException e) {
								e.printStackTrace();
								throw new MyException();

							}

							prstmt1 = conn1.prepareStatement(getValPerStation);
							for(int m=0;m<col.size();m++) {
								prstmt1.setInt(1, Integer.parseInt(stn_id));
								prstmt1.setInt(2, Integer.parseInt(period_id));
								prstmt1.setInt(3, Integer.parseInt(col.get(m)));
								
								rs2=prstmt1.executeQuery();
								if ( rs2 != null ) {
									while (rs2.next()) {
										usageAl1.add(rs2.getString("val"));
									}
								}
								try {
									if(rs2!=null) {
										rs2.close();
									}
								}catch(SQLException e) {
									e.printStackTrace();
									throw new MyException();
								}
								}
							
							usageAl2.add(usageAl1);
							}
						}
					f15suvo1.setUsage(usageAl2);
					f15suvoAl1.add(f15suvo1);
				}
			}
		conn1.commit();	
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
					conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}
		
		f15jxldvo1 = new From1_5JExcelDataVO();
		f15jxldvo1.setStationid(stn_id);
		f15jxldvo1.setCols(col);
		f15jxldvo1.setSeasons(seasons);
		f15jxldvo1.setUsage(usageList);
		f15jxldvo1.setVals(f15suvoAl1);
		
		return f15jxldvo1;
		
	}

}
