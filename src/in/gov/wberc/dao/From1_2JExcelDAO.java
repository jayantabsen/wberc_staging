package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.From1_1JExcelDataVO;
import in.gov.wberc.vo.StationRowVO;

public class From1_2JExcelDAO {

	public From1_1JExcelDataVO getF1_2JExcelData(String ph_id, String util_id) throws MyException, SQLException {

		From1_1JExcelDataVO f12jxlvo = null;
		
		PreparedStatement prstmt1 = null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
		
		ArrayList<String> cols = null;
		ArrayList<String> rows = null;
		ArrayList<StationRowVO> vals = null;
		StationRowVO stn_rowvo = null;
		ArrayList<ArrayList<String>> valForStationType = null;
		ArrayList<String> stn_row = null;
//		Queries
		String getPetitionYear = QueryConstants.QUERY_GET_PETTION_YEAR;
		String getStationTypes = QueryConstants.QUERY_GET_STATION_TYPES;
		String getStationTypesForCommission = QueryConstants.QUERY_GET_STATION_TYPES_FOR_UTIL0;
		String getStationsPerStationType = QueryConstants.QUERY_GET_STATIONS_PER_STATION_TYPE;
		String getStationsPerStationTypeForCommision = QueryConstants.QUERY_GET_STATIONS_PER_STATION_TYPE_FOR_UTIL0;

		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					cols.add(rs1.getString("py_id"));
				}
			}
			
			rows = new ArrayList<String>();
			vals = new ArrayList<StationRowVO>();
			
			int int_utilid = Integer.parseInt(util_id);
			if (int_utilid != 0) {
				prstmt1 = conn1.prepareStatement(getStationTypes);
				prstmt1.setInt(1, int_utilid);
			} else {
				prstmt1 = conn1.prepareStatement(getStationTypesForCommission);

			}
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				while(rs1.next()) {
					stn_rowvo = new StationRowVO();
					int stypeid = Integer.parseInt(rs1.getString("stype_id"));
					stn_rowvo.setStation_type(rs1.getString("stype_desc"));
					
					
					if (int_utilid != 0) {
						prstmt1 = conn1.prepareStatement(getStationsPerStationType);
						prstmt1.setInt(2, int_utilid);

					} else {
						prstmt1 = conn1.prepareStatement(getStationsPerStationTypeForCommision);
					}
					
					prstmt1.setInt(1, stypeid);
					rs2=prstmt1.executeQuery();
					
					if(rs2!=null) {
						valForStationType = new ArrayList<ArrayList<String>>();
						while(rs2.next()) {
							stn_row = new ArrayList<String>();
							stn_row.add(rs2.getString("stn_desc"));
							String stnid = rs2.getString("stn_id");
							rows.add(stnid);
							int int_stnid = Integer.parseInt(stnid);
							String getValPerStation = QueryConstants.QUERY_GET_F1_2_DATA_VAL;
							
							for ( int i = 0; i<cols.size(); i++ ) {
								prstmt1 = conn1.prepareStatement(getValPerStation);
								prstmt1.setInt(1, int_stnid);
								prstmt1.setInt(2, Integer.parseInt(cols.get(i)));
								rs3=prstmt1.executeQuery();
								if(rs3!=null) {
									if(rs3.next()) {
										stn_row.add(rs3.getString("val"));
									}
								}
							}
							
							valForStationType.add(stn_row);
						}
						stn_rowvo.setValForStationType(valForStationType);
					}
					vals.add(stn_rowvo);
				}
			}
			
			f12jxlvo = new From1_1JExcelDataVO();
			f12jxlvo.setRows(rows);
			f12jxlvo.setCols(cols);
			f12jxlvo.setVals(vals);
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(conn1!=null) {
				conn1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
			if(rs2!=null) {
				rs2.close();
			}
			if(rs3!=null) {
				rs3.close();
			}
		}
		
		return f12jxlvo;
	}
	
}
