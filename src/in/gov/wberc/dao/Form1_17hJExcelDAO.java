//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17hJExcelDataVO;

public class Form1_17hJExcelDAO {
	
	public Form1_17hJExcelDataVO getF1_17hJExcelData( String form_id, String py_id, String user_id ) 
			throws MyException, SQLException {
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PHID;
		String query2 = QueryConstants.QUERY_GET_F1_17hVAL;
		
		Form1_17hJExcelDataVO f117hxldvo = new Form1_17hJExcelDataVO();
		f117hxldvo.setForm_id(form_id);
		f117hxldvo.setPy_id(py_id);
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(py_id);
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			// Getting data
			prstmt1 = conn1.prepareStatement(query2);
			
			/*
			select exp_own, exp_con, basic_pay_own, basic_pay_con, dear_allowance_own, dear_allowance_con, other_allowance_own, 
			other_allowance_con, gratuity_own, gratuity_con, pf_contrib_own, pf_contrib_con, pension_contrib_own, pension_contrib_con, 
			bonus_own, bonus_con, ltc_own, ltc_con, leave_encashment_own, leave_encashment_con, welfare_expend_own, welfare_expend_con, other_own, 
			other_con, shortfall_pf_own, shortfall_pf_con, emp_incentive_own, emp_incentive_con, personnel_no_own, personnel_no_con 
			from t_arrdf1_17h where py_id=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1.setInt(1, int_py_id);
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, int_ph_id);
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					f117hxldvo.setPy_id(py_id);
					f117hxldvo.setForm_id(form_id);
					f117hxldvo.setUser_id(user_id);
					
					f117hxldvo.setExp_own(rs1.getString("exp_own"));
					f117hxldvo.setExp_con(rs1.getString("exp_con"));
					f117hxldvo.setBasic_pay_own(rs1.getString("basic_pay_own"));
					f117hxldvo.setBasic_pay_con(rs1.getString("basic_pay_con"));
					f117hxldvo.setDear_allowance_own(rs1.getString("dear_allowance_own"));
					f117hxldvo.setDear_allowance_con(rs1.getString("dear_allowance_con"));
					f117hxldvo.setOther_allowance_own(rs1.getString("other_allowance_own"));
					f117hxldvo.setOther_allowance_con(rs1.getString("other_allowance_con"));
					f117hxldvo.setGratuity_own(rs1.getString("gratuity_own"));
					f117hxldvo.setGratuity_con(rs1.getString("gratuity_con"));
					f117hxldvo.setPf_contrib_own(rs1.getString("pf_contrib_own"));
					f117hxldvo.setPf_contrib_con(rs1.getString("pf_contrib_con"));
					f117hxldvo.setPension_contrib_own(rs1.getString("pension_contrib_own"));
					f117hxldvo.setPension_contrib_con(rs1.getString("pension_contrib_con"));
					f117hxldvo.setBonus_own(rs1.getString("bonus_own"));
					f117hxldvo.setBonus_con(rs1.getString("bonus_con"));
					f117hxldvo.setLtc_own(rs1.getString("ltc_own"));
					f117hxldvo.setLtc_con(rs1.getString("ltc_con"));
					f117hxldvo.setLeave_encashment_own(rs1.getString("leave_encashment_own"));
					f117hxldvo.setLeave_encashment_con(rs1.getString("leave_encashment_con"));
					f117hxldvo.setWelfare_expend_own(rs1.getString("welfare_expend_own"));
					f117hxldvo.setWelfare_expend_con(rs1.getString("welfare_expend_con"));
					f117hxldvo.setOther_own(rs1.getString("other_own"));
					f117hxldvo.setOther_con(rs1.getString("other_con"));
					f117hxldvo.setShortfall_pf_own(rs1.getString("shortfall_pf_own"));
					f117hxldvo.setShortfall_pf_con(rs1.getString("shortfall_pf_con"));
					f117hxldvo.setEmp_incentive_own(rs1.getString("emp_incentive_own"));
					f117hxldvo.setEmp_incentive_con(rs1.getString("emp_incentive_con"));
					f117hxldvo.setPersonnel_no_own(rs1.getString("personnel_no_own"));
					f117hxldvo.setPersonnel_no_con(rs1.getString("personnel_no_con"));
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
	
		return f117hxldvo;
	}
	
	public boolean updateF1_17hMatrixToDatabase(Form1_17hJExcelDataVO data)
			throws MyException, SQLException {
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			String query1 = QueryConstants.QUERY_GET_PHID;
			String query2 = QueryConstants.QUERY_F1_17h_UPDATE;
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(data.getPy_id());
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			/*
			update t_arrdf1_17h set exp_own=?, exp_con=?, basic_pay_own=?, basic_pay_con=?, dear_allowance_own=1?, dear_allowance_con=?, 
			other_allowance_own=?, other_allowance_con=?, gratuity_own=?, gratuity_con=?, pf_contrib_own=?, pf_contrib_con=?, 
			pension_contrib_own=?, pension_contrib_con=?, bonus_own=?, bonus_con=?, ltc_own=?, ltc_con=?, leave_encashment_own=?, 
			leave_encashment_con=?, welfare_expend_own=?, welfare_expend_con=?, other_own=?, other_con=?, shortfall_pf_own=?, 
			shortfall_pf_con=?, emp_incentive_own=?, emp_incentive_con=?, personnel_no_own=?, personnel_no_con=?, updated_on=?, 
			updated_by=? where py_id=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setDouble(1, Double.parseDouble(data.getExp_own()));
			prstmt1.setDouble(2, Double.parseDouble(data.getExp_con()));
			prstmt1.setDouble(3, Double.parseDouble(data.getBasic_pay_own()));
			prstmt1.setDouble(4, Double.parseDouble(data.getBasic_pay_con()));
			prstmt1.setDouble(5, Double.parseDouble(data.getDear_allowance_own()));
			prstmt1.setDouble(6, Double.parseDouble(data.getDear_allowance_con()));
			prstmt1.setDouble(7, Double.parseDouble(data.getOther_allowance_own()));
			prstmt1.setDouble(8, Double.parseDouble(data.getOther_allowance_con()));
			prstmt1.setDouble(9, Double.parseDouble(data.getGratuity_own()));
			prstmt1.setDouble(10, Double.parseDouble(data.getGratuity_con()));
			prstmt1.setDouble(11, Double.parseDouble(data.getPf_contrib_own()));
			prstmt1.setDouble(12, Double.parseDouble(data.getPf_contrib_con()));
			prstmt1.setDouble(13, Double.parseDouble(data.getPension_contrib_own()));
			prstmt1.setDouble(14, Double.parseDouble(data.getPension_contrib_con()));
			prstmt1.setDouble(15, Double.parseDouble(data.getBonus_own()));
			prstmt1.setDouble(16, Double.parseDouble(data.getBonus_con()));
			prstmt1.setDouble(17, Double.parseDouble(data.getLtc_own()));
			prstmt1.setDouble(18, Double.parseDouble(data.getLtc_con()));
			prstmt1.setDouble(19, Double.parseDouble(data.getLeave_encashment_own()));
			prstmt1.setDouble(20, Double.parseDouble(data.getLeave_encashment_con()));
			prstmt1.setDouble(21, Double.parseDouble(data.getWelfare_expend_own()));
			prstmt1.setDouble(22, Double.parseDouble(data.getWelfare_expend_con()));
			prstmt1.setDouble(23, Double.parseDouble(data.getOther_own()));
			prstmt1.setDouble(24, Double.parseDouble(data.getOther_con()));
			prstmt1.setDouble(25, Double.parseDouble(data.getShortfall_pf_own()));
			prstmt1.setDouble(26, Double.parseDouble(data.getShortfall_pf_con()));
			prstmt1.setDouble(27, Double.parseDouble(data.getEmp_incentive_own()));
			prstmt1.setDouble(28, Double.parseDouble(data.getEmp_incentive_con()));
			prstmt1.setDouble(29, Double.parseDouble(data.getPersonnel_no_own()));
			prstmt1.setDouble(30, Double.parseDouble(data.getPersonnel_no_con()));
			prstmt1.setTimestamp(31, timestamp);
			prstmt1.setInt(32, Integer.parseInt(data.getUser_id()));
			prstmt1.setInt(33, int_py_id);
			prstmt1.setInt(34, Integer.parseInt(data.getForm_id()));
			prstmt1.setInt(35, int_ph_id);
			
			prstmt1.executeUpdate();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
		
	}
	
}
