/*12.01.2018 Priyanka..@Created*/
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.FormVo;
import in.gov.wberc.vo.PititionHeaderVo;
import in.gov.wberc.vo.UserVO;

public class AnnexureListDao {
	
	public PititionHeaderVo getAnnexureList (String phid, String utilid) throws MyException, SQLException {
		
		String annexureFormList = QueryConstants.ANNEXURE_FORM_LIST_BY_PITITION_HEADER_ID;
		PititionHeaderVo pitition = new PititionHeaderVo();
		
		PreparedStatement prstmt1=null;	
		Connection conn1=null;
		ResultSet rs1=null;
		
		List<FormVo> formList = new ArrayList<FormVo>();
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(annexureFormList);
			prstmt1.setInt(1, Integer.parseInt(phid));
			prstmt1.setInt(2, Integer.parseInt(utilid));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				while(rs1.next()) {
					FormVo form = new FormVo();
					form.setFormid(rs1.getString("form_id"));
					form.setForm_code(rs1.getString("form_code"));
					form.setForm_desc(rs1.getString("form_desc"));
					form.setUnit_of_measurement(rs1.getString("unit_of_measurement"));
					form.setAnnexure_id(rs1.getString("annexure_id"));
					form.setLayout_code(rs1.getString("layout_code"));//19.01.2018
					form.setAnnexure_code(rs1.getString("annexure_code"));//19.01.2018
					form.setAnnexure_desc(rs1.getString("annexure_desc"));//19.01.2018
					formList.add(form);
				}
			}
			conn1.commit();
		}catch(Exception e) {
		    e.printStackTrace();
		    conn1.rollback();
		}finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		pitition.setPhid(phid);
		pitition.setUtilid(utilid);
		pitition.setFormList(formList);
		return pitition;
	}

}
