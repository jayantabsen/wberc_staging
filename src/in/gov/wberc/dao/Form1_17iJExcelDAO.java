//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17hJExcelDataVO;
import in.gov.wberc.vo.Form1_17iJExcelDataVO;

public class Form1_17iJExcelDAO {
	
	public Form1_17iJExcelDataVO getF1_17iJExcelData( String form_id, String py_id, String user_id ) 
			throws MyException, SQLException {
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PHID;
		String query2 = QueryConstants.QUERY_GET_F1_17iVAL;
		
		Form1_17iJExcelDataVO f117ixldvo = new Form1_17iJExcelDataVO();
		f117ixldvo.setForm_id(form_id);
		f117ixldvo.setPy_id(py_id);
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(py_id);
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			// Getting data
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, int_py_id);
			prstmt1.setInt(2, Integer.parseInt(form_id));
			prstmt1.setInt(3, int_ph_id);
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					/*
					select exp, basic_pay, dear_allowance, other_allowance, gratuity, pf_contrib, pension_contrib, bonus, ltc, 
					leave_encashment, welfare_expend, other, shortfall_pf, emp_incentive, personnel_no from t_arrdf1_17i where py_id=? 
					and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
					*/
					f117ixldvo.setPy_id(py_id);
					f117ixldvo.setForm_id(form_id);
					f117ixldvo.setUser_id(user_id);
					
					f117ixldvo.setExp(rs1.getString("exp"));
					f117ixldvo.setBasic_pay(rs1.getString("basic_pay"));
					f117ixldvo.setDear_allowance(rs1.getString("dear_allowance"));
					f117ixldvo.setOther_allowance(rs1.getString("other_allowance"));
					f117ixldvo.setGratuity(rs1.getString("gratuity"));
					f117ixldvo.setPf_contrib(rs1.getString("pf_contrib"));
					f117ixldvo.setPension_contrib(rs1.getString("pension_contrib"));
					f117ixldvo.setBonus(rs1.getString("bonus"));
					f117ixldvo.setLtc(rs1.getString("ltc"));
					f117ixldvo.setLeave_encashment(rs1.getString("leave_encashment"));
					f117ixldvo.setWelfare_expend(rs1.getString("welfare_expend"));
					f117ixldvo.setOther(rs1.getString("other"));
					f117ixldvo.setShortfall_pf(rs1.getString("shortfall_pf"));
					f117ixldvo.setEmp_incentive(rs1.getString("emp_incentive"));
					f117ixldvo.setPersonnel_no(rs1.getString("personnel_no"));
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
	
		return f117ixldvo;
	}
	
	public boolean updateF1_17iMatrixToDatabase(Form1_17iJExcelDataVO data)
			throws MyException, SQLException {
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			String query1 = QueryConstants.QUERY_GET_PHID;
			String query2 = QueryConstants.QUERY_F1_17i_UPDATE;
			
			// Getting ph_id form py_id
			int int_py_id = Integer.parseInt(data.getPy_id());
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			/*
			update t_arrdf1_17i set exp=?, basic_pay=?, dear_allowance=?, other_allowance=?, gratuity=?, pf_contrib=?, 
			pension_contrib=?, bonus=?, ltc=?, leave_encashment=?, welfare_expend=?, other=?, shortfall_pf=?, emp_incentive=?, 
			personnel_no=?, updated_on=?, updated_by=? where py_id=? and form_instance_id=(select form_instance_id from 
			t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setDouble(1, Double.parseDouble(data.getExp()));
			prstmt1.setDouble(2, Double.parseDouble(data.getBasic_pay()));
			prstmt1.setDouble(3, Double.parseDouble(data.getDear_allowance()));
			prstmt1.setDouble(4, Double.parseDouble(data.getOther_allowance()));
			prstmt1.setDouble(5, Double.parseDouble(data.getGratuity()));
			prstmt1.setDouble(6, Double.parseDouble(data.getPf_contrib()));
			prstmt1.setDouble(7, Double.parseDouble(data.getPension_contrib()));
			prstmt1.setDouble(8, Double.parseDouble(data.getBonus()));
			prstmt1.setDouble(9, Double.parseDouble(data.getLtc()));
			prstmt1.setDouble(10, Double.parseDouble(data.getLeave_encashment()));
			prstmt1.setDouble(11, Double.parseDouble(data.getWelfare_expend()));
			prstmt1.setDouble(12, Double.parseDouble(data.getOther()));
			prstmt1.setDouble(13, Double.parseDouble(data.getShortfall_pf()));
			prstmt1.setDouble(14, Double.parseDouble(data.getEmp_incentive()));
			prstmt1.setDouble(15, Double.parseDouble(data.getPersonnel_no()));
			prstmt1.setTimestamp(16, timestamp);
			prstmt1.setInt(17, Integer.parseInt(data.getUser_id()));
			prstmt1.setInt(18, int_py_id);
			prstmt1.setInt(19, Integer.parseInt(data.getForm_id()));
			prstmt1.setInt(20, int_ph_id);
			
			prstmt1.executeUpdate();
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
		
	}
	
}
