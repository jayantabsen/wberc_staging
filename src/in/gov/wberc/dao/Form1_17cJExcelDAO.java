//*** By Najma
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17JExcelDataVO;

public class Form1_17cJExcelDAO {
	
	@SuppressWarnings("resource")
	public Form1_17JExcelDataVO getF1_17cJExcelData(String ph_id,String form_id) 
			throws MyException, SQLException {
		
		Form1_17JExcelDataVO f117cjxldvo1 = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> col = null;
		ArrayList<String> miscids = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();
		
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		String getMiscIds = QueryConstants.QUERY_GET_MISCIDS;
		String getMiscName = QueryConstants.QUERY_GET_MISC_NAME;
		String getValPerMisc = QueryConstants.QUERY_GET_F1_17c_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(getPetitionYear);		
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(getMiscIds);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids = new ArrayList<String>();
				while(rs1.next()) {
					miscids.add(rs1.getString("misc_form_label_id"));
				}
			}
			
			for (String miscid : miscids) {
				prstmt1 = conn1.prepareStatement(getMiscName);
				prstmt1.setInt(1, Integer.parseInt(miscid));
				rs1=prstmt1.executeQuery();
				
				ArrayList<String> val = new ArrayList<String>();

				if(rs1!=null) {
					while(rs1.next()) {
						val.add(rs1.getString("misc_form_label_desc"));
					}
				}
				
				for(String pid : col) {
					prstmt1 = conn1.prepareStatement(getValPerMisc);
					prstmt1.setInt(1, Integer.parseInt(pid));
					prstmt1.setInt(2, Integer.parseInt(miscid));
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							val.add(rs1.getString("val"));
						}
					}
				}
				
				valAl.add(val);
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(prstmt1!=null) {
				prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
		}
	
		f117cjxldvo1 = new Form1_17JExcelDataVO();
		f117cjxldvo1.setFormid(form_id);
		f117cjxldvo1.setCols(col);	
		f117cjxldvo1.setMiscids(miscids);
		f117cjxldvo1.setVals(valAl);

		return f117cjxldvo1;
	}
}
