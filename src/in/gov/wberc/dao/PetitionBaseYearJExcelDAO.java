package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.PetitionBaseYearJExcelDataVO;

public class PetitionBaseYearJExcelDAO {

	public PetitionBaseYearJExcelDataVO getPetitionBaseYearJExcelData() throws MyException, SQLException {
		
		PetitionBaseYearJExcelDataVO pbyjxldvo = null;
		
		Connection conn1 = null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		ArrayList<String> ph = null;
		ArrayList<ArrayList<String>> phAl = null;
		String query1 = QueryConstants.QUERY_GET_PETITION_BASE_YEARS_FOR_COMBO;
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				phAl = new ArrayList<ArrayList<String>>();
				while(rs1.next()) {
					ph = new ArrayList<String>();
					ph.add(rs1.getString("pby_id"));
					ph.add(rs1.getString("base_year"));
					phAl.add(ph);
				}
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
				conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
			
		}
		pbyjxldvo = new PetitionBaseYearJExcelDataVO();
		pbyjxldvo.setBase_year(phAl);
		
		return pbyjxldvo;
		
	}
	
}
