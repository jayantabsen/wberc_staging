//*** By Najma Updated By JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17JExcelDataVO;

public class Form1_6cJExcelDAO {
	
	public Form1_17JExcelDataVO getF1_6cJExcelData(String py_id, String util_id, String form_id) 
			throws MyException, SQLException {
		
		Form1_17JExcelDataVO f16cjxldvo1 = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> col = null;
		ArrayList<String> miscids = null;
		ArrayList<String> miscnms = null;
		ArrayList<String> miscrow = null;
		ArrayList<ArrayList<String>> valAl = null;
		
		String query1 = QueryConstants.QUERY_GET_MONTH;
		String query2 = QueryConstants.QUERY_GET_MISCIDS;
		String query3 = QueryConstants.QUERY_GET_F1_6c_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("month_id"));
				}
			}

			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				miscids = new ArrayList<String>();
				miscnms = new ArrayList<String>();
				while(rs1.next()) {
					miscids.add(rs1.getString("misc_form_label_id"));
					miscnms.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			// SELECT val FROM t_arrdf1_6c where py_id=? and month_id=? and misc_form_label_id=?
			int int_py_id = Integer.parseInt(py_id);
			valAl = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(query3);
			for (int i = 0; i < miscids.size(); i++) {
				miscrow =  new ArrayList<String>();
				int int_misc_id = Integer.parseInt(miscids.get(i));
				miscrow.add(miscnms.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_month_id = Integer.parseInt(col.get(j));
					
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_month_id);
					prstmt1.setInt(3, int_misc_id);
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							miscrow.add(rs1.getString("val"));
						}
					}
				}
				valAl.add(miscrow);
			}
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		f16cjxldvo1 = new Form1_17JExcelDataVO();
		f16cjxldvo1.setPy_id(py_id);
		f16cjxldvo1.setCols(col);	
		f16cjxldvo1.setMiscids(miscids);
		f16cjxldvo1.setVals(valAl);

		return f16cjxldvo1;
	}
}
