package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.StationRowVO;

public class Form1_1DataUpdateDAO {

	ArrayList<String> stationidAl;
	ArrayList<String> pyidAl;
	ArrayList<StationRowVO> srvoAl;
	ArrayList<ArrayList<String>> stnPyidVal = null;
	
	public Form1_1DataUpdateDAO(ArrayList<String> stationidAl, ArrayList<String> pyidAl, 
			ArrayList<StationRowVO> srvoAl) {
		this.stationidAl = stationidAl;
		this.pyidAl = pyidAl;
		this.srvoAl = srvoAl;
		
		stnPyidVal = new ArrayList<ArrayList<String>>();
		for ( StationRowVO srvo : srvoAl ) {
			stnPyidVal.addAll(srvo.getValForStationType());
		}
	}
	
	public boolean updateF1_1MatrixToDatabase() throws MyException {
		
		String query1 = QueryConstants.QUERY_UPDATE_FORM11_MATRIX;
		
		Connection conn1=null;
		
		PreparedStatement prstmt1 = null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			int int_stationid = -1;
			int int_pyid = -1;
			for (int i = 0; i < stationidAl.size(); i++) {
				int_stationid = Integer.parseInt(stationidAl.get(i));
				for (int j = 0; j < pyidAl.size(); j++) {
					int_pyid = Integer.parseInt(pyidAl.get(j));
					double val = Double.parseDouble(stnPyidVal.get(i).get(j+1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_stationid);
					prstmt1.setInt(3, int_pyid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw new MyException();
			}
			throw new MyException();
		} finally {
				try {
					if(prstmt1!=null) {
		                prstmt1.close();
		                }
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
		}
		
		return true;
		
	}
	
}
