

//*** By Najma
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17JExcelDataVO;


public class Form1_17cDataUpdateDAO {

	public boolean updateF1_17cMatrixToDatabase(Form1_17JExcelDataVO data)
			throws MyException, SQLException {
		
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();
		String formid = data.getFormid();

		
		// Data collection segment
		ArrayList<ArrayList<String>>[] ar_of_alal = new ArrayList[values.size()];
		int indx = 0;
		for (int i=0;i<ar_of_alal.length;i++ ) {
			ArrayList<ArrayList<String>> alal = ar_of_alal[i];
			ar_of_alal[indx++] = alal;
		}

		String query1 = QueryConstants.QUERY_UPDATE_F1_17c;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		//Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i < miscids.size(); i++) {
				int miscid = Integer.parseInt(miscids.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values.get(i).get(j + 1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, miscid);
					prstmt1.setInt(3, pyid);
					prstmt1.setInt(4, Integer.parseInt(formid));
					prstmt1.setInt(5, pyid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {

			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(conn1!=null) {
				conn1.close();
			}
		}
		return true;
	}
	
}
