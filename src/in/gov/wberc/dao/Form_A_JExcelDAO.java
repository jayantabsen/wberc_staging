/*** @author Najma on 25-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form_A_JExcelDataVO;

public class Form_A_JExcelDAO {

	public Form_A_JExcelDataVO getF_A_JExcelData(String stn_id, String py_id, String form_id, String user_id) throws SQLException, MyException, ParseException {
		
		Form_A_JExcelDataVO formAref = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> value = null;
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		
		String Query_getFormAval= QueryConstants.QUERY_GET_F_A_VAL;
		
		try{
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(Query_getFormAval);
			prstmt1.setInt(1, Integer.parseInt(stn_id));
			prstmt1.setInt(2, Integer.parseInt(py_id));
			rs1=prstmt1.executeQuery();

			if(rs1!=null) {
				values = new ArrayList<ArrayList<String>>();
				while(rs1.next()) {
					value = new ArrayList<String>();
					
					value.add(rs1.getString("unit_no"));
					value.add(rs1.getString("outage_from"));
					value.add(rs1.getString("outage_to"));
					value.add(rs1.getString("nature"));
					value.add(rs1.getString("duration"));
					value.add(rs1.getString("summary"));
					value.add(rs1.getString("planned_maintenance"));
					value.add(rs1.getString("scheduled_maintenance"));
					value.add(rs1.getString("actual_maintenance"));
					value.add(rs1.getString("remarks"));
					values.add(value);
				}
			}
		
			conn1.commit();
		}
		catch(SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		}
		finally {						
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		formAref = new Form_A_JExcelDataVO();
		formAref.setStn_id(stn_id);
		formAref.setPy_id(py_id);
		formAref.setForm_id(form_id);
		formAref.setUser_id(user_id);
		formAref.setVals(values);
		return formAref;
		
	}
	
	
	@SuppressWarnings("resource")
	public boolean saveF_A_JExcelData(Form_A_JExcelDataVO data) throws SQLException, MyException, ParseException {
		
		int stn_id = Integer.parseInt(data.getStn_id());
		int py_id = Integer.parseInt(data.getPy_id());
		int form_id = Integer.parseInt(data.getForm_id());
		int user_id = Integer.parseInt(data.getUser_id());
		ArrayList<ArrayList<String>> values = data.getVals();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String Query_saveFormAval= QueryConstants.QUERY_INSERT_t_arrdfa;
		String Query_deleteFormAval= QueryConstants.QUERY_DELETE_F_A_VAL;
		String getPhid = QueryConstants.QUERY_GET_PHID;
		String getFormInsId = QueryConstants.QUERY_GET_FORM_INSTANCE_ID;
		
		try{
			conn1=DBUtil.getConnection();
			
			//Get Ph id here	
			int ph_id = 0;
			prstmt1 = conn1.prepareStatement(getPhid);
			prstmt1.setInt(1, py_id);
			rs1 = prstmt1.executeQuery();
			if(rs1 != null) {
				while(rs1.next()) {
					ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			// Get Form Instance Id
			int form_instance_id = 0;
			prstmt1 = conn1.prepareStatement(getFormInsId);
			prstmt1.setInt(1, form_id);
			prstmt1.setInt(2, ph_id);
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					form_instance_id = Integer.parseInt(rs1.getString("form_instance_id"));
				}
			}
			
			// Delete Previous Value Here
			prstmt1 = conn1.prepareStatement(Query_deleteFormAval);
			prstmt1.setInt(1, stn_id);
			prstmt1.setInt(2, py_id);
			prstmt1.setInt(3, form_id);
			prstmt1.setInt(4, py_id);
			prstmt1.executeUpdate();
			
			// Save Current Value Here
			SimpleDateFormat dateFormatted = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String dt = null;
			Date dt_date = null;
			Calendar cal_dt = Calendar.getInstance();
			Timestamp dtInTimeStamp = null;
			
			prstmt1 = conn1.prepareStatement(Query_saveFormAval);
			for (int i = 0; i < values.size(); i++) {
				ArrayList<String> row = values.get(i);
				
				// outage_from date
				dt = row.get(1);
				dt_date = dateFormatted.parse(dt);
				cal_dt.setTime(dt_date);
				dtInTimeStamp = new Timestamp(cal_dt.getTimeInMillis());
				prstmt1.setTimestamp(5, dtInTimeStamp);
				// outage_to date
				dt = row.get(2);
				dt_date = dateFormatted.parse(dt);
				cal_dt.setTime(dt_date);
				dtInTimeStamp = new Timestamp(cal_dt.getTimeInMillis());
				prstmt1.setTimestamp(6, dtInTimeStamp);

				prstmt1.setInt(1, form_instance_id);
				prstmt1.setInt(2, py_id);
				prstmt1.setInt(3, stn_id);
				prstmt1.setString(4, row.get(0));
				prstmt1.setString(7, row.get(3));
				prstmt1.setString(8, row.get(4));
				prstmt1.setString(9, row.get(5));
				prstmt1.setString(10, row.get(6));
				prstmt1.setString(11, row.get(7));
				prstmt1.setString(12, row.get(8));
				prstmt1.setString(13, row.get(9));
				prstmt1.setTimestamp(14, timestamp);
				prstmt1.setInt(15, user_id);
				prstmt1.setTimestamp(16, timestamp);
				prstmt1.setInt(17, user_id);
				
				prstmt1.executeUpdate();
			}
			conn1.commit();
		}
		catch(SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		}
		finally {						
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
		
	}
	
	
}
