package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_4aJExcelDataVO;

public class Form1_4aDataUpdateDAO {

	public boolean updateF1_4aMatrixToDatabase(Form1_4aJExcelDataVO data)
			throws MyException, SQLException {
		
		String str_stationid = data.getStationid();
		ArrayList<String> cols = data.getCols();
		ArrayList<String> seasons = data.getSeasons();
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		String query1 = QueryConstants.QUERY_UPDATE_F14a;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		//Database Update Segment
		int int_stationid = Integer.parseInt(str_stationid);
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i<vals.size(); i++ ) {
				int seasonid = Integer.parseInt(seasons.get(i));
				ArrayList<String> ref = vals.get(i);
				for ( int j = 0; j<ref.size(); j++) {
					if ( j == 0 ) {
						continue;
					}
					int pyid = Integer.parseInt(cols.get(j-1));
					double val = Double.parseDouble(ref.get(j));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_stationid);
					prstmt1.setInt(3, seasonid);
					prstmt1.setInt(4, pyid);
					prstmt1.addBatch();
					
				}
				
			}
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
				conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
			}
		return true;
		
	}
	
}
