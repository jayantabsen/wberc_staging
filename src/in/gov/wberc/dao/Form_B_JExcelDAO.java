//01.02.2018 Priyanka
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form_B_JExcelDataVO;

public class Form_B_JExcelDAO {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Form_B_JExcelDataVO getF_B_JExcelData(String py_id, String user_id,String ph_id,String form_id) throws SQLException, MyException {
		
		Form_B_JExcelDataVO formB_vo = new Form_B_JExcelDataVO();
		
		Connection conn=null;
		PreparedStatement prstmt = null;	
		ResultSet rs=null;
		
		ArrayList<String> gen_list =  new ArrayList<String>();
		ArrayList<String> trans_list =  new ArrayList<String>();
		ArrayList<String> dist_list =  new ArrayList<String>();
		ArrayList<String> meter_list =  new ArrayList<String>();
		ArrayList<String> other_list =  new ArrayList<String>();
		
		ArrayList<String> colIds = new ArrayList<String>();
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		
		String Query_getFormBval= QueryConstants.QUERY_GET_F_B_VAL;
		
		try {
			conn=DBUtil.getConnection();
			prstmt = conn.prepareStatement(Query_getFormBval);
			prstmt.setInt(1, Integer.parseInt(py_id));
			rs=prstmt.executeQuery();
			
			if(rs!=null) {
				int count = 4;
				while(rs.next()) {
					if(rs.getInt("misc_form_label_id")>0) {
						colIds.add("m_"+rs.getString("misc_form_label_id"));
						gen_list.add(rs.getString("cost_gen_assets"));
						trans_list.add(rs.getString("cost_trans_assets"));
						dist_list.add(rs.getString("cost_dist_assets"));
						meter_list.add(rs.getString("cost_meter_assets"));
						other_list.add(rs.getString("cost_other_assets"));
					}else {
						colIds.add(count,"d_"+rs.getString("depreciation_rate_id"));
						gen_list.add(count, rs.getString("cost_gen_assets"));
						trans_list.add(count, rs.getString("cost_trans_assets"));
						dist_list.add(count, rs.getString("cost_dist_assets"));
						meter_list.add(count, rs.getString("cost_meter_assets"));
						other_list.add(count, rs.getString("cost_other_assets"));
						count++;
					}
				}
				values.add(gen_list);
				values.add(trans_list);
				values.add(dist_list);
				values.add(meter_list);
				values.add(other_list);
				
				formB_vo.setPh_id(ph_id);
				formB_vo.setPy_id(py_id);
				formB_vo.setUser_id(user_id);
				formB_vo.setForm_id(form_id);
				formB_vo.setColIds(colIds);
				formB_vo.setVals(values);
			}
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
			throw new MyException();
		}
		finally {						
			if(conn!=null) {
				conn.close();
			}
			if(prstmt!=null) {
				prstmt.close();
			}
			if(rs!=null) {
				rs.close();
			}
		}
		return formB_vo;
	}

	@SuppressWarnings("resource")
	public boolean saveF_B_JExcelData(Form_B_JExcelDataVO data) throws MyException, ParseException, SQLException {
		ArrayList<ArrayList<String>> values = data.getVals();
		ArrayList<String> gen_list = values.get(0);
		ArrayList<String> trans_list = values.get(1);
		ArrayList<String> dist_list = values.get(2);
		ArrayList<String> meter_list = values.get(3);
		ArrayList<String> other_list = values.get(4);
		ArrayList<String> colIds = data.getColIds();
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		int formInstanceId = 0;
		
		Connection conn=null;
		PreparedStatement prstmt = null;	
		ResultSet rs=null;
		
		String daleteFromB = QueryConstants.QUERY_DELETE_FROM_F_B_VAL;
		String insertForm_b = QueryConstants.QUERY_INSERT_t_arrdfb;
    	String getFormInstanceId = QueryConstants.QUERY_GET_FORM_INSTANCE_ID;
    	
		try{
			conn=DBUtil.getConnection();
		  	prstmt = conn.prepareStatement(getFormInstanceId);
			prstmt.setInt(1, Integer.parseInt(data.getForm_id()));
			prstmt.setInt(2, Integer.parseInt(data.getPh_id()));
			rs=prstmt.executeQuery();
			
			if (rs.next()) {
				formInstanceId = Integer.parseInt(rs.getString("form_instance_id"));
			}
			
			prstmt = conn.prepareStatement(daleteFromB);
			prstmt.setInt(1, Integer.parseInt(data.getPy_id()));
			prstmt.setInt(2, formInstanceId);
			prstmt.executeUpdate();
			
			prstmt = conn.prepareStatement(insertForm_b);
			
			for(int i = 0; i<colIds.size(); i++) {
				String col = colIds.get(i);
				int colId = Integer.parseInt(col.substring(2, col.length()));
				
				if(col.substring(0, 1).equalsIgnoreCase("m")) {
					prstmt.setInt(1, formInstanceId);
					prstmt.setInt(2, Integer.parseInt(data.getPy_id()));
					prstmt.setDouble(3, Double.parseDouble(gen_list.get(i)));
					prstmt.setDouble(4, Double.parseDouble(trans_list.get(i)));
					prstmt.setDouble(5, Double.parseDouble(dist_list.get(i)));
					prstmt.setDouble(6, Double.parseDouble(meter_list.get(i)));
					prstmt.setDouble(7, Double.parseDouble(other_list.get(i)));
					prstmt.setInt(8, colId);
					prstmt.setInt(9, 0);
					prstmt.setTimestamp(10, timestamp);
					prstmt.setInt(11, Integer.parseInt(data.getUser_id()));
					prstmt.setTimestamp(12, timestamp);
					prstmt.setInt(13, Integer.parseInt(data.getUser_id()));
						
					prstmt.executeUpdate();
				}else {
					prstmt.setInt(1, formInstanceId);
					prstmt.setInt(2, Integer.parseInt(data.getPy_id()));
					prstmt.setDouble(3, Double.parseDouble(gen_list.get(i)));
					prstmt.setDouble(4, Double.parseDouble(trans_list.get(i)));
					prstmt.setDouble(5, Double.parseDouble(dist_list.get(i)));
					prstmt.setDouble(6, Double.parseDouble(meter_list.get(i)));
					prstmt.setDouble(7, Double.parseDouble(other_list.get(i)));
					prstmt.setInt(8, 0);
					prstmt.setInt(9, colId);
					prstmt.setTimestamp(10, timestamp);
					prstmt.setInt(11, Integer.parseInt(data.getUser_id()));
					prstmt.setTimestamp(12, timestamp);
					prstmt.setInt(13, Integer.parseInt(data.getUser_id()));
						
					prstmt.executeUpdate();
				}
			}
			conn.commit();
		}catch(Exception e) {
			conn.rollback();
			e.printStackTrace();
			throw new MyException();
		}
		finally {						
			if(conn!=null) {
				conn.close();
			}
			if(prstmt!=null) {
				prstmt.close();
			}
			if(rs!=null) {
				rs.close();
			}
		}
		return true;
	}

}
