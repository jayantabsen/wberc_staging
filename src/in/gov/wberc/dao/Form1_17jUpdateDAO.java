//*** By JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17gJExcelDataVO;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;

public class Form1_17jUpdateDAO {

	public boolean updateF1_17jUpdate(Form1_17jJExcelDataVO data)
			throws MyException, SQLException {
		
		int int_py_id = Integer.parseInt(data.getPyid());
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();

		String query1 = QueryConstants.UPDATE_F1__17jVAL;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;

		// Database Update Segment
		/*
		update t_arrdf1_17j set val=? where py_id=? and misc_form_label_id_1=? and 
		misc_form_label_id_2=?
		*/
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i < miscids.size(); i++) {
				int miscid = Integer.parseInt(miscids.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int misc_form_label_id_1 = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values.get(i).get(j + 1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, misc_form_label_id_1);
					prstmt1.setInt(4, miscid);
					
					System.out.println(prstmt1);
					
					prstmt1.addBatch();
				}
			}
			
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
	
}
