
//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;

public class ValidatePumpStationid {

	public boolean validateStationid(String pump_stn_id) throws MyException, SQLException {
		
		String query1 = QueryConstants.QUERY_VALIDATE_PUMP_STATIONS;
		
		PreparedStatement prstmt1=null;
		Connection conn1=null;
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(pump_stn_id));
			rs1=prstmt1.executeQuery();
			
			if( rs1 != null ) {	
				if ( rs1.next() ) {
					int no_of_rows = Integer.parseInt(rs1.getString("no"));
					if ( no_of_rows > 0 ) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
						
		}
	}

}
