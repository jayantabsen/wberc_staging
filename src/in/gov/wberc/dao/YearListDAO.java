package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.LabeledYearListVO;
import in.gov.wberc.vo.YearListVO;
import in.gov.wberc.vo.YearVO;


public class YearListDAO {

	public LabeledYearListVO getPetitionYearLst(String pdid) throws MyException {
		
		String query1 = QueryConstants.QUERY_GET_PETITION_YEARS;
		
		YearVO year = null;
		ArrayList<YearVO> prev_yearsAl = null;
		ArrayList<YearVO> base_yearsAl = null;
		ArrayList<YearVO> ensu_yearsAl = null;
		YearListVO prev_ylvo = null;
		YearListVO base_ylvo = null;
		YearListVO ensu_ylvo = null;
		ArrayList<YearListVO> ylAl = null;
		LabeledYearListVO lylvo = null;
		
		PreparedStatement prstmt1=null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(pdid));
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {				
				prev_yearsAl = new ArrayList<YearVO>();
				base_yearsAl = new ArrayList<YearVO>();
				ensu_yearsAl = new ArrayList<YearVO>();
				prev_ylvo = new YearListVO();
				base_ylvo = new YearListVO();
				ensu_ylvo = new YearListVO();
				lylvo = new LabeledYearListVO();
				while(rs1.next()) {
					year = new YearVO();
					year.setYearval(rs1.getString("year_nm"));
					int seq = Integer.parseInt(rs1.getString("seq"));
					if ( seq < 0 ) {
						prev_yearsAl.add(year);
					} else {
						if ( seq > 0 ) {
							ensu_yearsAl.add(year);
						} else {
							base_yearsAl.add(year);
						}
					}
				}
				prev_ylvo.setLabel("Previous Year");
				prev_ylvo.setYearsAl(prev_yearsAl);
				base_ylvo.setLabel("Base Year");
				base_ylvo.setYearsAl(base_yearsAl);
				ensu_ylvo.setLabel("Ensuing Year");
				ensu_ylvo.setYearsAl(ensu_yearsAl);
				ylAl = new ArrayList<YearListVO>();
				ylAl.add(prev_ylvo);
				ylAl.add(base_ylvo);
				ylAl.add(ensu_ylvo);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
						
		}
		lylvo = new LabeledYearListVO();
		lylvo.setLabeledYearsAl(ylAl);
		return lylvo;
	}
	
	public boolean validatePdid(String pdid) throws MyException {
		
		String query1 = QueryConstants.QUERY_VALIDATE_PDID;
		
		PreparedStatement prstmt1=null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(pdid));
			
			rs1=prstmt1.executeQuery();
			if( rs1 != null ) {	
				if ( rs1.next() ) {
					int no_of_rows = Integer.parseInt(rs1.getString("no"));
					if ( no_of_rows > 0 ) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					
					if(prstmt1!=null) {
						prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
						
		}
	}
	
	public boolean validatePyid(String py_id) throws MyException {
		
		String query1 = QueryConstants.QUERY_VALIDATE_PY_ID;
		
		PreparedStatement prstmt1=null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(py_id));
			
			rs1=prstmt1.executeQuery();
			if( rs1 != null ) {	
				if ( rs1.next() ) {
					int no_of_rows = Integer.parseInt(rs1.getString("no"));
					if ( no_of_rows > 0 ) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			try {
				
				if(prstmt1!=null) {
					prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
				
			}catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			}		
		}
	}
	
}
