//*** By JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17dJExcelDataVO;

public class Form1_17dUpdateDAO {

	public boolean updateF1_17dUpdate(Form1_17dJExcelDataVO data)
			throws MyException, SQLException {
		
		int int_py_id = Integer.parseInt(data.getPy_id());
		int int_form_id = Integer.parseInt(data.getForm_id());
		int int_user_id = Integer.parseInt(data.getUser_id());
		ArrayList<ArrayList<String>> values = data.getVals();
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String query1 = QueryConstants.QUERY_GET_PHID;
		String query2 = QueryConstants.QUERY_GET_FORM_INSTANCE_ID;
		String query3 = QueryConstants.DELETE_F1__17dVAL;
		String query4 = QueryConstants.QUERY_INSERT_t_arrdf1_17d;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			
			// Get ph_id => select ph_id from t_petition_year where py_id=?
			int int_ph_id = 0;
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_py_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					int_ph_id = Integer.parseInt(rs1.getString("ph_id"));
				}
			}
			
			// Get form_instance_id => select form_instance_id from t_form_instance where form_id = ? and ph_id= ?
			int int_form_instance_id = 0;
			prstmt1 = conn1.prepareStatement(query2);
			prstmt1.setInt(1, int_form_id);
			prstmt1.setInt(2, int_ph_id);
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if (rs1.next()) {
					int_form_instance_id = Integer.parseInt(rs1.getString("form_instance_id"));
				}
			}
			
			/*
			Delete => delete from t_arrdf1_17d where form_instance_id=(select form_instance_id from 
			t_form_instance where form_id = ? and ph_id= ?) and py_id=?
			*/
			prstmt1 = conn1.prepareStatement(query3);
			prstmt1.setInt(1, int_form_id);
			prstmt1.setInt(2, int_ph_id);
			prstmt1.setInt(3, int_py_id);
			
			prstmt1.executeUpdate();
			
			/*
			Insert => INSERT INTO t_arrdf1_17d (form_instance_id, py_id, loan_desc, repayable_loan, repayment_rate, drawal_rate, 
			created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			*/
			prstmt1 = conn1.prepareStatement(query4);
			for (int i = 0; i < values.size(); i++) {
				ArrayList<String> row = values.get(i);
				
				prstmt1.setInt(1, int_form_instance_id);
				prstmt1.setInt(2, int_py_id);
				prstmt1.setString(3, row.get(0));
				prstmt1.setDouble(4, Double.parseDouble(row.get(1)));
				prstmt1.setDouble(5, Double.parseDouble(row.get(2)));
				prstmt1.setDouble(6, Double.parseDouble(row.get(3)));
				prstmt1.setTimestamp(7, timestamp);
				prstmt1.setInt(8, int_user_id);
				prstmt1.setTimestamp(9, timestamp);
				prstmt1.setInt(10, int_user_id);
				
				System.out.println(prstmt1);
				
				prstmt1.executeUpdate();
			}
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
	
}
