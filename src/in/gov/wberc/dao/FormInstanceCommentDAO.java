//*** JBS
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.FormInstanceCommentVO;

public class FormInstanceCommentDAO {
	
	public FormInstanceCommentVO getFormInstanceCommen( String form_id, String ph_id, String user_id ) 
			throws MyException, SQLException {
		FormInstanceCommentVO ficvo = null;
		
		int int_form_id = Integer.parseInt(form_id);
		int int_ph_id = Integer.parseInt(ph_id);
		int int_user_id = Integer.parseInt(user_id);
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> comments = null;

		String query1 = QueryConstants.QUERY_GET_FORM_INSTANCE_COMMENTS;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			prstmt1 = conn1.prepareStatement(query1);
			/*
			select comment from t_arr_comment where created_on=? and form_instance_id=
			(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1.setInt(1, int_user_id);
			prstmt1.setInt(2, int_form_id);
			prstmt1.setInt(3, int_ph_id);
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				comments = new ArrayList<String>();
				while(rs1.next()) {
					comments.add(rs1.getString("comment"));
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		/*
			private String ph_id;
			private String form_id;
			private String user_id;
			private ArrayList<String> comments;
		*/
		ficvo = new FormInstanceCommentVO();
		ficvo.setPh_id(ph_id);
		ficvo.setForm_id(form_id);
		ficvo.setUser_id(user_id);
		ficvo.setComments(comments);

		return ficvo;
	}
	
	public boolean updateFormInstanceComment(FormInstanceCommentVO data)
			throws MyException, SQLException {
		
		int int_ph_id = Integer.parseInt(data.getPh_id());
		int int_form_id = Integer.parseInt(data.getForm_id());
		int int_user_id = Integer.parseInt(data.getUser_id());
		ArrayList<String> comments = data.getComments();
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String query1 = QueryConstants.QUERY_GET_FORM_INSTANCE_ID;
		String query2 = QueryConstants.QUERY_DELETE_FORM_INSTANCE_COMMENTS;
		String query3 = QueryConstants.QUERY_INSERT_FORM_INSTANCE_COMMENTS;
		String query4 = QueryConstants.QUERY_GET_USER_UTILID;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;

		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			
			// Getting form_instance_id
			prstmt1 = conn1.prepareStatement(query1);
			/*
			select form_instance_id from t_form_instance where form_id=? and ph_id=?
			*/
			prstmt1.setInt(1, int_form_id);
			prstmt1.setInt(2, int_ph_id);
			rs1=prstmt1.executeQuery();
			int int_form_instance_id = 0;
			if(rs1!=null) {
				if(rs1.next()) {
					int_form_instance_id = Integer.parseInt(rs1.getString("form_instance_id"));
				}
			}
			prstmt1.close();
			rs1.close();
			
			// Getting form_instance_id
			prstmt1 = conn1.prepareStatement(query4);
			/*
			select entity_id m_user where user_id=?
			*/
			prstmt1.setInt(1, int_user_id);
			rs1=prstmt1.executeQuery();
			int int_util_id = 0;
			if(rs1!=null) {
				if(rs1.next()) {
					int_util_id = Integer.parseInt(rs1.getString("entity_id"));
				}
			}
			prstmt1.close();
			rs1.close();
			boolean iscommission = false;
			if (int_util_id == 0) {
				iscommission = true;
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			/*
			Delete => delete from t_arr_comment where form_instance_id=? and created_by=?
			*/
			prstmt1.setInt(1, int_form_instance_id);
			prstmt1.setInt(2, int_user_id);
			prstmt1.executeUpdate();
			prstmt1.close();
			
			/*
			Insert => insert into t_arr_comment (form_instance_id, comment, created_on, created_by, 
			updated_on, updated_by, iscommission) values (?, ?, ?, ?, ?, ?, ?)
			*/
			prstmt1 = conn1.prepareStatement(query3);
			for ( int i = 0; i<comments.size(); i++  ) {
				
				prstmt1.setInt(1, int_form_instance_id);
				prstmt1.setString(2, comments.get(i));
				prstmt1.setTimestamp(3, timestamp);
				prstmt1.setInt(4, int_user_id);
				prstmt1.setTimestamp(5, timestamp);
				prstmt1.setInt(6, int_user_id);
				prstmt1.setBoolean(7, iscommission);
				
				prstmt1.executeUpdate();
			}
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		return true;
	}
}
