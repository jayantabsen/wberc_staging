//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_4bSeasonUsageVO;
import in.gov.wberc.vo.Form1_4bJExcelDataVO;


public class Form1_4bDataUpdateDAO {

	public boolean updateF1_4bMatrixToDatabase(Form1_4bJExcelDataVO data)
			throws MyException, SQLException {
		
		String pump_stationid = data.getPump_stationid();
		ArrayList<String> cols = data.getCols();
		ArrayList<String> seasons = data.getSeasons();
		ArrayList<ArrayList<String>> usage = data.getUsage();
		ArrayList<F1_4bSeasonUsageVO> vals = data.getVals();
		
		// Data collection segment
		ArrayList<ArrayList<String>>[] ar_of_alal = new ArrayList[seasons.size()];
		int indx = 0;
		for (F1_4bSeasonUsageVO f13suvo1 : vals ) {
			ArrayList<ArrayList<String>> alal = f13suvo1.getUsage();
			ar_of_alal[indx++] = alal;
		}
		
		String query1 = QueryConstants.QUERY_UPDATE_F14b;
		String query8= QueryConstants.QUERY_GET_PERIOD_SEASON_WISE;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		
		//Database Update Segment
				int int_pump_stationid = Integer.parseInt(pump_stationid);
				int seasonid = 1;
				ArrayList<String> usageList = null;

				try {
					conn1=DBUtil.getConnection();
					prstmt1 = conn1.prepareStatement(query1);
					for ( ArrayList<ArrayList<String>> ref1 : ar_of_alal ) {
						prstmt1 = conn1.prepareStatement(query8);
						prstmt1.setInt(1, seasonid);
						rs1=prstmt1.executeQuery();
						
						if(rs1!=null) {
							usageList = new ArrayList<String>();
							while(rs1.next()) {
								usageList.add(rs1.getString("period_id"));
							}
						}
						int period= 0;
						for ( ArrayList<String> ref2 : ref1 ) {
							for ( int i = 0; i < ref2.size()-1; i++ ) {
								prstmt1 = conn1.prepareStatement(query1);
								double val = Double.parseDouble(ref2.get(i+1));
								int pyid = Integer.parseInt(cols.get(i));				
								prstmt1.setDouble(1, val);
								prstmt1.setInt(2, int_pump_stationid);
								prstmt1.setInt(3, Integer.parseInt(usageList.get(period)));
								prstmt1.setInt(4, pyid);
								prstmt1.addBatch();
								prstmt1.executeBatch();
							}
							period++;
						}
						seasonid++;
					}
					prstmt1.executeBatch();
					conn1.commit();
				} catch(SQLException e) {
					e.printStackTrace();
					conn1.rollback();
				} finally {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
					}		
				
				return true;
		
	}
	
}
