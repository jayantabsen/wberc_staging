package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_4bSeasonUsageVO;
import in.gov.wberc.vo.Form1_4bJExcelDataVO;

public class Form1_4bGrossJExcelDAO_V2 {

	public Form1_4bJExcelDataVO getF1_4bGrossJExcelData(String ph_id, String form_id, String util_id) 
			throws MyException, SQLException {
		
		Form1_4bJExcelDataVO f14bjxldvo1 = null;
		F1_4bSeasonUsageVO f14bsuvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		ResultSet rs2=null;
	
		ArrayList<String> col = null;
		ArrayList<String> seasons = null;
		ArrayList<String> usageAl1 = null;
		ArrayList<String> usage = null;
		ArrayList<ArrayList<String>> usageAl2 = null;
		ArrayList<ArrayList<String>> usageList = new ArrayList<ArrayList<String>>();
		ArrayList<F1_4bSeasonUsageVO> f14bsuvoAl1 = new ArrayList<F1_4bSeasonUsageVO>();
		
		String get_pyid = QueryConstants.QUERY_GET_PYID;
		String get_stationids = QueryConstants.QUERY_GET_SEASONIDS;
		String get_seasonnm = QueryConstants.QUERY_GET_SEASONNM;
		String get_period_name = QueryConstants.QUERY_GET_PERIOD_NAME;
		String get_form1_4b_data_marix = QueryConstants.QUERY_GET_F1_4b_GROSS_DATA_VAL;
		String get_form1_4b_seasonwise_period= QueryConstants.QUERY_GET_PERIOD_SEASON_WISE;
				
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(get_pyid);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(get_stationids);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String seasonid : seasons) {
				prstmt1 = conn1.prepareStatement(get_form1_4b_seasonwise_period);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					usage = new ArrayList<String>();
					while(rs1.next()) {
						usage.add(rs1.getString("period_id"));
					}
				}
				usageList.add(usage);
				f14bsuvo = new F1_4bSeasonUsageVO();
				prstmt1 = conn1.prepareStatement(get_seasonnm);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					
					if(rs1.next()) {
						
						f14bsuvo.setSeason(rs1.getString("season_desc"));
						usageAl2 = new ArrayList<ArrayList<String>>();
						ArrayList<String> periodPerSeason = null;
						prstmt1 = conn1.prepareStatement(get_form1_4b_seasonwise_period);
						prstmt1.setInt(1, Integer.parseInt(seasonid));

						rs2=prstmt1.executeQuery();
						
						if(rs2!=null) {
							periodPerSeason = new ArrayList<String>();
							while(rs2.next()) {
								periodPerSeason.add(rs2.getString("period_id"));
							}
						}
						try {

							if (rs2 != null) {
								rs2.close();
							}
						} catch (SQLException e) {
							e.printStackTrace();
							throw new MyException();

						}

						for (String period_id : periodPerSeason) {
							
							prstmt1 = conn1.prepareStatement(get_period_name);
							prstmt1.setInt(1, Integer.parseInt(period_id));
							usageAl1 = new ArrayList<String>();
							rs2=prstmt1.executeQuery();
							if ( rs2 != null ) {
								if(rs2.next()) {
									usageAl1.add(rs2.getString("period_desc"));
								}
							}
							
							try {
								if (rs2 != null) {
									rs2.close();
								}
							} catch (SQLException e) {
								e.printStackTrace();
								throw new MyException();

							}

							prstmt1 = conn1.prepareStatement(get_form1_4b_data_marix);
							for(int m=0;m<col.size();m++) {
								prstmt1.setInt(1, Integer.parseInt(period_id));
								prstmt1.setInt(2, Integer.parseInt(col.get(m)));

								rs2=prstmt1.executeQuery();
								if ( rs2 != null ) {
									while (rs2.next()) {
										usageAl1.add(rs2.getString("val"));
									}
								}
								try {
									if(rs2!=null) {
										rs2.close();
									}
								}catch(SQLException e) {
									e.printStackTrace();
									throw new MyException();
								}
								}
							
							usageAl2.add(usageAl1);
							}
								
							
						}
					f14bsuvo.setUsage(usageAl2);
					f14bsuvoAl1.add(f14bsuvo);
				}
				
			}
		conn1.commit();	
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
					conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
			}
		
		f14bjxldvo1 = new Form1_4bJExcelDataVO();
		f14bjxldvo1.setPump_stationid("0");
		f14bjxldvo1.setCols(col);
		f14bjxldvo1.setSeasons(seasons);
		f14bjxldvo1.setUsage(usageList);
		f14bjxldvo1.setVals(f14bsuvoAl1);
		
		return f14bjxldvo1;
		
	}
	
}
