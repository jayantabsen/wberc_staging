package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.F1_3SeasonUsageVO1;
import in.gov.wberc.vo.Form1_3JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_3jExcelDAO implements TransInterfaceVO {

	public Form1_3JExcelDataVO getF1_3JExcelData(String ph_id, String form_id, String util_id, String stn_id) 
			throws MyException, SQLException {
		
		Form1_3JExcelDataVO f13jxldvo1 = null;
		F1_3SeasonUsageVO1 f13suvo1 = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		ResultSet rs2=null;
	
		ArrayList<String> col = null;
		ArrayList<String> seasons = null;
		ArrayList<String> usageAl1 = null;
		ArrayList<String> usage = null;
		ArrayList<ArrayList<String>> usageAl2 = null;
		ArrayList<ArrayList<String>> usageList = new ArrayList<ArrayList<String>>();
		ArrayList<F1_3SeasonUsageVO1> f13suvoAl1 = new ArrayList<F1_3SeasonUsageVO1>();
		
		String get_pyid = QueryConstants.QUERY_GET_PYID;
		String get_stationids = QueryConstants.QUERY_GET_SEASONIDS;
		String get_seasonnm = QueryConstants.QUERY_GET_SEASONNM;
		String get_period_name = QueryConstants.QUERY_GET_PERIOD_NAME;
		String get_form1_3_data_marix = QueryConstants.QUERY_GET_F1_3_DATA_VAL;
		String get_form1_3_seasonwise_period= QueryConstants.QUERY_GET_PERIOD_SEASON_WISE;
				
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(get_pyid);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(get_stationids);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String seasonid : seasons) {
				prstmt1 = conn1.prepareStatement(get_form1_3_seasonwise_period);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					usage = new ArrayList<String>();
					while(rs1.next()) {
						usage.add(rs1.getString("period_id"));
					}
				}
				usageList.add(usage);
				f13suvo1 = new F1_3SeasonUsageVO1();
				prstmt1 = conn1.prepareStatement(get_seasonnm);
				prstmt1.setInt(1, Integer.parseInt(seasonid));
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					
					if(rs1.next()) {
						
						f13suvo1.setSeason(rs1.getString("season_desc"));
						usageAl2 = new ArrayList<ArrayList<String>>();
						ArrayList<String> periodPerSeason = null;
						prstmt1 = conn1.prepareStatement(get_form1_3_seasonwise_period);
						prstmt1.setInt(1, Integer.parseInt(seasonid));

						rs2=prstmt1.executeQuery();
						
						if(rs2!=null) {
							periodPerSeason = new ArrayList<String>();
							while(rs2.next()) {
								periodPerSeason.add(rs2.getString("period_id"));
							}
						}
						try {

							if (rs2 != null) {
								rs2.close();
							}
						} catch (SQLException e) {
							e.printStackTrace();
							throw new MyException();

						}

						for (String period_id : periodPerSeason) {
							
							prstmt1 = conn1.prepareStatement(get_period_name);
							prstmt1.setInt(1, Integer.parseInt(period_id));
							usageAl1 = new ArrayList<String>();
							rs2=prstmt1.executeQuery();
							if ( rs2 != null ) {
								if(rs2.next()) {
									usageAl1.add(rs2.getString("period_desc"));
								}
							}
							
							try {
								if (rs2 != null) {
									rs2.close();
								}
							} catch (SQLException e) {
								e.printStackTrace();
								throw new MyException();

							}

							prstmt1 = conn1.prepareStatement(get_form1_3_data_marix);
							for(int m=0;m<col.size();m++) {
								prstmt1.setInt(1, Integer.parseInt(stn_id));
								prstmt1.setInt(2, Integer.parseInt(period_id));
								prstmt1.setInt(3, Integer.parseInt(col.get(m)));

								rs2=prstmt1.executeQuery();
								if ( rs2 != null ) {
									while (rs2.next()) {
										usageAl1.add(rs2.getString("val"));
									}
								}
								try {
									if(rs2!=null) {
										rs2.close();
									}
								}catch(SQLException e) {
									e.printStackTrace();
									throw new MyException();
								}
								}
							
							usageAl2.add(usageAl1);
							}
								
							
						}
					f13suvo1.setUsage(usageAl2);
					f13suvoAl1.add(f13suvo1);
				}
				
			}
		conn1.commit();	
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
					conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
			}
		
		f13jxldvo1 = new Form1_3JExcelDataVO();
		f13jxldvo1.setStationid(stn_id);
		f13jxldvo1.setCols(col);
		f13jxldvo1.setSeasons(seasons);
		f13jxldvo1.setUsage(usageList);
		f13jxldvo1.setVals(f13suvoAl1);
		
		return f13jxldvo1;
		
	}

}
