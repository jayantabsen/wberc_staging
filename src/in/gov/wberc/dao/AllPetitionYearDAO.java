package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.AllPetitionYearVO;
import in.gov.wberc.vo.EnsuingYearVO;

public class AllPetitionYearDAO {

public AllPetitionYearVO getAllPetitionYears(String ph_id) throws MyException, SQLException {
		
		AllPetitionYearVO apyvo = null;
		
		Connection conn1 = null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		String query1 = QueryConstants.QUERY_GET_ALL_PETITION_YEAR;
		EnsuingYearVO ey = null;
		ArrayList<EnsuingYearVO> all_petition_year = null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				all_petition_year = new ArrayList<EnsuingYearVO>();
				while(rs1.next()) {
					ey = new EnsuingYearVO();
					ey.setPy_id(rs1.getString("py_id"));
					ey.setYear_nm(rs1.getString("year_nm"));
					all_petition_year.add(ey);
				}
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		apyvo = new AllPetitionYearVO();
		apyvo.setAll_petition_year(all_petition_year);
		
		return apyvo;
		
	}
	
}
