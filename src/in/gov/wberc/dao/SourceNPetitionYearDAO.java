//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.SourceNPetitionYearVO;
import in.gov.wberc.vo.YearListVO;
import in.gov.wberc.vo.YearVO;

public class SourceNPetitionYearDAO {

	public SourceNPetitionYearVO getSourceNPetitionYear(String formid, String sourceid, String pdid) throws MyException {
		
		SourceNPetitionYearVO sourcepyvo = null;
		
		String query1 = QueryConstants.QUERY_SOURCE_DETAILS;
		String query2 = QueryConstants.QUERY_SOURCE_UNIT;
		
		PreparedStatement prstmt1 = null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		
		// Getting stationid, stationnm and capacity
		String sourceid_from_db = null;
		String sourcenm = null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(sourceid));
			
			rs1=prstmt1.executeQuery();
			if ( rs1 != null ) {
				if ( rs1.next() ) {
					sourceid_from_db = rs1.getString("epsid");
					sourcenm = rs1.getString("source_nm");
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
			}			
		
		
		// Getting stationid, stationnm and capacity
		String computational_unit = null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, Integer.parseInt(formid));
			
			rs1=prstmt1.executeQuery();
			if ( rs1 != null ) {
				if ( rs1.next() ) {
					computational_unit = rs1.getString("unit_of_measurement");
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
			}			
		
		
		// Getting petition year details
		String query3 = QueryConstants.QUERY_GET_PETITION_YEARS;
		
		YearVO year = null;
		ArrayList<YearVO> prev_yearsAl = null;
		ArrayList<YearVO> base_yearsAl = null;
		ArrayList<YearVO> ensu_yearsAl = null;
		YearListVO prev_ylvo = null;
		YearListVO base_ylvo = null;
		YearListVO ensu_ylvo = null;
		ArrayList<YearListVO> labeledYearsAl = null;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query3);
			
			prstmt1.setInt(1, Integer.parseInt(pdid));
			
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {				
				prev_yearsAl = new ArrayList<YearVO>();
				base_yearsAl = new ArrayList<YearVO>();
				ensu_yearsAl = new ArrayList<YearVO>();
				prev_ylvo = new YearListVO();
				base_ylvo = new YearListVO();
				ensu_ylvo = new YearListVO();
				while(rs1.next()) {
					year = new YearVO();
					year.setYearval(rs1.getString("year_nm"));
					int seq = Integer.parseInt(rs1.getString("seq"));
					if ( seq < 0 ) {
						prev_yearsAl.add(year);
					} else {
						if ( seq > 0 ) {
							ensu_yearsAl.add(year);
						} else {
							base_yearsAl.add(year);
						}
					}
				}
				prev_ylvo.setLabel("Previous Year");
				prev_ylvo.setYearsAl(prev_yearsAl);
				base_ylvo.setLabel("Base Year");
				base_ylvo.setYearsAl(base_yearsAl);
				ensu_ylvo.setLabel("Ensuing Year");
				ensu_ylvo.setYearsAl(ensu_yearsAl);

				labeledYearsAl = new ArrayList<YearListVO>();
				labeledYearsAl.add(prev_ylvo);
				labeledYearsAl.add(base_ylvo);
				labeledYearsAl.add(ensu_ylvo);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
					if(rs1!=null) {
						rs1.close();
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
					throw new MyException();
				}
			}			
		
		sourcepyvo = new SourceNPetitionYearVO();
		sourcepyvo.setSourceid(sourceid_from_db);;
		sourcepyvo.setSourcenm(sourcenm);;
		sourcepyvo.setUnit_of_calc(computational_unit);
		sourcepyvo.setLabeledYearsAl(labeledYearsAl);
		
		return sourcepyvo;
		
	}
	
}
