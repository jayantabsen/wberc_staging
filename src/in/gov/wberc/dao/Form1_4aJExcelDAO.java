package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_4aJExcelDataVO;

public class Form1_4aJExcelDAO {

	public Form1_4aJExcelDataVO getF1_4aJExcelData(String ph_id, String stn_id)
			throws MyException, SQLException {
		
		Form1_4aJExcelDataVO f14ajxldvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		
		// Getting Station ID - which is already with us as argument
		
		// Getting PYIDs
		String getPetitionYears = QueryConstants.QUERY_GET_PYID;
		String getSeasonIds = QueryConstants.QUERY_GET_SEASONIDS;
		String getSeasonName = QueryConstants.QUERY_GET_SEASONNM;
		String getValPerSeason = QueryConstants.QUERY_GET_SEASONWISE_F14a_DATA;
		

		ArrayList<String> seasons = null;
		ArrayList<ArrayList<String>> alal = new ArrayList<ArrayList<String>>();
		ArrayList<String> al = null;
		ArrayList<String> col = null;

		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYears);
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			prstmt1 = conn1.prepareStatement(getSeasonIds);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String season : seasons ) { //***************** Season
				al = new ArrayList<String>();
				int int_seasonid = Integer.parseInt(season);
				
				prstmt1 = conn1.prepareStatement(getSeasonName);
				prstmt1.setInt(1, int_seasonid);
				rs1=prstmt1.executeQuery();
				if(rs1!=null) {
					if(rs1.next()) {
						al.add( rs1.getString("season_desc") );
					}
				}
				for (String pyid : col ) { //****************** Petition Year
					int int_pyid = Integer.parseInt(pyid);
					prstmt1 = conn1.prepareStatement(getValPerSeason);
					prstmt1.setInt(1, Integer.parseInt(stn_id));
					prstmt1.setInt(2, int_pyid);
					prstmt1.setInt(3, int_seasonid);
					
					rs1=prstmt1.executeQuery();
					if(rs1!=null) {
						if(rs1.next()) {
							al.add( rs1.getString("val") );
						}
					}
				}
				alal.add(al);
				
			}
			
		conn1.commit();	
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(conn1!=null) {
					conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
			}
		
		f14ajxldvo = new Form1_4aJExcelDataVO();
		f14ajxldvo.setStationid(stn_id);
		f14ajxldvo.setCols(col);
		f14ajxldvo.setSeasons(seasons);
		f14ajxldvo.setVals(alal);
		
		return f14ajxldvo;
		
	}
	
}
