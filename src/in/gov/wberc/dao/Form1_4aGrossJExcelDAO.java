
//*** By Najma

package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_4bGrossJExcelDataVO;

public class Form1_4aGrossJExcelDAO {

	public Form1_4bGrossJExcelDataVO getF1_4aGrossJExcelData(String ph_id, String form_id, String util_id) 
			throws MyException, SQLException {
		
		Form1_4bGrossJExcelDataVO f14bgjxldvo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1=null;
		
		// Getting PYIDs
		ArrayList<String> col = null;
		ArrayList<String> seasons = null;
		ArrayList<ArrayList<String>> alal = new ArrayList<ArrayList<String>>();
		ArrayList<String> al = null;

		String getPetitionYears = QueryConstants.QUERY_GET_PYID;
		String getSeasonIds = QueryConstants.QUERY_GET_SEASONIDS;
		String getSeasonName = QueryConstants.QUERY_GET_SEASONNM;
		String getValForAllSeason = QueryConstants.QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_4a;
		
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYears);
			
			prstmt1.setInt(1, Integer.parseInt(ph_id));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("py_id"));
				}
			}
			
			prstmt1 = conn1.prepareStatement(getSeasonIds);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				seasons = new ArrayList<String>();
				while(rs1.next()) {
					seasons.add(rs1.getString("season_id"));
				}
			}
			
			for (String season : seasons ) { //***************** Season
				int int_seasonid = Integer.parseInt(season);
				al = new ArrayList<String>();
				
				prstmt1 = conn1.prepareStatement(getSeasonName);
				prstmt1.setInt(1, int_seasonid);
				rs1=prstmt1.executeQuery();
				if(rs1!=null) {
					if(rs1.next()) {
						String all_season_name = rs1.getString("season_desc");
						all_season_name = all_season_name.substring(0,1).toUpperCase() + all_season_name.substring(1).toLowerCase();
						al.add("Total " + all_season_name + ": ");
					}
				}
				
				for (String pyid : col ) { //****************** Petition Year
					int int_pyid = Integer.parseInt(pyid);
					prstmt1 = conn1.prepareStatement(getValForAllSeason);
					prstmt1.setInt(1, int_pyid);
					prstmt1.setInt(2, int_seasonid);
					rs1=prstmt1.executeQuery();
					if(rs1!=null) {
						if(rs1.next()) {
							al.add( rs1.getString("sumval") );
						}
					}
				}
				alal.add(al);
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
				if(prstmt1!=null) {
				prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}
	
		f14bgjxldvo = new Form1_4bGrossJExcelDataVO();
		f14bgjxldvo.setCols(col);
		f14bgjxldvo.setSeasons(seasons);
		f14bgjxldvo.setGross(alal);
		
		return f14bgjxldvo;
	}
	
}
