package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.vo.UserVO;

import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.MenuStatusVO;
import in.gov.wberc.vo.SaverVO;

public class CredentialsDAO {

	@SuppressWarnings("resource")
	public UserVO getUserCredentials(String token) throws MyException, SQLException {
		String query1 = QueryConstants.QUERY_GET_USER_FROM_TOKEN;
		String query2 = QueryConstants.QUERY_GET_USER_CREDENTIALS;
		String query3 = QueryConstants.QUERY_GET_USER_MENUS;
		String query4 = QueryConstants.QUERY_GET_MENUS;
		String query5 = QueryConstants.QUERY_GET_MENUS1;
		
		UserVO user = null;
		
		PreparedStatement prstmt1=null;	
		Connection conn1=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
		
		String userid = "";
		ArrayList<String> granted_menucodes = new ArrayList<String>();
		
		ArrayList<MenuStatusVO> menuobjects_l1 = null;
		ArrayList<MenuStatusVO> menuobjects_l2 = null;
		ArrayList<MenuStatusVO> menuobjects_l3 = null;
		MenuStatusVO ms1 = null;
		MenuStatusVO ms2 = null;
		MenuStatusVO ms3 = null;

		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setString(1, token);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				user= new UserVO(); 
				while(rs1.next()) {
					user.setLoginid(rs1.getString("login_id"));
				}
				
				prstmt1 =conn1.prepareStatement(query2);
				prstmt1.setString(1, user.getLoginid());
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					while(rs1.next()) {
						userid = rs1.getString("user_id");
						String fnm = rs1.getString("fnm");
						String mnm = rs1.getString("mnm");
						String lnm = rs1.getString("lnm");
						String fullnm = null;
						if (mnm != null && mnm.length() != 0) {
							fullnm = fnm+" "+mnm+" "+lnm;
						} else {
							fullnm = fnm+" "+lnm;
						}
						user.setName(fullnm);
						user.setEmail(rs1.getString("user_mail"));
						user.setPhone(rs1.getString("user_phone"));
						user.setLoginid(rs1.getString("login_id"));
						user.setLastlogin(rs1.getString("last_login"));
						user.setUtilid(rs1.getString("entity_id"));
						//Priyanka 12.01.2018
						user.setUserid(userid);
						user.setRoleid(rs1.getString("role_id"));
						user.setRole_code(rs1.getString("role_code"));
						user.setRole_desc(rs1.getString("role_desc"));

						if(rs1.getString("util_desc")==null ) {
							user.setUtilName("Commission Member");
						}else {
							user.setUtilName(rs1.getString("util_desc"));
						}
						//Priyanka 12.01.2018
						user.setToken(token);
					}
				}
			}
			
			prstmt1 = conn1.prepareStatement(query3);
			prstmt1.setInt(1, Integer.parseInt(userid));
			rs1=prstmt1.executeQuery();
			
			if (rs1!=null) {
				while ( rs1.next() ) {
					granted_menucodes.add(rs1.getString("menu_code"));
				}
			}
			
			
			
			prstmt1 = conn1.prepareStatement(query4);
			prstmt1.setInt(1,Integer.parseInt(user.getRoleid()) );//Priyanka 11.01.2018
			prstmt1.setInt(2, 1);
			rs1=prstmt1.executeQuery();
			
			if (rs1!=null) {
				menuobjects_l1 = new ArrayList<MenuStatusVO>();
				while ( rs1.next() ) {
					ms1 = new MenuStatusVO();
					//pk,id,menu_loc, name, parent_name, level
					String pk1 = rs1.getString("pk");
					ms1.setId(rs1.getString("id"));
					ms1.setMenu_loc(rs1.getString("menu_loc"));
					ms1.setName(rs1.getString("name"));
					//ms1.setState("0");
					ms1.setParent_name(rs1.getString("parent_name"));
					ms1.setLevel(rs1.getString("level"));
					ms1.setLink(rs1.getString("link"));
					
					prstmt1 = conn1.prepareStatement(query5);
					/*Priyanka 12.01.2018 @Start*/
					prstmt1.setInt(1,Integer.parseInt(user.getRoleid()) );
					prstmt1.setInt(2, 2);
					prstmt1.setInt(3, Integer.parseInt(pk1));
					/*Priyanka 12.01.2018 @End*/
					
					rs2=prstmt1.executeQuery();
					if (rs2!=null) {
						menuobjects_l2 = new ArrayList<MenuStatusVO>();
						while ( rs2.next() ) {
							ms2 = new MenuStatusVO();
							//id,menu_loc, name, parent_name, level
							String pk2 = rs2.getString("pk");
							ms2.setId(rs2.getString("id"));
							ms2.setMenu_loc(rs2.getString("menu_loc"));
							ms2.setName(rs2.getString("name"));
							//ms2.setState("0");
							ms2.setParent_name(rs2.getString("parent_name"));
							ms2.setLevel(rs2.getString("level"));
							ms2.setLink(rs2.getString("link"));
							
							prstmt1 = conn1.prepareStatement(query5);
							
							/*Priyanka 12.01.2018 @Start*/
							prstmt1.setInt(1,Integer.parseInt(user.getRoleid()) );
							prstmt1.setInt(2, 3);
							prstmt1.setInt(3, Integer.parseInt(pk2));
							/*Priyanka 12.01.2018 @End*/
							
							rs3=prstmt1.executeQuery();
							
							if (rs3!=null) {
								menuobjects_l3 = new ArrayList<MenuStatusVO>();
								while ( rs3.next() ) {
									ms3 = new MenuStatusVO();
									//id,menu_loc, name, parent_name, level
									ms3.setId(rs3.getString("id"));
									ms3.setMenu_loc(rs3.getString("menu_loc"));
									ms3.setName(rs3.getString("name"));
									//ms3.setState("0");
									ms3.setParent_name(rs3.getString("parent_name"));
									ms3.setLevel(rs3.getString("level"));
									ms3.setLink(rs3.getString("link"));
									
									menuobjects_l3.add(ms3);
								}
							}
							ms2.setChildAl(menuobjects_l3);
							menuobjects_l2.add(ms2);
						}
						ms1.setChildAl(menuobjects_l2);
					}
					menuobjects_l1.add(ms1);
				}
			}
			conn1.commit();
		} catch (Exception e1) {
			e1.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}

			if(rs1!=null) {
				rs1.close();
			}
			if(rs2!=null) {
				rs2.close();
			}
			if(rs3!=null) {
				rs3.close();
			}
		}

		user.setMenu_status(menuobjects_l1);
		
		return user;
	}
	
	@Test
	public void updateUserCredentials(UserVO user, SaverVO saver) throws MyException, SQLException {
		String query = QueryConstants.QUERY_UPDATE_USER_PASSWORD;
		
		PreparedStatement prstmt=null;
		Connection conn=null;
		ResultSet rs=null;
		
		try {
			conn = DBUtil.getConnection();
			prstmt = conn.prepareStatement(query);
			
			prstmt.setString(1, saver.getcPassWord());
			prstmt.setString(2, user.getLoginid());
			
			prstmt.executeUpdate();
			conn.commit();
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}finally{
				if(conn!=null) {
					conn.close();
				}
				if(prstmt!=null) {
				prstmt.close();
				}
				if(rs!=null) {
					rs.close();
				}
		}
	}
	
}
