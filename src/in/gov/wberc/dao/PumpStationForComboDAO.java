package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.PumpStationF1_4bVO;
import in.gov.wberc.vo.PumpStationListF1_4bVO;

public class PumpStationForComboDAO {

	public PumpStationListF1_4bVO getPumpStationsForCombo(String util_id) throws MyException, SQLException {
		
		String query1 = QueryConstants.QUERY_PUMP_STATION_FOR_COMBO;
		
		PreparedStatement prstmt1 = null;
		Connection conn1=null;
		ResultSet rs1=null;
		
		// Getting pump_station IDs & pump_station Names for an utility
		PumpStationListF1_4bVO pslf14bvo = null;
		ArrayList<PumpStationF1_4bVO> psf14bAl = null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(util_id));
			rs1=prstmt1.executeQuery();
			
			if ( rs1 != null ) {
				psf14bAl = new ArrayList<PumpStationF1_4bVO>();
				while ( rs1.next() ) {
					PumpStationF1_4bVO psf14b = new PumpStationF1_4bVO();
					psf14b.setPump_stn_id(rs1.getString("pump_stn_id"));
					psf14b.setPump_stn_desc(rs1.getString("pump_stn_desc"));
					psf14bAl.add(psf14b);
				}
			}
			conn1.commit();
		}catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		}finally{
				if(conn1!=null) {
					conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}		
		}
		pslf14bvo = new PumpStationListF1_4bVO();
		pslf14bvo.setPump_stations(psf14bAl);
		
		return pslf14bvo;
	}
	
}
