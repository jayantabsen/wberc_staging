package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.DashboardPetitionVO;
import in.gov.wberc.vo.FormNotesVO;

public class FormNotesDAO {

	public FormNotesVO getFormNotes(String form_id) throws MyException, SQLException {
		
		FormNotesVO fnvo = null;
		int int_form_id = Integer.parseInt(form_id);
		
		Connection conn1 = null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;
		
		ArrayList<String> notes = null;
		String query1 = QueryConstants.QUERY_GET_FORM_NOTES;
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_form_id);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				notes = new ArrayList<String>();
				while(rs1.next()) {
					/*
					select note from m_form_notes where form_id=? and isactive='TRUE'
					*/
					notes.add(rs1.getString("note"));
				}
			}
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		fnvo = new FormNotesVO();
		fnvo.setNotes(notes);
		
		return fnvo;
		
	}
	
}
