package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.DocumentVO;
import in.gov.wberc.vo.FileListingVO;

public class FileListingDAO {
	
	public static Session createCMISSession(String host, String port) {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "password");

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, "http://"+host+":"+port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Create session.
		// Alfresco only provides one repository.
		Repository repository = factory.getRepositories(parameter).get(0);
		System.out.println("Rep folder name-->"+repository.getId());

		parameter.put(SessionParameter.REPOSITORY_ID, repository.getId());

		// create session
		Session session = factory.createSession(parameter);
		return session;
	}
	
	public FileListingVO getListOfFile(String phid, String utilid, String module) throws MyException {
		
		// Getting base_year, utility-name
		String base_year = "";
		String util_abbr = "";
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PETITION_YEAR_FORM_PHID;
		String query2 = QueryConstants.QUERY_GET_UTILITY_ABBREVIATION_BY_ID;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(phid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					base_year = rs1.getString("base_year");
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, Integer.parseInt(utilid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					util_abbr = rs1.getString("util_desc");
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		} finally {
			try {
				if(prstmt1!=null) {
					prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			}
		}
		
		String folderPath = "/WBERC/" + base_year + "/" + util_abbr + "/" + module;
		
		Session session = createCMISSession("182.75.177.246", "8282");
		Folder parentFolder = getFolderByPath(session, folderPath);
		
		ArrayList<DocumentVO> docvoAl = new ArrayList<DocumentVO>();
		DocumentVO docvo = null;
		FileListingVO data = null;
		ItemIterable<CmisObject> contentItems = parentFolder.getChildren();
        for (CmisObject contentItem : contentItems) {
            if (contentItem instanceof Document) {
            	docvo = new DocumentVO();
                Document docMetadata = (Document) contentItem;
                ContentStream docContent = docMetadata.getContentStream();
                
                docvo.setDocName(docMetadata.getName());
                String documentid = docMetadata.getId();
                docvo.setDocId(documentid.substring(documentid.lastIndexOf('/')+1, documentid.indexOf(';')).trim());
                docvo.setDocType(docContent.getMimeType());
                docvo.setPath(docMetadata.getPaths().toString());
                
                System.out.println("[Name=" + docMetadata.getName()  + "]--[Mimetype=" + docContent.getMimeType() 
                	+ "]--[ID=" +  documentid.substring(documentid.lastIndexOf('/')+1, documentid.indexOf(';')).trim()
                	+ "]--[Path=" + docMetadata.getPaths() + "]");
                docvoAl.add(docvo);
                
            }
        }
		
        data = new FileListingVO();
        data.setFileList(docvoAl);
        
		return data;
		
	}
	
	public static FileListingVO getListOfFile_ById(String ph_id, String form_id) throws MyException {
		
		Session session = createCMISSession("182.75.177.246", "8282");

		String queryString = "SELECT * FROM erms:Attachments where erms:PetitionID = '"+ph_id+"' and erms:FormID = '"+form_id+"'" ;
		
		ArrayList<DocumentVO> docvoAl = new ArrayList<DocumentVO>();
		DocumentVO docvo = null;
		FileListingVO data = null;
		// execute query
		ItemIterable<QueryResult> results = session.query(queryString, false);
		for (QueryResult qResult : results) {
			docvo = new DocumentVO();
			
			String objectId = qResult.getPropertyValueByQueryName("cmis:objectId");
			Document doc = (Document) session.getObject(session.createObjectId(objectId));
			System.out.println("DOcument-->"+doc.getName());
			System.out.println("DOcumentURL-->"+doc.getContentUrl());
			
			docvo.setDocName(doc.getName());
            String documentid = doc.getId();
            docvo.setDocId(documentid.substring(documentid.lastIndexOf('/')+1, documentid.indexOf(';')).trim());
            docvo.setDescription(doc.getDescription());
            docvo.setPath(doc.getContentUrl());
            try {
            	docvo.setDocType(qResult.getPropertyValueByQueryName("erms:AttachmentType").toString());
            } catch (NullPointerException ex) {
            	docvo.setDocType("--");
            }
            docvo.setFormId(qResult.getPropertyValueByQueryName("erms:FormID").toString());
            
            docvoAl.add(docvo);
		}
        
		data = new FileListingVO();
        data.setFileList(docvoAl);
		
		return data;
		
	}
	
	/**
	 * Finds folder by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public Folder getFolderByPath(Session session, String path) {
		try {
		return (Folder) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
	public static void main(String args[]) throws MyException{
		getListOfFile_ById("104", "1");
	}

}
