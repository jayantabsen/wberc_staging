//*** JBS
package in.gov.wberc.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_17dJExcelDataVO;
import in.gov.wberc.vo.Form1_17fJExcelDataVO;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;

public class Form1_17fJExcelDAO {
	
	public Form1_17fJExcelDataVO getF1_17fJExcelData( String form_id, String ph_id, String user_id ) 
			throws MyException, SQLException {
		Form1_17fJExcelDataVO f117fjxldvo = null;
		
		int int_form_id = Integer.parseInt(form_id);
		int int_ph_id = Integer.parseInt(ph_id);
		int int_user_id = Integer.parseInt(user_id);
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> cols = null;
		ArrayList<String> row = null;
		ArrayList<ArrayList<String>> rowAl = new ArrayList<ArrayList<String>>();

		String query1 = QueryConstants.QUERY_GET_PYID;
		String query2 = QueryConstants.QUERY_GET_insurance_desc;
		String query3 = QueryConstants.QUERY_GET_F1__17f_DATA;
		
		try {
			conn1=DBUtil.getConnection();
			conn1.setAutoCommit(false);
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, int_ph_id);
			rs1=prstmt1.executeQuery();
			Integer[] s = new Integer[12];
			int row_count = 0;
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					String str_py_id = rs1.getString("py_id");
					cols.add(str_py_id);
					s[row_count++] = Integer.parseInt(str_py_id);
				}
			}
			Array array = conn1.createArrayOf("INTEGER", s);
			
			// Getting insurance_desc count
			prstmt1 = conn1.prepareStatement(query2);
			/*
			select distinct COALESCE(insurance_desc, '*') as desc from t_arrdf1_17f where py_id = ANY(?) and 
			form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)
			*/
			prstmt1.setArray(1, array);
			prstmt1.setInt(2, int_form_id);
			prstmt1.setInt(3, int_ph_id);
			System.out.println(prstmt1);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				rowAl = new ArrayList<ArrayList<String>>();
				while(rs1.next()) {
					row = new ArrayList<String>();
					row.add(rs1.getString("desc"));
					rowAl.add(row);
				}
			}
			
			prstmt1 = conn1.prepareStatement(query3);
			/*
			select COALESCE(insurance_desc, '*') as desc, val from t_arrdf1_17f where py_id = ANY(?) and form_instance_id=
			(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) order by insurance_desc, py_id
			*/
			prstmt1.setArray(1, array);
			prstmt1.setInt(2, int_form_id);
			prstmt1.setInt(3, int_ph_id);
			System.out.println(prstmt1);
			rs1=prstmt1.executeQuery();
			if(rs1!=null) {
				while(rs1.next()) {
					String insurance_desc = rs1.getString("desc");
					for (int i = 0; i<rowAl.size(); i++) {
						if (rowAl.get(i).get(0).equals(insurance_desc)) {
							rowAl.get(i).add(rs1.getString("val"));
							break;
						}
					}
				}
			}
			
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		/*
			private String ph_id;
			private String form_id;
			private String user_id;
			private ArrayList<String> cols;
			private ArrayList<ArrayList<String>> vals;
		*/
		f117fjxldvo = new Form1_17fJExcelDataVO();
		f117fjxldvo.setPh_id(ph_id);
		f117fjxldvo.setForm_id(form_id);
		f117fjxldvo.setUser_id(user_id);
		f117fjxldvo.setCols(cols);
		f117fjxldvo.setVals(rowAl);

		return f117fjxldvo;
	}
}
