/*** @author Najma on 01-Feb-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.Form1_19aJExcelDataVO;
import in.gov.wberc.vo.Form1_19aObjectVO;

public class Form1_19aJExcelDAO {
	
	@SuppressWarnings("resource")
	public Form1_19aJExcelDataVO getF1_19aData(String form_id, String util_id, String ph_id, String user_id,
			String expenditure) throws MyException, SQLException {

		Form1_19aJExcelDataVO form1_19aRef = null;
		Form1_19aObjectVO formDataValRef = new Form1_19aObjectVO();

		Connection conn1 = null;
		PreparedStatement prstmt1 = null;
		ResultSet rs1 = null;

		ArrayList<String> miscids_for_CapitalExpenditure = null;
		ArrayList<String> miscids_for_SpecialProjects = null;
		ArrayList<String> stn_ids = null;

		ArrayList<String> misc_names_for_CapitalExpenditure = null;
		ArrayList<String> misc_names_for_SpecialProjects = null;
		ArrayList<String> stn_names = null;

		ArrayList<String> misc_row_for_CapitalExpenditure = null;
		ArrayList<String> misc_row_for_SpecialProjects = null;
		ArrayList<String> station_row = null;

		ArrayList<ArrayList<String>> valAl_for_CapitalExpenditure = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_SpecialProjects = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> valAl_for_stations = new ArrayList<ArrayList<String>>();
		// For petition years
		String getPetitionYear = QueryConstants.QUERY_GET_PYID;
		// For miscs
		String getMiscIds_for_CapitalExpenditure = QueryConstants.QUERY_GET_CAPITAL_EXPENDITURE_MISCID_N_NAME;
		String getCapitalExpenditureVal = QueryConstants.QUERY_GET_CAPITAL_EXPENDITURE_VAL;

		String getMiscIds_for_SpecialProjects = QueryConstants.QUERY_GET_SPECIAL_PROJECTS_MISCID_N_NAME;
		String getSpecialProjectsVal = QueryConstants.QUERY_GET_SPECIAL_PROJECTS_VAL;
		
		// For stations
		String getStations = QueryConstants.QUERY_GET_UTILWISE_STATIONS;
		String getStationsVal = QueryConstants.QUERY_GETF1_19aSTATIONS_VAL;

		ArrayList<String> cols = null;

		try {
			conn1 = DBUtil.getConnection();
			// Fetch Petition year
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			prstmt1.setInt(1, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				cols = new ArrayList<String>();
				while (rs1.next()) {
					cols.add(rs1.getString("py_id"));
				}
			}

			// Fetch For Capital Expenditure
			prstmt1 = conn1.prepareStatement(getMiscIds_for_CapitalExpenditure);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_CapitalExpenditure = new ArrayList<String>();
				misc_names_for_CapitalExpenditure = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_CapitalExpenditure.add(rs1.getString("misc_form_label_id"));
					misc_names_for_CapitalExpenditure.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_CapitalExpenditure = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getCapitalExpenditureVal);

			for (int i = 0; i < miscids_for_CapitalExpenditure.size(); i++) {
				misc_row_for_CapitalExpenditure = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_CapitalExpenditure.get(i));
				misc_row_for_CapitalExpenditure.add(misc_names_for_CapitalExpenditure.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int int_py_id = Integer.parseInt(cols.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					rs1 = prstmt1.executeQuery();
					if (rs1 != null) {
						while (rs1.next()) {
							  if (expenditure.equals("plan")) {
								misc_row_for_CapitalExpenditure.add(rs1.getString("val_plan"));
							} else if(expenditure.equals("nonplan")){
								misc_row_for_CapitalExpenditure.add(rs1.getString("val_nonplan"));
							}
							  else if(expenditure.equals("total")){
								misc_row_for_CapitalExpenditure.add(rs1.getString("total"));
							}
						}
					}
				}
				valAl_for_CapitalExpenditure.add(misc_row_for_CapitalExpenditure);
			}
			formDataValRef.setVals_for_miscs_for_CapitalExpenditure(valAl_for_CapitalExpenditure);

			// Fetch For Special Projects
			prstmt1 = conn1.prepareStatement(getMiscIds_for_SpecialProjects);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setInt(2, Integer.parseInt(ph_id));

			rs1 = prstmt1.executeQuery();

			if (rs1 != null) {
				miscids_for_SpecialProjects = new ArrayList<String>();
				misc_names_for_SpecialProjects = new ArrayList<String>();
				while (rs1.next()) {
					miscids_for_SpecialProjects.add(rs1.getString("misc_form_label_id"));
					misc_names_for_SpecialProjects.add(rs1.getString("misc_form_label_desc"));
				}
			}

			valAl_for_SpecialProjects = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getSpecialProjectsVal);

			for (int i = 0; i < miscids_for_SpecialProjects.size(); i++) {
				misc_row_for_SpecialProjects = new ArrayList<String>();
				int int_misc_form_label_id = Integer.parseInt(miscids_for_SpecialProjects.get(i));
				misc_row_for_SpecialProjects.add(misc_names_for_SpecialProjects.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int int_py_id = Integer.parseInt(cols.get(j));
					prstmt1.setInt(1, int_py_id);
					prstmt1.setInt(2, int_misc_form_label_id);
					prstmt1.setInt(3, Integer.parseInt(form_id));
					prstmt1.setInt(4, Integer.parseInt(ph_id));

					rs1 = prstmt1.executeQuery();

					if (rs1 != null) {
						while (rs1.next()) {
							  if (expenditure.equals("plan")) {
								misc_row_for_SpecialProjects.add(rs1.getString("val_plan"));
							} else if (expenditure.equals("nonplan")){
								misc_row_for_SpecialProjects.add(rs1.getString("val_nonplan"));
							}
							  else if(expenditure.equals("total")){
								  misc_row_for_SpecialProjects.add(rs1.getString("total"));
							}
						}
					}
				}
				valAl_for_SpecialProjects.add(misc_row_for_SpecialProjects);
			}
			formDataValRef.setVals_for_miscs_for_SpecialProjects(valAl_for_SpecialProjects);

			// Fetch id and name for stations
			int int_util_id = Integer.parseInt(util_id);

			prstmt1 = conn1.prepareStatement(getStations);
			prstmt1.setInt(1, int_util_id);
			rs1 = prstmt1.executeQuery();
			if (rs1 != null) {
				stn_ids = new ArrayList<String>();
				stn_names = new ArrayList<String>();
				while (rs1.next()) {
					stn_ids.add(rs1.getString("stn_id"));
					stn_names.add(rs1.getString("stn_desc"));
				}
			}
			
			
			// Fetch val for stations
			prstmt1 = conn1.prepareStatement(getStationsVal);
			int int_form_id = Integer.parseInt(form_id);
			valAl_for_stations = new ArrayList<ArrayList<String>>();
			for ( int i = 0; i<stn_ids.size(); i++ ) {
				int int_stn_id = Integer.parseInt(stn_ids.get(i));
				station_row = new ArrayList<String>();
				station_row.add(stn_names.get(i));
				for ( int j = 0; j<cols.size(); j++ ) {
					int int_py_id = Integer.parseInt(cols.get(j));
					
					prstmt1.setInt(1, int_stn_id);
					prstmt1.setInt(2, int_py_id);
					prstmt1.setInt(3, int_form_id);
					prstmt1.setInt(4, Integer.parseInt(ph_id));
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						if (rs1.next()) {
							station_row.add(rs1.getString("val_plan"));
						}
					}
				}
				valAl_for_stations.add(station_row);
			}
			formDataValRef.setVals_for_stations(valAl_for_stations);
			
			conn1.commit();
		} catch (SQLException e) {
			conn1.rollback();
			e.printStackTrace();
			throw new MyException();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}

			if (rs1 != null) {
				rs1.close();
			}
		}

		form1_19aRef = new Form1_19aJExcelDataVO();
		form1_19aRef.setForm_id(form_id);
		form1_19aRef.setPh_id(ph_id);
		form1_19aRef.setUser_id(user_id);
		form1_19aRef.setUtil_id(util_id);
		form1_19aRef.setCols(cols);
		form1_19aRef.setExpenditure(expenditure);
		// Set misc id and val
		form1_19aRef.setMiscids_for_CapitalExpenditure(miscids_for_CapitalExpenditure);
		form1_19aRef.setMiscids_for_SpecialProjects(miscids_for_SpecialProjects);
		form1_19aRef.setVals(formDataValRef);;
		// Set station val
		form1_19aRef.setStn_ids(stn_ids);
		return FrontendFormula.appendFormula_row_1_19a(form1_19aRef);
//		return form1_19aRef;

	}


	@SuppressWarnings("resource")
	public boolean updateF1_19aData(Form1_19aJExcelDataVO data)
			throws MyException, SQLException {
		Form1_19aObjectVO data_obj = new Form1_19aObjectVO();
		// Get petition years
		ArrayList<String> cols = data.getCols();
		// Get misc ids for groups
		ArrayList<String> miscids_for_CapitalExpenditure = data.getMiscids_for_CapitalExpenditure();
		ArrayList<String> miscids_for_SpecialProjects = data.getMiscids_for_SpecialProjects();
		ArrayList<String> station_ids= data.getStn_ids();
		// Get values for groups
		data_obj = data.getVals();
		ArrayList<ArrayList<String>> values_for_CapitalExpenditure = data_obj.getVals_for_miscs_for_CapitalExpenditure();
		ArrayList<ArrayList<String>> values_for_SpecialProjects = data_obj.getVals_for_miscs_for_SpecialProjects();
		ArrayList<ArrayList<String>> values_for_Stations = data_obj.getVals_for_stations();
		// Get form id
		int formid = Integer.parseInt(data.getForm_id());
		// Get user id
		int userid = Integer.parseInt(data.getUser_id());
		// Get petition id
		int phid = Integer.parseInt(data.getPh_id());
		// Get util id
		int utilid = Integer.parseInt(data.getUtil_id());
		String expenditure = data.getExpenditure();

		// Update query for miscs
		String updateQuery_f1_19a_CapitalExpenditurePlanVal = QueryConstants.QUERY_UPDATE_CAPITAL_EXPENDITURE_PLAN_VAL;
		String updateQuery_f1_19a_CapitalExpenditureNonPlanVal = QueryConstants.QUERY_UPDATE_CAPITAL_EXPENDITURE_NONPLAN_VAL;

		String updateQuery_f1_19a_SpecialProjectsPlanVal = QueryConstants.QUERY_UPDATE_SPECIAL_PROJECTS_PLAN_VAL;
		String updateQuery_f1_19a_SpecialProjectsNonPlanVal = QueryConstants.QUERY_UPDATE_SPECIAL_PROJECTS_NONPLAN_VAL;
		// Update query for sations
		String updateQuery_f1_19a_StationsVal = QueryConstants.QUERY_UPDATE_F1_19aSTATIONS_VAL;


		Connection conn1=null;
		PreparedStatement prstmt1 = null;
		
		// Get current time
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		// Database Update Segment
		try {
			conn1=DBUtil.getConnection();
			
			// Update for Capital Expenditure
			if(expenditure.equals("plan")) {
				prstmt1 = conn1.prepareStatement(updateQuery_f1_19a_CapitalExpenditurePlanVal);
			}
			else if(expenditure.equals("nonplan")) {
				prstmt1 = conn1.prepareStatement(updateQuery_f1_19a_CapitalExpenditureNonPlanVal);
			}
			
			for ( int i = 0; i < miscids_for_CapitalExpenditure.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_CapitalExpenditure.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_CapitalExpenditure.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);
					
					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			
			// Update for Special Projects
			if(expenditure.equals("plan"))  {
				prstmt1 = conn1.prepareStatement(updateQuery_f1_19a_SpecialProjectsPlanVal);
			} else if(expenditure.equals("nonplan")) {
				prstmt1 = conn1.prepareStatement(updateQuery_f1_19a_SpecialProjectsNonPlanVal);
			}
			for (int i = 0; i < miscids_for_SpecialProjects.size(); i++) {
				int miscid = Integer.parseInt(miscids_for_SpecialProjects.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int pyid = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_SpecialProjects.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, pyid);
					prstmt1.setInt(5, miscid);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			int int_stn_id = -1;
			int int_py_id = -1;
			prstmt1 = conn1.prepareStatement(updateQuery_f1_19a_StationsVal);
			for (int i = 0; i < station_ids.size(); i++) {
				int_stn_id = Integer.parseInt(station_ids.get(i));
				for (int j = 0; j < cols.size(); j++) {
					int_py_id = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values_for_Stations.get(i).get(j + 1));
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, userid);
					prstmt1.setInt(4, int_stn_id);
					prstmt1.setInt(5, int_py_id);
					prstmt1.setInt(6, formid);
					prstmt1.setInt(7, phid);

					prstmt1.addBatch();
				}
			}
			prstmt1.executeBatch();
			
			
			
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
		}
		
		return true;
	}




}
