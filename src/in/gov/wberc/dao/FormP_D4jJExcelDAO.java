/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.FormP_D4jJExcelDataVO;

public class FormP_D4jJExcelDAO {
	
	@SuppressWarnings("resource")
	public FormP_D4jJExcelDataVO getFormP_D4_JExcelData(String stn_id, String ph_id, String form_id, String user_id) throws SQLException, MyException{
		FormP_D4jJExcelDataVO fpd4_jxl_vo = null;
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		ArrayList<String> col = null;
		ArrayList<String> col_names = null;
		ArrayList<String> row = null;
		ArrayList<String> row_names = null;
		ArrayList<String> misc_row = null;
		ArrayList<ArrayList<String>> valAl = new ArrayList<ArrayList<String>>();
		
		String getMiscIdsForRowNColumn = QueryConstants.QUERY_GET_MISCIDS_FOR_ROW_COLUMN;
		String getFormPD4Val = QueryConstants.QUERY_GET_F_P_D4_VAL;
		
		try {
			conn1=DBUtil.getConnection();
			
			// Get Column misc id & name here
			prstmt1 = conn1.prepareStatement(getMiscIdsForRowNColumn);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setBoolean(2, true);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				col = new ArrayList<String>();
				col_names = new ArrayList<String>();
				while(rs1.next()) {
					col.add(rs1.getString("misc_form_label_id"));
					col_names.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			// Get row misc id & name here
			prstmt1 = conn1.prepareStatement(getMiscIdsForRowNColumn);
			prstmt1.setInt(1, Integer.parseInt(form_id));
			prstmt1.setBoolean(2, false);
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				row = new ArrayList<String>();
				row_names = new ArrayList<String>();
				while(rs1.next()) {
					row.add(rs1.getString("misc_form_label_id"));
					row_names.add(rs1.getString("misc_form_label_desc"));
				}
			}
			
			
			// Get value for misc ids
			valAl = new ArrayList<ArrayList<String>>();
			prstmt1 = conn1.prepareStatement(getFormPD4Val);
			
			for (int i = 0; i < row.size(); i++) {
				misc_row =  new ArrayList<String>();
				int int_misc_id_row = Integer.parseInt(row.get(i));
				misc_row.add(row_names.get(i));
				for ( int j = 0; j < col.size(); j++) {
					int int_misc_id_col = Integer.parseInt(col.get(j));
					
					prstmt1.setInt(1, Integer.parseInt(stn_id));
					prstmt1.setInt(2, int_misc_id_col);
					prstmt1.setInt(3, int_misc_id_row);
					prstmt1.setInt(4, Integer.parseInt(form_id));
					prstmt1.setInt(5, Integer.parseInt(ph_id));
					
					rs1=prstmt1.executeQuery();
					
					if(rs1!=null) {
						while(rs1.next()) {
							misc_row.add(rs1.getString("val"));
						}
					}
				}
				valAl.add(misc_row);
			}
			
			conn1.commit();	
		}
		catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(conn1!=null) {
				conn1.close();
			}
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
		}
		
		fpd4_jxl_vo = new FormP_D4jJExcelDataVO(); 
		fpd4_jxl_vo.setForm_id(form_id);
		fpd4_jxl_vo.setPh_id(ph_id);
		fpd4_jxl_vo.setUser_id(user_id);
		fpd4_jxl_vo.setStn_id(stn_id);
		fpd4_jxl_vo.setCols(col);
		fpd4_jxl_vo.setColNames(col_names);
		fpd4_jxl_vo.setMiscids(row);
		fpd4_jxl_vo.setVals(valAl);
		
		return fpd4_jxl_vo;
	}

	
	
//	Data Update
	public boolean updateF_P_D4_JExcelData(FormP_D4jJExcelDataVO data)
			throws MyException, SQLException {
		
		int stn_id = Integer.parseInt(data.getStn_id());
		int form_id = Integer.parseInt(data.getForm_id());
		int ph_id = Integer.parseInt(data.getPh_id());
		int user_id = Integer.parseInt(data.getUser_id());

		ArrayList<String> rows = data.getMiscids();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();

		String updateFormPD4 = QueryConstants.UPDATE_F_P_D4_VAL;
		Connection conn1=null;
		PreparedStatement prstmt1 = null;

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(updateFormPD4);
			for ( int i = 0; i < rows.size(); i++) {
				int misc_row_id = Integer.parseInt(rows.get(i));
				for ( int j = 0; j < cols.size(); j++) {
					int misc_col_id = Integer.parseInt(cols.get(j));
					double val = Double.parseDouble(values.get(i).get(j + 1));
					
					prstmt1.setDouble(1, val);
					prstmt1.setTimestamp(2, timestamp);
					prstmt1.setInt(3, user_id);
					prstmt1.setInt(4, stn_id);
					prstmt1.setInt(5, misc_col_id);
					prstmt1.setInt(6, misc_row_id);
					prstmt1.setInt(7, form_id);
					prstmt1.setInt(8, ph_id);
					
					prstmt1.addBatch();
				}
			}
			
			prstmt1.executeBatch();
			conn1.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if (conn1 != null) {
				conn1.close();
			}
			if (prstmt1 != null) {
				prstmt1.close();
			}
		}
		
		return true;
	}
}
