package in.gov.wberc.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import in.gov.wberc.exception.MyException;
import in.gov.wberc.vo.Form1_18JExcelDataVO_1;
import in.gov.wberc.vo.Form1_18JExcelDataVO_2;
import in.gov.wberc.vo.Form1_20JExcelDataVO;

public class FormCalculationDAO {

	public static ArrayList<String> getComputationResultFrom_F1_20a(String ph_id, String util_id, String form_id, String user_id) 
			throws MyException, SQLException {

		// Original data computation required.
		Form1_20JExcelDataVO data = new Form1_20aJExcelDAO().getF1_20aJExcelData(ph_id, util_id, form_id, user_id);
		
		ArrayList<String> cols = data.getCols();
		ArrayList<String> miscids = data.getMiscids();
		ArrayList<ArrayList<String>> values = data.getVals();
		if (values == null) {
			System.out.println("No data exists in Form 20(a)");
			String[] array = {"Average Equity Base (a)", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000"};
			return new ArrayList<String>(Arrays.asList(array));
		}
		
		ArrayList<String> result = new ArrayList<String>();
		
		try {
			
			result.add("Average Equity Base (a)");
			
			double line_4[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// 1+3
				double cell_val1 = Double.parseDouble(values.get(0).get(i + 1)); // 1
				double cell_val2 = Double.parseDouble(values.get(2).get(i + 1)); // 3
				line_4[i] = cell_val1 + cell_val2;
			}
			
			double line_7[] = new double[8];
			ArrayList<String> get1_18Calculation = get1_18Calculation("39", util_id, ph_id, user_id); // form_id of 1.18 is 39
			for ( int i = 0; i<cols.size(); i++ ) {
				// 5-6
				double cell_val1 = Double.parseDouble(get1_18Calculation.get(i + 1)); // 5
				double cell_val2 = Double.parseDouble(values.get(3).get(i + 1)); // 6
				line_7[i] = cell_val1 - cell_val2;
			}
			
			double line_8[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// 30% of line 7
				line_7[i] = line_8[i] * 0.3;
			}
			
			double line_9[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// lower of 3 and 8
				double cell_val = Double.parseDouble(values.get(2).get(i + 1)); // 3
				line_9[i] = (cell_val<line_8[i] ? cell_val : line_8[i]);
			}
			
			double line_11[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// (9 + 10)
				double cell_val = Double.parseDouble(values.get(4).get(i + 1)); // 10
				line_11[i] = line_9[i] + cell_val;
			}
			
			double line_12[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// (2 + 11)
				double cell_val = Double.parseDouble(values.get(1).get(i + 1)); // 2
				line_12[i] = cell_val + line_11[i];
			}
			
			double line_13[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// (2 + 12)/2
				double cell_val = Double.parseDouble(values.get(1).get(i + 1)); // 2
				line_13[i] = (cell_val + line_12[i]) / 2;
			}
			
			for ( int i = 0; i<cols.size(); i++ ) {
				result.add(String.valueOf(line_13[i]));
			}
		
		} catch(Exception ex) {
			ex.printStackTrace();
			String[] array = {"Average Equity Base (a)", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000"};
			return new ArrayList<String>(Arrays.asList(array));
		}
		
		return result;
	}
	
	public static ArrayList<String> get1_18Calculation(String form_id, String util_id, String ph_id, String user_id) 
			throws MyException, SQLException {
		
		Form1_18JExcelDataVO_1 f1_18jxldao_1 = new Form1_18JExcelDAO_1().getF1_18Data_1( form_id, util_id, ph_id, user_id );
		
		ArrayList<String> cols = f1_18jxldao_1.getCols(); // py_id(s)
		ArrayList<String> rows1 = f1_18jxldao_1.getRows1(); // stn_id(s) + formula
		ArrayList<String> rows2 = f1_18jxldao_1.getRows2(); // misc_form_label_id(s)
		Form1_18JExcelDataVO_2 val = f1_18jxldao_1.getVal();
		ArrayList<ArrayList<String>> station_vals = val.getStation_vals();
		ArrayList<ArrayList<String>> misc_vals = val.getMisc_vals();
		
		if (val == null || station_vals == null || station_vals.size() == 0 || misc_vals == null || misc_vals.size() == 0) {
			System.out.println("No data exists in Form 1.18");
			String[] array = {"Net Addition to the original cost of fixed assets during the year (vide submission in form 1.18)", 
					"0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000"};
			return new ArrayList<String>(Arrays.asList(array));
		}
		
		ArrayList<String> result = new ArrayList<String>();
		try
		{
		
			result.add("Net Addition to the original cost of fixed assets during the year (vide submission in form 1.18)");
			
			double calc_line[] = new double[8];
			for ( int i = 0; i<cols.size(); i++ ) {
				// Sum for all stations excluding the last row
				for ( int j = 0; j<rows1.size()-1; j++ ) {
					double cell_val = Double.parseDouble(station_vals.get(j).get(i + 1));
					calc_line[i] += cell_val;
				}
			}
			
			for ( int i = 0; i<cols.size(); i++ ) {
				// Sum for all misc. rows
				for ( int j = 0; j<rows2.size(); j++ ) {
					double cell_val = Double.parseDouble(station_vals.get(j).get(i + 1));
					calc_line[i] += cell_val;
				}
			}
			
			for ( int i = 0; i<cols.size(); i++ ) {
				result.add(String.valueOf(calc_line[i]));
			}
		
		} catch(Exception ex) {
			ex.printStackTrace();
			String[] array = {"Net Addition to the original cost of fixed assets during the year (vide submission in form 1.18)", 
					"0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000", "0.000"};
			return new ArrayList<String>(Arrays.asList(array));
		}
		
		return result;
		
	}
	
}
