package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.StationF1_3VO;
import in.gov.wberc.vo.StationListF1_3VO;

public class StationForComboDAO {

	public StationListF1_3VO getStationsForCombo(String utilid) throws MyException, SQLException {
		
		String query1 = QueryConstants.QUERY_STATION_FOR_COMBO;
		
		PreparedStatement prstmt1 = null;
		Connection conn1=null;
		ResultSet rs1=null;
		
		// Getting Type IDs + Type Names for Stations
		StationListF1_3VO slf13vo = null;
		ArrayList<StationF1_3VO> sf13Al = null;
		
		try {
			conn1=DBUtil.getConnection();
			
			prstmt1 = conn1.prepareStatement(query1);
			prstmt1.setInt(1, Integer.parseInt(utilid));
			rs1=prstmt1.executeQuery();
			
			if ( rs1 != null ) {
				sf13Al = new ArrayList<StationF1_3VO>();
				while ( rs1.next() ) {
					StationF1_3VO sf13 = new StationF1_3VO();
					sf13.setId(rs1.getString("stationid"));
					sf13.setName(rs1.getString("stationnm"));
					sf13Al.add(sf13);
				}
			}
			conn1.commit();
		}catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		}finally{
				if(conn1!=null) {
				conn1.close();
				}
				if(prstmt1!=null) {
				prstmt1.close();
				}
				
				if(rs1!=null) {
					rs1.close();
				}
		}
		slf13vo = new StationListF1_3VO();
		slf13vo.setStations(sf13Al);
		return slf13vo;
		
	}
	
}
