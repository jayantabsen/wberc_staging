package in.gov.wberc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.utils.DBUtil;
import in.gov.wberc.vo.From1_1aJExcelDataVO;
import in.gov.wberc.vo.StationNUnitValVO;

public class Form1_1aJExcelDAO {

	public From1_1aJExcelDataVO getF1_1aJExcelData(String phid, String utilid, String formid) throws MyException, SQLException {

	From1_1aJExcelDataVO f11ajxlvo = null;
		
		PreparedStatement prstmt1 = null;
		
		Connection conn1=null;
		
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
		
		ArrayList<String> cols = null;
		ArrayList<String> rows = null;
		ArrayList<StationNUnitValVO> vals = null;
		StationNUnitValVO unit_rowvo = null;
		ArrayList<ArrayList<String>> valForStations = null;
		ArrayList<String> unit_row = null;
//		Queries
		String getPetitionYear = QueryConstants.QUERY_GET_PETTION_YEAR;
		String getStations = QueryConstants.QUERY_GET_STATIONID_STATIONNM;
		String getStationsForCommission = QueryConstants.QUERY_GET_STATIONSF11a_FOR_UTIL0;
		String getUnitsPerStation = QueryConstants.QUERY_GET_UNITS_PER_STATION;
		String getValPerUnit = QueryConstants.QUERY_GET_F1_1aUNITWISE_VALUE;

		int formId = Integer.parseInt(formid);

		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(getPetitionYear);
			
			prstmt1.setInt(1, Integer.parseInt(phid));
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				cols = new ArrayList<String>();
				while(rs1.next()) {
					cols.add(rs1.getString("py_id"));
				}
			}
			
			rows = new ArrayList<String>();
			vals = new ArrayList<StationNUnitValVO>();
			
			int int_utilid = Integer.parseInt(utilid);
			if (int_utilid != 0) {
				prstmt1 = conn1.prepareStatement(getStations);
				prstmt1.setInt(1, int_utilid);

			} else {
				prstmt1 = conn1.prepareStatement(getStationsForCommission);

			}
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				while(rs1.next()) {
					unit_rowvo = new StationNUnitValVO();
					int stn_id = Integer.parseInt(rs1.getString("stn_id"));
					unit_rowvo.setStation(rs1.getString("stn_desc"));
					
						prstmt1 = conn1.prepareStatement(getUnitsPerStation);
						prstmt1.setInt(1, stn_id);
					
						rs2=prstmt1.executeQuery();
					
					if(rs2!=null) {
						valForStations = new ArrayList<ArrayList<String>>();
						while(rs2.next()) {
							unit_row = new ArrayList<String>();
							unit_row.add(rs2.getString("unit_desc"));
							String unit_id = rs2.getString("unit_id");
							rows.add(unit_id);
							int int_unit_id = Integer.parseInt(unit_id);
							
							for ( int i = 0; i<cols.size(); i++ ) {
								prstmt1 = conn1.prepareStatement(getValPerUnit);
								prstmt1.setInt(1, int_unit_id);
								prstmt1.setInt(2, Integer.parseInt(cols.get(i)));
								rs3=prstmt1.executeQuery();
								if(rs3!=null) {
									if(rs3.next()) {
										unit_row.add(rs3.getString("val"));
									}
								}
							}
							
							valForStations.add(unit_row);
						}
						unit_rowvo.setUnitValForStation(valForStations);
					}
					vals.add(unit_rowvo);
				}
			}
			
			f11ajxlvo = new From1_1aJExcelDataVO();
			f11ajxlvo.setFormid(formid);
			f11ajxlvo.setRows(rows);
			f11ajxlvo.setCols(cols);
			f11ajxlvo.setVals(vals);
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			conn1.rollback();
		} finally {
			if(prstmt1!=null) {
				prstmt1.close();
			}
			if(conn1!=null) {
				conn1.close();
			}
			if(rs1!=null) {
				rs1.close();
			}
			if(rs2!=null) {
				rs2.close();
			}
			if(rs3!=null) {
				rs3.close();
			}
		}
		
		return f11ajxlvo;
	}
	
}
