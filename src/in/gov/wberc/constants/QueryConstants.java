package in.gov.wberc.constants;

public class QueryConstants {
	
	public static String QUERY_LOGIN_AUTH = "select pwd from m_user where login_id = ? ORDER BY login_id";
	
	public static String QUERY_FIRST_TIME_USER_CHECK = "select login_id from t_user_token where login_id=?";
	
	public static String QUERY_INSERT_USER = "insert into t_user_token (utoken_id, login_id, from_date, "
			+ "to_date, token) values (?, ?, (SELECT CURRENT_DATE), (SELECT CURRENT_DATE + INTERVAL '30 day'), ?)";
	
	public static String QUERY_TOKEN_VALIDITY = "select token from t_user_token where token = ? and CURRENT_DATE between from_date and to_date";
	
	public static String QUERY_UPDATE_USER_TOKEN_VALIDITY="update t_user_token set from_date = " 
			+ "(SELECT CURRENT_DATE), to_date = (SELECT CURRENT_DATE + INTERVAL '30 day') where login_id = ?";
	
	public static String QUERY_GET_TOKEN_FROM_USER = "select token from t_user_token where login_id=?";
	
	public static String QUERY_DELETE_TOKEN_ON_LOGOUT = "DELETE FROM t_user_token WHERE login_id =? and token = ?";
	
	public static String QUERY_GET_USER_FROM_TOKEN = "select login_id from t_user_token where token = ?";
	
	public static String QUERY_UPDATE_USER_PASSWORD = "update m_user SET pwd=? where login_id=?";
	
	public static String QUERY_VALIDATE_UTILID = "SELECT count(*) as no FROM m_utility MU WHERE MU.util_id=?";
	
	/*public static String QUERY_GET_UNITID = "SELECT distinct ST.unitid FROM m_utility_station_type ST,"
			+ " m_utility_station SM WHERE ST.stypeid = SM.stypeid and ST.isactive=1 ORDER BY ST.stypeid";	
	*/
	
	public static String QUERY_STATION_UNIT = "SELECT unit_of_measurement FROM m_form WHERE form_id=?";
	
	//public static String QUERY_VALIDATE_STATIONID = "SELECT count(*) as no FROM m_utility_station MUS WHERE MUS.stationid=?";
	
	//public static String QUERY_GET_STATION_NAME = "SELECT MUS.stationnm AS stationnm FROM m_utility_station MUS WHERE MUS.stationid=?";
	
	//public static String QUERY_GET_TIME_OF_DAYIDS = "SELECT todid FROM m_global_time_of_day ORDER BY todid";
	
	//public static String QUERY_GET_SINGLE_SEASONNM = "SELECT seasonnm FROM m_global_season WHERE seasonid=?";
	
	//public static String QUERY_GET_SINGLE_TIME_OF_DAY = "SELECT timeofday FROM m_global_time_of_day WHERE todid=?";

	//public static String QUERY_VALIDATE_PUMP_STATIONID = "SELECT count(*) as no FROM m_utility_pump_station  MUPS WHERE MUPS.pump_stationid=?";

	public static String QUERY_SOURCE_FOR_COMBO = "SELECT eps_id,source_nm FROM m_eng_purchasing_source WHERE util_id=?";
	
	public static String QUERY_SOURCE_DETAILS = "SELECT eps_id, source_nm FROM m_energy_purchase_source where eps_id=?";
	
	public static String QUERY_SOURCE_UNIT = "SELECT unit_of_measurement FROM m_form WHERE form_id=?";
	
	//public static String QUERY_GET_PBY = "SELECT pbyid, base_year FROM m_petition_base_year order by pbyid";
	
	public static String QUERY_GET_USER_MENUS = "SELECT menu_code FROM mp_user_role, mp_menu_role, m_menu WHERE "
			+ "mp_user_role.role_id=mp_menu_role.role_id AND mp_menu_role.menu_id=m_menu.menu_id AND "
			+ "mp_user_role.user_id=?";
	
	public static String QUERY_GET_PETITION_YEARS = "SELECT PY.year_nm, PY.seq FROM t_petition_year	 PY  WHERE PY.ph_id=? order by PY.seq";
	
	public static String QUERY_VALIDATE_PDID = "SELECT count(*) as no FROM t_petition_header PD WHERE PD.ph_id=?";
	
	public static String QUERY_GET_PYID = "SELECT py_id FROM t_petition_year where ph_id=? and isactive ='TRUE' order by seq";
	
	public static String QUERY_VALIDATE_FORMID = "SELECT count(*) as no FROM m_form MF WHERE MF.form_id=?";
	
	public static String QUERY_GET_MISC_NAME = "SELECT misc_form_label_desc, remarks FROM m_misc_form_labels WHERE misc_form_label_id=?";
	
	public static String QUERY_VALIDATE_MISCID = "SELECT count(*) as no FROM m_misc_form_labels MF WHERE MF.misc_form_label_id=?";
	
	public static String QUERY_GET_MONTH = "SELECT month_id FROM m_month order by seq";
	
	
	
	public static String QUERY_GET_MONTHID_N_PYID = "SELECT py_id, year_nm FROM t_petition_year where ph_id=? and seq>0 and isactive = 'TRUE'";
	
	public static String QUERY_STATION_DETAILS = "SELECT stn_id, stn_desc, capacity FROM m_util_stn  where stn_id=?";
	
	public static String QUERY_STATION_FOR_COMBO = "SELECT ST.stn_id as stationid, ST.stn_desc as "
			+ "stationnm FROM m_util_stn ST WHERE ST.util_id=? ORDER BY ST.stype_id, ST.stn_id";
	
	public static String QUERY_VALIDATE_STATIONS = "SELECT count(*) as no FROM m_util_stn MU WHERE MU.stn_id=?";
	
	public static String QUERY_GET_STATIONS = "SELECT ST.stype_id as stypeid, ST.stn_id as stationid,"
			+ " ST.stn_desc as stationnm FROM m_util_stn ST, m_util_stn_type STY WHERE "
			+ "ST.stype_id=STY.stype_id AND ST.util_id=? ORDER BY STY.stype_id ";
//	1.1
	public static String QUERY_GET_F1_1STATIONWISE_VALUE = "SELECT val FROM t_arrdf1_1 WHERE stn_id=? and py_id=? ORDER BY py_id";
	
	public static String QUERY_GET_TYPES = "SELECT distinct ST.stype_id FROM m_util_stn_type ST,"
			+ " m_util_stn SM WHERE ST.stype_id = SM.stype_id and ST.isactive=1 ORDER BY ST.stype_id";
	
	public static String QUERY_UPDATE_FORM11_MATRIX = "update t_arrdf1_1 set val=? where stn_id=? and py_id=?";

//	1.1a
	public static String QUERY_GET_STATIONSF11a = "SELECT ST.stn_id as stationid FROM m_util_stn"
			+ " ST WHERE ST.util_id=? ORDER BY ST.stn_id";
	
	public static String QUERY_GET_STATIONSF11a_FOR_UTIL0 = "SELECT ST.stn_id as stationid FROM m_util_stn"
			+ " ST  ORDER BY ST.stn_id";
	
	public static String QUERY_GET_UNITS = "SELECT UT.stn_id as stationid, UT.unit_id as unitid, "
			+ "UT.unit_desc as unitnm FROM m_util_unit UT, m_util_stn ST WHERE UT.stn_id="
			+ "ST.stn_id AND ST.util_id=? ORDER BY ST.stn_id, UT.unit_id";
	
	public static String QUERY_GET_F1_1aUNITWISE_VALUE = "SELECT DF.val as val"
			+ " FROM t_arrdf1_1a DF WHERE  DF.unit_id=? and DF.py_id=? ORDER BY DF.py_id";
	
	
//	1.2
	public static String QUERY_GET_F1_2STATIONWISE_VALUE = "SELECT  DF.val as val"
			+ " FROM t_arrdf1_2 DF WHERE DF.stn_id=? and DF.arrpyid=?  ORDER BY DF.arrpyid";
	
	
//	1.2a
	public static String QUERY_GET_F1_2aUNITWISE_VALUE = "SELECT DF.val as val"
			+ " FROM t_arrdf1_2a DF WHERE  DF.unit_id=? and DF.py_id=? ORDER BY DF.py_id";
	
	
//	1.3
	public static String QUERY_GET_SEASONNM = "SELECT season_desc FROM m_season WHERE season_id=? and isactive ='TRUE'";
	
	public static String QUERY_GET_SEASONIDS = "SELECT season_id FROM m_season where isactive ='TRUE' ORDER BY season_id";
	
	public static String QUERY_GET_PERIOD_SEASON_WISE = "SELECT period_id FROM m_period WHERE season_id=? ";
	
	public static String QUERY_GET_PERIOD_NAME = "SELECT period_desc FROM m_period WHERE period_id=?";
	
	public static String QUERY_GET_F1_3USAGEWISE_VALUE = "SELECT DF.val as val FROM t_arrdf1_3 DF "
			+ "WHERE DF.arrstationid=? and DF.arrseasonid=? and DF.arrperiodid=? and DF.arrpyid=? ORDER BY DF.arrpyid";
	
	public static String QUERY_UPDATE_F13 = "UPDATE t_arrdf1_3 SET val=? WHERE stn_id=? AND period_id=? AND py_id=?";
	
	public static String QUERY_GET_SEASONWISE_SUM_OF_VAL = "SELECT SUM(val) as sumval FROM t_arrdf1_3 WHERE "
			+ "arrpyid=? and arrseasonid=?";
	
//	1.4a
	public static String QUERY_GET_SEASONWISE_F14a_DATA = "SELECT val FROM t_arrdf1_4a WHERE stn_id=? AND "
			+ "py_id=? AND season_id=? ORDER BY season_id, py_id";
	
	public static String QUERY_UPDATE_F14a = "UPDATE t_arrdf1_4a SET val=? WHERE stn_id=? AND season_id=? AND py_id=?";
	
	public static String QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_4a = "SELECT SUM(val) as sumval FROM t_arrdf1_4a WHERE py_id=? and season_id=?";
	
//	1.4b
	public static String QUERY_PUMP_STATION_FOR_COMBO = "SELECT PST.pump_stn_id as pump_stn_id, "
			+ "PST.pump_stn_desc as pump_stn_desc FROM m_util_pump_stn PST WHERE PST.util_id=? "
			+ "ORDER BY PST.pump_stn_desc";
	
	public static String QUERY_PUMP_STATION_DETAILS = "SELECT pump_stn_id, pump_stn_desc, capacity FROM m_util_pump_stn  where pump_stn_id=?";
	
	public static String GET_MEASURING_UNIT = "SELECT unit_of_measurement FROM m_form where form_id=?";
	
	public static String QUERY_GET_F1_4bUSAGEWISE_VALUE = "SELECT val FROM t_arrdf1_4b WHERE pump_stn_id=? "
			+ "and period_id=? and py_id=? ORDER BY py_id";
	
	public static String QUERY_UPDATE_F14b = "UPDATE t_arrdf1_4b SET val=? WHERE pump_stn_id=? AND period_id=? AND py_id=?";
	
	public static String QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_4b = "SELECT SUM(val) as sumval FROM t_arrdf1_4b WHERE "
			+ "arrpyid=? and arrseasonid=?";
	
//	1.5
	
	public static String QUERY_STATION_TYPES = "SELECT stype_id, stype_desc FROM m_util_stn_type WHERE isactive=1";
	
	public static String QUERY_STATIONS = "SELECT stn_desc FROM m_util_stn WHERE stype_id=?";
	
	public static String QUERY_GET_STATIONS11a = "SELECT ST.stype_id as stypeid, ST.stn_id as stationid,"
			+ " ST.stn_desc as stationnm FROM m_util_stn ST, m_util_stn_type STY WHERE "
			+ "ST.stype_id=STY.stype_id ORDER BY STY.stype_id, ST.stn_id";
	
	public static String QUERY_UNITS = "SELECT unit_desc FROM m_util_unit WHERE stn_id=? ORDER BY unit_id";
	
	public static String QUERY_GET_ALL_PHID = "SELECT ph_id FROM t_petition_header ORDER BY base_year";
	
	//Changed for dynamic menulist depending on role_id...Priyanka..11.01.2018
	public static String QUERY_GET_MENUS = "SELECT m.menu_id as pk, m.menu_code as id, m.menu_list_loc "
			+ "as menu_loc, m.menu_desc as name, m.menu_desc as parent_name, m.level as level, m.link as link "
			+"FROM m_menu m WHERE m.menu_id IN (SELECT mr.menu_id FROM mp_menu_role mr WHERE mr.role_id = ? AND mr.isactive ='TRUE') "
			+ "AND m.level=? ORDER BY m.menu_id "; 
	
	//Changed for user credentials...Priyanka..11.01.2018
	public static String QUERY_GET_USER_CREDENTIALS = "SELECT MUser.user_id, MUser.user_fnm as fnm, "
			+ "MUser.user_mnm as mnm, MUser.user_lnm as lnm, MUser.user_mail, MUser.user_phone, MUser.login_id, "
            + "MUser.last_login, MUser.entity_id, ur.role_id, r.role_code, r.role_desc, u.util_desc "
            + "FROM  mp_user_role ur, m_role r ,m_user MUser LEFT JOIN m_utility u ON MUser.entity_id = u.util_id " 
            + "WHERE MUser.login_id=? AND ur.user_id = MUser.user_id AND ur.role_id = r.role_id  AND MUser.isactive='TRUE' ";
	
	//Changed for dynamic menulist depending on role_id...Priyanka..12.01.2018
	public static String QUERY_GET_MENUS1 = "SELECT m.menu_id as pk, m.menu_code as id, m.menu_list_loc as menu_loc, "
			+ "m.menu_desc as name, m.menu_desc as parent_name, m.level as level, m.link as link  "
			+ "FROM m_menu m WHERE m.menu_id IN (SELECT mr.menu_id FROM mp_menu_role mr WHERE mr.role_id = ? AND mr.isactive ='TRUE')  "
			+ "AND m.level=? AND m.parent_menu_id =? ORDER BY m.menu_id ";
	
	// JBS -- Create New Petition
	public static String QUERY_INSERT_t_arr_form = "insert into t_form_instance (arrformid, form_id, util_id, ph_id, "
			+ "isfinished, created_on, created_by, updated_on, updated_by, isactive) values (?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?)";
	
	
	//File Upload ***** JBS
	public static final String QUERY_GET_PETITION_YEAR_FORM_PHID = "select base_year from t_petition_header where ph_id=?";
	
//	1.2 added
	public static String QUERY_GET_STATION_TYPES = "select distinct m_util_stn_type.stype_id, stype_desc from m_util_stn_type, "
			+ " m_util_stn where m_util_stn_type.stype_id = m_util_stn.stype_id and util_id=? order by m_util_stn_type.stype_id";
	
	public static String QUERY_GET_STATION_TYPES_FOR_UTIL0 = "select distinct m_util_stn_type.stype_id, stype_desc from m_util_stn_type, m_util_stn "
			+ " where m_util_stn_type.stype_id = m_util_stn.stype_id  order by m_util_stn_type.stype_id";

	public static String QUERY_GET_STATIONS_PER_STATION_TYPE = "select stn_id, stn_desc from m_util_stn where stype_id=? and util_id=? order by stn_id";
	
	public static String QUERY_GET_STATIONS_PER_STATION_TYPE_FOR_UTIL0 = "select stn_id, stn_desc from m_util_stn where stype_id=? order by stn_id";

	public static String QUERY_GET_PETTION_YEAR = "SELECT py_id FROM t_petition_year where ph_id=? order by seq";
	
	public static String QUERY_GET_VAL_STATION_WISE = "SELECT val FROM t_arrdf1_2 WHERE stn_id=? and py_id=?";

//	1.1a added
	public static String QUERY_GET_UNITS_PER_STATION = "select unit_desc, unit_id from m_util_unit where stn_id = ?";
	
	public static String QUERY_GET_STATIONID_STATIONNM = "SELECT ST.stn_id, ST.stn_desc FROM m_util_stn"
			+ " ST WHERE ST.util_id=? ORDER BY ST.stn_id";
	
	
	
	
	
	
	
	// Added/Altered after modification of the database tables on 18/01/2018
	// Added by JBS to rectify error after substituting QueryConstant file from Prianka
	public static String QUERY_GET_PBY = "SELECT py_id, year_nm FROM t_petition_year where ph_id=? order by seq";
	public static String QUERY_GET_UTILITY_STATION_TYPE_V1 = "select distinct m_util_stn_type.stype_id, m_util_stn_type.stype_desc from "
			+ "m_util_stn_type, m_util_stn where m_util_stn_type.stype_id = m_util_stn.stype_id and util_id=? order by "
			+ "m_util_stn_type.stype_id";
	public static String QUERY_GET_UTILITY_STATION_TYPE_V2 = "select distinct m_util_stn_type.stype_id, m_util_stn_type.stype_desc from "
			+ "m_util_stn_type, m_util_stn where m_util_stn_type.stype_id = m_util_stn.stype_id order by m_util_stn_type.stype_id";
	public static String QUERY_GET_UTILITY_STATION_V1 = "select stn_id, stn_desc from m_util_stn where stype_id=? and util_id=? order by stn_id";
	public static String QUERY_GET_UTILITY_STATION_V2 = "select stn_id, stn_desc from m_util_stn where stype_id=? order by stn_id";
	// JBS for 1.1a
	public static String QUERY_GET_F1_1_DATA_VAL = "SELECT val FROM t_arrdf1_1 WHERE stn_id=? and py_id=?";
	public static String QUERY_UPDATE_FORM1_1a_MATRIX = "update t_arrdf1_1a set val=? where unit_id=? and "
			+ "py_id=? and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
			+ "form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	// JBS for 1.2
	public static String QUERY_GET_F1_2_DATA_VAL = "SELECT val FROM t_arrdf1_2 WHERE stn_id=? and py_id=?";
	// JBS for 1.2a
	public static String QUERY_UPDATE_FORM12a_MATRIX = "update t_arrdf1_2a set val=? where unit_id=? and "
			+ "py_id=? and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
			+ "form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	public static String QUERY_GET_F1_3_DATA_VAL = "SELECT DF.val as val FROM t_arrdf1_3 DF WHERE "
			+ "DF.stn_id=? and DF.period_id=? and DF.py_id=? ORDER BY DF.py_id";
	
	// 21/01/2018
	public static String QUERY_GET_ATTACHMENT_TYPE = "SELECT attachment_type_desc FROM m_attachment_type where "
			+ "attachment_type_id=? AND isactive ='TRUE'";
	public static String QUERY_GET_F1_3_GROSS_DATA_VAL = "SELECT sum(val) as val FROM t_arrdf1_3 WHERE period_id=? and py_id=?";
	public static String QUERY_GET_F1_4b_GROSS_DATA_VAL = "SELECT sum(val) as val FROM t_arrdf1_4b WHERE period_id=? and py_id=?";
	public static String QUERY_GET_MISCIDS = "SELECT misc_form_label_id, misc_form_label_desc, remarks FROM m_misc_form_labels where "
			+ "form_id=? order by misc_form_label_id";
	public static String QUERY_GET_F1_6c_VAL = "SELECT val FROM t_arrdf1_6c where py_id=? and month_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F1_6c = "UPDATE t_arrdf1_6c SET val=? WHERE py_id=? and month_id=? and misc_form_label_id=?";
	public static String QUERY_GET_F1_11_VAL = "SELECT val FROM t_arrdf1_11  where stn_id =? and py_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F1_11 = "UPDATE t_arrdf1_11 SET val=? WHERE stn_id=? AND py_id=? AND misc_form_label_id=?";
	public static String QUERY_GET_F1_12_VAL = "SELECT val FROM t_arrdf1_12 where py_id=? and stn_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F112 = "UPDATE t_arrdf1_12 SET val=? where py_id=? and stn_id=? and misc_form_label_id=?";
	public static String QUERY_GET_F1_17g_VAL = "SELECT val FROM t_arrdf1_17g where py_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F1_17g = "UPDATE t_arrdf1_17g SET val=? where py_id=? and misc_form_label_id=?";
	public static String QUERY_GET_F1_18a_VAL = "SELECT val FROM t_arrdf1_18a where py_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F1_18a = "UPDATE t_arrdf1_18a SET val=? where py_id=? and misc_form_label_id=?";
	public static String QUERY_GET_F1_24_VAL = "SELECT val FROM t_arrdf1_24 where py_id=? and misc_form_label_id=?";
	public static String QUERY_UPDATE_F1_24 = "UPDATE t_arrdf1_24 SET val=? where py_id=? and misc_form_label_id=?";
	public static String QUERY_VALIDATE_PUMP_STATIONS = "SELECT count(*) as no FROM m_util_pump_stn MU WHERE MU.pump_stn_id=?";
	public static String QUERY_GET_MISCIDS_FOR_1_17j = "SELECT misc_form_label_id, misc_form_label_desc, remarks FROM m_misc_form_labels where "
			+ "form_id=? and iscolumn=? order by misc_form_label_id";
	public static String QUERY_GET_F1__17jVAL = "SELECT val FROM t_arrdf1_17j where py_id=? and misc_form_label_id_1=? and "
			+ "misc_form_label_id_2=? and form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_VALIDATE_PY_ID = "SELECT count(*) as no FROM t_petition_year WHERE py_id=?";
	public static String QUERY_GET_PHID = "select ph_id from t_petition_year where py_id=?";
	public static String UPDATE_F1__17jVAL = "update t_arrdf1_17j set val=? where py_id=? and misc_form_label_id_1=? and "
			+ "misc_form_label_id_2=?";
	public static String QUERY_GET_ENSUING_PETITION_YEAR = "SELECT py_id, year_nm FROM t_petition_year where ph_id=? and "
			+ "seq>0 and isactive = 'TRUE'";
	public static String QUERY_GET_F1_22_VAL = "SELECT val FROM t_arrdf1_22 where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_22 = "update t_arrdf1_22 set val=? where py_id=? and misc_form_label_id=?";
	public static String QUERY_GET_F1__17d_DATA = "select loan_desc, repayable_loan, repayment_rate, drawal_rate from t_arrdf1_17d"
			+ " where form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) and py_id=?";
	public static String QUERY_GET_FORM_INSTANCE_ID = "select form_instance_id from t_form_instance where form_id=? and ph_id=?";
	public static String DELETE_F1__17dVAL = "delete from t_arrdf1_17d where form_instance_id=(select form_instance_id from "
			+ "t_form_instance where form_id = ? and ph_id= ?) and py_id=?";
	public static String QUERY_GET_ALL_PETITION_YEAR = "SELECT py_id, year_nm FROM t_petition_year where ph_id=? and "
			+ "isactive = 'TRUE' order by seq";
	public static String QUERY_GET_COALGRADE_IDs = "select coalgrade_id from m_coalgrade order by coalgrade_id";
	public static String QUERY_GET_UTIL_COAL_FIELD = "select util_coal_field_id, coal_field_desc from m_coal_field, mp_util_coal_field"
			+ " where m_coal_field.coal_field_id=mp_util_coal_field.coal_field_id and util_id=? order by m_coal_field.coal_field_id";
	public static String QUERY_GET_FD1_VAL = "SELECT val FROM t_arrdfd1 where stn_id=? and py_id=? and util_coal_field_id=? and "
			+ "coalgrade_id=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_FD1_VAL = "update t_arrdfd1 set val=? where stn_id=? and py_id=? and util_coal_field_id=? and "
			+ "coalgrade_id=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_insurance_desc = "select distinct COALESCE(insurance_desc, ' ') as desc from t_arrdf1_17f where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_F1__17f_DATA = "select COALESCE(insurance_desc, ' ') as desc, val from t_arrdf1_17f where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) order by insurance_desc, py_id";
	public static String QUERY_DELETE_F1__17f = "delete from t_arrdf1_17f where py_id = ANY(?) and form_instance_id=?";
	public static String QUERY_GET_investment_desc = "select distinct COALESCE(investment_desc, ' ') as desc from t_arrdf1_18c1 where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_F1__18c1_DATA = "select COALESCE(investment_desc, ' ') as desc, val from t_arrdf1_18c1 where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) order by investment_desc, py_id";
	public static String QUERY_DELETE_F1__18c1 = "delete from t_arrdf1_18c1 where py_id = ANY(?) and form_instance_id=?";
	public static String QUERY_GET_income_desc = "select distinct COALESCE(income_desc, ' ') as desc from t_arrdf1_18c2 where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_F1__18c2_DATA = "select COALESCE(income_desc, ' ') as desc, val from t_arrdf1_18c2 where py_id = ANY(?) and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?) order by income_desc, py_id";
	public static String QUERY_DELETE_F1__18c2 = "delete from t_arrdf1_18c2 where py_id = ANY(?) and form_instance_id=?";
	public static String QUERY_GET_F1_17hVAL = "select exp_own, exp_con, basic_pay_own, basic_pay_con, dear_allowance_own, "
			+ "dear_allowance_con, other_allowance_own, other_allowance_con, gratuity_own, gratuity_con, pf_contrib_own, "
			+ "pf_contrib_con, pension_contrib_own, pension_contrib_con, bonus_own, bonus_con, ltc_own, ltc_con, leave_encashment_own, "
			+ "leave_encashment_con, welfare_expend_own, welfare_expend_con, other_own, other_con, shortfall_pf_own, shortfall_pf_con, "
			+ "emp_incentive_own, emp_incentive_con, personnel_no_own, personnel_no_con from t_arrdf1_17h where py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_F1_17h_UPDATE = "update t_arrdf1_17h set exp_own=?, exp_con=?, basic_pay_own=?, basic_pay_con=?, dear_allowance_own=?, "
			+ "dear_allowance_con=?, other_allowance_own=?, other_allowance_con=?, gratuity_own=?, gratuity_con=?, pf_contrib_own=?, "
			+ "pf_contrib_con=?, pension_contrib_own=?, pension_contrib_con=?, bonus_own=?, bonus_con=?, ltc_own=?, ltc_con=?, leave_encashment_own=?, "
			+ "leave_encashment_con=?, welfare_expend_own=?, welfare_expend_con=?, other_own=?, other_con=?, shortfall_pf_own=?, shortfall_pf_con=?, "
			+ "emp_incentive_own=?, emp_incentive_con=?, personnel_no_own=?, personnel_no_con=?, updated_on=?, updated_by=? where py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_F1_17iVAL = "select exp, basic_pay, dear_allowance, other_allowance, gratuity, pf_contrib, pension_contrib, "
			+ "bonus, ltc, leave_encashment, welfare_expend, other, shortfall_pf, emp_incentive, personnel_no from t_arrdf1_17i where py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_F1_17i_UPDATE = "update t_arrdf1_17i set exp=?, basic_pay=?, dear_allowance=?, other_allowance=?, gratuity=?, "
			+ "pf_contrib=?, pension_contrib=?, bonus=?, ltc=?, leave_encashment=?, welfare_expend=?, other=?, shortfall_pf=?, emp_incentive=?, "
			+ "personnel_no=?, updated_on=?, updated_by=? where py_id=? and form_instance_id=(select form_instance_id from "
			+ "t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_FPe_GENCO_VAL = "select COALESCE(package_name, ' ') as package_name, COALESCE(work_scope, ' ') as work_scope, "
			+ "COALESCE(awarded_by, ' ') as awarded_by, COALESCE(bid_recv_no, ' ') as bid_recv_no, TO_CHAR(award_date, 'YYYY-MM-DD HH24:MI:SS') "
			+ "as award_date, TO_CHAR(work_start_date, 'YYYY-MM-DD HH24:MI:SS') as work_start_date, TO_CHAR(work_end_date, 'YYYY-MM-DD HH24:MI:SS') as "
			+ "work_end_date, COALESCE(award_value, ' ') as award_value, COALESCE(firm_escalated, ' ') as firm_escalated, "
			+ "COALESCE(actual_expenditure, ' ') as actual_expenditure, tax, cost from t_arrdfp_e_genco where stn_id=? and " 
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_FPe_GENCO_UPDATE = "update t_arrdfp_e_genco set package_name=?, work_scope=?, awarded_by=?, bid_recv_no=?, "
			+ "award_date=?, work_start_date=?, work_end_date=?, award_value=?, firm_escalated=?, actual_expenditure=?, tax=?, cost=?, "
			+ "updated_on=?, updated_by=? where stn_id=? and form_instance_id=(select form_instance_id from t_form_instance where "
			+ "form_id = ? and ph_id= ?)";
	public static String QUERY_GET_PETITION_BASE_YEAR = "SELECT ph_id, base_year FROM t_petition_header where isactive ='TRUE' order by ph_id";
	public static String QUERY_GET_PETITION_DETAILS = "SELECT t_petition_header.ph_id as ph_id, t_petition_header.base_year as base_year, "
			+ "module_type_desc, TO_CHAR(t_petition_header.created_on, 'YYYY-MM-DD HH24:MI:SS') as created_on, m_user.user_fnm as fnm, "
			+ "m_user.user_mnm as mnm, m_user.user_lnm as lnm FROM t_petition_header, m_module_type, m_user where t_petition_header.module_type_id="
			+ "m_module_type.module_type_id and t_petition_header.created_by=m_user.user_id and petition_state_id=1 and t_petition_header.isactive="
			+ "'TRUE' and t_petition_header.created_by=? order by ph_id";
	public static String QUERY_GET_ATTACHMENT_TYPE_LIST = "SELECT attachment_type_id, attachment_type_desc FROM m_attachment_type "
			+ "WHERE form_id=? ORDER BY attachment_type_id";
	public static String QUERY_GET_F1_18VAL_1 = "select val from t_arrdf1_18_1 where stn_id=? and py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_UTILWISE_STATIONS = "select stn_id, stn_desc from m_util_stn where util_id=? order by stn_id";
	public static String QUERY_UPDATE_F1_18VAL_1 = "update t_arrdf1_18_1 set val=?, updated_on=?, updated_by=? where stn_id=? and py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_F1_18VAL_2 = "select val from t_arrdf1_18_2 where misc_form_label_id=? and py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_18VAL_2 = "update t_arrdf1_18_2 set val=?, updated_on=?, updated_by=? where misc_form_label_id=? and "
			+ "py_id=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_GET_FORM_NOTES = "select note from m_form_note where form_id=? and isactive='TRUE'";
	public static String QUERY_GET_FORM_INSTANCE_COMMENTS = "select COALESCE(comment, ' ') as comment from t_arr_comment where "
			+ "created_by=? and form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_DELETE_FORM_INSTANCE_COMMENTS = "delete from t_arr_comment where form_instance_id=? and created_by=?";
	public static String QUERY_INSERT_FORM_INSTANCE_COMMENTS = "insert into t_arr_comment (form_instance_id, comment, created_on, "
			+ "created_by, updated_on, updated_by, iscommission) values (?, ?, ?, ?, ?, ?, ?)";
	public static String QUERY_GET_USER_UTILID = "select entity_id from m_user where user_id=?";
	
	
	
	
	
	
	
	
	
	//19.01.2018 Priyanka	
	
	
	//1.1 Priyanka 17.01.2018	
		public static String QUERY_INSERT_t_arrdf1_1 = "INSERT INTO t_arrdf1_1 (form_instance_id, stn_id, py_id, "
				+ "	val, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		  
	//1.1a Priyanka 17.01.2018	
		public static String QUERY_INSERT_t_arrdf1_1a = "INSERT INTO t_arrdf1_1a (form_instance_id, unit_id, py_id, val, created_on, "
				+ "	created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		  
	//1.2 Priyanka 16.01.2018	
		public static String QUERY_INSERT_t_arrdf1_2 = "INSERT INTO t_arrdf1_2 (form_instance_id, stn_id, py_id, " 
				+ "	val, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	//1.2a Priyanka 16.01.2018	
		public static String QUERY_INSERT_t_arrdf1_2a = "INSERT INTO t_arrdf1_2a (form_instance_id, "
				+ " unit_id, py_id, val, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	//List of seasons and periods... Priyanka 17.01.2018	
		public static String QUERY_GET_LIST_OF_SEASONS_AND_PERIODS = "SELECT p.period_id ,s.season_id, s.season_desc , p.period_desc "
				+ " FROM m_season s,m_period p WHERE s.season_id = p.season_id AND s.isactive = 'true' AND p.isactive = 'true' ORDER BY s.season_id ";
		
	//1.3 Priyanka 17.01.2018	
		public static String QUERY_INSERT_t_arrdf1_3 = "INSERT INTO t_arrdf1_3 (form_instance_id, stn_id,  period_id, py_id	, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	//1.4a Priyanka 17.01.2018
		public static String QUERY_INSERT_t_arrdf1_4a = "INSERT INTO t_arrdf1_4a (form_instance_id, stn_id, season_id, py_id, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		//1.6a Priyanka 17.01.2018
		public static String QUERY_INSERT_t_arrdf1_6a = "INSERT INTO t_arrdf1_6a (form_instance_id, arrseasonid, arrperiodid, arrpyid, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
	//List Of misc_form_label_id .. Priyanka 17.01.2018
		public static String QUERY_GET_LIST_OF_MISCID = " SELECT m.misc_form_label_id,m.iscolumn,m.sequence FROM m_misc_form_labels m WHERE m.form_id = ? AND "
				+ "m.isactive = 'true' ORDER BY m.misc_form_label_id ";
		  
	//List of Month id order by sequence .. priyanka 18.01.2018 		
		public static String QUERY_GET_LIST_OF_MONTH = "SELECT m.month_id,m.month_desc FROM m_month m WHERE m.isactive = 'true' ORDER BY m.seq ";
		
		public static String QUERY_INSERT_t_petition_header = "insert into t_petition_header (util_id, base_year, created_on, created_by, "
				+ "updated_on, updated_by, isactive, module_type_id, petition_state_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		public static String QUERY_INSERT_t_petition_year = "insert into t_petition_year (year_nm, seq, isactive, ph_id) values (?, ?, ?, ?)";
		
		public static String QUERY_GET_UTILITY_ABBREVIATION_BY_ID = "select util_code from m_utility where util_id=?";
		
	//Annexure form List .... Priyanka....12.01.2018
		public static String ANNEXURE_FORM_LIST_BY_PITITION_HEADER_ID = "SELECT f.form_id, f.form_code, f.form_desc, f.unit_of_measurement, f.module_type_id,"
				+" f.annexure_id, f.layout_code, a.annexure_code, a.annexure_desc  "  
				+" FROM m_form f LEFT JOIN m_annexure a ON a.annexure_id = f.annexure_id AND a.isactive = 'true' "
				+" LEFT JOIN mp_form_util_type fut ON f.form_id = fut.form_id AND fut.isactive = 'true' "  
				+" LEFT JOIN m_utility u ON fut.utype_id = u.utype_id AND u.isactive = 'true' LEFT JOIN t_petition_header ph  " 
				+" ON u.util_id = ph.util_id AND ph.isactive = 'true' WHERE ph.ph_id = ? AND ph.util_id = ? ORDER BY f.form_id ";
		// JBS -- Create New Petition
		public static String QUERY_INSERT_t_form_instance = "insert into t_form_instance (form_id, util_id, ph_id, "
				+ "isfinished, created_on, created_by, updated_on, updated_by, isactive) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		public static String QUERY_UTILWISE_STATIONS = "SELECT stn_id FROM m_util_stn WHERE util_id=? AND isactive = 'true' ORDER BY stn_id";
		
		public static String QUERY_PHIDWISE_PYID = "SELECT py_id FROM t_petition_year WHERE ph_id=? AND isactive = 'true'";
		
		public static String QUERY_UPDATE_FORM11a_MATRIX = "update t_arrdf1_1a set val=? where form_instance_id=2 and unit_id=? and py_id=?";
		
		public static String QUERY_UPDATE_FORM12_MATRIX = "update t_arrdf1_2 set val=? where stn_id=? and py_id=?";

		
		public static String QUERY_GET_STATION_UNIT_FOR_UTILITY = "select uu.stn_id, uu.unit_id "
				+ "from m_util_unit uu, m_util_stn us where uu.stn_id=us.stn_id and us.util_id=?";
		
		
//		By Najma
//		For 1.5
		public static String QUERY_GET_F1_5_DATA_VAL = "SELECT DF.val as val FROM t_arrdf1_5 DF WHERE "
				+ "DF.stn_id=? and DF.period_id=? and DF.py_id=? ORDER BY DF.py_id";
		public static String QUERY_UPDATE_F15 = "UPDATE t_arrdf1_5 SET val=? WHERE stn_id=?  AND"
				+ " period_id=? AND py_id=?";
		public static String QUERY_GET_SEASONWISE_SUM_OF_VAL_F1_5 = "SELECT SUM(val) as sumval FROM t_arrdf1_5  where period_id = ? and py_id = ? " 
				+ "and form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
		public static String QUERY_GET_PERIOD_NAME_SEASON_WISE = "SELECT period_id,period_desc FROM m_period WHERE season_id=? ";

		
//20.01.2018

//1.4b Priyanka 17.01.2018
	public static String QUERY_INSERT_t_arrdf1_4b = "INSERT INTO t_arrdf1_4b (form_instance_id, pump_stn_id, period_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.5 Priyanka 17.01.2018
	public static String QUERY_INSERT_t_arrdf1_5 = "INSERT INTO t_arrdf1_5 (form_instance_id, stn_id, period_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.17b Priyanka 17.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17b = "INSERT INTO t_arrdf1_17b (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

//1.17c Priyanka 17.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17c = "INSERT INTO t_arrdf1_17c (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.6c Priyanka 18.01.2018	
	public static String QUERY_INSERT_t_arrdf1_6c = "INSERT INTO t_arrdf1_6c (form_instance_id, misc_form_label_id, py_id, val, month_id, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
				
//1.17e Priyanka 17.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17e = "INSERT INTO t_arrdf1_17e (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.11 Priyanka 17.01.2018	
	public static String QUERY_INSERT_t_arrdf1_11 = "INSERT INTO t_arrdf1_11 (form_instance_id, misc_form_label_id, py_id, stn_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.12 Priyanka 17.01.2018	
	public static String QUERY_INSERT_t_arrdf1_12 = "INSERT INTO t_arrdf1_12 (form_instance_id, misc_form_label_id, py_id, stn_id, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//List Of Pump Station.Priyanka.20.01.2018
	public static String QUERY_GET_LIST_OF_PUMPSTATION = "SELECT pump_stn_id FROM m_util_pump_stn WHERE util_id=? AND isactive = 'true' ORDER BY pump_stn_id";

	
	
	
	
//	By Najma
//	For Form 1.17b
	public static String QUERY_GET_F1_17b_VAL = "SELECT val FROM t_arrdf1_17b where py_id=? and misc_form_label_id=? ORDER BY arrdf117b_id ";
	public static String QUERY_UPDATE_F1_17b = "UPDATE t_arrdf1_17b SET val=? WHERE  misc_form_label_id=? AND py_id=? "
			+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
			+ "	form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	
//	For Form 1.17c
	public static String QUERY_GET_F1_17c_VAL = "SELECT val FROM t_arrdf1_17c  where py_id=? and misc_form_label_id=? ORDER BY arrdf117c_id ";
	public static String QUERY_UPDATE_F1_17c = "UPDATE t_arrdf1_17c SET val=? WHERE  misc_form_label_id=? AND py_id=?" 
			+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where " 
			+ "form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";

//	For Form 1.17e
	public static String QUERY_GET_F1_17e_VAL = "SELECT val FROM t_arrdf1_17e  where py_id=? and misc_form_label_id=? ORDER BY arrdf117e_id ";
	public static String QUERY_UPDATE_F1_17e = "UPDATE t_arrdf1_17e SET val=? WHERE  misc_form_label_id=? AND py_id=?" 
	+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where " 
	+ "form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	
// For Form 1.18b
	public static String QUERY_GET_F1_18b_VAL = "SELECT val FROM t_arrdf1_18b  where py_id=? and misc_form_label_id=? ORDER BY arrdf1_18b_id ";
	public static String QUERY_UPDATE_F1_18b = "UPDATE t_arrdf1_18b SET val=? WHERE  misc_form_label_id=? AND py_id=? "
			+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
			+ "	form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	
	// For Form 1.20a
	public static String QUERY_GET_F1_20a_VAL = "SELECT val FROM t_arrdf1_20a  where py_id=? and misc_form_label_id=? ORDER BY arrdf1_20a_id ";
	public static String QUERY_UPDATE_F1_20a = "UPDATE t_arrdf1_20a SET val=? WHERE  misc_form_label_id=? AND py_id=? "
				+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
				+ "	form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";
	
	// For Form 1.20a
	public static String QUERY_GET_F1_20b_VAL = "SELECT val FROM t_arrdf1_20b  where py_id=? and misc_form_label_id=? ORDER BY arrdf1_20b_id ";
	public static String QUERY_UPDATE_F1_20b = "UPDATE t_arrdf1_20b SET val=? WHERE  misc_form_label_id=? AND py_id=? "
				+ "and form_instance_id=(select form_instance_id from t_form_instance, t_petition_year where "  
				+ "	form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=?)";


	
	
	
	
//1.17g Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17g = "INSERT INTO t_arrdf1_17g (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.17j Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17j = "INSERT INTO t_arrdf1_17j (form_instance_id, misc_form_label_id_1, misc_form_label_id_2, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.18a Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_18a = "INSERT INTO t_arrdf1_18a (form_instance_id, misc_form_label_id, py_id, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";	
	
//1.18b Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_18b = "INSERT INTO t_arrdf1_18b (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

//1.20a Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_20a = "INSERT INTO t_arrdf1_20a (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.20b Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_20b = "INSERT INTO t_arrdf1_20b (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

//1.22 Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_22 = "INSERT INTO t_arrdf1_22 (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.24 Priyanka 23.01.2018	
	public static String QUERY_INSERT_t_arrdf1_24 = "INSERT INTO t_arrdf1_24 (form_instance_id, misc_form_label_id, py_id, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//Form A Priyanka 24.01.2018	
	public static String QUERY_INSERT_t_arrdfa = "INSERT INTO t_arrdfa (form_instance_id, py_id, stn_id, unit_no, outage_from, outage_to, "
			+ " nature, duration, summary, planned_maintenance, scheduled_maintenance, actual_maintenance, remarks, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
//1.17d Priyanka 24.01.2018	
	public static String QUERY_INSERT_t_arrdf1_17d = "INSERT INTO t_arrdf1_17d (form_instance_id, py_id, loan_desc, repayable_loan,"
			+ " repayment_rate, drawal_rate, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	
	
//	By Najma
//	For Form A
	public static String QUERY_GET_F_A_VAL = "SELECT unit_no,TO_CHAR(outage_from, 'YYYY-MM-DD HH24:MI:SS') as outage_from, TO_CHAR(outage_to, 'YYYY-MM-DD HH24:MI:SS') as outage_to, nature, duration, summary, planned_maintenance, scheduled_maintenance, "
			+ " actual_maintenance, remarks FROM t_arrdfa  where stn_id =? and py_id=? ";

	public static String QUERY_DELETE_F_A_VAL = "delete from t_arrdfa where stn_id = ? and py_id = ? and "
			+ "form_instance_id = (select form_instance_id from t_form_instance, t_petition_year where " 
			+ "	form_id=? and t_form_instance.ph_id=t_petition_year.ph_id and py_id=? ) ";
	

//Form D2 Priyanka 24.01.2018	
	public static String QUERY_INSERT_t_arrdfd2 = "INSERT INTO t_arrdfd2 (form_instance_id, stn_id, misc_form_label_id, cost,"
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
//List of Coal Field...Priyanka.25.01.2018
	public static String QUERY_GET_LIST_OF_UTIL_COALFIELD_ID = "SELECT util_coal_field_id FROM mp_util_coal_field WHERE util_id=? AND isactive = 'true' ORDER BY util_coal_field_id";
	
//List of Coalgrade Id...Priyanka.25.01.2018
	public static String QUERY_GET_LIST_OF_COALGRADEID = "SELECT coalgrade_id FROM m_coalgrade WHERE isactive = 'true' ORDER BY coalgrade_id";
	
//Form D1 Priyanka 25.01.2018	
	public static String QUERY_INSERT_t_arrdfd1 = "INSERT INTO t_arrdfd1 (form_instance_id, stn_id, py_id, util_coal_field_id, coalgrade_id, val, "
				+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
//1.17f Priyanka 25.01.2018
	public static String QUERY_INSERT_t_arrdf1_17f = "INSERT INTO t_arrdf1_17f (form_instance_id, py_id, insurance_desc, val, "
			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

//1.18c1 Priyanka 25.01.2018
  	public static String QUERY_INSERT_t_arrdf1_18c1 = "INSERT INTO t_arrdf1_18c1 (form_instance_id, py_id, investment_desc, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

//1.18c2 Priyanka 25.01.2018
  	public static String QUERY_INSERT_t_arrdf1_18c2 = "INSERT INTO t_arrdf1_18c2 (form_instance_id, py_id, income_desc, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

 //Form P(E) .. Priyanka 25.01.2018
  	public static String QUERY_INSERT_t_arrdfp_e_genco = "INSERT INTO t_arrdfp_e_genco (form_instance_id, stn_id, package_name, work_scope, " 
  			+ " awarded_by, bid_recv_no, award_date, work_start_date, work_end_date, award_value, firm_escalated, actual_expenditure, "
  			+ " tax, cost, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	  
//1.17h Priyanka 29.01.2018
  	public static String QUERY_INSERT_t_arrdf1_17h = "INSERT INTO t_arrdf1_17h (form_instance_id, py_id, exp_own, exp_con, basic_pay_own, "
  			+ " basic_pay_con, dear_allowance_own, dear_allowance_con, other_allowance_own, other_allowance_con, gratuity_own, gratuity_con, "
  			+ " pf_contrib_own, pf_contrib_con, pension_contrib_own, pension_contrib_con, bonus_own, bonus_con, ltc_own, ltc_con, leave_encashment_own, "
  			+ " leave_encashment_con, welfare_expend_own, welfare_expend_con, other_own, other_con, shortfall_pf_own, shortfall_pf_con, "
  			+ " emp_incentive_own, emp_incentive_con, personnel_no_own, personnel_no_con, created_on, created_by, updated_on, updated_by) "
  			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.17i Priyanka 29.01.2018
  	public static String QUERY_INSERT_t_arrdf1_17i = "INSERT INTO t_arrdf1_17i (form_instance_id, py_id, exp, basic_pay, dear_allowance, "
  			+ " other_allowance, gratuity, pf_contrib, pension_contrib, bonus, ltc, leave_encashment, welfare_expend, other, shortfall_pf, "
  			+ " emp_incentive, personnel_no, created_on, created_by, updated_on, updated_by) "
  			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

//Set Pitition Inactive.. Priyanka 29.01.2018    
  	public static String QUERY_SET_PITITION_INACTIVE = "UPDATE t_petition_header SET isactive = 'false' WHERE ph_id = ? ";
  	
//1.21 Priyanka 29.01.2018	
  	public static String QUERY_INSERT_t_arrdf1_21 = "INSERT INTO t_arrdf1_21 (form_instance_id, misc_form_label_id, py_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
 
  	
//Form P(D4) Priyanka 30.01.2018
  	public static String QUERY_INSERT_t_arrdfp_d4 = "INSERT INTO t_arrdfp_d4 (form_instance_id, stn_id, misc_form_label_id_1, misc_form_label_id_2, val, " 
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"	;
 
//1.18  Priyanka 30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_18_1 = "INSERT INTO t_arrdf1_18_1 (form_instance_id, stn_id, py_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
 
//1.18 Priyanka 30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_18_2 = "INSERT INTO t_arrdf1_18_2 (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
 		
//1.19a Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_19a1 = "INSERT INTO t_arrdf1_19a1 (form_instance_id, stn_id, py_id, val_plan, val_nonplan, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.19a Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_19a2 = "INSERT INTO t_arrdf1_19a2 (form_instance_id, misc_form_label_id, py_id, val_plan, val_nonplan, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
  
//1.19a Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_19a3 = "INSERT INTO t_arrdf1_19a3 (form_instance_id, misc_form_label_id, py_id, val_plan, val_nonplan, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
   
//1.23 Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_23_1 = "INSERT INTO t_arrdf1_23_1 (form_instance_id, stn_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    	
//1.23 Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_23_2 = "INSERT INTO t_arrdf1_23_2 (form_instance_id, stn_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

//1.23 Priyanka  30.01.2018
  	public static String QUERY_INSERT_t_arrdf1_23_3 = "INSERT INTO t_arrdf1_23_3 (form_instance_id, stn_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
  	
  	
  	
  	
//  By Najma
//  For Form D2
	public static String QUERY_GET_F_D2_VAL = "SELECT cost FROM t_arrdfd2  where stn_id =? and misc_form_label_id=? and form_instance_id = (select form_instance_id from t_form_instance where form_id = ? and ph_id = ?)";
	public static String QUERY_UPDATE_F_D2 = "UPDATE t_arrdfd2 SET cost=? WHERE stn_id=? AND misc_form_label_id=? and form_instance_id = (select form_instance_id from t_form_instance where form_id = ? and ph_id = ?  )";
//  For Form 1.21
	public static String QUERY_GET_F1_21_VAL = "SELECT val FROM t_arrdf1_21 where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_21 = "update t_arrdf1_21 set val=? where py_id=? and misc_form_label_id=?";
	
//	For Form P(D4)
	public static String QUERY_GET_MISCIDS_FOR_ROW_COLUMN = "SELECT misc_form_label_id, misc_form_label_desc, remarks FROM m_misc_form_labels where "
			+ "form_id=? and iscolumn=? order by misc_form_label_id";
	public static String QUERY_GET_F_P_D4_VAL = "SELECT val FROM t_arrdfp_d4 where stn_id=? and misc_form_label_id_1=? and "
			+ "misc_form_label_id_2=? and form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String UPDATE_F_P_D4_VAL = "update t_arrdfp_d4 set val=? , updated_on=? , updated_by=? where stn_id=?  and misc_form_label_id_1=? and "
			+ "misc_form_label_id_2=? and form_instance_id = (select form_instance_id from t_form_instance where form_id = ? and ph_id = ?  )";

//	For Form 1.23
	// Group 1
	public static String QUERY_GET_F1_23_1_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id,MF.misc_form_label_desc FROM t_arrdf1_23_1 DF,m_misc_form_labels MF  where DF.stn_id =?" 
			+ "and DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ "and DF.misc_form_label_id =  MF.misc_form_label_id";
	public static String QUERY_GET_F1_23_1_VAL = "SELECT val FROM t_arrdf1_23_1 where stn_id = ? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_23_1 = "update t_arrdf1_23_1 set val=? , updated_on=? , updated_by=? where  stn_id=? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";


	// Group 2
	public static String QUERY_GET_F1_23_2_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id,MF.misc_form_label_desc FROM t_arrdf1_23_2 DF,m_misc_form_labels MF  where DF.stn_id =?" 
			+ "and DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ "and DF.misc_form_label_id =  MF.misc_form_label_id";
	public static String QUERY_GET_F1_23_2_VAL = "SELECT val FROM t_arrdf1_23_2 where stn_id = ? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_23_2 = "update t_arrdf1_23_2 set val=? , updated_on=? , updated_by=? where  stn_id=? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";

	// Group 3
	public static String QUERY_GET_F1_23_3_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id,MF.misc_form_label_desc FROM t_arrdf1_23_3 DF,m_misc_form_labels MF  where DF.stn_id =?" 
			+ "and DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ "and DF.misc_form_label_id =  MF.misc_form_label_id";
	public static String QUERY_GET_F1_23_3_VAL = "SELECT val FROM t_arrdf1_23_3 where stn_id = ? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_23_3 = "update t_arrdf1_23_3 set val=? , updated_on=? , updated_by=? where  stn_id=? and py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";

	
//Form 6 Priyanka 30.01.2018
	public static String QUERY_INSERT_t_arrdf6_income = "INSERT INTO t_arrdf6_income (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static String QUERY_INSERT_t_arrdf6_expense = "INSERT INTO t_arrdf6_expense (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static String QUERY_INSERT_t_arrdf6_asset = "INSERT INTO t_arrdf6_asset (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static String QUERY_INSERT_t_arrdf6_add_fund = "INSERT INTO t_arrdf6_add_fund (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static String QUERY_INSERT_t_arrdf6_utilise_fund = "INSERT INTO t_arrdf6_utilise_fund (form_instance_id, py_id, misc_form_label_id, val, "
  			+ " created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	

// By Najma
	// For Form 6 
	// Incomes
	public static String QUERY_GET_F6_INCOMES_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id, MF.misc_form_label_desc, MF.sequence FROM t_arrdf6_income DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ " and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_F6_INCOMES_VAL = "SELECT val FROM t_arrdf6_income where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F6_INCOMES_VAL= "update t_arrdf6_income set val=? , updated_on=? , updated_by=? where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Expenses
	public static String QUERY_GET_F6_EXPENSES_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id, MF.misc_form_label_desc ,MF.sequence FROM t_arrdf6_expense DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ " and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_F6_EXPENSES_VAL = "SELECT val FROM t_arrdf6_expense where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F6_EXPENSES_VAL= "update t_arrdf6_expense set val=? , updated_on=? , updated_by=? where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Assets
	public static String QUERY_GET_F6_ASSETS_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id, MF.misc_form_label_desc ,MF.sequence FROM t_arrdf6_asset DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ " and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_F6_ASSETS_VAL = "SELECT val FROM t_arrdf6_asset where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F6_ASSETS_VAL= "update t_arrdf6_asset set val=? , updated_on=? , updated_by=? where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Utilisation Add Fund
	public static String QUERY_GET_F6_UTILISATION_ADD_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id, MF.misc_form_label_desc ,MF.sequence FROM t_arrdf6_add_fund DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ " and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_F6_UTILISATION_ADD_VAL = "SELECT val FROM t_arrdf6_add_fund where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F6_UTILISATION_ADD_VAL= "update t_arrdf6_add_fund set val=? , updated_on=? , updated_by=? where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Utilisation Capital Fund
	public static String QUERY_GET_F6_UTILISATION_CAPITAL_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id, MF.misc_form_label_desc ,MF.sequence FROM t_arrdf6_utilise_fund DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ " and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_F6_UTILISATION_CAPITAL_VAL = "SELECT val FROM t_arrdf6_utilise_fund where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F6_UTILISATION_CAPITAL_VAL= "update t_arrdf6_utilise_fund set val=? , updated_on=? , updated_by=? where py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
// By Najma
	// For Form 1.19a
	// Capital Expenditure
	public static String QUERY_GET_CAPITAL_EXPENDITURE_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id,MF.misc_form_label_desc, MF.sequence FROM t_arrdf1_19a2 DF,m_misc_form_labels MF  where " 
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ "and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_CAPITAL_EXPENDITURE_VAL = "SELECT val_plan, val_nonplan, (val_plan + val_nonplan) as total FROM t_arrdf1_19a2 where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_CAPITAL_EXPENDITURE_PLAN_VAL = "update t_arrdf1_19a2 set val_plan=? , updated_on=? , updated_by=? where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_CAPITAL_EXPENDITURE_NONPLAN_VAL = "update t_arrdf1_19a2 set val_nonplan=? , updated_on=? , updated_by=? where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Special Projects
	public static String QUERY_GET_SPECIAL_PROJECTS_MISCID_N_NAME = "SELECT  Distinct DF.misc_form_label_id,MF.misc_form_label_desc, MF.sequence FROM t_arrdf1_19a3 DF,m_misc_form_labels MF  where  "
			+ " DF.form_instance_id = (select IF.form_instance_id from t_form_instance IF where IF.form_id = ?  and IF.ph_id = ?)"
			+ "and DF.misc_form_label_id =  MF.misc_form_label_id order by MF.sequence asc";
	public static String QUERY_GET_SPECIAL_PROJECTS_VAL = "SELECT val_plan, val_nonplan , (val_plan + val_nonplan) as total FROM t_arrdf1_19a3 where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_SPECIAL_PROJECTS_PLAN_VAL = "update t_arrdf1_19a3 set val_plan=? , updated_on=? , updated_by=? where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_SPECIAL_PROJECTS_NONPLAN_VAL = "update t_arrdf1_19a3 set val_nonplan=? , updated_on=? , updated_by=? where  py_id=? and misc_form_label_id=? and "
			+ "form_instance_id= (select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	
	// Station
	public static String QUERY_GETF1_19aSTATIONS_VAL = "select val_plan from t_arrdf1_19a1 where stn_id=? and py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";
	public static String QUERY_UPDATE_F1_19aSTATIONS_VAL = "update t_arrdf1_19a1 set val_plan=?, updated_on=?, updated_by=? where stn_id=? and py_id=? and "
			+ "form_instance_id=(select form_instance_id from t_form_instance where form_id = ? and ph_id= ?)";

	//Form B Priyanka 31.01.2018
	public static String QUERY_GET_LIST_OF_DEPRECIATION_RATE = "SELECT dr.depreciation_rate_id FROM m_depreciation_rate dr WHERE dr.isactive = 'true' ORDER BY dr.sequence ";
		
	public static String QUERY_INSERT_t_arrdfb = "INSERT INTO t_arrdfb (form_instance_id, py_id, cost_gen_assets, cost_trans_assets, cost_dist_assets, cost_meter_assets, "
			+ " cost_other_assets, misc_form_label_id, depreciation_rate_id, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static String QUERY_GET_F_B_VAL  = "SELECT b.form_instance_id, b.py_id , b.cost_gen_assets, b.cost_trans_assets, b.cost_dist_assets, "
			+ " b.cost_meter_assets, b.cost_other_assets, b.misc_form_label_id, b.depreciation_rate_id FROM t_arrdfb b WHERE b.py_id = ? ORDER BY b.depreciation_rate_id"; 
		
	public static String QUERY_DELETE_FROM_F_B_VAL = "DELETE FROM t_arrdfb WHERE py_id = ? AND form_instance_id = ? ";
		
	//Form 1.19(b) Priyanka 01.02.2018
	public static String QUERY_GET_LIST_OF_CAPITAL_EXP_TYPE = "SELECT cet.capital_exp_type_id FROM m_capital_exp_type cet WHERE cet.isactive = 'true' ORDER BY cet.sequence ";
		
	public static String QUERY_INSERT_t_arrdf1_19b1 = "INSERT INTO t_arrdf1_19b1 (form_instance_id, stn_id, misc_form_label_id, py_id, capital_exp_type_id, "
			+ " val_plan, val_nonplan, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
	public static String QUERY_INSERT_t_arrdf1_19b2 = "INSERT INTO t_arrdf1_19b2 (form_instance_id, project_desc, capital_exp_type_id, py_id, misc_form_label_id, "
			+ " val_plan, val_nonplan, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
	public static String QUERY_INSERT_t_arrdf1_19b3 = "INSERT INTO t_arrdf1_19b3 (form_instance_id, project_desc, capital_exp_type_id, py_id, misc_form_label_id, "
			+ " val_plan, val_nonplan, created_on, created_by, updated_on, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		

	
	// By Najma
	public static String QUERY_GET_PETITION_BASE_YEARS_FOR_COMBO = "SELECT pby_id, base_year FROM m_petition_base_year where isactive='TRUE' order by pby_id";

//Form P(D1) Priyanka 02.02.2018
	public static String QUERY_GET_LIST_OF_SPECIFIC_TYPE_STATION = "SELECT us.stn_id FROM m_util_stn us,m_util_stn_type ust "
			+" WHERE us.stype_id = ust.stype_id AND us.isactive='true' AND us.isactive='true' AND ust.stype_code = ? ORDER BY us.stn_id ";
	
	public static String QUERY_INSERT_t_arrdfp_d1 = "INSERT INTO t_arrdfp_d1 (form_instance_id, stn_id, misc_form_label_id, original_estimate_val, "
			+ " capital_expenditure_val, liability_val, created_on, created_by, updated_on, updated_by) "
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
//Form P(D2) Priyanka 02.02.2018
	public static String QUERY_INSERT_t_arrdfp_d2 = "INSERT INTO t_arrdfp_d2 (form_instance_id, stn_id, misc_form_label_id, original_cost_val, "
			+ " capital_expenditure_val, liability_val, admitted_cost_val, created_on, created_by, updated_on, updated_by) "
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
//Comment Priyanka 03.01.2018
	public static String QUERY_INSERT_t_arr_comment = "INSERT INTO t_arr_comment (form_instance_id, comment, iscommission, created_on, created_by, updated_on, updated_by) "
			+ " VALUES (?, ?, ?, ?, ?, ?, ?)";
	  
}
