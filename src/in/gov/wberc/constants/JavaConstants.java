package in.gov.wberc.constants;

public class JavaConstants {

	public static String DIM_OTHER_LOSSES = "Other Losses";
//	By Najma
//	1.17b
	public static String GROSS_SALES = "Gross Sales";
	public static String LESS_DEPRECIATION = "Less: Depreciation";
	public static String LESS_DRE = "Less: Deferred Revenue Expenditure";
	public static String LESS_RETURN_ON_EQUITY = "Less: Return on Equity";
	public static String ALLOWABLE_GROSS_SALES = "Allowable Gross Sales for Working Capital";
	public static String INTEREST_STATE_BANK_SHORT_TERM = "Interest at State Bank Short Term";
	public static String INTEREST_ON_WORKING_CAPITAL = "Interest on Working Capital";
	
//	1.17c
	public static String GURANTEE_COMMISSION = "Guarantee Commission";
	public static String FRONTEND_FEES = "Front- End Fees";
	public static String BANK_CHARGES = "Bank Charges";
	public static String PUBLIC_DEPOSIT = "Public Deposit and Advance against Electricity Bill";
	public static String OPENING_CHARGES = "L/C Opening charges";
	public static String FEESNEXPENSES = "Fees and Expenses for Restructuring";
	public static String OTHERS = "Others(specify)";
	
//	1.17e
	public static String TOTAL_ALLOWABLE_REPAYMENT = "Total allowable repayment of loan";
	public static String ORIGINAL_LOAN_AMOUNT_NET = "1/10th of original loan amount net of disallowed loans ";
	public static String MAX_PERMISSIBLE_AMOUNT = "Maximum permissible amount of loan repayment";
	public static String DEPRECIATION = " Depreciation as per Form B";
	
//	1.6c
	public static String NON_DRAWAL_CONCERN_DISTRIBUTION = "Non-drawal by concern distribution licensee due to low demand";
	public static String BAD_COAL = "Bad Coal";
	public static String POOR_COAL_STOCK = "Poor Coal Stock";
	public static String FORCED_OUTAGE = "Forced Outage";
	public static String PLANNED_OUTAGE = "Planned Outage";
	public static String TRANSMISSION_RESTRICTION= "Transmission Restriction";
	public static String GENERATION_RESTRICTION_FOR_PARTIAL= "Generation restriction for partial equipment availability";
	public static String NON_DRAWAL_BY_OTHER_THAN_DISTRIBUTION= "Non-drawal by other than distribution licensee against schedule drawal";

//	1.11
	public static String GROSS_GENERATION = "Gross Generation";
	public static String AUXILLIARY_CONSUMPTION = "Auxilliary Consumption";
	public static String STATION_HEAT_RATE= "Station Heat Rate";
	public static String SPECIFIC_OIL_CONSUMPTION= "Specific Oil Consumption";
	public static String HEAT_VALUE_OF_OIL = "Heat Value of Oil";
	public static String HEAT_VALUE_OF_COAL = "Heat Value of Coal";
	public static String COST_REQUIREMENT = "Coal Requirement considering Transit Loss";
	public static String AVERAGE_PRICE_OF_OIL = "Average Price of Oil";
	public static String AVERAGE_PRICE_OF_COAL = "Average Price of Coal";

//1.12
	public static String COST_ENERGY_OF_OWN_GEN = "Cost of Energy from own Generation - all stations";
	public static String EXCLUDES_EXPENSE = "Excludes expense shown under any other head";
	public static String COAL= "Coal";
	public static String OIL= "Oil";
	public static String COAL_ASH_HANDLING_CHARGES= "Coal & Ash Handling Charges";
	public static String DEMMURGE_FOR_TRANSPORTATION= "Demmurge for Transportation of Fuel";
	public static String WATER_CHARGES= "Water Charges";
	public static String CONSUMPTION_OF_STORES_N_SPARES= "Consumption of stores & spares";
	public static String BUILDINGS= "Buildings";
	public static String PLANT_N_MACHINERIES= "Plant & Machineries";
	public static String OTHERS_PLANT_N_MACHINERIES= "Plant & Machineries";
	public static String SALARIES_N_WAGES= "Salaries & Wages";
	public static String BONUS= "Bonus";
	public static String CONTRIBUTION_TO_FUNDS= "Contribution to Funds";
	public static String WELFARE_EXPENSES= "Welfare Expenses";
	public static String DEPR= "Depreciation";
	public static String TRAVELLING_EXPENSES= "Travelling Expenses";
	public static String VEHICLE_MAINTENANCE= "Vehicle Maintenance";
	public static String TELEPHONE_EXPENSES= "Telephone Expenses";
	public static String SECURITY_CHARGES= "Security Charges";
	public static String OTHER_MANAGEMENT_ADMIN_EXPENSES= "Other Management & Administrative Expenses";
	public static String EXPENSES_DUE_TO_PENALTY= "Expenses due to Penalty, Fines etc.";
	public static String OTHERS_111= "Others (specify)";


//Form P(D4) Priyanka 02.02.2018
	public static String THERMAL_POWER_STATION= "TPS";
	public static String HYDEL_POWER_STATION= "HPS";



	

}
