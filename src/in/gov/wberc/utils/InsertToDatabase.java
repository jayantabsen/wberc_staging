package in.gov.wberc.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;

public class InsertToDatabase {
	public static void main(String args[]) throws MyException {
		//tableForm1_3d();
		//tableForm1_4ad();
//		tableForm1_4bd();
//		tableForm1_6c();
//		tableForm1_11();
//		tableForm1_1();
//		tableForm1_12();
//		tableForm1_1a();
//		tableForm1_2();
		tableForm1_2a();
//		tableForm1_3();
//		tableForm1_4a();
//		tableForm1_4b();
//		tableForm1_5();

	}
	private static void tableForm1_3d() throws MyException {
		double val = 1.5;
		String stations[] = {"1~1", "2~2", "3~1", "4~2", "5~2"};
		int seasons[] = {1, 2, 3};
		int timeofdays[] = {1, 2, 3};
		int pyids[] = {1, 2, 3, 4, 5, 6, 7, 8};
		
		String query1 = "insert into d_form1_3 (f13did, f13mid, stationid, seasonid, todid, pyid, stypeid, val)"
				+ " values (?, 1, ?, ?, ?, ?, ?, ?)";
		
		int f13did = Integer.parseInt(GetMaxID.getMaxID("d_form1_3", "f13did"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( String station : stations ) {
				String x[] = station.split("~");
				int stationid = Integer.parseInt(x[0]);
				int stypeid = Integer.parseInt(x[1]);
				for ( int i = 0; i< seasons.length; i++  ) {
					int seasonid = seasons[i];
					for ( int j = 0; j<timeofdays.length; j++ ) {
						int todid = timeofdays[j];
						for ( int k = 0; k<pyids.length; k++ ) {
							int pyid = pyids[k];
							
							prstmt1.setInt(1, ++f13did);
							prstmt1.setInt(2, stationid);
							prstmt1.setInt(3, seasonid);
							prstmt1.setInt(4, todid);
							prstmt1.setInt(5, pyid);
							prstmt1.setInt(6, stypeid);
							prstmt1.setDouble(7, ++val);
							prstmt1.addBatch();
							
						}
					}
				}
			}
			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	private static void tableForm1_4ad() throws MyException {
		double val = 1.5;
		String stations[] = {"1~1", "2~2", "3~1", "4~2", "5~2"};
		int seasons[] = {1, 2, 3};
		int pyids[] = {1, 2, 3, 4, 5, 6, 7, 8};
		
		String query1 = "insert into d_form1_4a (f14adid, f14amid, stationid, seasonid, pyid, stypeid, val)"
				+ " values (?, 1, ?, ?, ?, ?, ?)";
		
		int f14adid = Integer.parseInt(GetMaxID.getMaxID("d_form1_4a", "f14adid"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( String station : stations ) {
				String x[] = station.split("~");
				int stationid = Integer.parseInt(x[0]);
				int stypeid = Integer.parseInt(x[1]);
				for ( int i = 0; i< seasons.length; i++  ) {
					int seasonid = seasons[i];
						for ( int j = 0; j<pyids.length; j++ ) {
							int pyid = pyids[j];
							
							prstmt1.setInt(1, ++f14adid);
							prstmt1.setInt(2, stationid);
							prstmt1.setInt(3, seasonid);
							prstmt1.setInt(4, pyid);
							prstmt1.setInt(5, stypeid);
							prstmt1.setDouble(6, ++val);
							prstmt1.addBatch();
							
						}
				}
			}
			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}

	}
	
	private static void tableForm1_4bd() throws MyException {
		double val = 0.5;
		int seasons[] = {1, 2, 3};
		int timeofdays[] = {1, 2, 3};
		int pyids[] = {1, 2, 3, 4, 5, 6, 7, 8};
		
		String query1 = "insert into d_form1_4b (f14bdid, f14bmid, pump_stationid, seasonid, todid, "
				+ "pyid, val) values (?, 1, 1, ?, ?, ?, ?)";
		
		int f14bdid = Integer.parseInt(GetMaxID.getMaxID("d_form1_4b", "f14bdid"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< seasons.length; i++  ) {
				int seasonid = seasons[i];
				for ( int j = 0; j<timeofdays.length; j++ ) {
					int todid = timeofdays[j];
					for ( int k = 0; k<pyids.length; k++ ) {
						int pyid = pyids[k];
						
						prstmt1.setInt(1, ++f14bdid);
						prstmt1.setInt(2, seasonid);
						prstmt1.setInt(3, todid);
						prstmt1.setInt(4, pyid);
						prstmt1.setDouble(5, ++val);
						prstmt1.addBatch();
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	
	private static void tableForm1_6c() throws MyException {
		double val = 0.5;
		int pys[] = {6,7,8};
		int miscids[] = {20,21,22,23,24,25,26,27};
		int months[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		
		String query1 = "insert into t_arrdf1_6c (arrdf16cid, arrformid, arrmiscid, arrpyid, val, "
				+ "monthid, created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf16cid = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_6c", "arrdf16cid"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< pys.length; i++  ) {
				int py = pys[i];
				for ( int j = 0; j<miscids.length; j++ ) {
					int misc = miscids[j];
					for ( int k = 0; k<months.length; k++ ) {
						int month = months[k];
						
						prstmt1.setInt(1, ++arrdf16cid);
						prstmt1.setInt(2, 11);
						prstmt1.setInt(3, misc);
						prstmt1.setInt(4, py);
						prstmt1.setDouble(5, ++val);
						prstmt1.setInt(6, month);

						prstmt1.addBatch();
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	private static void tableForm1_11() throws MyException {
		double val = 1.00;
		int pys[] = {1,2,3,4,5,6,7,8};
		int miscids[] = {28,29,30,31,32,33,34,35,36};
		int stations[] = {1, 2, 3, 4};
		
		String query1 = "insert into t_arrdf1_11 (arrdf111id, arrformid, arrmiscid, arrpyid, arrstnid,val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf111id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_11", "arrdf111id"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
				for ( int j = 0; j<miscids.length; j++ ) {
					int misc = miscids[j];
					for ( int k = 0; k<pys.length; k++ ) {
						int py = pys[k];
						
						prstmt1.setInt(1, ++arrdf111id);
						prstmt1.setInt(2, 13);
						prstmt1.setInt(3, misc);
						prstmt1.setInt(4, py);
						prstmt1.setInt(5, st);
						prstmt1.setDouble(6, ++val);

						prstmt1.addBatch();
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	
	private static void tableForm1_1() throws MyException {
		double val = 2.00;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {1, 2, 3, 4};
		
		String query1 = "insert into t_arrdf1_1 (arrdf11id, arrformid, stnid, arrpyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf11id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_1", "arrdf11id"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
					for ( int k = 0; k<pys.length; k++ ) {
						int py = pys[k];
						
						prstmt1.setInt(1, ++arrdf11id);
						prstmt1.setInt(2, 1);
						prstmt1.setInt(3, st);
						prstmt1.setInt(4, py);			
						prstmt1.setDouble(5, ++val);

						prstmt1.addBatch();
						
					
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	private static void tableForm1_12() throws MyException {
		double val = 1.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int miscids[] = {37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59};
		int stations[] = {1, 2, 3, 4};
		
		String query1 = "insert into t_arrdf1_12 (arrdf112id, arrformid, arrmiscid, arrpyid, arrstnid,val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf112id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_12", "arrdf112id"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
				for ( int j = 0; j<miscids.length; j++ ) {
					int misc = miscids[j];
					for ( int k = 0; k<pys.length; k++ ) {
						int py = pys[k];
						
						prstmt1.setInt(1, ++arrdf112id);
						prstmt1.setInt(2, 14);
						prstmt1.setInt(3, misc);
						prstmt1.setInt(4, py);
						prstmt1.setInt(5, st);
						prstmt1.setDouble(6, ++val);

						prstmt1.addBatch();
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	private static void tableForm1_1a() throws MyException {
		double val = 2.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {4};
		int units[] = {8};
		
		String query1 = "insert into t_arrdf1_1a (arrdf11aid, arrformid, stnid,unitid, pyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		String getUnitIdPerStation = "SELECT unitid FROM m_util_unit  where stnid =?";
		int arrdf11aid = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_1a", "arrdf11aid"));

		
		ResultSet rs1=null;

		PreparedStatement prstmt1 = null;
		PreparedStatement prstmt2 = null;

		
		Connection conn1=null;
		Connection conn2=null;
		ArrayList<String> unitList = null;
		for(int i=0;i<stations.length;i++) {
			int st = stations[i];
			try {
				conn1=DBUtil.getConnection();
				prstmt1 = conn1.prepareStatement(getUnitIdPerStation);
				
				prstmt1.setInt(1, st);
				
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					unitList = new ArrayList<String>();
					while(rs1.next()) {
						unitList.add(rs1.getString("unitid"));
					}
				}
				
				
					try {
					conn2=DBUtil.getConnection();
					prstmt2 = conn2.prepareStatement(query1);
						for ( int j = 0; j<unitList.size(); j++ ) {
							for ( int k = 0; k<pys.length; k++ ) {
								int py = pys[k];
								
								prstmt2.setInt(1, ++arrdf11aid);
								prstmt2.setInt(2, 2);
								prstmt2.setInt(3, st);
								prstmt2.setInt(4, Integer.parseInt(unitList.get(j)));
								prstmt2.setInt(5, py);
								prstmt2.setDouble(6, ++val);

								prstmt2.addBatch();
								
							}
						}
					

					prstmt2.executeBatch();
					conn2.commit();
					
				} catch(SQLException e) {
					e.printStackTrace();
					try {
						conn2.rollback();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} finally {
					
						try {
							if(prstmt2!=null) {
							prstmt2.close();
							}
							if(conn2!=null) {
								conn2.close();
							}
						}catch(SQLException e) {
							e.printStackTrace();
						}
					
				}
				
				
				
				
				
			} catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			} finally {
				
					try {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
						if(rs1!=null) {
							rs1.close();
						}
					}catch(SQLException e) {
						e.printStackTrace();
						throw new MyException();
					}
				}
		}	
	}
	
	
	
	
	private static void tableForm1_2() throws MyException {
		double val = 2.00;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {1, 2, 3, 4};
		
		String query1 = "insert into t_arrdf1_2 (arrdf12id, arrformid, stnid, arrpyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf11id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_2", "arrdf12id"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
					for ( int k = 0; k<pys.length; k++ ) {
						int py = pys[k];
						
						prstmt1.setInt(1, ++arrdf11id);
						prstmt1.setInt(2, 3);
						prstmt1.setInt(3, st);
						prstmt1.setInt(4, py);			
						prstmt1.setDouble(5, ++val);

						prstmt1.addBatch();
						
					
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	
	private static void tableForm1_2a() throws MyException {
		double val = 1.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {5,6,7};
		int units[] = {9,10,11};
		
		String query1 = "insert into t_arrdf1_2a (arrdf12aid, arrformid, stnid,unitid, pyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		String getUnitIdPerStation = "SELECT unitid FROM m_util_unit  where stnid =?";
		int arrdf12aid = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_2a", "arrdf12aid"));

		
		ResultSet rs1=null;

		PreparedStatement prstmt1 = null;
		PreparedStatement prstmt2 = null;

		
		Connection conn1=null;
		Connection conn2=null;
		ArrayList<String> unitList = null;
		for(int i=0;i<stations.length;i++) {
			int st = stations[i];
			try {
				conn1=DBUtil.getConnection();
				prstmt1 = conn1.prepareStatement(getUnitIdPerStation);
				
				prstmt1.setInt(1, st);
				
				rs1=prstmt1.executeQuery();
				
				if(rs1!=null) {
					unitList = new ArrayList<String>();
					while(rs1.next()) {
						unitList.add(rs1.getString("unitid"));
					}
				}
				
				
					try {
					conn2=DBUtil.getConnection();
					prstmt2 = conn2.prepareStatement(query1);
						for ( int j = 0; j<unitList.size(); j++ ) {
							for ( int k = 0; k<pys.length; k++ ) {
								int py = pys[k];
								
								prstmt2.setInt(1, ++arrdf12aid);
								prstmt2.setInt(2, 4);
								prstmt2.setInt(3, st);
								prstmt2.setInt(4, Integer.parseInt(unitList.get(j)));
								prstmt2.setInt(5, py);
								prstmt2.setDouble(6, ++val);

								prstmt2.addBatch();
								
							}
						}
					

					prstmt2.executeBatch();
					conn2.commit();
					
				} catch(SQLException e) {
					e.printStackTrace();
					try {
						conn2.rollback();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} finally {
					
						try {
							if(prstmt2!=null) {
							prstmt2.close();
							}
							if(conn2!=null) {
								conn2.close();
							}
						}catch(SQLException e) {
							e.printStackTrace();
						}
					
				}
				
				
				
				
				
			} catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			} finally {
				
					try {
						if(prstmt1!=null) {
						prstmt1.close();
						}
						if(conn1!=null) {
							conn1.close();
						}
						if(rs1!=null) {
							rs1.close();
						}
					}catch(SQLException e) {
						e.printStackTrace();
						throw new MyException();
					}
				}
		}	
	}
	
	
	
	private static void tableForm1_3() throws MyException {
		double val = 2.00;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {1, 2, 3, 4};
		int seasons[] = {1, 2, 3};
		int periods[] = {1, 2, 3};

		
		String query1 = "insert into t_arrdf1_3 (arrdf13d, arrformid, arrstationid, arrseasonid,arrperiodid,arrpyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf13id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_3", "arrdf13d"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
				for ( int j = 0; j<seasons.length; j++ ) {
					int season = seasons[j];
					for ( int k = 0; k<periods.length; k++ ) {
						int period = periods[k];
						for ( int l = 0; l<pys.length; l++ ) {
							int py = pys[l];
							prstmt1.setInt(1, ++arrdf13id);
							prstmt1.setInt(2, 5);
							prstmt1.setInt(3, st);
							prstmt1.setInt(4, season);
							prstmt1.setInt(5, period);
							prstmt1.setInt(6, py);
							prstmt1.setDouble(7, ++val);

							prstmt1.addBatch();
						}
						
					
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	
	private static void tableForm1_4a() throws MyException {
		double val = 1.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int seasons[] = {1,2,3};
		int stations[] = {1, 2, 3, 4};
		
		String query1 = "insert into t_arrdf1_4a (arrdf14aid, arrformid, arrstationid, arrseasonid,arrpyid,val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf14aid = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_4a", "arrdf14aid"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
				for ( int j = 0; j<seasons.length; j++ ) {
					int season = seasons[j];
					for ( int k = 0; k<pys.length; k++ ) {
						int py = pys[k];
						
						prstmt1.setInt(1, ++arrdf14aid);
						prstmt1.setInt(2, 6);
						prstmt1.setInt(3, st);
						prstmt1.setInt(4, season);
						prstmt1.setInt(5, py);
						prstmt1.setDouble(6, ++val);

						prstmt1.addBatch();
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	

	private static void tableForm1_4b() throws MyException {
		double val = 2.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int pump_stations[] = {1, 2};
		int seasons[] = {1, 2, 3};
		int periods[] = {1, 2, 3};

		
		String query1 = "insert into t_arrdf1_4b (arrdf14bid, arrformid, arrpstationid, arrseasonid,arrperiodid,arrpyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf13id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_4b", "arrdf14bid"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< pump_stations.length; i++  ) {
				int pst = pump_stations[i];
				for ( int j = 0; j<seasons.length; j++ ) {
					int season = seasons[j];
					for ( int k = 0; k<periods.length; k++ ) {
						int period = periods[k];
						for ( int l = 0; l<pys.length; l++ ) {
							int py = pys[l];
							prstmt1.setInt(1, ++arrdf13id);
							prstmt1.setInt(2, 5);
							prstmt1.setInt(3, pst);
							prstmt1.setInt(4, season);
							prstmt1.setInt(5, period);
							prstmt1.setInt(6, py);
							prstmt1.setDouble(7, ++val);

							prstmt1.addBatch();
						}
						
					
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	private static void tableForm1_5() throws MyException {
		double val = 1.50;
		int pys[] = {1,2,3,4,5,6,7,8};
		int stations[] = {1, 2, 3, 4};
		int seasons[] = {1, 2, 3};
		int periods[] = {1, 2, 3};

		
		String query1 = "insert into t_arrdf1_5 (arrdf15id, arrformid, arrstationid, arrseasonid,arrperiodid,arrpyid, val ,"
				+ "created_on, created_by, updated_on, updated_by) values (?,?,?,?,?,?,?,'2018-01-01',1,'2018-01-01',1)";
		
		int arrdf15id = Integer.parseInt(GetMaxID.getMaxID("t_arrdf1_5", "arrdf15id"));
		
		Connection conn1 = DBUtil.getConnection();;
		PreparedStatement prstmt1 = null;
		
		try {
			
			conn1.setAutoCommit(false);
			prstmt1 = conn1.prepareStatement(query1);
			for ( int i = 0; i< stations.length; i++  ) {
				int st = stations[i];
				for ( int j = 0; j<seasons.length; j++ ) {
					int season = seasons[j];
					for ( int k = 0; k<periods.length; k++ ) {
						int period = periods[k];
						for ( int l = 0; l<pys.length; l++ ) {
							int py = pys[l];
							prstmt1.setInt(1, ++arrdf15id);
							prstmt1.setInt(2, 5);
							prstmt1.setInt(3, st);
							prstmt1.setInt(4, season);
							prstmt1.setInt(5, period);
							prstmt1.setInt(6, py);
							prstmt1.setDouble(7, ++val);

							prstmt1.addBatch();
						}
						
					
						
					}
				}
			}

			prstmt1.executeBatch();
			conn1.commit();
			
		} catch(SQLException e) {
			e.printStackTrace();
			try {
				conn1.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			
				try {
					if(prstmt1!=null) {
					prstmt1.close();
					}
					if(conn1!=null) {
						conn1.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			
		}
	}
	
	
	}

