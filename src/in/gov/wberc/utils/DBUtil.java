package in.gov.wberc.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import in.gov.wberc.exception.MyException;



public class DBUtil {

	private static final String JDBC_DRIVER   = ApplicationContext.getValueFromFile("databaseConfig", "JDBC_DRIVER", (Object[])null);
	private static final String JDBC_URL      = ApplicationContext.getValueFromFile("databaseConfig", "JDBC_URL", (Object[])null);

	private static final String JDBC_USER     = ApplicationContext.getValueFromFile("databaseConfig", "JDBC_USER", (Object[])null);
	private static final String JDBC_PASSWORD = ApplicationContext.getValueFromFile("databaseConfig", "JDBC_PASS", (Object[])null);
	
	private static final String DATASOURCE_NAME = ApplicationContext.getValueFromFile("databaseConfig", "DATASOURCE_NAME", (Object[])null);

    private static DataSource datasource;


    // Stops initialization of it's object
    private DBUtil() {

    }
    
    static {
        // DBUtil.init();
    }

    private static void init() {
        try {
            DBUtil.datasource = DBUtil.prepareDataSource();
        } catch (MyException ex) {
            ex.printStackTrace();
            CommonUtils.log("init: Exception during getting InitialContext:" + ex);
        }
    }

    private static DataSource prepareDataSource() throws MyException {
        DataSource ds = null;
        try {
            InitialContext ic = new InitialContext();
            CommonUtils.log("Datasource Used: " + DBUtil.DATASOURCE_NAME);
            ds = (DataSource)ic.lookup(DBUtil.DATASOURCE_NAME);
            if (ds == null) {
                throw new MyException("Cannot lookup datasource!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ds;
    }

    public static Connection getConnection() {
        
        Connection conn = null;
        
        try {
            // Get connection from data source look up, if fails then get it
            // from traditional driver manager 
           /* if (datasource != null) {
                conn = datasource.getConnection();
                System.out.println("Got connection from datasource");
            } else {*/
                Class.forName(JDBC_DRIVER);
                conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD); //"jdbc:mysql://127.0.0.1:3306/namdb","namdb","password"
                conn.setAutoCommit(false);
                //System.out.println("Get connection from Driver manager");
            //}         
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
        	cnfe.printStackTrace();
        }

        return conn;
    }


    public static void rollbackConnection(Connection conn) {
        try {
            if (null != conn && !conn.isClosed()) {
                conn.rollback();
            }
        } catch (SQLException e) {
            CommonUtils.log(e.getMessage() + e);
        }
    }

    public static void close(Object obj) {
        if (obj == null) {
            return;
        }
        try {
            if (obj instanceof Connection) {
                closeConnection((Connection)obj);
            } else if (obj instanceof Statement) {
                closeStatement((Statement)obj);
            } else if (obj instanceof ResultSet) {
                closeResultSet((ResultSet)obj);
            } else if (obj instanceof PreparedStatement) {
                closePreparedStatement((PreparedStatement)obj);
            } else if (obj instanceof CallableStatement) {
                closeCallableStatement((CallableStatement)obj);
            }
        } catch (Exception e) {
            CommonUtils.log(e.getMessage()+ e);
        }
    }


    private static void closeConnection(Connection conn) {
        try {
            if (null != conn && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            CommonUtils.log(e.getMessage() + e);
        }
    }


    private static void closeStatement(Statement statement) {
        try {
            if (null != statement) {
                statement.close();
            }
        } catch (SQLException e) {
//            LOGGER.error(e.getMessage(), e);
        	CommonUtils.log(e.getMessage()+ e);
        }
    }

    private static void closePreparedStatement(PreparedStatement pStatement) {
        try {
            if (null != pStatement) {
                pStatement.close();
            }
        } catch (SQLException e) {
            CommonUtils.log(e.getMessage()+e);
        }
    }

    private static void closeCallableStatement(CallableStatement cStatement) {
        try {
            if (null != cStatement) {
                cStatement.close();
            }
        } catch (SQLException e) {
            CommonUtils.log(e.getMessage()+ e);
        }
    }

    private static void closeResultSet(ResultSet resultSet) {
        try {
            if (null != resultSet) {
                resultSet.close();
            }
        } catch (SQLException e) {
            CommonUtils.log(e.getMessage()+ e);
        }
    }

    public static synchronized long getNextSequence(Connection conn,
                                                    String sequenceName) throws SQLException {
        String query =
            "select " + sequenceName + ".nextval  as SEQVAL from dual";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long seqnextVal = 0;
        try {
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                seqnextVal = rs.getLong("SEQVAL");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            CommonUtils.log("Got error to sequence val for the sequence name: " +
                         sequenceName + e);
            throw e;
        } finally {
            DBUtil.close(rs);
            DBUtil.close(pstmt);
        }

        return seqnextVal;
    }
    
   

    public static String getStringFromCLOB(java.sql.Clob inputClob) throws SQLException {
        String outputStr = "";
        if (inputClob != null && inputClob.length() != 0) {
            outputStr = inputClob.getSubString(1, (int)inputClob.length());   
        }
        
        return  outputStr;
    }
    
    public static void freeCLOB(java.sql.Clob inputClob) {
        if (inputClob != null) {
            try {
                inputClob.free();
            } catch (SQLException e) {
                CommonUtils.log(e.getMessage()+ e);
            }
        }
    }
    
    public static byte[] getByteArrayFromObject(Object object) throws IOException {
        
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        byte[] byteArray = null;
        
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);

            byteArray = baos.toByteArray();
        } finally {
            if (baos != null) {
                baos.close();
            }
            if (oos != null) {
                oos.close();
            }   
        } 
        
        return byteArray;
    }
    

    public static Object getObjectFromBlob (Blob inputBlob) throws SQLException,
                                IOException, ClassNotFoundException {
        
        ByteArrayInputStream baip = null;
        ObjectInputStream ois = null;
        Object object = null;
        
        try {
            long blobLength = inputBlob.length();

            int pos = 1; // position is 1-based
            int len = (int) blobLength;
            byte[] bytes = inputBlob.getBytes(pos, len);
            baip = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(baip);
            object = ois.readObject();
            
        } finally {
            if (baip != null) {
                baip.close();
            }
            if (ois != null) {
                ois.close();
            }   
        }
                
        return object;
    }
        
    public static void freeBLOB(java.sql.Clob inputBlob) {
        if (inputBlob != null) {
            try {
                inputBlob.free();
            } catch (SQLException e) {
                CommonUtils.log(e.getMessage()+ e);
            }
        }
    }

        
    public static void main(String[] args) {
        DBUtil.getConnection();
    }
    
}
