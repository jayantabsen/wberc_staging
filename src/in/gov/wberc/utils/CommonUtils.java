package in.gov.wberc.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class CommonUtils {
	public static Date convertStringToDate(String dateString) {
		Date date = null;
		DateFormat simpleDateFormat;
		
		try {
			System.out.println(" Date before parsing::"+dateString);
			simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			
			 date = simpleDateFormat.parse(dateString);
		    
		    //System.out.println("Parsed Date:"+date);
			
			//System.out.println("Parsed Date::"+s2);
		} catch(Exception  exception) {
			System.out.println("Exception :"+exception);
		}
		
		
		return date;
	}
	
	public static String convertDateToString(Date date, String format) {
		
		DateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	
	}
	
	public static String booleanToStringStatus(boolean isSuccess) {
		String strStatus = (isSuccess) ? "Y" : "N";
		return strStatus;
	}

	public static void log(String message) {
		System.out.println("Inside " + message);
	}
	
	
	public static boolean isNull(Integer value) {
		return (value == null || value.intValue() == 0);
	}
	
	public static boolean isNull(Long value) {
		return (value == null || value.longValue() == 0);
	}
	
	public static boolean isNull(String value) {
		return (value == null || value.isEmpty());
	}
	
	public static boolean isNull(Calendar value) {
		return (value == null);
	}

	public static boolean isNull(Object value) {
		return (value == null);
	}

    public static java.util.Date addMonths(final java.util.Date date, final int months) {
        java.util.Date calculatedDate = null;

        if (date != null) {
            final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, months);
            calculatedDate = new java.util.Date(calendar.getTime().getTime());
        }

        return calculatedDate;
    }

    
	private final static double AVERAGE_MILLIS_PER_MONTH = 365.24 * 24 * 60 * 60 * 1000 / 12;

    public static long getDaysDifference(final java.util.Date startDate, final java.util.Date endDate) {
    	long diff = endDate.getTime() - startDate.getTime();
    	long diffInDays = diff / (1000 * 60 * 60 * 24);
        System.out.println("The 21st century (up to " + endDate + ") is "
            + (diff / (1000 * 60 * 60 * 24)) + " days old.");
        return diffInDays;
        
    }

    public static final long getMonthsDifference(java.util.Date startDate, java.util.Date endDate) {
    	long diff = endDate.getTime() - startDate.getTime();
    	long monthsDifference = (long) (diff / AVERAGE_MILLIS_PER_MONTH);
        System.out.println("Months between " + endDate + " and " + startDate + " is " + monthsDifference);
        return monthsDifference;
    }

	public static Object getBlankStringForNull(Object obj) {
		if(obj == null) {
			return "";
		} else {
			return obj;
		}
	}
	
	public static int getCurrentYear() {
		int currentYear;
		Date currentDate = new Date();
		currentYear = currentDate.getYear() + 1900;		
		System.out.println(currentDate + " " + currentYear + " " + currentDate.getDate() + " " + (currentDate.getMonth()+1));
		return currentYear;
	}
	
	public String sentenceCase(String message){
		
		if(message!=null && message.trim().length()>0 && !message.trim().equalsIgnoreCase("")){
			String temp[]=message.split(" ");	
			message="";
			for(int i=0;i<temp.length;i++){
				
				String first[]=temp[i].split("");
				String firstFull="";
				if(first.length>1){
				firstFull=first[1].toUpperCase();
				for(int j=2;j<first.length;j++){
					firstFull+=first[j].toLowerCase();
				}
				}
				message+=firstFull;
				if(i<temp.length){
					message+=" ";
				}
			}
		}
		return message;
	}
	
	public static String formattedDateStr(String date) {
		String temp="";
		int i=0;
		
		if(date.length()!=8)
			return null;
		
		
		temp = date.substring(0, 2)+'/'+date.substring(2, 4)+'/'+date.substring(4);
		/*while(i<date.length()+2) {
			System.out.println(i);
			char c=date.charAt(i);
			if(i==2 || i==4)
				{
					temp=temp+'/';
					//System.out.println(c);
				}
			else {
				temp=temp+c;
				
			}
			i++;
		}*/
		
		
		return temp;
	}
	
	public static String issueToken() {
		Random random = new SecureRandom();
		String token = new BigInteger(159, random).toString(32);
		return token;
	}
	
	public static void main(String[] args) {
		String token=issueToken();
		System.out.println(token);
	}

    
}
