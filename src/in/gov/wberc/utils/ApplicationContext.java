package in.gov.wberc.utils;

import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class ApplicationContext {

	private static final Logger LOGGER = Logger.getLogger(ApplicationContext.class);

	/** The file name properties mapping. */
	private static Map<String, Properties> fileNamePropertiesMapping = new HashMap<String, Properties>();

	/**
	 * To get all the key value pairs from the property file.
	 *
	 * @param filePath the file path
	 * @return Properties
	 */
	public static Properties getPropertiesFromFile(String filePath) {

		Properties properties = new Properties();
		
		if (fileNamePropertiesMapping.containsKey(filePath)) {
			properties = fileNamePropertiesMapping.get(filePath);
		} else {
			try {
				ResourceBundle resourceBundle = ResourceBundle.getBundle(filePath);
				String key = null;
				if (resourceBundle != null) {
					Enumeration<String> localenum = resourceBundle.getKeys();
					while (localenum.hasMoreElements()) {
						key = localenum.nextElement();
						properties.put(key, resourceBundle.getString(key));
					}
				}
				
				fileNamePropertiesMapping.put(filePath, properties);
				
			} catch (MissingResourceException ex) {
//				CommonUtils.log("MissingResourceException Exception in getPropertiesFromFile\n" + ex);
				LOGGER.error("MissingResourceException Exception in getPropertiesFromFile", ex);
			}
		}
		
		return properties;
	}
	
	/**
	 * To get the value of a particular key from a specified properties file.
	 *
	 * @param filePath the file path
	 * @param key the key
	 * @param params the params
	 * @return the value
	 */
	public static String getValueFromFile(String filePath, String key, Object... params) {
		String valueFromFile = "";
		try {     
			if (params == null) {       
				valueFromFile = getPropertiesFromFile(filePath).getProperty(key);     
			} else {       
				valueFromFile = MessageFormat.format(getPropertiesFromFile(filePath).getProperty(key), params);
			}   
		} catch (Exception e) {     
//			CommonUtils.log("getValueFromFile failed: \n" + e.getMessage());
			LOGGER.error("getValueFromFile failed:" + e.getMessage());
		}   
		return valueFromFile;
	}
	
    public static final HashMap<String, String> getValueMapFromFile(String filePath, String property,  Object... params) {
    	HashMap<String, String> map = new LinkedHashMap<String, String>();
    	String mapStr = ApplicationContext.getValueFromFile(filePath, property, params);
		
		
		if(mapStr != null && !mapStr.isEmpty()) {
			String[] listStr = mapStr.split("[,]");
			for(int i = 0; i < listStr.length; i++) {
				String[] propertyElement = listStr[i].split("[|]");
				String key = propertyElement[0];
				String value = (propertyElement.length > 1) ? propertyElement[1] : " ";
				map.put(key, value);
			}
		}


		return map;
    	
    }
	
	public static void main(String[] args) {
		System.out.println(ApplicationContext.getValueFromFile("apiConfig", "COMPANY_ID", (Object[])null));
	}
	
}
