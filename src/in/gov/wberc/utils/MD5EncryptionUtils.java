package in.gov.wberc.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import sun.misc.BASE64Encoder;

public class MD5EncryptionUtils {
	private static String convertToHex(byte[] data) { 
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) { 
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do { 
				if ((0 <= halfbyte) && (halfbyte <= 9)) 
					buf.append((char) ('0' + halfbyte));
				else 
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while(two_halfs++ < 1);
		} 
		return buf.toString();
    } 
 
    public static String encrypt(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] md5hash = new byte[32];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        md5hash = md.digest();
        return convertToHex(md5hash);
    }
    
    public static String encryptSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchProviderException{
    	String pwCompareStr = "";
    	byte[] messageByte = text.getBytes();
    	
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1", "SUN");
        sha1.update(messageByte);
        
        byte[] digestByte = sha1.digest();
        BASE64Encoder b64Encoder = new BASE64Encoder();
        pwCompareStr = (b64Encoder.encode(digestByte));
        pwCompareStr = new StringBuilder("{SHA-1}").append(pwCompareStr).toString();
        return pwCompareStr;
    }
    
   /* public static String issueJWT(String loginID) {
    	byte[] key = getSignatureKey();
    	 
    	/*SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	Calendar c = Calendar.getInstance();
    	c.setTime(new Date()); 
    	c.add(Calendar.DATE, 30); // Adding 30 days
    	String expirationDate = sdf.format(c.getTime());
    	Date expirationDate;

    	String jwt = 
    	    Jwts.builder().setIssuer("http://trustyapp.com/")
    	        .setSubject("loginID")
    	        .setExpiration(expirationDate)
    	        .put("scope", "self api/buy") 
    	        .signWith(SignatureAlgorithm.HS256,key)
    	        .compact();
    }*/
    
    
    public static void main(String[] args) throws IOException {
 
    	String text = "5678";
    	try { 
    		System.out.println("MD5 hash of string \""+text+"\": " + MD5EncryptionUtils.encryptSHA1(text));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	}
    
}
