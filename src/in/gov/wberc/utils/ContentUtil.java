/**
 * 
 */
package in.gov.wberc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

import in.gov.wberc.vo.DocumentVO;

/**
 * @author rajat
 *
 */
public class ContentUtil {

	public String createFolder(String folderName, String folderPath) {
		Session session = createCMISSession("182.75.177.246", "8282");
		Folder root = session.getRootFolder();
		System.out.println("Root folder name-->"+root.getName());
		// properties
		// (minimal set: name and object type id)
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
		properties.put(PropertyIds.NAME, folderName);

		// create the folder
		Folder newFolder = root.createFolder(properties);
		return newFolder.getId();
	}

	public Session createCMISSession(String host, String port) {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "password");

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, "http://"+host+":"+port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Create session.
		// Alfresco only provides one repository.
		Repository repository = factory.getRepositories(parameter).get(0);
		System.out.println("Rep folder name-->"+repository.getId());

		parameter.put(SessionParameter.REPOSITORY_ID, repository.getId());

		// create session
		Session session = factory.createSession(parameter);
		return session;
	}

	public String checkinDocument(File file, String path, DocumentVO docVO) {
		Session session = createCMISSession("182.75.177.246", "8282");
		Folder parent = (Folder) session.getObjectByPath(path);
		// properties 
		// (minimal set: name and object type id)
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "D:erms:Attachments");
		properties.put(PropertyIds.NAME, docVO.getDocName());
		properties.put("erms:PetitionID", docVO.getPetitionId());

		// content
		byte[] content = "Hello World!".getBytes();
		InputStream stream = null;
		ContentStream contentStream = null;
		try {
			stream = new FileInputStream(file);
			contentStream = new ContentStreamImpl(file.getName(), BigInteger.valueOf(content.length), "text/plain", stream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stream != null) {
					stream.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// create a major version
		Document newDoc = parent.createDocument(properties, contentStream, VersioningState.MAJOR);
		return newDoc.getId();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ContentUtil util = new ContentUtil();
		//util.checkinDocument();

	}

}
