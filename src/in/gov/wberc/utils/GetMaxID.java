package in.gov.wberc.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import in.gov.wberc.exception.MyException;

public class GetMaxID {

	public static String getMaxID(String table, String pknm) throws MyException {
		String result = "0";
		String query = "SELECT MAX("+pknm+") as mid FROM "+table;
		System.out.println(query);
		
		Statement stmt=null;
		Connection conn=null;
		ResultSet rs=null;
		try {
			conn = DBUtil.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if ( rs.next() ) {
				result = rs.getString("mid");
				if ( result == null ) {
					result = "0";
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		}finally{
			if(stmt!=null) {
				try {
					stmt.close();
					if(conn!=null) {
						conn.close();
					}
					if(rs!=null) {
						rs.close();
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
}
