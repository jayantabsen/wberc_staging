package in.gov.wberc.to;

import in.gov.wberc.vo.FormPeJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class FormPeDataUpdateTO implements TransInterfaceVO{
	
	private FormPeJExcelDataVO data;
	private String message;
	private int status;
	public FormPeJExcelDataVO getData() {
		return data;
	}
	public void setData(FormPeJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
