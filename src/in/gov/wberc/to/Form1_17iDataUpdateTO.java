package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_17iJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;


public class Form1_17iDataUpdateTO implements TransInterfaceVO {

	private Form1_17iJExcelDataVO data;
	private String message;
	private int status;

	public Form1_17iJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_17iJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
