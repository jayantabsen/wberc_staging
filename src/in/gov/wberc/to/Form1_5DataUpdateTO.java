//** By Najma

package in.gov.wberc.to;

import in.gov.wberc.vo.From1_5JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_5DataUpdateTO implements TransInterfaceVO {

	private From1_5JExcelDataVO data;
	private String message;
	private int status;
	public From1_5JExcelDataVO getData() {
		return data;
	}
	public void setData(From1_5JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
