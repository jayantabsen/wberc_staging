/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.FormP_D4jJExcelDataVO;

public class Form_P_D4_DataUpdateTO {
	
	private FormP_D4jJExcelDataVO data;
	private String message;
	private int status;
	
	public FormP_D4jJExcelDataVO getData() {
		return data;
	}
	public void setData(FormP_D4jJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
