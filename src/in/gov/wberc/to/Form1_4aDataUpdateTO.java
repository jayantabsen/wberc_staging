package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_4aJExcelDataVO;

public class Form1_4aDataUpdateTO {

	private Form1_4aJExcelDataVO data;
	private String message;
	private int status;
	public Form1_4aJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_4aJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
