package in.gov.wberc.to;

import in.gov.wberc.vo.From1_1JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class From1_1DataUpdateTO implements TransInterfaceVO {

	String message;
	int status;
	From1_1JExcelDataVO data;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public From1_1JExcelDataVO getData() {
		return data;
	}
	public void setData(From1_1JExcelDataVO data) {
		this.data = data;
	}
	
}
