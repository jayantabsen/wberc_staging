/*** @author Najma on 01-Feb-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_19aJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_19aUpdateTO implements TransInterfaceVO{
	private Form1_19aJExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_19aJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_19aJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
