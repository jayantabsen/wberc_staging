
//** By Najma

package in.gov.wberc.to;

import in.gov.wberc.vo.FormInstanceCommentVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class FormInstanceCommentUpdateTO implements TransInterfaceVO {

	private FormInstanceCommentVO data;
	private String message;
	private int status;
	
	public FormInstanceCommentVO getData() {
		return data;
	}
	public void setData(FormInstanceCommentVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
