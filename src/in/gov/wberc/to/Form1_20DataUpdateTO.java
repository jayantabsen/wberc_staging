package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_20JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_20DataUpdateTO implements TransInterfaceVO{
	private Form1_20JExcelDataVO data;
	private String message;
	private int status;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Form1_20JExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_20JExcelDataVO data) {
		this.data = data;
	}
	
}
