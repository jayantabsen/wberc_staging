/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.Form_D2_JExcelDataVO;

public class Form_D2_DataUpdateTO {
	
	private Form_D2_JExcelDataVO data;
	private String message;
	private int status;
	
	public Form_D2_JExcelDataVO getData() {
		return data;
	}
	public void setData(Form_D2_JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
