package in.gov.wberc.to;

import in.gov.wberc.vo.From1_2JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class From1_2DataUpdateTO implements TransInterfaceVO {

	String message;
	int status;
	From1_2JExcelDataVO data;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public From1_2JExcelDataVO getData() {
		return data;
	}
	public void setData(From1_2JExcelDataVO data) {
		this.data = data;
	}
	
}
