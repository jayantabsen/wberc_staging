/*** @author Najma on 25-Jan-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.Form_A_JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form_A_DataSaveTO implements TransInterfaceVO{
	private Form_A_JExcelDataVO data;
	private String message;
	private int status;
	
	public Form_A_JExcelDataVO getData() {
		return data;
	}
	public void setData(Form_A_JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
