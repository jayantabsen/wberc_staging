package in.gov.wberc.to;

import java.util.List;
import javax.ws.rs.core.StreamingOutput;

import in.gov.wberc.vo.TransInterfaceVO;

public class MyAPIResponseTO implements TransInterfaceVO {
	
	String message;
	int status;
	TransInterfaceVO data;
	List<? extends TransInterfaceVO> listData;
	StreamingOutput stream;
	
	/**
	 * @return the stream
	 */
	public StreamingOutput getStream() {
		return stream;
	}

	/**
	 * @param stream the stream to set
	 */
	public void setStream(StreamingOutput stream) {
		this.stream = stream;
	}
	
	public MyAPIResponseTO(String message, int i, List<? extends TransInterfaceVO> rows) {
		this.message = message;
		this.status = i;
		this.listData = rows;
	}
	
	public MyAPIResponseTO(String message, int status, TransInterfaceVO data) {
		super();
		this.message = message;
		this.status = status;
		this.data = data;
	}
	
	public MyAPIResponseTO() {
		// Constructor without argument.
	}
	
	public MyAPIResponseTO(String message, int i) {
		this.message=message;
		this.status=i;
	}
	
	public MyAPIResponseTO(String message, int i, StreamingOutput stream) {
		this.message=message;
		this.status=i;
		this.stream=stream;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the data
	 */
	public TransInterfaceVO getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(TransInterfaceVO data) {
		this.data = data;
	}
	/**
	 * @return the listData
	 */
	public List<?> getListData() {
		return listData;
	}
	/**
	 * @param rows the listData to set
	 */
	public void setListData(List<? extends TransInterfaceVO> rows) {
		this.listData = rows;
	}
	
}
