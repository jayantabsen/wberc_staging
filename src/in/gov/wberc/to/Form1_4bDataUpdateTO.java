//*** By Najma

package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_4bJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_4bDataUpdateTO implements TransInterfaceVO {

	private Form1_4bJExcelDataVO data;
	private String message;
	private int status;
	public Form1_4bJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_4bJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
