
//** By Najma

package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_17gJExcelDataVO;
import in.gov.wberc.vo.From1_11JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_17gUpdateTO implements TransInterfaceVO {

	private Form1_17gJExcelDataVO data;
	private String message;
	private int status;
	public Form1_17gJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_17gJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
