//** By JBS
package in.gov.wberc.to;

import in.gov.wberc.vo.FormD1JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class FormD1UpdateTO implements TransInterfaceVO {

	private FormD1JExcelDataVO data;
	private String message;
	private int status;
	
	public FormD1JExcelDataVO getData() {
		return data;
	}
	public void setData(FormD1JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
