/*** @author Najma on 31-Jan-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_23JExcelDataVO;

public class Form1_23DataUpdateTO {
	
	private Form1_23JExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_23JExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_23JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
