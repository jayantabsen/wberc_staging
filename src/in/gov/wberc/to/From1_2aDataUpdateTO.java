package in.gov.wberc.to;

import in.gov.wberc.vo.From1_2aJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class From1_2aDataUpdateTO implements TransInterfaceVO {

	String message;
	int status;
	From1_2aJExcelDataVO data;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public From1_2aJExcelDataVO getData() {
		return data;
	}
	public void setData(From1_2aJExcelDataVO data) {
		this.data = data;
	}
	
}
