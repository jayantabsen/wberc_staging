//01.02.2018 Priyanka
package in.gov.wberc.to;

import in.gov.wberc.vo.Form_B_JExcelDataVO;


public class Form_B_DataSaveTO {
	private Form_B_JExcelDataVO data;
	private String message;
	private int status;

	public Form_B_JExcelDataVO getData() {
		return data;
	}

	public void setData(Form_B_JExcelDataVO data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
