/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_21JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_21DataUpdateTO implements TransInterfaceVO {
	private Form1_21JExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_21JExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_21JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
