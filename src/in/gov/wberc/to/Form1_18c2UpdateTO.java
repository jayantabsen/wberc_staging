//** By JBS
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_18c2JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_18c2UpdateTO implements TransInterfaceVO {

	private Form1_18c2JExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_18c2JExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_18c2JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
