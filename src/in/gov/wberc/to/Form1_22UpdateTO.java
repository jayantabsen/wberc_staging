
//** By JBS
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_22JExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_22UpdateTO implements TransInterfaceVO {

	private Form1_22JExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_22JExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_22JExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
