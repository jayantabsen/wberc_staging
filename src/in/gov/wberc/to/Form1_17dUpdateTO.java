//** By JBS
package in.gov.wberc.to;

import in.gov.wberc.vo.Form1_17dJExcelDataVO;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;
import in.gov.wberc.vo.TransInterfaceVO;

public class Form1_17dUpdateTO implements TransInterfaceVO {

	private Form1_17dJExcelDataVO data;
	private String message;
	private int status;
	
	public Form1_17dJExcelDataVO getData() {
		return data;
	}
	public void setData(Form1_17dJExcelDataVO data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
