package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_3DataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_3DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;

import in.gov.wberc.vo.F1_3SeasonUsageVO1;
import in.gov.wberc.vo.Form1_3JExcelDataVO;

@Secured
@Path("/f1_3DataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_3DataUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_3Data(Form1_3DataUpdateTO req) throws SQLException {
		
		Form1_3JExcelDataVO data = req.getData();
		String stationid = data.getStationid();
		ArrayList<String> cols = data.getCols();
		ArrayList<String> seasons = data.getSeasons();
		ArrayList<ArrayList<String>> usage = data.getUsage();
		ArrayList<F1_3SeasonUsageVO1> vals = data.getVals();
		
		// Station Id Validation
		if ( stationid == null || stationid.length() == 0 ) {
			return new MyAPIResponseTO("Invalid Station Id !!", 0);
		} else {
			try {
				Integer.parseInt(stationid);
			} catch (NumberFormatException ex) {
				return new MyAPIResponseTO("Invalid Station Id !!", 0);
			}
		}
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}
		// season-id validation
		for ( String seasonid : seasons )
		{
			try
			{
				Integer.parseInt(seasonid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid SeasonId !!", 0);
			}
		}
		// time-of-day-id validation
		for ( ArrayList<String> todid : usage )
		{
			try
			{
				for(String period : todid) {
					Integer.parseInt(period);

				}
				
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid Time-Of-DayId !!", 0);
			}
		}
		if ( vals == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_3DataUpdateDAO f13dudao = new Form1_3DataUpdateDAO();
		try {
			boolean res = f13dudao.updateF1_3MatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.3 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.3 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.3 Data update failed",0);
		}
		
		
	}
	
}
