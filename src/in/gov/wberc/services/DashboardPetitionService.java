package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.DashboardPetitionDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.DashboardPetitionVO;

@Secured
@Path("/dashboard_petition_details")
@Produces(MediaType.APPLICATION_JSON)
public class DashboardPetitionService {

	@GET
	public MyAPIResponseTO getDashboardPetition(@QueryParam("user_id") String user_id) throws SQLException {
		
		DashboardPetitionVO data = null;
		DashboardPetitionDAO dpdao = new DashboardPetitionDAO();
		try {
			data = dpdao.getDashboardPetition(user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception occured !!!",0);
		}
		return new MyAPIResponseTO("Dashboard Petition Table JSON delivered successfully.", 1, data);
		
	}
}
