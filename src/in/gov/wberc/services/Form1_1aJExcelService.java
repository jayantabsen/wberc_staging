package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_1aJExcelDAO;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_1aJExcelDataVO;

@Secured
@Path("/f1_1aJExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_1aJExcelService {

	@GET
	public MyAPIResponseTO getF1_1aJExcel(@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("form_id") String form_id) throws SQLException {
		
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		if(Integer.parseInt(util_id) != 0) {
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		}
		
		From1_1aJExcelDataVO data = null;
		Form1_1aJExcelDAO f11jxldao = new Form1_1aJExcelDAO();
		try {
			data = f11jxldao.getF1_1aJExcelData(ph_id, util_id, form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.1a matrix fetched Successfully", 1, data);
		
	}
	
}
