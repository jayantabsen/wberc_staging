package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_1aDataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_1aDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_1aJExcelDataVO;
import in.gov.wberc.vo.StationNUnitValVO;

@Secured
@Path("/f1_1aDataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_1aDataUpdateService {

	@POST
	public MyAPIResponseTO updateForm1_1aData(Form1_1aDataUpdateTO req) throws SQLException {
		
		From1_1aJExcelDataVO f11jxldvo = (From1_1aJExcelDataVO)req.getData();
		try {
			ArrayList<String> unitidAl = f11jxldvo.getRows();
			ArrayList<String> pyidAl = f11jxldvo.getCols();
			ArrayList<StationNUnitValVO> urvoAl = f11jxldvo.getVals();
			String formid = f11jxldvo.getFormid();
			
			for ( String stationid : unitidAl )
			{
				try
				{
					int int_stationid = Integer.parseInt(stationid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Station ID: "+ex.getMessage(),0);
				}
			}
			for ( String pyid : pyidAl )
			{
				try
				{
					int int_pyid = Integer.parseInt(pyid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
				}
			}
			
			if ( urvoAl == null ) {
				return new MyAPIResponseTO("Invalid Data Segment !!", 0);
			}
			
			for ( StationNUnitValVO srvo : urvoAl ) {
				ArrayList<ArrayList<String>> units = srvo.getUnitValForStation();
				for ( ArrayList<String> unit : units ) {
					if ( pyidAl.size() != unit.size()-1 ) {
						return new MyAPIResponseTO("Invalid No. of Unit Data",0);
					}
					for ( int i = 1; i <  unit.size(); i++ ) {
						try
						{
							double val = Double.parseDouble(unit.get(i));
						} catch(Exception ex) {
							return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
						}
					}
				}
				
			}
			// From data update
			Form1_1aDataUpdateDAO f11dudao = new Form1_1aDataUpdateDAO(unitidAl, pyidAl, urvoAl,formid);
			boolean res = f11dudao.updateF1_1aMatrixToDatabase();
			if ( res ) {
				return new MyAPIResponseTO("Form 1.1a Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.1a Data update failed",0);
			}
			
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.1a Data update failed",0);
		}
		
	}
		
}
