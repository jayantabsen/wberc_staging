/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormP_D4jJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form_P_D4_DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormP_D4jJExcelDataVO;

@Secured
@Path("/")
public class Form_P_D4_JExcelService {

	
	@GET
	@Path("/f_P_D4_JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF_P_D4_JExcel(
			@QueryParam("stn_id") String stn_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("form_id") String form_id,
			@QueryParam("user_id") String user_id) throws SQLException, ParseException {
			
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		
		// Validate the petition header
		YearListDAO yldao = new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Petition Header Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		// Validate stationid
		ValidateStationid valistnid = new ValidateStationid();
		try {
			boolean flag = valistnid.validateStationid(stn_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Station Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		

		FormP_D4jJExcelDataVO data = null;
		FormP_D4jJExcelDAO f_P_D4_dao = new FormP_D4jJExcelDAO();
		try {
			data = f_P_D4_dao.getFormP_D4_JExcelData(stn_id, ph_id, form_id,user_id);
		} 
		catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve", 0);
		}
		return new MyAPIResponseTO("Form P(D4) matrix fetched Successfully", 1, data);
		
	}

	
	//	Data Update Service
	@POST
	@Path("/f_P_D4_DataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm_P_d4_Data(Form_P_D4_DataUpdateTO req) throws SQLException, ParseException {
		
		FormP_D4jJExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// Form data update
		FormP_D4jJExcelDAO f_P_D4_update_dao = new FormP_D4jJExcelDAO();
		try {
			boolean res = f_P_D4_update_dao.updateF_P_D4_JExcelData( data );
			if ( res ) {
				return new MyAPIResponseTO("Form P(D4) Data Matrix successfully saved",1);
			} else {
				return new MyAPIResponseTO("Form P(D4) Data save failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form P(D4) Data save failed",0);
		}
		
	}
}
