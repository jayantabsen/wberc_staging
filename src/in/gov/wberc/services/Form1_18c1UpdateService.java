//** By: JBS
package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_18c1UpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_18c1UpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_18c1JExcelDataVO;

@Secured
@Path("/f1_18c1Update")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_18c1UpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_18c1Update(Form1_18c1UpdateTO req)
			throws SQLException {
		
		Form1_18c1JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		if ( vals == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_18c1UpdateDAO f118c1udao = new Form1_18c1UpdateDAO();
		try {
			boolean res = f118c1udao.updateF1_18c1Update( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.18c(1) Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.18c(1) Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.18c(1) Data update failed",0);
		}
		
	}
	
}

