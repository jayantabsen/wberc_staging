package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.exception.MyException;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.LoginCredentialsVO;
import in.gov.wberc.vo.TokenVO;
import in.gov.wberc.vo.UserVO;
import in.gov.wberc.utils.MD5EncryptionUtils;
import in.gov.wberc.dao.AuthenticatorDAO;
import in.gov.wberc.dao.CredentialsDAO;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON)
public class LoginService {
	
	@POST
	public MyAPIResponseTO authenticateUser(LoginCredentialsVO user) throws SQLException {
		try {
			TokenVO token = new TokenVO();
			UserVO data = new UserVO();
			CredentialsDAO userCredentials = new CredentialsDAO();
			
			String password = user.getPassword();
			String loginID = user.getLoginID();
			
			if ( password == null || loginID == null || password.length() == 0 || loginID.length() == 0 ) {
				return new MyAPIResponseTO("Invalid credential",0);
			}
			
			String hashedPass="";
			
			AuthenticatorDAO authUser = new AuthenticatorDAO();
			
			String pwCompareStr = authUser.fetchUserPass(loginID, password);
			
			try {
				hashedPass=MD5EncryptionUtils.encryptSHA1(password);
			}catch(Exception e) {
				return new MyAPIResponseTO("Password couldn't be hashed",0);
			}
			
			boolean valid = authUser.authenticate(hashedPass, pwCompareStr);
			
			if(valid) {
				//checks user already in token database and validate token
				if(authUser.checkUserINTokenDatabase(loginID)!=true) {
					token.setToken(authUser.newUser(loginID));
					data = userCredentials.getUserCredentials(token.getToken());
					return new MyAPIResponseTO("Login Successful",1,data);
				} else {
					token.setToken(authUser.getToken(loginID));
					token.setToken((authUser.updateCurrentUserTokenValidity(loginID)));
					data = userCredentials.getUserCredentials(token.getToken());
					return new MyAPIResponseTO("Login Successful",1,data);
				}
			}
			else {
				return new MyAPIResponseTO("Invalid Login Credentials",0);
			}
		}
		catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("No login Credentials",0);
		}
	}
}
