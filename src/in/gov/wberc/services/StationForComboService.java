package in.gov.wberc.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.StationForComboDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.StationListF1_3VO;

@Secured
@Path("/stncombo")
@Produces(MediaType.APPLICATION_JSON)
public class StationForComboService {

	@GET
	public MyAPIResponseTO getStationForCombo(@QueryParam("form_id") String form_id,
			@QueryParam("util_id") String util_id) {
		
		if (form_id == null || form_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Form ID.",0);
		}
		if (util_id == null || util_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Utility ID.",0);
		}
		
		StationForComboDAO tsldao= new StationForComboDAO();
		
		try
		{
			switch ( Integer.parseInt(form_id) ) {
				case 5: 
				case 6:
					StationListF1_3VO data = null;
					try {
						data = tsldao.getStationsForCombo(util_id);
					} catch (MyException ex) {
						ex.printStackTrace();
						return new MyAPIResponseTO("Exception In Station for Combo Service",0);
					}
					return new MyAPIResponseTO("Stations for Combo Served Successfully",1,data);
				//***************************************************************************
				default:
					return new MyAPIResponseTO("Invalid Form Reference", 0);
			}
		} catch (Exception ex) {
			return new MyAPIResponseTO("Exception in: "+ex.getMessage(), 0);
		}
		
	}
	
}
