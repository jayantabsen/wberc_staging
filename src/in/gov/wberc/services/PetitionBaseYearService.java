package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.PetitionBaseYearJExcelDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.PetitionBaseYearJExcelDataVO;

@Secured
@Path("/pbyear")
@Produces(MediaType.APPLICATION_JSON)
public class PetitionBaseYearService {

	@GET
	public MyAPIResponseTO authenticateUser() throws SQLException {
		
		PetitionBaseYearJExcelDataVO data = null;
		PetitionBaseYearJExcelDAO pbydao = new PetitionBaseYearJExcelDAO();
		try {
			data = pbydao.getPetitionBaseYearJExcelData();
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Base Years Retrieve",0);
		}
		return new MyAPIResponseTO("Create Petition Year Combo Data fetched Successfully", 1, data);
		
	}
}
