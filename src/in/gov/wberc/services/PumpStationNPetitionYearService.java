package in.gov.wberc.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.PumpStationNPetitionYearDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.PumpStationNPetitionYearVO;

@Secured
@Path("/pstn_py")
@Produces(MediaType.APPLICATION_JSON)
public class PumpStationNPetitionYearService {

	@GET
	public MyAPIResponseTO getPumpStationDetails_PYears(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("pump_stn_id") String pump_stn_id) {
		
		if (form_id == null || form_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Form ID.",0);
		}
		if (ph_id == null || ph_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Petition ID.",0);
		}
		if (pump_stn_id == null || pump_stn_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Pump-Station ID.",0);
		}
		
		PumpStationNPetitionYearDAO psnpydao= new PumpStationNPetitionYearDAO();
		
		try
		{
			switch ( Integer.parseInt(form_id) ) {
				case 7:
					PumpStationNPetitionYearVO data = null;
					try {
						data = psnpydao.getPumpStationNPetitionYear(form_id, pump_stn_id, ph_id);
					} catch (MyException ex) {
						ex.printStackTrace();
						return new MyAPIResponseTO("Exception In Pump-Station for Combo Service!",0);
					}
					return new MyAPIResponseTO("Pump-Stations for Combo Served Successfully",1,data);
				//***************************************************************************
				default:
					return new MyAPIResponseTO("Invalid Form Reference", 0);
			}
		} catch (Exception ex) {
			return new MyAPIResponseTO("Exception in: "+ex.getMessage(), 0);
		}
		
	}
	
}
