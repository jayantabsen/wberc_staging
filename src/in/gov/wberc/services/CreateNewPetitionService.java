package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.CreateNewPetition4ARRDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.CreateNewPetition4ARRVO;
import in.gov.wberc.vo.CreateNewPetitionCredentialVO;

@Secured
@Path("/create_new_petition")
@Produces(MediaType.APPLICATION_JSON)
public class CreateNewPetitionService {

	@POST
	public MyAPIResponseTO createNewPetitionYear(CreateNewPetitionCredentialVO cred) throws SQLException {
		
		String base_year = cred.getBase_year();
		String prev_year = cred.getPrev_year();
		String ensu_year = cred.getEnsu_year();
		String utilid = cred.getUtilid();
		String userid = cred.getUserid();
		String module = cred.getModule().toUpperCase();
		
		if ( !base_year.matches("\\d{4}-\\d{2}") ) {
			return new MyAPIResponseTO("Invalid Pretition Base Year Credentials. Use yyyy-yy.",0);
		}
		int int_prev_year = 0;
		try {
			int_prev_year = Integer.parseInt(prev_year);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid No. Of Previous Years.",0);
		}
		int int_ensuing_year = 0;
		try {
			int_ensuing_year = Integer.parseInt(ensu_year);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid No. Of Ensuing Years.",0);
		}
		
		CreateNewPetition4ARRDAO cnpdao = new CreateNewPetition4ARRDAO();
		CreateNewPetition4ARRVO data = null;
		try
		{
			data = cnpdao.createNewPetition4ARR(base_year, int_prev_year, int_ensuing_year, utilid, userid, module);
			if ( data != null ) {
				return new MyAPIResponseTO("New Petition Year Created Successfully.",1, data);
			} else {
				return new MyAPIResponseTO("Petition Year Creation failed!",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Petition Year Creation failed!",0);
		}
		
	}
	
}
