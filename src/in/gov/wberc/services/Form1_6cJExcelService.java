//*** By Najma Updated By JBS
package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_6cJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17JExcelDataVO;

@Secured
@Path("/f1_6cJExcelDataRead")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_6cJExcelService {
	
	@GET
	public MyAPIResponseTO getF1_6cJExcel(@QueryParam("py_id") String py_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("form_id") String form_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
	
		Form1_17JExcelDataVO data = null;
		Form1_6cJExcelDAO f16cjxldao = new Form1_6cJExcelDAO();
		try {
			data = f16cjxldao.getF1_6cJExcelData(py_id, util_id, form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.6c matrix fetched Successfully",1, data);
	}

}
