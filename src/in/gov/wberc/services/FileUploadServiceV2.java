package in.gov.wberc.services;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.utils.DBUtil;

@Path("/file_upload_v2")
public class FileUploadServiceV2 {

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO fileUpload(
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("file") FormDataBodyPart body,
			@FormDataParam("docName") String docName,
			@FormDataParam("description") String description,
			@FormDataParam("petitionHeaderId") String petitionHeaderId,
			@FormDataParam("AttachmentType") String AttachmentType,
			@FormDataParam("formID") String formID,
			@FormDataParam("utilid") String utilid,
			@FormDataParam("module") String module)  {
		
		// Required the above field validations
		if ( uploadedInputStream == null ) {
			return new MyAPIResponseTO("Invalid File !!!", 0);
		}
		if ( docName == null || docName.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid File Name !!!", 0);
		}
		if ( petitionHeaderId == null || petitionHeaderId.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid Utility !!!", 0);
		}
		if ( AttachmentType == null || AttachmentType.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid Module !!!", 0);
		}
		if ( formID == null || formID.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid Module !!!", 0);
		}
		if ( utilid == null || utilid.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid Module !!!", 0);
		}
		if ( module == null || module.trim().length() == 0 ) {
			return new MyAPIResponseTO("Invalid Module !!!", 0);
		}
		
		String mimeType = body.getMediaType().toString();
		
		// Getting base_year, utility-name
		String base_year = "";
		String util_abbr = "";
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PETITION_YEAR_FORM_PHID;
		String query2 = QueryConstants.QUERY_GET_UTILITY_ABBREVIATION_BY_ID;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(petitionHeaderId));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					base_year = rs1.getString("base_year");
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, Integer.parseInt(utilid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					util_abbr = rs1.getString("util_code");
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			//throw new MyException();
			return new MyAPIResponseTO("SQL Exception", 0); 
		} finally {
			try {
				if(prstmt1!=null) {
					prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
				//throw new MyException();
				return new MyAPIResponseTO("SQL Exception", 0); 
			}
		}
		
		String serverFilePath = "/WBERC/" + base_year + "/" + util_abbr + "/" + module.toUpperCase().trim();
		
		Session session = createCMISSession("182.75.177.246", "8282");
		Folder parent = (Folder) session.getObjectByPath(serverFilePath);
		// properties 
		// (minimal set: name and object type id)
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "D:erms:Attachments");
		properties.put(PropertyIds.NAME, docName);
		List<String> secondary = new ArrayList();
		secondary.add("P:cm:titled");
		properties.put(PropertyIds.SECONDARY_OBJECT_TYPE_IDS, secondary);
		//properties.put("cm:title", "My Very Fancy Document");
		properties.put("cm:description", description);
		properties.put("erms:FormID", formID);
		properties.put("erms:PetitionID", petitionHeaderId);
		properties.put("erms:AttachmentType", AttachmentType);
		
		ContentStream contentStream = new ContentStreamImpl(docName, BigInteger.valueOf(fileDetail.getSize()), 
				mimeType, uploadedInputStream);
		
		// create a major version
		try
		{
			Document newDoc = parent.createDocument(properties, contentStream, VersioningState.MAJOR);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Duplicate File Name.",0);
		}
		
		return new MyAPIResponseTO("File Uploaded Successfully.",1);
	}
	
	public Session createCMISSession(String host, String port) {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "password");

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, "http://"+host+":"+port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Create session.
		// Alfresco only provides one repository.
		Repository repository = factory.getRepositories(parameter).get(0);
		System.out.println("Rep folder name-->"+repository.getId());

		parameter.put(SessionParameter.REPOSITORY_ID, repository.getId());

		// create session
		Session session = factory.createSession(parameter);
		return session;
	}
	
}
