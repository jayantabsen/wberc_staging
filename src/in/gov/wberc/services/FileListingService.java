package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FileListingDAO;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FileListingVO;

@Secured
@Path("/file_listing")
@Produces(MediaType.APPLICATION_JSON)
public class FileListingService {

	@GET
	public MyAPIResponseTO fileListing(
			@QueryParam("ph_id") String ph_id,
			@QueryParam("form_id") String form_id) throws MyException, SQLException {
		
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Header Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		FileListingVO data = null;
		try {
			data = FileListingDAO.getListOfFile_ById(ph_id, form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Request Execution Error !!!",0);
		}
		return new MyAPIResponseTO("File Listing Retrieved Successfully !!!", 1, data);
		
	}
	
}
