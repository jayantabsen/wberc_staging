package in.gov.wberc.services;

import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_1DataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.From1_1DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_1JExcelDataVO;
import in.gov.wberc.vo.StationRowVO;

@Secured
@Path("/f1_1DataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_1DataUpdateService {

	@POST
	public MyAPIResponseTO updateForm1_1Data(From1_1DataUpdateTO req) {
		
		From1_1JExcelDataVO f11jxldvo = (From1_1JExcelDataVO)req.getData();
		try {
			ArrayList<String> stationidAl = f11jxldvo.getRows();
			ArrayList<String> pyidAl = f11jxldvo.getCols();
			ArrayList<StationRowVO> srvoAl = f11jxldvo.getVals();
			
			// Numeric field validation
			for ( String stationid : stationidAl )
			{
				try
				{
					int int_stationid = Integer.parseInt(stationid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Station ID: "+ex.getMessage(),0);
				}
			}
			for ( String pyid : pyidAl )
			{
				try
				{
					int int_pyid = Integer.parseInt(pyid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
				}
			}
			
			// From data update
			Form1_1DataUpdateDAO f11dudao = new Form1_1DataUpdateDAO(stationidAl, pyidAl, srvoAl);
			boolean res = f11dudao.updateF1_1MatrixToDatabase();
			if ( res ) {
				return new MyAPIResponseTO("Form 1.1 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.1 Data update failed",0);
			}
			
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.1 Data update failed",0);
		}
		
	}
	
}
