package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_4aDataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_4aDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_4aJExcelDataVO;

@Secured
@Path("/f1_4aDataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_4aDataUpdateService {

	@POST
	public MyAPIResponseTO updateForm1_4aData(Form1_4aDataUpdateTO req) throws SQLException {
		
		Form1_4aJExcelDataVO data = req.getData();
		String stationid = data.getStationid();
		ArrayList<String> cols = data.getCols();
		ArrayList<String> seasons = data.getSeasons();
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		// Station Id Validation
		if ( stationid == null || stationid.length() == 0 ) {
			return new MyAPIResponseTO("Invalid Station Id !!", 0);
		} else {
			try {
				Integer.parseInt(stationid);
			} catch (NumberFormatException ex) {
				return new MyAPIResponseTO("Invalid Station Id !!", 0);
			}
		}
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}
		// season-id validation
		for ( String seasonid : seasons )
		{
			try
			{
				Integer.parseInt(seasonid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid SeasonId !!", 0);
			}
		}
		
		Form1_4aDataUpdateDAO f14adudao = new Form1_4aDataUpdateDAO();
		try {
			boolean res = f14adudao.updateF1_4aMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.4a Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.4a Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.4a Data update failed",0);
		}
		
	}
	
}
