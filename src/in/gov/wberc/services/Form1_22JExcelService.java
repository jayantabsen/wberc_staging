package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_22JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_22JExcelDataVO;

@Secured
@Path("/f1_22JExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_22JExcelService {

	@GET
	public MyAPIResponseTO getF1_22JExcel(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_22JExcelDataVO data = null;
		Form1_22JExcelDAO f117jjxldao = new Form1_22JExcelDAO();
		try {
			data = f117jjxldao.getF1_22JExcelData(form_id, util_id, ph_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.22 Matrix Fetched Successfully",1, data);
		
	}
	
}
