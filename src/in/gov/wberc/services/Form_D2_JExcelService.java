/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form_D2_JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form_D2_DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form_D2_JExcelDataVO;

@Secured
@Path("/")
public class Form_D2_JExcelService {
	
	@GET
	@Path("/f_D2_JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF_A_JExcel(
			@QueryParam("stn_id") String stn_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("form_id") String form_id) throws SQLException, ParseException {
			
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		
		// Validate the petition header
		YearListDAO yldao = new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Petition Header Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		// Validate stationid
		ValidateStationid valistnid = new ValidateStationid();
		try {
			boolean flag = valistnid.validateStationid(stn_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Station Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		

		Form_D2_JExcelDataVO data = null;
		Form_D2_JExcelDAO f_D2_dao = new Form_D2_JExcelDAO();
		try {
			data = f_D2_dao.getF_D2_JExcelData(stn_id, ph_id, form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve", 0);
		}
		return new MyAPIResponseTO("Form D2 matrix fetched Successfully", 1, data);
		
	}

	
	
//	Data Update Service
	@POST
	@Path("/f_D2_DataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm_D2_Data(Form_D2_DataUpdateTO req) throws SQLException, ParseException {
		
		Form_D2_JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// Form data update
		Form_D2_JExcelDAO f_D2_update_dao = new Form_D2_JExcelDAO();
		try {
			boolean res = f_D2_update_dao.updateF_D2_JExcelData( data );
			if ( res ) {
				return new MyAPIResponseTO("Form D2 Data Matrix successfully saved",1);
			} else {
				return new MyAPIResponseTO("Form D2 Data save failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form D2 Data save failed",0);
		}
		
		
	}
}
