package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.LabeledYearListVO;

@Secured
@Path("/petitionyear")
@Produces(MediaType.APPLICATION_JSON)
public class YearService {

	@GET
	public MyAPIResponseTO getPetitionYears(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials",0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		switch ( Integer.parseInt(form_id) ) {
			case 1: 
			case 5:
			case 9:
				LabeledYearListVO data = null;
				try {
					data = yldao.getPetitionYearLst(ph_id);
				} catch (MyException ex) {
					ex.printStackTrace();
					return new MyAPIResponseTO("Exception In Pretition Year Retrieve", 0);
				}
				return new MyAPIResponseTO("Petition years fetched Successfully", 1, data);
			//***************************************************************************
			default:
				return new MyAPIResponseTO("Invalid Form Reference", 0);
		}
	}
	
}

