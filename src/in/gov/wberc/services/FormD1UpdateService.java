//** By: JBS

package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormD1UpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.FormD1UpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormD1JExcelDataVO;

@Secured
@Path("/fD1Update")
@Produces(MediaType.APPLICATION_JSON)
public class FormD1UpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_17jUpdate(FormD1UpdateTO req)
			throws SQLException {
		
		FormD1JExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols(); // coalgrade_id(s)
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}

		if ( vals == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		FormD1UpdateDAO fd1udao = new FormD1UpdateDAO();
		try {
			boolean res = fd1udao.updateFD1Update( data );
			if ( res ) {
				return new MyAPIResponseTO("Form D1 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form D1 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form D1 Data update failed",0);
		}
		
	}
	
}

