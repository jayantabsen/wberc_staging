package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.AttachmentTypeDAO;
import in.gov.wberc.dao.Form1_4bJExcelDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.AttachmentTypeVO;
import in.gov.wberc.vo.Form1_4bJExcelDataVO;

@Secured
@Path("/attachment_type")
@Produces(MediaType.APPLICATION_JSON)
public class AttachmentTypeService {

	@GET
	public MyAPIResponseTO getAttachmentType(@QueryParam("attachment_type_id") String attachment_type_id) throws SQLException {
		
		AttachmentTypeVO data = null;
		AttachmentTypeDAO attypedao = new AttachmentTypeDAO();
		try {
			data = attypedao.getAttachmentType(attachment_type_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid Attachment Type.",0);
		}
		
		return new MyAPIResponseTO("Attachment Type Successfully Delivered.", 1, data);
		
	}
	
}
