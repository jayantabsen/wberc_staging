//** By: JBS

package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_22UpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_22UpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_22JExcelDataVO;

@Secured
@Path("/f1_22Update")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_22UpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_22Update(Form1_22UpdateTO req)
			throws SQLException {
		
		Form1_22JExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}

		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_22UpdateDAO f122adudao = new Form1_22UpdateDAO();
		try {
			boolean res = f122adudao.updateF1_22Update( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.22 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.22 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.22 Data update failed",0);
		}
		
	}
	
}

