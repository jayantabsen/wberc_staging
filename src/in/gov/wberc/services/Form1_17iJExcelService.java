package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17iJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17iDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17iJExcelDataVO;

@Secured
@Path("/")
public class Form1_17iJExcelService {

	@GET
	@Path("/f1_17iJExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_17iJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("py_id") String py_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the py_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Ensuing Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_17iJExcelDataVO data = null;
		Form1_17iJExcelDAO f117ijxldao = new Form1_17iJExcelDAO();
		try {
			data = f117ijxldao.getF1_17iJExcelData(form_id, py_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.17i matrix fetched Successfully",1, data);
		
	}
	
	@POST
	@Path("/f1_17iDataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_7iData(Form1_17iDataUpdateTO req) throws SQLException, ParseException {
		
		Form1_17iJExcelDataVO data = req.getData();
		
		// From data update
		Form1_17iJExcelDAO f117iudao = new Form1_17iJExcelDAO();
		try {
			boolean res = f117iudao.updateF1_17iMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.17i Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.17i Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.17i Data update failed",0);
		}
		
	}
	
}
