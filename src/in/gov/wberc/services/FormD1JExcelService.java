package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormD1JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormD1JExcelDataVO;

@Secured
@Path("/fD1JExcel")
@Produces(MediaType.APPLICATION_JSON)
public class FormD1JExcelService {

	@GET
	public MyAPIResponseTO getF1_17jJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("stn_id") String stn_id,
			@QueryParam("py_id") String py_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the py_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Ensuing Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		FormD1JExcelDataVO data = null;
		FormD1JExcelDAO fd1jxldao = new FormD1JExcelDAO();
		try {
			data = fd1jxldao.getFD1JExcelData(form_id, util_id, stn_id, py_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form D(1) matrix fetched Successfully", 1, data);
		
	}
	
}
