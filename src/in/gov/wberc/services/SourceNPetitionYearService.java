//*** By Najma
package in.gov.wberc.services;

import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.SourceNPetitionYearDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.SourceNPetitionYearVO;
import in.gov.wberc.vo.SourcePyCredentialVO;

@Secured
//@Path("/source_py")
@Produces(MediaType.APPLICATION_JSON)
public class SourceNPetitionYearService {

	@POST
	public MyAPIResponseTO getStationDetails_PYears(SourcePyCredentialVO cred) {
		
		String formid = cred.getFormid();
		String pdid = cred.getPdid();
		String sourceid = cred.getSourceid();
		
		if (formid == null || formid.length() == 0) {
			return new MyAPIResponseTO("Invalid Form ID.",0);
		}
		if (pdid == null || pdid.length() == 0) {
			return new MyAPIResponseTO("Invalid Petition ID.",0);
		}
		if (sourceid == null || sourceid.length() == 0) {
			return new MyAPIResponseTO("Invalid Source ID.",0);
		}
		
		SourceNPetitionYearDAO sourcepydao= new SourceNPetitionYearDAO();
		
		try
		{
			switch ( Integer.parseInt(formid) ) {
				case 5: 
				case 6:
				case 9:
					SourceNPetitionYearVO data = null;
					try {
						data = sourcepydao.getSourceNPetitionYear(formid, sourceid, pdid);
					} catch (MyException ex) {
						ex.printStackTrace();
						return new MyAPIResponseTO("Exception In Source for Combo Service!",0);
					}
					return new MyAPIResponseTO("Sources for Combo Served Successfully",1,data);
				//***************************************************************************
				default:
					return new MyAPIResponseTO("Invalid Form Reference", 0);
			}
		} catch (Exception ex) {
			return new MyAPIResponseTO("Exception in: "+ex.getMessage(), 0);
		}
		
	}
	
}

