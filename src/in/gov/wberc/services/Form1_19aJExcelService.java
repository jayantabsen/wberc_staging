/*** @author Najma on 01-Feb-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_19aJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_19aUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_19aJExcelDataVO;
import in.gov.wberc.vo.Form1_19aObjectVO;

@Secured
@Path("/")
public class Form1_19aJExcelService {
	@GET
	@Path("/f1_19aJExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_19aJExcel(
			@QueryParam("form_id") String form_id,
			@QueryParam("util_id") String util_id ,
			@QueryParam("ph_id") String ph_id ,
			@QueryParam("user_id") String user_id,
			@QueryParam("expenditure") String expenditure
			) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		
		// Validate utilid for WBSEDCL or any other
		if(Integer.parseInt(util_id) != 0) {
			ValidateUtilid valiutilid = new ValidateUtilid();
			try {
				boolean flag = valiutilid.validateUtilid(util_id);
				if ( flag == false ) {
					return new MyAPIResponseTO("Invalid Utility Credentials", 0);
				}
			} catch (MyException e) {
				e.printStackTrace();
			}
		}
		
		Form1_19aJExcelDataVO data = null;
		Form1_19aJExcelDAO f1_19a_dao = new Form1_19aJExcelDAO();
		try {
			data = f1_19a_dao.getF1_19aData( form_id, util_id, ph_id, user_id, expenditure);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form1.19a Matrix Fetched Successfully",1, data);
		
	}

	@POST
	@Path("/f1_19aDataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_19aData(Form1_19aUpdateTO req) throws SQLException, ParseException {
		
		Form1_19aJExcelDataVO data = req.getData();
		
		Form1_19aObjectVO values = new Form1_19aObjectVO();
		values = data.getVals();
		
		ArrayList<ArrayList<String>> valuesCapitalExpenditure = values.getVals_for_miscs_for_CapitalExpenditure();
		ArrayList<ArrayList<String>> valuesSpecialProjects = values.getVals_for_miscs_for_SpecialProjects();
		ArrayList<ArrayList<String>> valuesStations = values.getVals_for_stations();

		if ( valuesCapitalExpenditure == null || valuesSpecialProjects== null || valuesStations== null) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		// From data update
		Form1_19aJExcelDAO f1_19a_dao = new Form1_19aJExcelDAO();
		try {
			boolean res = f1_19a_dao.updateF1_19aData( data );
			if ( res ) {
				return new MyAPIResponseTO("From 1.19a Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("From 1.19a Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("From 1.19a Data update failed",0);
		}
		
	}

}
