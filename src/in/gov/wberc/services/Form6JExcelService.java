/*** @author Najma on 31-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form_6JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form6DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form_6_JExcelDataVO;

@Secured
@Path("/")
public class Form6JExcelService {
	@GET
	@Path("/f6JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_23JExcel(
			@QueryParam("ph_id") String ph_id ,
			@QueryParam("form_id") String form_id,
			@QueryParam("user_id") String user_id
			) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form_6_JExcelDataVO data = null;
		Form_6JExcelDAO f6_dao = new Form_6JExcelDAO();
		try {
			data = f6_dao.getF_6JExcelData( ph_id, form_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 6 Matrix Fetched Successfully",1, data);
	}

	//	Data Update Service
	@POST
	@Path("/f6DataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm6Data(Form6DataUpdateTO req) throws SQLException, ParseException {
		
		Form_6_JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values_for_incomes = data.getVals_for_Incomes();
		ArrayList<ArrayList<String>> values_for_expenses = data.getVals_for_Expenses();
		ArrayList<ArrayList<String>> values_for_assets = data.getVals_for_Assets();
		ArrayList<ArrayList<String>> values_for_add_fund = data.getVals_for_UtilisationAddFund();
		ArrayList<ArrayList<String>> values_for_capital_fund = data.getVals_for_UtilisationCapitalFund();


		if ( values_for_incomes == null || values_for_expenses == null || values_for_assets == null || values_for_add_fund == null || values_for_capital_fund == null) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form_6JExcelDAO f6_update_dao = new Form_6JExcelDAO();
		try {
			boolean res = f6_update_dao.updateF6( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 6 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 6 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 6 Data update failed",0);
		}
		
		
	}
}
