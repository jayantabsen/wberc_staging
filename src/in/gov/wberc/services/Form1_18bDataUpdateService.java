package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_18bDataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_18DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_18JExcelDataVO;

@Secured
@Path("/f1_18bDataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_18bDataUpdateService {

	@POST
	public MyAPIResponseTO updateForm1_18bData(Form1_18DataUpdateTO req) throws SQLException {
		
		Form1_18JExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}


		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_18bDataUpdateDAO f1_18b_dao = new Form1_18bDataUpdateDAO();
		try {
			boolean res = f1_18b_dao.updateF1_18bMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.18b Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.18b Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.18b Data update failed",0);
		}
		
		
	}
}
