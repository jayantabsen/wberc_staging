//** By Najma Updated By: JBS
package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17gUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17gUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17gJExcelDataVO;

@Secured
@Path("/f1_17gUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17gUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_17gUpdate(Form1_17gUpdateTO req) throws SQLException {
		
		Form1_17gJExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}

		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_17gUpdateDAO f117gdudao = new Form1_17gUpdateDAO();
		try {
			boolean res = f117gdudao.updateF1_17gUpdate( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.17g Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.17g Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.17g Data update failed",0);
		}
		
		
	}
	
}

