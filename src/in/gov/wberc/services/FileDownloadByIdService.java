package in.gov.wberc.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;

import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;

//@Secured
@Path("/file_download_by_id")
@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.WILDCARD })
public class FileDownloadByIdService {

	@GET
	public Response fileDownload(
			@QueryParam("docId") String docId) throws MyException, IOException {
		
		Session session = createCMISSession("182.75.177.246", "8282");
		Document document = (Document)session.getObject(docId);
		
		Property<String> p = document.getProperty(PropertyIds.OBJECT_ID);

		String s = p.getValue();
		
		final InputStream in = document.getContentStream().getStream();
		
		StreamingOutput stream = null;
		try {
	        stream = new StreamingOutput() {
	            public void write(OutputStream out) throws IOException, WebApplicationException {
	                try {
	                    int read = 0;
	                        byte[] bytes = new byte[1024];

	                        while ((read = in.read(bytes)) != -1) {
	                            out.write(bytes, 0, read);
	                        }
	                } catch (Exception e) {
	                    throw new WebApplicationException(e);
	                }
	            }
	        };
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		return Response.ok(stream).header("Content-Disposition","attachment; filename = "+document.getName()).build();
	}
	
	public Session createCMISSession(String host, String port) {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "password");

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, "http://"+host+":"+port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Create session.
		// Alfresco only provides one repository.
		Repository repository = factory.getRepositories(parameter).get(0);
		System.out.println("Rep folder name-->"+repository.getId());

		parameter.put(SessionParameter.REPOSITORY_ID, repository.getId());

		// create session
		Session session = factory.createSession(parameter);
		return session;
	}
	
}
