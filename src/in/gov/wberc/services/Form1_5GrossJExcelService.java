//* By Najma
package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_5GrossJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_5GrossJExcelDataVO;

@Secured
@Path("/f1_5SumJExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_5GrossJExcelService {

	@GET
	public MyAPIResponseTO getF1_5JExcel(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_5GrossJExcelDataVO data = null;
		Form1_5GrossJExcelDAO f15gjxldao = new Form1_5GrossJExcelDAO();
		try {
			data = f15gjxldao.getF1_5GrossJExcelData(ph_id,form_id,util_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.5<Gross> matrix fetched Successfully", 1, data);
		
	}
	
}
