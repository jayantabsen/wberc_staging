package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_4bJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidatePumpStationid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_4bJExcelDataVO;

@Secured
@Path("/f1_4bJExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_4bJExcelData {

	@GET
	public MyAPIResponseTO getF1_4bJExcel(@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("form_id") String form_id,
			@QueryParam("pump_stn_id") String pump_stn_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate pump_stationid
		ValidatePumpStationid valipstanid = new ValidatePumpStationid();
		try {
			boolean flag = valipstanid.validateStationid(pump_stn_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pump-Station ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_4bJExcelDataVO data = null;
		Form1_4bJExcelDAO f14bjxldao = new Form1_4bJExcelDAO();
		try {
			data = f14bjxldao.getF1_4bJExcelData(ph_id, form_id, util_id, pump_stn_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.4b matrix fetched Successfully", 1, data);
		
	}

}
