package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17dJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17dJExcelDataVO;

@Secured
@Path("/f1_17dJExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17dJExcelService {

	@GET
	public MyAPIResponseTO getF1_17dJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("py_id") String py_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the petition year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_17dJExcelDataVO data = null;
		Form1_17dJExcelDAO f117djxldao = new Form1_17dJExcelDAO();
		try {
			data = f117djxldao.getF1_17dJExcelData(form_id, py_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.17d matrix fetched Successfully",1, data);
		
	}
	
}
