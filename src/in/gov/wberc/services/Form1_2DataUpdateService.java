package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_2DataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.From1_1DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_1JExcelDataVO;
import in.gov.wberc.vo.StationRowVO;

@Secured
@Path("/f1_2DataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_2DataUpdateService {
	@POST
	public MyAPIResponseTO updateForm1_1Data(From1_1DataUpdateTO req) throws SQLException {
		
		From1_1JExcelDataVO f11jxldvo = (From1_1JExcelDataVO)req.getData();
		try {
			ArrayList<String> stationidAl = f11jxldvo.getRows();
			ArrayList<String> pyidAl = f11jxldvo.getCols();
			ArrayList<StationRowVO> srvoAl = f11jxldvo.getVals();
			
			// Numeric field validation
			for ( String stationid : stationidAl )
			{
				try
				{
					int int_stationid = Integer.parseInt(stationid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Station ID: "+ex.getMessage(),0);
				}
			}
			for ( String pyid : pyidAl )
			{
				try
				{
					int int_pyid = Integer.parseInt(pyid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
				}
			}
			
			// From data update
			Form1_2DataUpdateDAO f12dudao = new Form1_2DataUpdateDAO(stationidAl, pyidAl, srvoAl);
			boolean res = f12dudao.updateF1_2MatrixToDatabase();
			if ( res ) {
				return new MyAPIResponseTO("Form 1.2 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.2 Data update failed",0);
			}
			
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.2 Data update failed",0);
		}
		
	}
}
