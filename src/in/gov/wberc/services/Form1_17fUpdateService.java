//** By: JBS
package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17fUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17fUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17fJExcelDataVO;

@Secured
@Path("/f1_17fUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17fUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_17fUpdate(Form1_17fUpdateTO req)
			throws SQLException {
		
		Form1_17fJExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> vals = data.getVals();
		
		if ( vals == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_17fUpdateDAO f117fudao = new Form1_17fUpdateDAO();
		try {
			boolean res = f117fudao.updateF1_17fUpdate( data );
			if ( res ) {
				return new MyAPIResponseTO("From1.17f Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("From1.17f Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("From1.17f Data update failed",0);
		}
		
	}
	
}

