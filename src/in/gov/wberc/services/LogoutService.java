package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import in.gov.wberc.dao.AuthenticationTokenDAO;
import in.gov.wberc.dao.CredentialsDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.UserVO;

import javax.ws.rs.core.MediaType;

@Secured
@Path("/logout")
public class LogoutService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO onLogOut(@HeaderParam("Authorization")String authorizationHeader) {
		try {
			String token = authorizationHeader.substring("Bearer".length()).trim();
			
			UserVO user = new UserVO();
			CredentialsDAO userCredentials = new CredentialsDAO();
			user = userCredentials.getUserCredentials(token);
			
			
			AuthenticationTokenDAO auth = new AuthenticationTokenDAO();
			System.out.println("token"+token);
			System.out.println("login id"+user.getLoginid());

			auth.logOutUser(user.getLoginid(), token);
			return new MyAPIResponseTO("Logged out", 1);
			
		}
		catch(MyException e1) {
			return new MyAPIResponseTO("Error Etrade", 0);
		}
		catch(SQLException e1) {
			return new MyAPIResponseTO("Error sql", 0);
		}
	}
	

}
