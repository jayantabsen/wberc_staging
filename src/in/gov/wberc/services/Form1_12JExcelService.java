//*** By Najma Updated By: JBS
package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_12JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_11JExcelDataVO;

@Secured
@Path("/f1_12JExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_12JExcelService {
	
	@GET
	public MyAPIResponseTO getF1_12JExcel(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("stn_id") String stn_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate stationid for WBSEDCL or any other
		ValidateStationid valstid = new ValidateStationid();
		try {
			boolean flag = valstid.validateStationid(stn_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Station Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
	
		From1_11JExcelDataVO data = null;
		Form1_12JExcelDAO f112jxldao = new Form1_12JExcelDAO();
		try {
			data = f112jxldao.getF1_12JExcelData(form_id, ph_id, stn_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.12 matrix fetched Successfully",1, data);
	}

}
