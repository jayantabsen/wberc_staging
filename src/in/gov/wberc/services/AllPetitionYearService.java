package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.AllPetitionYearDAO;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.AllPetitionYearVO;

@Secured
@Path("/all_petition_year")
@Produces(MediaType.APPLICATION_JSON)
public class AllPetitionYearService {

	@GET
	public MyAPIResponseTO getAllYearFromPetition(@QueryParam("ph_id") String ph_id) throws SQLException {
		
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Header Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		AllPetitionYearVO data = null;
		AllPetitionYearDAO apydao = new AllPetitionYearDAO();
		try {
			data = apydao.getAllPetitionYears(ph_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Ensuing Years Retrieve", 0);
		}
		return new MyAPIResponseTO("All Petition Year Combo Data fetched Successfully", 1, data);
		
	}
	
}
