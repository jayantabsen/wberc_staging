/*12.01.2018 Priyanka..@Created*/
package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.AnnexureListDao;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.PititionHeaderVo;

@Secured
@Path("/getAnnexureList")
@Produces(MediaType.APPLICATION_JSON)
public class AnnexureListService  {
	
	@GET
	public MyAPIResponseTO annexureList(@QueryParam("phid") String phid,@QueryParam("utilid") String utilid) throws SQLException {
		AnnexureListDao annexureListDao = new AnnexureListDao();
		try {
			PititionHeaderVo data = annexureListDao.getAnnexureList(phid,utilid);
			if(data!=null) {
				System.out.println(data);
				return new MyAPIResponseTO("Annexure List", 1, data);
			}else {
				return new MyAPIResponseTO("No Form Found For This Pitition",0);
			}
			
		}
		catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("No Form Found For This Pitition",0);
		}
	}
}

