package in.gov.wberc.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.StationNPetitionYearDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.StationNPetitionYearVO;

@Secured
@Path("/stn_py")
@Produces(MediaType.APPLICATION_JSON)
public class StationNPetitionYearService {

	@GET
	public MyAPIResponseTO getStationDetails_PYears(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("station_id") String station_id) {
		
		if (form_id == null || form_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Form ID.",0);
		}
		if (ph_id == null || ph_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Petition ID.",0);
		}
		if (station_id == null || station_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Utility ID.",0);
		}
		
		StationNPetitionYearDAO snpydao= new StationNPetitionYearDAO();
		
		try
		{
			switch ( Integer.parseInt(form_id) ) {
				case 5:
					StationNPetitionYearVO data = null;
					try {
						data = snpydao.getStationNPetitionYear(form_id, station_id, ph_id);
					} catch (MyException ex) {
						ex.printStackTrace();
						return new MyAPIResponseTO("Exception In Station for Combo Service!",0);
					}
					return new MyAPIResponseTO("Stations for Combo Served Successfully",1,data);
				//***************************************************************************
				default:
					return new MyAPIResponseTO("Invalid Form Reference", 0);
			}
		} catch (Exception ex) {
			return new MyAPIResponseTO("Exception in: "+ex.getMessage(), 0);
		}
		
	}
	
}
