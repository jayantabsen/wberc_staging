/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_21JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_21DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_21JExcelDataVO;

@Secured
@Path("/")
public class Form1_21JExcelService {
	
	@GET
	@Path("/f1_21JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_22JExcel(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid From ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_21JExcelDataVO data = null;
		Form1_21JExcelDAO f1_21_dao = new Form1_21JExcelDAO();
		try {
			data = f1_21_dao.getF1_21JExcelData(form_id, ph_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.21 Matrix Fetched Successfully",1, data);
		
	}


	//	Data Update Service
	@POST
	@Path("/f1_21DataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_21Data(Form1_21DataUpdateTO req) throws SQLException, ParseException {
		
		Form1_21JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_21JExcelDAO f121_update_dao = new Form1_21JExcelDAO();
		try {
			boolean res = f121_update_dao.updateF1_21( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.21 Data Matrix successfully saved",1);
			} else {
				return new MyAPIResponseTO("Form 1.21 Data save failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.21 Data save failed",0);
		}
		
		
	}

}
