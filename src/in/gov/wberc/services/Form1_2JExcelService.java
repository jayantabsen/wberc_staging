package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.From1_2JExcelDAO;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_1JExcelDataVO;

@Secured
@Path("/f1_2JExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_2JExcelService {

	@GET
	public MyAPIResponseTO getF1_2JExcel(@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id) throws SQLException {
		
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		if(Integer.parseInt(util_id) != 0) {
			ValidateUtilid valiutilid = new ValidateUtilid();
			try {
				boolean flag = valiutilid.validateUtilid(util_id);
				if ( flag == false ) {
					return new MyAPIResponseTO("Invalid Utility Credentials", 0);
				}
			} catch (MyException e) {
				e.printStackTrace();
			}
		}
		
		From1_1JExcelDataVO data = null;
		From1_2JExcelDAO f12jxldao = new From1_2JExcelDAO();
		try {
			data = f12jxldao.getF1_2JExcelData(ph_id, util_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.2 matrix fetched Successfully", 1, data);
		
	}
}
