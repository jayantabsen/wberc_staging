package in.gov.wberc.services;

import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.NewPetitionMsgCredentialVO;
import in.gov.wberc.vo.NewPetitionYearMessageVO;

@Secured
@Path("/new_pyear_msg")
@Produces(MediaType.APPLICATION_JSON)
public class PetitionYearMsgService {
	
	@POST
	public MyAPIResponseTO computePetitionYearComponents(NewPetitionMsgCredentialVO cred) {
		
		String base_year = cred.getBase_year();
		String prev_year = cred.getPrev_year();
		String ensu_year = cred.getEnsu_year();
		
		if ( !base_year.matches("\\d{4}-\\d{2}") ) {
			return new MyAPIResponseTO("Invalid Pretition Base Year Credentials. Use yyyy-yy.",0);
		}
		int int_prev_year = 0;
		try {
			int_prev_year = Integer.parseInt(prev_year);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid No. Of Previous Years.",0);
		}
		int int_ensuing_year = 0;
		try {
			int_ensuing_year = Integer.parseInt(ensu_year);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid No. Of Ensuing Years.",0);
		}
		
		ArrayList<String> prev_years = new ArrayList<String>();
		int int_y1 = Integer.parseInt(base_year.substring(0, 4)); //2017
		for (int i = int_prev_year - 1; i>=0; i--) {
			prev_years.add(String.valueOf(int_y1 - (i + 1)) + "-" + String.valueOf(int_y1 - i).substring(2));
		}
		StringBuilder msg = new StringBuilder("You are about to initiate a new ARR/MYT petition with Base Year "
		+ base_year + ", Previous Year(s): ");
		for ( String py : prev_years ) {
			msg.append(py + ", ");
		}
		msg = new StringBuilder(msg.toString().substring(0, msg.length()-2));
		msg.append(" and Ensuing Year(s): ");
		ArrayList<String> ensu_years = new ArrayList<String>();
		for (int i = 0; i<int_ensuing_year; i++) {
			ensu_years.add(String.valueOf(int_y1 + (i + 1)) + "-" 
					+ String.valueOf(int_y1 + (i + 2)).substring(2));
		}
		for ( String ey : ensu_years ) {
			msg.append(ey + ", ");
		}
		msg = new StringBuilder(msg.toString().substring(0, msg.length()-2));
		msg.append(".");
		
		NewPetitionYearMessageVO data =  new NewPetitionYearMessageVO();
		data.setMsg(msg.toString());
		
		return new MyAPIResponseTO("Create Petition Year Message Successfully Delivered.", 1, data);
		
	}
	
}
