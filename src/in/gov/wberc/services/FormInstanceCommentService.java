package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormInstanceCommentDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.FormInstanceCommentUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormInstanceCommentVO;

@Secured
@Path("/")
public class FormInstanceCommentService {

	@GET
	@Path("/get_form_comment")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getFormInstanceComments(@QueryParam("form_id") String form_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the petition year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		FormInstanceCommentVO data = null;
		FormInstanceCommentDAO fcommentdao = new FormInstanceCommentDAO();
		try {
			data = fcommentdao.getFormInstanceCommen(form_id, ph_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form Comment matrix fetched Successfully",1, data);
		
	}
	
	@POST
	@Path("/update_form_comment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateFormInstanceComments(FormInstanceCommentUpdateTO req)
			throws SQLException {
		
		FormInstanceCommentVO data = req.getData();
		ArrayList<String> comments = data.getComments();
		if ( comments == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		FormInstanceCommentDAO fcdao = new FormInstanceCommentDAO();
		try {
			boolean res = fcdao.updateFormInstanceComment( data );
			if ( res ) {
				return new MyAPIResponseTO("Form Comments updated",1);
			} else {
				return new MyAPIResponseTO("Form Comment update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form Comment update failed",0);
		}
		
	}
	
}
