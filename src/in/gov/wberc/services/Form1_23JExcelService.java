/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form_1_23JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_23DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_23JExcelDataVO;

@Secured
@Path("/")
public class Form1_23JExcelService {

	@GET
	@Path("/f1_23JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_23JExcel(
			@QueryParam("stn_id") String stn_id,
			@QueryParam("ph_id") String ph_id ,
			@QueryParam("form_id") String form_id,
			@QueryParam("user_id") String user_id,
			@QueryParam("util_id") String util_id
			) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the ph_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate stationid
		ValidateStationid valistanid = new ValidateStationid();
		try {
			boolean flag = valistanid.validateStationid(stn_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Station ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_23JExcelDataVO data = null;
		Form_1_23JExcelDAO f1_23_dao = new Form_1_23JExcelDAO();
		try {
			data = f1_23_dao.getF_1_23JExcelData(stn_id, ph_id, form_id, user_id, util_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.23 Matrix Fetched Successfully",1, data);
		
	}

	//	Data Update Service
	@POST
	@Path("/f1_23DataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_23Data(Form1_23DataUpdateTO req) throws SQLException, ParseException {
		
		Form1_23JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values_for_generation = data.getVals_for_Incentive_for_Generation();
		ArrayList<ArrayList<String>> values_for_hydropower = data.getVals_for_Incentive_for_Hydropower();
		ArrayList<ArrayList<String>> values_for_transmission = data.getVals_for_Incentive_for_Transmission();

		if ( values_for_generation == null || values_for_hydropower == null || values_for_transmission == null) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form_1_23JExcelDAO f123_update_dao = new Form_1_23JExcelDAO();
		try {
			boolean res = f123_update_dao.updateF1_23( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.23 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.23 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.23 Data update failed",0);
		}
		
	}

}
