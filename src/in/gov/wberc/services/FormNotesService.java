package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormNotesDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormNotesVO;

@Secured
@Path("/form_notes")
@Produces(MediaType.APPLICATION_JSON)
public class FormNotesService {

	@GET
	public MyAPIResponseTO getForm_notes(@QueryParam("form_id") String form_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		FormNotesVO data = null;
		FormNotesDAO dpdao = new FormNotesDAO();
		try {
			data = dpdao.getFormNotes(form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception occured !!!",0);
		}
		return new MyAPIResponseTO("Form Notes delivered successfully.", 1, data);
		
	}
	
}
