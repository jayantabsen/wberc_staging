package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_3jExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_3JExcelDataVO;

@Secured
@Path("/f1_3JExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_3JExcelService {

	@GET
	public MyAPIResponseTO getF1_3JExcel(@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("form_id") String form_id,
			@QueryParam("stn_id") String stn_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the base year
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePdid(ph_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate utilid for WBSEDCL or any other
		ValidateUtilid valiutilid = new ValidateUtilid();
		try {
			boolean flag = valiutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		// Validate stationid
		ValidateStationid valistanid = new ValidateStationid();
		try {
			boolean flag = valistanid.validateStationid(stn_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Station ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		//From1_3JExcelDataVO data = null;
		Form1_3JExcelDataVO data = null;
		//Form1_3JExcelDAO f13jxldao = new Form1_3JExcelDAO();
		Form1_3jExcelDAO f13jxldao = new Form1_3jExcelDAO();
		try {
			data = f13jxldao.getF1_3JExcelData(ph_id, form_id, util_id, stn_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.3 matrix fetched Successfully", 1, data);
		
	}
	
}
