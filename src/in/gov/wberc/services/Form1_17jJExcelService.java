package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17jJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17jJExcelDataVO;

@Secured
@Path("/f1_17jJExcel")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17jJExcelService {

	@GET
	public MyAPIResponseTO getF1_17jJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("py_id") String py_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the py_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Ensuing Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_17jJExcelDataVO data = null;
		Form1_17jJExcelDAO f117jjxldao = new Form1_17jJExcelDAO();
		try {
			data = f117jjxldao.getF1_17jJExcelData(form_id, py_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.17j matrix fetched Successfully",1, data);
		
	}
	
}
