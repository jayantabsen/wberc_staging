/*** @author Najma on 25-Jan-2018 */
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form_A_JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form_A_DataSaveTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form_A_JExcelDataVO;

@Secured
@Path("/")
public class From_A_JExcelService {
	
	@GET
	@Path("/f_A_JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF_A_JExcel(
			@QueryParam("stn_id") String stn_id,
			@QueryParam("py_id") String py_id,
			@QueryParam("form_id") String form_id,
			@QueryParam("user_id") String user_id) throws SQLException, ParseException {
			
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		// Validate the base year
		YearListDAO yldao = new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}

		// Validate stationid
		ValidateStationid valistnid = new ValidateStationid();
		try {
			boolean flag = valistnid.validateStationid(stn_id);
			if (flag == false) {
				return new MyAPIResponseTO("Invalid Station Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		

		Form_A_JExcelDataVO data = null;
		Form_A_JExcelDAO f_A_dao = new Form_A_JExcelDAO();
		try {
			data = f_A_dao.getF_A_JExcelData(stn_id, py_id,form_id,user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Petition Year Retrieve", 0);
		}
		return new MyAPIResponseTO("Form A matrix fetched Successfully", 1, data);
		
	}
	
	//	Data Save & Delete Service
	@POST
	@Path("/f_A_DataSave")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm_A_Data(Form_A_DataSaveTO req) throws SQLException, ParseException {
		
		Form_A_JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form_A_JExcelDAO f_save_del_dao = new Form_A_JExcelDAO();
		try {
			boolean res = f_save_del_dao.saveF_A_JExcelData( data );
			if ( res ) {
				return new MyAPIResponseTO("Form A Data Matrix successfully saved",1);
			} else {
				return new MyAPIResponseTO("Form A Data save failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form rom A Data save failed",0);
		}
		
	}
	
}
