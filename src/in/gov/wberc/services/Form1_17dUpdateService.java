//** By: JBS
package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17dUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17dUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17dJExcelDataVO;

@Secured
@Path("/f1_17dUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17dUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_17dUpdate(Form1_17dUpdateTO req)
			throws SQLException {
		
		Form1_17dJExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_17dUpdateDAO f117dudao = new Form1_17dUpdateDAO();
		try {
			boolean res = f117dudao.updateF1_17dUpdate( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.17d Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.17d Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.17d Data update failed",0);
		}
		
	}
	
}

