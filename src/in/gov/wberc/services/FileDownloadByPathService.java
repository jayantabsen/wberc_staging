package in.gov.wberc.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;

import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

import in.gov.wberc.constants.QueryConstants;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.utils.DBUtil;

@Secured
@Path("/file_download_by_path")
@Produces({ MediaType.APPLICATION_OCTET_STREAM })
public class FileDownloadByPathService {

	@GET
	public Response fileDownload(
			@QueryParam("docName") String docName,
			@QueryParam("phid") String phid,
			@QueryParam("utilid") String utilid,
			@QueryParam("module") String module) throws MyException, IOException {
		
		Session session = createCMISSession("182.75.177.246", "8282");
		
		// Getting base_year, utility-name
		String base_year = "";
		String util_abbr = "";
		
		Connection conn1=null;
		PreparedStatement prstmt1 = null;	
		ResultSet rs1=null;
		
		String query1 = QueryConstants.QUERY_GET_PETITION_YEAR_FORM_PHID;
		String query2 = QueryConstants.QUERY_GET_UTILITY_ABBREVIATION_BY_ID;
		
		try {
			conn1=DBUtil.getConnection();
			prstmt1 = conn1.prepareStatement(query1);
			
			prstmt1.setInt(1, Integer.parseInt(phid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					base_year = rs1.getString("base_year");
				}
			}
			
			prstmt1 = conn1.prepareStatement(query2);
			
			prstmt1.setInt(1, Integer.parseInt(utilid));
			
			rs1=prstmt1.executeQuery();
			
			if(rs1!=null) {
				if(rs1.next()) {
					util_abbr = rs1.getString("util_abbr");
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new MyException();
		} finally {
			try {
				if(prstmt1!=null) {
					prstmt1.close();
				}
				if(conn1!=null) {
					conn1.close();
				}
				if(rs1!=null) {
					rs1.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
				throw new MyException();
			}
		}
		
		String serverFileSpace = "/" + base_year + "/" + util_abbr + "/" + module + "/" + docName;
		
		Document document = findDocumentForPath(session, serverFileSpace);
		
		Property<String> p = document.getProperty(PropertyIds.OBJECT_ID);

		String s = p.getValue();
		
		final InputStream in = document.getContentStream().getStream();
		
		StreamingOutput stream = null;
		try {
	        stream = new StreamingOutput() {
	            public void write(OutputStream out) throws IOException, WebApplicationException {
	                try {
	                    int read = 0;
	                        byte[] bytes = new byte[1024];

	                        while ((read = in.read(bytes)) != -1) {
	                            out.write(bytes, 0, read);
	                        }
	                } catch (Exception e) {
	                    throw new WebApplicationException(e);
	                }
	            }
	        };
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		return Response.ok(stream).header("content-disposition","attachment; filename = "+docName).build();
		
	}
	
	public Session createCMISSession(String host, String port) {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "password");

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, "http://"+host+":"+port+"/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Create session.
		// Alfresco only provides one repository.
		Repository repository = factory.getRepositories(parameter).get(0);
		System.out.println("Rep folder name-->"+repository.getId());

		parameter.put(SessionParameter.REPOSITORY_ID, repository.getId());

		// create session
		Session session = factory.createSession(parameter);
		return session;
	}
	
	/**
	 * Finds document by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Document findDocumentForPath(Session session, String path) {
		try {
		return (Document) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
}
