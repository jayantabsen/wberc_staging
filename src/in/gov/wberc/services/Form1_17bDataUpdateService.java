//** By Najma Edited by JBS

package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17bDataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17JExcelDataVO;

@Secured
@Path("/f1_17bDataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_17bDataUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_17bData(Form1_17DataUpdateTO req) throws SQLException {
		
		Form1_17JExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}


		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_17bDataUpdateDAO f117bdudao = new Form1_17bDataUpdateDAO();
		try {
			boolean res = f117bdudao.updateF1_17bMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.17b Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.17b Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.17b Data update failed",0);
		}
		
		
	}
	
}

