package in.gov.wberc.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.AttachmentTypeListingDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.AttachmentTypeListingVO;

@Secured
@Path("/attachment_type_listing")
@Produces(MediaType.APPLICATION_JSON)
public class AttachmentTypeListingService {

	@GET
	public MyAPIResponseTO getAttachmentType(@QueryParam("form_id") String form_id) throws SQLException {
		
		AttachmentTypeListingVO data = null;
		AttachmentTypeListingDAO attypelstdao = new AttachmentTypeListingDAO();
		try {
			data = attypelstdao.getAttachmentTypeListing(form_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Invalid Attachment Type.",0);
		}
		
		return new MyAPIResponseTO("Attachment Type Listing Successfully Delivered.", 1, data);
		
	}
	
}
