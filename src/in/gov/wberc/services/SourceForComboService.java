//*** By Najma
package in.gov.wberc.services;

import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.SourceForComboDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.SourceListF1_6aVO;

@Secured
//@Path("/sourceCombo")
@Produces(MediaType.APPLICATION_JSON)
public class SourceForComboService {

	@POST
	public MyAPIResponseTO getSourceForCombo(@QueryParam("ph_id") String ph_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("form_id") String form_id) {
		
		if (form_id == null || form_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Form ID.",0);
		}
		if (ph_id == null || ph_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Petition ID.",0);
		}
		if (util_id == null || util_id.length() == 0) {
			return new MyAPIResponseTO("Invalid Utility ID.",0);
		}
		
		SourceForComboDAO tsldao= new SourceForComboDAO();
		
		try
		{
			//System.out.println("data in source:"+form_id);

			switch ( Integer.parseInt(form_id) ) {
				case 11:
					SourceListF1_6aVO data = null;
					try {
						data = tsldao.getSourcesForCombo(util_id);
					} catch (MyException ex) {
						ex.printStackTrace();
						return new MyAPIResponseTO("Exception In Sources for Combo Service",0);
					}
					return new MyAPIResponseTO("Sources for Combo Served Successfully",1,data);
				//***************************************************************************
				default:
					return new MyAPIResponseTO("Invalid Form Reference", 0);
			}
		} catch (Exception ex) {
			return new MyAPIResponseTO("Exception in: "+ex.getMessage(), 0);
		}
		
	}
	
}
