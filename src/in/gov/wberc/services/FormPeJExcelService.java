package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.FormPeJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateStationid;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.FormPeDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.FormPeJExcelDataVO;

@Secured
@Path("/")
public class FormPeJExcelService {

	@GET
	@Path("/fPeJExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getFPeJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("stn_id") String stn_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate stationid for WBSEDCL or any other
		ValidateStationid valstid = new ValidateStationid();
		try {
			boolean flag = valstid.validateStationid(stn_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Station Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		FormPeJExcelDataVO data = null;
		FormPeJExcelDAO fpejxldao = new FormPeJExcelDAO();
		try {
			data = fpejxldao.getFPeJExcelData(form_id, stn_id, ph_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form P (e) matrix fetched Successfully",1, data);
		
	}
	
	@POST
	@Path("/fPeDataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateFormPeData(FormPeDataUpdateTO req) throws SQLException, ParseException {
		
		FormPeJExcelDataVO data = req.getData();
		
		// From data update
		FormPeJExcelDAO fpeudao = new FormPeJExcelDAO();
		try {
			boolean res = fpeudao.updateFPeMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form P(e) Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form P(e) Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form P(e) Data update failed",0);
		}
		
	}
	
}
