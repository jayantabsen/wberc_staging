//** By Najma Updated By: JBS

package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_12DataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_11DataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_11JExcelDataVO;

@Secured
@Path("/f1_12DataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_12DataUpdateService {
	
	@POST
	public MyAPIResponseTO updateForm1_12Data(Form1_11DataUpdateTO req) throws SQLException {
		
		From1_11JExcelDataVO data = req.getData();
		ArrayList<String> cols = data.getCols();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		// pyid-s validation
		for ( String pyid : cols )
		{
			try
			{
				Integer.parseInt(pyid);
			} catch(Exception ex) {
				return new MyAPIResponseTO("Invalid PyId !!", 0);
			}
		}


		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}
		
		// From data update
		Form1_12DataUpdateDAO f112dudao = new Form1_12DataUpdateDAO();
		try {
			boolean res = f112dudao.updateF1_12MatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.12 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.12 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.12 Data update failed",0);
		}
		
		
	}
	
}

