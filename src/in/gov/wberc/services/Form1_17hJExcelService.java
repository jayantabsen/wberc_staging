package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_17hJExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_17hDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_17hJExcelDataVO;

@Secured
@Path("/")
public class Form1_17hJExcelService {

	@GET
	@Path("/f1_17hJExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_17hJExcel(@QueryParam("form_id") String form_id,
			@QueryParam("py_id") String py_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate the py_id
		YearListDAO yldao= new YearListDAO();
		try {
			boolean flag = yldao.validatePyid(py_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Ensuing Pretition Year Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_17hJExcelDataVO data = null;
		Form1_17hJExcelDAO f117hjxldao = new Form1_17hJExcelDAO();
		try {
			data = f117hjxldao.getF1_17hJExcelData(form_id, py_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.17h matrix fetched Successfully",1, data);
		
	}
	
	@POST
	@Path("/f1_7hDataUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_7hData(Form1_17hDataUpdateTO req) throws SQLException, ParseException {
		
		Form1_17hJExcelDataVO data = req.getData();
		
		// From data update
		Form1_17hJExcelDAO f117hudao = new Form1_17hJExcelDAO();
		try {
			boolean res = f117hudao.updateF1_17hMatrixToDatabase( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.7h Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.7h Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.7h Data update failed",0);
		}
		
	}
	
}
