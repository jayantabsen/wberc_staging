package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form1_18JExcelDAO_1;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.ValidateUtilid;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form1_18UpdateTO_1;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form1_18JExcelDataVO_1;

@Secured
@Path("/")
public class Form1_18JExcelService_1 {

	@GET
	@Path("/f1_18JExcel_1")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF1_18JExcel(@QueryParam("form_id") String form_id,
			@QueryParam("util_id") String util_id,
			@QueryParam("ph_id") String ph_id,
			@QueryParam("user_id") String user_id) throws SQLException {
		
		// Validate formid
		ValidateFormid valiformid = new ValidateFormid();
		try {
			boolean flag = valiformid.validateFormid(form_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
				
		// Validate util_id
		ValidateUtilid valutilid = new ValidateUtilid();
		try {
			boolean flag = valutilid.validateUtilid(util_id);
			if ( flag == false ) {
				return new MyAPIResponseTO("Invalid Utility Credentials", 0);
			}
		} catch (MyException e) {
			e.printStackTrace();
		}
		
		Form1_18JExcelDataVO_1 data = null;
		Form1_18JExcelDAO_1 f1_18jxldao_1 = new Form1_18JExcelDAO_1();
		try {
			data = f1_18jxldao_1.getF1_18Data_1(form_id, util_id, ph_id, user_id);
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Exception In Pretition Year Retrieve",0);
		}
		return new MyAPIResponseTO("Form 1.18_1 matrix fetched Successfully",1, data);
		
	}
	
	@POST
	@Path("/f1_18Update_1")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm1_18_1Data(Form1_18UpdateTO_1 req) throws SQLException, ParseException {
		
		Form1_18JExcelDataVO_1 data = req.getData();
		
		// From data update
		Form1_18JExcelDAO_1 f1_18udao_1 = new Form1_18JExcelDAO_1();
		try {
			boolean res = f1_18udao_1.updateF1_18Data_1( data );
			if ( res ) {
				return new MyAPIResponseTO("Form 1.18_1 Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.18_1 Data update failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.18_1 Data update failed",0);
		}
		
	}
	
}
