//01.02.2018 Priyanka
package in.gov.wberc.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.Form_B_JExcelDAO;
import in.gov.wberc.dao.ValidateFormid;
import in.gov.wberc.dao.YearListDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.Form_B_DataSaveTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.Form_B_JExcelDataVO;

@Secured
@Path("/")

public class From_B_JExcelService {
	Form_B_JExcelDAO f_B_dao = new Form_B_JExcelDAO();
	
	@GET
	@Path("/f_B_JExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO getF_A_JExcel(@QueryParam("ph_id") String ph_id, @QueryParam("py_id") String py_id,
			@QueryParam("form_id") String form_id, @QueryParam("user_id") String user_id) throws SQLException, ParseException {
		
		ValidateFormid valiformid = new ValidateFormid();
		YearListDAO yldao = new YearListDAO();
		Form_B_JExcelDataVO data = null;
		
		try {
			boolean validForm = valiformid.validateFormid(form_id);
			if (validForm == false) {
				return new MyAPIResponseTO("Invalid Form ID Credentials", 0);
			}
			
			boolean ValidPetition = yldao.validatePyid(py_id);
			if (ValidPetition == false) {
				return new MyAPIResponseTO("Invalid Petition Year Credentials", 0);
			}
			
			data = f_B_dao.getF_B_JExcelData(py_id,user_id,ph_id,form_id);
			if(data==null) {
				return new MyAPIResponseTO("Exception In Petition Year Retrieve", 0);
			}
			
		} catch (MyException e) {
			e.printStackTrace();
		}
			
		return new MyAPIResponseTO("Form B matrix fetched Successfully", 1, data);
	}
	
	@POST
	@Path("/f_B_DataSave")
	@Produces(MediaType.APPLICATION_JSON)
	public MyAPIResponseTO updateForm_B_Data(Form_B_DataSaveTO req) throws SQLException, ParseException {
		
		Form_B_JExcelDataVO data = req.getData();
		ArrayList<ArrayList<String>> values = data.getVals();
		
		if ( values == null ) {
			return new MyAPIResponseTO("Invalid Data Segment !!", 0);
		}

		try {
			boolean res = f_B_dao.saveF_B_JExcelData(data);
			if ( res ) {
				return new MyAPIResponseTO("Form B Data Matrix successfully saved",1);
			} else {
				return new MyAPIResponseTO("Form B Data save failed",0);
			}
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form B Data save failed",0);
		}
	}
}

