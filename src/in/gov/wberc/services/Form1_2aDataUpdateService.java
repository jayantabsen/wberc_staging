package in.gov.wberc.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import in.gov.wberc.dao.From1_2aDataUpdateDAO;
import in.gov.wberc.exception.MyException;
import in.gov.wberc.secured.Secured;
import in.gov.wberc.to.From1_2aDataUpdateTO;
import in.gov.wberc.to.MyAPIResponseTO;
import in.gov.wberc.vo.From1_2aJExcelDataVO;
import in.gov.wberc.vo.StationNUnitValVO;

@Secured
@Path("/f1_2aDataUpdate")
@Produces(MediaType.APPLICATION_JSON)
public class Form1_2aDataUpdateService {

	@POST
	public MyAPIResponseTO updateForm1_2aData(From1_2aDataUpdateTO req) throws SQLException {
		
		From1_2aJExcelDataVO f11jxldvo = (From1_2aJExcelDataVO)req.getData();
		try {
			ArrayList<String> unitidAl = f11jxldvo.getRows();
			ArrayList<String> pyidAl = f11jxldvo.getCols();
			ArrayList<StationNUnitValVO> urvoAl = f11jxldvo.getVals();
			String formid = f11jxldvo.getFormid();
			
			// Numeric field validation
			for ( String unitid : unitidAl )
			{
				try
				{
					int int_unitid = Integer.parseInt(unitid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Unit ID: "+ex.getMessage(),0);
				}
			}
			for ( String pyid : pyidAl )
			{
				try
				{
					int int_pyid = Integer.parseInt(pyid);
				} catch(Exception ex) {
					return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
				}
			}
			// Station data Validation
			/*if ( unitidAl.size() != urvoAl.size() ) {
				return new MyAPIResponseTO("Invalid No. of Unit Data",0);
			}*/
			for ( StationNUnitValVO srvo : urvoAl ) {
				ArrayList<ArrayList<String>> units = srvo.getUnitValForStation();
				for ( ArrayList<String> unit : units ) {
					if ( pyidAl.size() != unit.size()-1 ) {
						return new MyAPIResponseTO("Invalid No. of Unit Data",0);
					}
					for ( int i = 1; i <  unit.size(); i++ ) {
						try
						{
							double val = Double.parseDouble(unit.get(i));
						} catch(Exception ex) {
							return new MyAPIResponseTO("Exception in Petition Year ID: "+ex.getMessage(),0);
						}
					}
				}
				
			}
			
			// From data update
			From1_2aDataUpdateDAO f12adudao = new From1_2aDataUpdateDAO(unitidAl, pyidAl, urvoAl, formid);
			boolean res = f12adudao.updateF1_2aMatrixToDatabase();
			if ( res ) {
				return new MyAPIResponseTO("Form 1.2a Data Matrix successfully updated",1);
			} else {
				return new MyAPIResponseTO("Form 1.2a Data update failed",0);
			}
			
		} catch (MyException ex) {
			ex.printStackTrace();
			return new MyAPIResponseTO("Form 1.2a Data update failed",0);
		}
		
	}
	
}
