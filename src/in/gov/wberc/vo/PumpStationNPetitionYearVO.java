package in.gov.wberc.vo;

import java.util.ArrayList;

public class PumpStationNPetitionYearVO implements TransInterfaceVO {

	private String pump_stationid = null;
	private String pump_stationnm = null;
	private String capacity = null;
	private String unit_of_calc = null;
	private ArrayList<YearListVO> labeledYearsAl = null;
	public String getPump_stationid() {
		return pump_stationid;
	}
	public void setPump_stationid(String pump_stationid) {
		this.pump_stationid = pump_stationid;
	}
	public String getPump_stationnm() {
		return pump_stationnm;
	}
	public void setPump_stationnm(String pump_stationnm) {
		this.pump_stationnm = pump_stationnm;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getUnit_of_calc() {
		return unit_of_calc;
	}
	public void setUnit_of_calc(String unit_of_calc) {
		this.unit_of_calc = unit_of_calc;
	}
	public ArrayList<YearListVO> getLabeledYearsAl() {
		return labeledYearsAl;
	}
	public void setLabeledYearsAl(ArrayList<YearListVO> labeledYearsAl) {
		this.labeledYearsAl = labeledYearsAl;
	}
	
}
