//*** By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class SourceListF1_6aVO implements TransInterfaceVO {

	private ArrayList<SourceF1_6aVO> sources = null;

	public ArrayList<SourceF1_6aVO> getSources() {
		return sources;
	}

	public void setSources(ArrayList<SourceF1_6aVO> sources) {
		this.sources = sources;
	}
	
	
}

