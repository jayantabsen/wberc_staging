
//*** By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class From1_11JExcelDataVO implements TransInterfaceVO {
	
	//	Station id
	private String stationid;
	//	Get Petition ids
	private ArrayList<String> cols;
	//Get Misc ids
	private ArrayList<String> miscids;
	//	Get vals
	private ArrayList<ArrayList<String>> vals;
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	
}
