// By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_5GrossJExcelDataVO implements TransInterfaceVO {

	// pyid-s
	private ArrayList<String> cols;
	// seasons-s
	private ArrayList<String> seasons;
	// gross-vals
	private ArrayList<F1_5SeasonUsageVO> gross;
	
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<String> seasons) {
		this.seasons = seasons;
	}
	public ArrayList<F1_5SeasonUsageVO> getGross() {
		return gross;
	}
	public void setGross(ArrayList<F1_5SeasonUsageVO> gross) {
		this.gross = gross;
	}

	
}
