package in.gov.wberc.vo;

public class NewPetitionYearMessageVO implements TransInterfaceVO {

	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
