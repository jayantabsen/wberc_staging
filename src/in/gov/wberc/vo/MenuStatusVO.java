package in.gov.wberc.vo;

import java.util.ArrayList;

public class MenuStatusVO {

	private String id;
	private String menu_loc;
	private String name;
	private String parent_name;
	private String level;
	private ArrayList<MenuStatusVO> childAl;//**
	private String link;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenu_loc() {
		return menu_loc;
	}
	public void setMenu_loc(String menu_loc) {
		this.menu_loc = menu_loc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParent_name() {
		return parent_name;
	}
	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public ArrayList<MenuStatusVO> getChildAl() {
		return childAl;
	}
	public void setChildAl(ArrayList<MenuStatusVO> childAl) {
		this.childAl = childAl;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
}
