//*** By JBS
package in.gov.wberc.vo;

import java.util.ArrayList;

public class FormPeJExcelDataVO implements TransInterfaceVO {
	
	private String stn_id;
	private String form_id;
	private String ph_id;
	private String user_id;
	private ArrayList<FormPeDataVO> val;
	public String getStn_id() {
		return stn_id;
	}
	public void setStn_id(String stn_id) {
		this.stn_id = stn_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<FormPeDataVO> getVal() {
		return val;
	}
	public void setVal(ArrayList<FormPeDataVO> val) {
		this.val = val;
	}
	
}
