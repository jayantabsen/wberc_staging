package in.gov.wberc.vo;

public class PumpStationF1_4bVO {

	private String pump_stn_id = "";
	private String pump_stn_desc = "";
	public String getPump_stn_id() {
		return pump_stn_id;
	}
	public void setPump_stn_id(String pump_stn_id) {
		this.pump_stn_id = pump_stn_id;
	}
	public String getPump_stn_desc() {
		return pump_stn_desc;
	}
	public void setPump_stn_desc(String pump_stn_desc) {
		this.pump_stn_desc = pump_stn_desc;
	}
	
}
