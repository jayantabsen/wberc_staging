//*** By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class FormD1JExcelDataVO implements TransInterfaceVO {
	
	private String stn_id;
	private String py_id;
	private String form_id;
	private ArrayList<String> cols; // coalgrade_id(s)
	private ArrayList<String> rows; // util_coal_field_id
	private ArrayList<ArrayList<String>> vals;
	public String getStn_id() {
		return stn_id;
	}
	public void setStn_id(String stn_id) {
		this.stn_id = stn_id;
	}
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getRows() {
		return rows;
	}
	public void setRows(ArrayList<String> rows) {
		this.rows = rows;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	
}
