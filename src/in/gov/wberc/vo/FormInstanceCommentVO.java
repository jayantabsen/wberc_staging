//*** By JBS
package in.gov.wberc.vo;

import java.util.ArrayList;

public class FormInstanceCommentVO implements TransInterfaceVO {
	
	private String ph_id;
	private String form_id;
	private String user_id;
	private ArrayList<String> comments;
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getComments() {
		return comments;
	}
	public void setComments(ArrayList<String> comments) {
		this.comments = comments;
	}
	
}
