package in.gov.wberc.vo;

import java.util.ArrayList;

public class YearListVO implements TransInterfaceVO {
	
	private String label = "";
	private ArrayList<YearVO> yearsAl = null;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ArrayList<YearVO> getYearsAl() {
		return yearsAl;
	}

	public void setYearsAl(ArrayList<YearVO> yearsAl) {
		this.yearsAl = yearsAl;
	}
	
}
