package in.gov.wberc.vo;

import java.util.ArrayList;

public class From1_2aJExcelDataVO implements TransInterfaceVO {

	// stationid-s
//	private ArrayList<String> stns;
	// unitid-s
	private ArrayList<String> rows;
	// pyid-s
	private ArrayList<String> cols;
	// vals
	private ArrayList<StationNUnitValVO> vals;
	private String formid;
		
	/*public ArrayList<String> getStns() {
		return stns;
	}
	public void setStns(ArrayList<String> stns) {
		this.stns = stns;
	}*/
	
	public ArrayList<String> getRows() {
		return rows;
	}
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	public void setRows(ArrayList<String> rows) {
		this.rows = rows;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
/*	public ArrayList<UnitRowVO> getVals() {
		return vals;
	}
	public void setVals(ArrayList<UnitRowVO> vals) {
		this.vals = vals;
	}*/
	public ArrayList<StationNUnitValVO> getVals() {
		return vals;
	}
	public void setVals(ArrayList<StationNUnitValVO> vals) {
		this.vals = vals;
	}
	
	
}
