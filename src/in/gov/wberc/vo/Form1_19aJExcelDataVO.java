/*** @author Najma on 01-Feb-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_19aJExcelDataVO implements TransInterfaceVO{
	
	private String form_id;
	private String util_id;
	private String ph_id;
	private String user_id;
	private String expenditure;
	// Get petition years
	private ArrayList<String> cols;
	// Get misc ids for Capital Expenditure
	private ArrayList<String> miscids_for_CapitalExpenditure; 
	// Get misc ids for Special Projects
	private ArrayList<String> miscids_for_SpecialProjects; 
	// Get station ids
	private ArrayList<String> stn_ids; 
	// Get values object
	private Form1_19aObjectVO vals;

	
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUtil_id() {
		return util_id;
	}
	public void setUtil_id(String util_id) {
		this.util_id = util_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getStn_ids() {
		return stn_ids;
	}
	public void setStn_ids(ArrayList<String> stn_ids) {
		this.stn_ids = stn_ids;
	}
	public ArrayList<String> getMiscids_for_CapitalExpenditure() {
		return miscids_for_CapitalExpenditure;
	}
	public void setMiscids_for_CapitalExpenditure(ArrayList<String> miscids_for_CapitalExpenditure) {
		this.miscids_for_CapitalExpenditure = miscids_for_CapitalExpenditure;
	}
	public ArrayList<String> getMiscids_for_SpecialProjects() {
		return miscids_for_SpecialProjects;
	}
	public void setMiscids_for_SpecialProjects(ArrayList<String> miscids_for_SpecialProjects) {
		this.miscids_for_SpecialProjects = miscids_for_SpecialProjects;
	}
	public Form1_19aObjectVO getVals() {
		return vals;
	}
	public void setVals(Form1_19aObjectVO vals) {
		this.vals = vals;
	}
	public String getExpenditure() {
		return expenditure;
	}
	public void setExpenditure(String expenditure) {
		this.expenditure = expenditure;
	}
	

}
