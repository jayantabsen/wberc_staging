//01.02.2018 Priyanka
package in.gov.wberc.vo;

import java.util.ArrayList;
import java.util.List;

public class Form_B_JExcelDataVO implements TransInterfaceVO {
 
	private String ph_id;
	private String py_id;
	private String form_id;
	private String user_id;
	private ArrayList<ArrayList<String>> vals;
	private ArrayList<String> colIds;
	
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public ArrayList<String> getColIds() {
		return colIds;
	}
	public void setColIds(ArrayList<String> colIds) {
		this.colIds = colIds;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
}
