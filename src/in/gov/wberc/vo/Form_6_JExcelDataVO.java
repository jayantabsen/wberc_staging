/*** @author Najma on 31-Jan-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form_6_JExcelDataVO implements TransInterfaceVO{
	
	//	Form Id
	private String form_id;
	//	Ph Id
	private String ph_id;
	//	User Id
	private String user_id;
	//  Petition year
	private ArrayList<String> cols;
	//  Get Misc ids
	private ArrayList<String> miscid_for_Incomes;
	private ArrayList<String> miscid_for_Expenses;
	private ArrayList<String> miscid_for_Assets;
	private ArrayList<String> miscid_for_UtilisationAddFund;
	private ArrayList<String> miscid_for_UtilisationCapitalFund;

	//	Get vals
	private ArrayList<ArrayList<String>> vals_for_Incomes;
	private ArrayList<ArrayList<String>> vals_for_Expenses;
	private ArrayList<ArrayList<String>> vals_for_Assets;
	private ArrayList<ArrayList<String>> vals_for_UtilisationAddFund;
	private ArrayList<ArrayList<String>> vals_for_UtilisationCapitalFund;
	
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getMiscid_for_Incomes() {
		return miscid_for_Incomes;
	}
	public void setMiscid_for_Incomes(ArrayList<String> miscid_for_Incomes) {
		this.miscid_for_Incomes = miscid_for_Incomes;
	}
	public ArrayList<String> getMiscid_for_Expenses() {
		return miscid_for_Expenses;
	}
	public void setMiscid_for_Expenses(ArrayList<String> miscid_for_Expenses) {
		this.miscid_for_Expenses = miscid_for_Expenses;
	}
	public ArrayList<String> getMiscid_for_Assets() {
		return miscid_for_Assets;
	}
	public void setMiscid_for_Assets(ArrayList<String> miscid_for_Assets) {
		this.miscid_for_Assets = miscid_for_Assets;
	}
	public ArrayList<ArrayList<String>> getVals_for_Incomes() {
		return vals_for_Incomes;
	}
	public void setVals_for_Incomes(ArrayList<ArrayList<String>> vals_for_Incomes) {
		this.vals_for_Incomes = vals_for_Incomes;
	}
	public ArrayList<ArrayList<String>> getVals_for_Expenses() {
		return vals_for_Expenses;
	}
	public void setVals_for_Expenses(ArrayList<ArrayList<String>> vals_for_Expenses) {
		this.vals_for_Expenses = vals_for_Expenses;
	}
	public ArrayList<ArrayList<String>> getVals_for_Assets() {
		return vals_for_Assets;
	}
	public void setVals_for_Assets(ArrayList<ArrayList<String>> vals_for_Assets) {
		this.vals_for_Assets = vals_for_Assets;
	}
	public ArrayList<String> getMiscid_for_UtilisationAddFund() {
		return miscid_for_UtilisationAddFund;
	}
	public void setMiscid_for_UtilisationAddFund(ArrayList<String> miscid_for_UtilisationAddFund) {
		this.miscid_for_UtilisationAddFund = miscid_for_UtilisationAddFund;
	}
	public ArrayList<String> getMiscid_for_UtilisationCapitalFund() {
		return miscid_for_UtilisationCapitalFund;
	}
	public void setMiscid_for_UtilisationCapitalFund(ArrayList<String> miscid_for_UtilisationCapitalFund) {
		this.miscid_for_UtilisationCapitalFund = miscid_for_UtilisationCapitalFund;
	}
	public ArrayList<ArrayList<String>> getVals_for_UtilisationAddFund() {
		return vals_for_UtilisationAddFund;
	}
	public void setVals_for_UtilisationAddFund(ArrayList<ArrayList<String>> vals_for_UtilisationAddFund) {
		this.vals_for_UtilisationAddFund = vals_for_UtilisationAddFund;
	}
	public ArrayList<ArrayList<String>> getVals_for_UtilisationCapitalFund() {
		return vals_for_UtilisationCapitalFund;
	}
	public void setVals_for_UtilisationCapitalFund(ArrayList<ArrayList<String>> vals_for_UtilisationCapitalFund) {
		this.vals_for_UtilisationCapitalFund = vals_for_UtilisationCapitalFund;
	}
	

}
