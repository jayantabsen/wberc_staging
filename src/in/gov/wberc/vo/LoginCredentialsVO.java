package in.gov.wberc.vo;

public class LoginCredentialsVO implements TransInterfaceVO {
	
	private String loginID;
	private String password;
	
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
