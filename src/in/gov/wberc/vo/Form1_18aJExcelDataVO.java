
//*** By JBS

package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_18aJExcelDataVO implements TransInterfaceVO {
	
	// Get Py_ids
	private ArrayList<String> cols;
	// Get Misc ids
	private ArrayList<String> miscids;
	// Get vals
	private ArrayList<ArrayList<String>> vals;
	
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}

}
