//*** By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class SourceNPetitionYearVO implements TransInterfaceVO {

	private String sourceid = null;
	private String sourcenm = null;
	private String capacity = null;
	private String unit_of_calc = null;
	private ArrayList<YearListVO> labeledYearsAl = null;
	public String getSourceid() {
		return sourceid;
	}
	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}
	public String getSourcenm() {
		return sourcenm;
	}
	public void setSourcenm(String sourcenm) {
		this.sourcenm = sourcenm;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getUnit_of_calc() {
		return unit_of_calc;
	}
	public void setUnit_of_calc(String unit_of_calc) {
		this.unit_of_calc = unit_of_calc;
	}
	public ArrayList<YearListVO> getLabeledYearsAl() {
		return labeledYearsAl;
	}
	public void setLabeledYearsAl(ArrayList<YearListVO> labeledYearsAl) {
		this.labeledYearsAl = labeledYearsAl;
	}

}
