/*12.01.2018 Priyanka..@Created*/
package in.gov.wberc.vo;

import java.io.Serializable;
import java.util.List;

public class PititionHeaderVo implements Serializable, TransInterfaceVO {
	
	private String phid;
	private String utilid;
	private List<FormVo> formList;
	
	public String getPhid() {
		return phid;
	}
	public void setPhid(String phid) {
		this.phid = phid;
	}
	public String getUtilid() {
		return utilid;
	}
	public void setUtilid(String utilid) {
		this.utilid = utilid;
	}
	public List<FormVo> getFormList() {
		return formList;
	}
	public void setFormList(List<FormVo> formList) {
		this.formList = formList;
	}
}
