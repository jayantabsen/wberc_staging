//*** By Najma


package in.gov.wberc.vo;

import java.util.ArrayList;

public class F1_5SeasonUsageVO {

	private String season = "";
	private ArrayList<ArrayList<String>> usage;
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public ArrayList<ArrayList<String>> getUsage() {
		return usage;
	}
	public void setUsage(ArrayList<ArrayList<String>> usage) {
		this.usage = usage;
	}
	
}

