package in.gov.wberc.vo;

import java.util.ArrayList;

public class AllPetitionYearVO implements TransInterfaceVO {
	
	// Using the ensuing year VO as it is perfect for current use 
	private ArrayList<EnsuingYearVO> all_petition_year;

	public ArrayList<EnsuingYearVO> getAll_petition_year() {
		return all_petition_year;
	}

	public void setAll_petition_year(ArrayList<EnsuingYearVO> all_petition_year) {
		this.all_petition_year = all_petition_year;
	}
	
}
