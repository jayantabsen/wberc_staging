package in.gov.wberc.vo;

public class NewPetitionMsgCredentialVO {

	private String base_year;
	private String prev_year;
	private String ensu_year;
	public String getBase_year() {
		return base_year;
	}
	public void setBase_year(String base_year) {
		this.base_year = base_year;
	}
	public String getPrev_year() {
		return prev_year;
	}
	public void setPrev_year(String prev_year) {
		this.prev_year = prev_year;
	}
	public String getEnsu_year() {
		return ensu_year;
	}
	public void setEnsu_year(String ensu_year) {
		this.ensu_year = ensu_year;
	}
	
}
