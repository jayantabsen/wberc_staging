//*** By JBS
package in.gov.wberc.vo;

public class Form1_17hJExcelDataVO implements TransInterfaceVO {
	
	private String py_id;
	private String form_id;
	private String user_id;
	private String exp_own;
	private String exp_con;
	private String basic_pay_own;
	private String basic_pay_con;
	private String dear_allowance_own;
	private String dear_allowance_con;
	private String other_allowance_own;
	private String other_allowance_con;
	private String gratuity_own;
	private String gratuity_con;
	private String pf_contrib_own;
	private String pf_contrib_con;
	private String pension_contrib_own;
	private String pension_contrib_con;
	private String bonus_own;
	private String bonus_con;
	private String ltc_own;
	private String ltc_con;
	private String leave_encashment_own;
	private String leave_encashment_con;
	private String welfare_expend_own;
	private String welfare_expend_con;
	private String other_own;
	private String other_con;
	private String shortfall_pf_own;
	private String shortfall_pf_con;
	private String emp_incentive_own;
	private String emp_incentive_con;
	private String personnel_no_own;
	private String personnel_no_con;
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getExp_own() {
		return exp_own;
	}
	public void setExp_own(String exp_own) {
		this.exp_own = exp_own;
	}
	public String getExp_con() {
		return exp_con;
	}
	public void setExp_con(String exp_con) {
		this.exp_con = exp_con;
	}
	public String getBasic_pay_own() {
		return basic_pay_own;
	}
	public void setBasic_pay_own(String basic_pay_own) {
		this.basic_pay_own = basic_pay_own;
	}
	public String getBasic_pay_con() {
		return basic_pay_con;
	}
	public void setBasic_pay_con(String basic_pay_con) {
		this.basic_pay_con = basic_pay_con;
	}
	public String getDear_allowance_own() {
		return dear_allowance_own;
	}
	public void setDear_allowance_own(String dear_allowance_own) {
		this.dear_allowance_own = dear_allowance_own;
	}
	public String getDear_allowance_con() {
		return dear_allowance_con;
	}
	public void setDear_allowance_con(String dear_allowance_con) {
		this.dear_allowance_con = dear_allowance_con;
	}
	public String getOther_allowance_own() {
		return other_allowance_own;
	}
	public void setOther_allowance_own(String other_allowance_own) {
		this.other_allowance_own = other_allowance_own;
	}
	public String getOther_allowance_con() {
		return other_allowance_con;
	}
	public void setOther_allowance_con(String other_allowance_con) {
		this.other_allowance_con = other_allowance_con;
	}
	public String getGratuity_own() {
		return gratuity_own;
	}
	public void setGratuity_own(String gratuity_own) {
		this.gratuity_own = gratuity_own;
	}
	public String getGratuity_con() {
		return gratuity_con;
	}
	public void setGratuity_con(String gratuity_con) {
		this.gratuity_con = gratuity_con;
	}
	public String getPf_contrib_own() {
		return pf_contrib_own;
	}
	public void setPf_contrib_own(String pf_contrib_own) {
		this.pf_contrib_own = pf_contrib_own;
	}
	public String getPf_contrib_con() {
		return pf_contrib_con;
	}
	public void setPf_contrib_con(String pf_contrib_con) {
		this.pf_contrib_con = pf_contrib_con;
	}
	public String getPension_contrib_own() {
		return pension_contrib_own;
	}
	public void setPension_contrib_own(String pension_contrib_own) {
		this.pension_contrib_own = pension_contrib_own;
	}
	public String getPension_contrib_con() {
		return pension_contrib_con;
	}
	public void setPension_contrib_con(String pension_contrib_con) {
		this.pension_contrib_con = pension_contrib_con;
	}
	public String getBonus_own() {
		return bonus_own;
	}
	public void setBonus_own(String bonus_own) {
		this.bonus_own = bonus_own;
	}
	public String getLtc_own() {
		return ltc_own;
	}
	public void setLtc_own(String ltc_own) {
		this.ltc_own = ltc_own;
	}
	public String getLtc_con() {
		return ltc_con;
	}
	public void setLtc_con(String ltc_con) {
		this.ltc_con = ltc_con;
	}
	public String getBonus_con() {
		return bonus_con;
	}
	public void setBonus_con(String bonus_con) {
		this.bonus_con = bonus_con;
	}
	public String getLeave_encashment_own() {
		return leave_encashment_own;
	}
	public void setLeave_encashment_own(String leave_encashment_own) {
		this.leave_encashment_own = leave_encashment_own;
	}
	public String getLeave_encashment_con() {
		return leave_encashment_con;
	}
	public void setLeave_encashment_con(String leave_encashment_con) {
		this.leave_encashment_con = leave_encashment_con;
	}
	public String getWelfare_expend_own() {
		return welfare_expend_own;
	}
	public void setWelfare_expend_own(String welfare_expend_own) {
		this.welfare_expend_own = welfare_expend_own;
	}
	public String getWelfare_expend_con() {
		return welfare_expend_con;
	}
	public void setWelfare_expend_con(String welfare_expend_con) {
		this.welfare_expend_con = welfare_expend_con;
	}
	public String getOther_own() {
		return other_own;
	}
	public void setOther_own(String other_own) {
		this.other_own = other_own;
	}
	public String getOther_con() {
		return other_con;
	}
	public void setOther_con(String other_con) {
		this.other_con = other_con;
	}
	public String getShortfall_pf_own() {
		return shortfall_pf_own;
	}
	public void setShortfall_pf_own(String shortfall_pf_own) {
		this.shortfall_pf_own = shortfall_pf_own;
	}
	public String getShortfall_pf_con() {
		return shortfall_pf_con;
	}
	public void setShortfall_pf_con(String shortfall_pf_con) {
		this.shortfall_pf_con = shortfall_pf_con;
	}
	public String getEmp_incentive_own() {
		return emp_incentive_own;
	}
	public void setEmp_incentive_own(String emp_incentive_own) {
		this.emp_incentive_own = emp_incentive_own;
	}
	public String getEmp_incentive_con() {
		return emp_incentive_con;
	}
	public void setEmp_incentive_con(String emp_incentive_con) {
		this.emp_incentive_con = emp_incentive_con;
	}
	public String getPersonnel_no_own() {
		return personnel_no_own;
	}
	public void setPersonnel_no_own(String personnel_no_own) {
		this.personnel_no_own = personnel_no_own;
	}
	public String getPersonnel_no_con() {
		return personnel_no_con;
	}
	public void setPersonnel_no_con(String personnel_no_con) {
		this.personnel_no_con = personnel_no_con;
	}
}
