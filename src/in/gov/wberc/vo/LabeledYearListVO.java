package in.gov.wberc.vo;

import java.util.ArrayList;

public class LabeledYearListVO implements TransInterfaceVO {

	private ArrayList<YearListVO> labeledYearsAl = null;

	public ArrayList<YearListVO> getLabeledYearsAl() {
		return labeledYearsAl;
	}

	public void setLabeledYearsAl(ArrayList<YearListVO> labeledYearsAl) {
		this.labeledYearsAl = labeledYearsAl;
	}
	
}
