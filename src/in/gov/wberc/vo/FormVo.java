/*12.01.2018 Priyanka..@Created*/
package in.gov.wberc.vo;

import java.io.Serializable;

public class FormVo implements Serializable, TransInterfaceVO {
	
	private String formid;
	private String form_code;
	private String form_desc;
	private String unit_of_measurement;
	private String annexure_id;
	private String layout_code;//19.01.2018
	private String annexure_code;//19.01.2018
	private String annexure_desc;//19.01.2018
	
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	public String getForm_code() {
		return form_code;
	}
	public void setForm_code(String form_code) {
		this.form_code = form_code;
	}
	public String getForm_desc() {
		return form_desc;
	}
	public void setForm_desc(String form_desc) {
		this.form_desc = form_desc;
	}
	public String getUnit_of_measurement() {
		return unit_of_measurement;
	}
	public void setUnit_of_measurement(String unit_of_measurement) {
		this.unit_of_measurement = unit_of_measurement;
	}
	public String getAnnexure_id() {
		return annexure_id;
	}
	public void setAnnexure_id(String annexure_id) {
		this.annexure_id = annexure_id;
	}
	public String getLayout_code() {
		return layout_code;
	}
	public void setLayout_code(String layout_code) {
		this.layout_code = layout_code;
	}
	public String getAnnexure_code() {
		return annexure_code;
	}
	public void setAnnexure_code(String annexure_code) {
		this.annexure_code = annexure_code;
	}
	public String getAnnexure_desc() {
		return annexure_desc;
	}
	public void setAnnexure_desc(String annexure_desc) {
		this.annexure_desc = annexure_desc;
	}
	
}
