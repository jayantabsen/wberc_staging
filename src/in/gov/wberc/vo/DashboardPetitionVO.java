package in.gov.wberc.vo;

import java.util.ArrayList;

public class DashboardPetitionVO implements TransInterfaceVO {

	private ArrayList<ArrayList<String>> petitions;

	public ArrayList<ArrayList<String>> getPetitions() {
		return petitions;
	}

	public void setPetitions(ArrayList<ArrayList<String>> petitions) {
		this.petitions = petitions;
	}
	
}
