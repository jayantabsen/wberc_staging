package in.gov.wberc.vo;

import java.util.ArrayList;

public class StationRowVO {

	private String station_type = "";
	private ArrayList<ArrayList<String>> valForStationType;
	public String getStation_type() {
		return station_type;
	}
	public void setStation_type(String station_type) {
		this.station_type = station_type;
	}
	public ArrayList<ArrayList<String>> getValForStationType() {
		return valForStationType;
	}
	public void setValForStationType(ArrayList<ArrayList<String>> valForStationType) {
		this.valForStationType = valForStationType;
	}
	
}
