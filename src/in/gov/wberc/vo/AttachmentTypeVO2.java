package in.gov.wberc.vo;

public class AttachmentTypeVO2 implements TransInterfaceVO {

	private String attachment_type_id;
	private String attachment_type_desc;

	public String getAttachment_type_id() {
		return attachment_type_id;
	}

	public void setAttachment_type_id(String attachment_type_id) {
		this.attachment_type_id = attachment_type_id;
	}

	public String getAttachment_type_desc() {
		return attachment_type_desc;
	}

	public void setAttachment_type_desc(String attachment_type_desc) {
		this.attachment_type_desc = attachment_type_desc;
	}
	
}
