package in.gov.wberc.vo;

public class MiscFormLabelVO {

	private int misc_form_label_id;
	private int form_id;
	private boolean iscolumn;
	private int sequence;
	
	public int getMisc_form_label_id() {
		return misc_form_label_id;
	}
	public void setMisc_form_label_id(int misc_form_label_id) {
		this.misc_form_label_id = misc_form_label_id;
	}
	public int getForm_id() {
		return form_id;
	}
	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}
	public boolean isIscolumn() {
		return iscolumn;
	}
	public void setIscolumn(boolean iscolumn) {
		this.iscolumn = iscolumn;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
}
