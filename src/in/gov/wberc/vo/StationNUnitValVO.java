package in.gov.wberc.vo;

import java.util.ArrayList;

public class StationNUnitValVO {
	private String station = "";
	private ArrayList<ArrayList<String>> unitValForStation;
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public ArrayList<ArrayList<String>> getUnitValForStation() {
		return unitValForStation;
	}
	public void setUnitValForStation(ArrayList<ArrayList<String>> unitValForStation) {
		this.unitValForStation = unitValForStation;
	}
	
	
}
