/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class FormP_D4jJExcelDataVO implements TransInterfaceVO{
	// Get Form Id
	private String form_id;
	// Get Ph Id
	private String ph_id;
	// Get User Id
	private String user_id;
	// Get Station id
	private String stn_id;
	// Get Misc Column Names Here
	private ArrayList<String> colNames;
	// Get Misc ids for columns
	private ArrayList<String> cols;
	// Get Misc ids for rows
	private ArrayList<String> miscids;
	// Get vals
	private ArrayList<ArrayList<String>> vals;
	
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public ArrayList<String> getColNames() {
		return colNames;
	}
	public void setColNames(ArrayList<String> colNames) {
		this.colNames = colNames;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getStn_id() {
		return stn_id;
	}
	public void setStn_id(String stn_id) {
		this.stn_id = stn_id;
	}
	
}
