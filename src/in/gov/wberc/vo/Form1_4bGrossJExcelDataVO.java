//*** By Najma

package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_4bGrossJExcelDataVO implements TransInterfaceVO {

	// pyid-s
	private ArrayList<String> cols;
	// seasons-s
	private ArrayList<String> seasons;
	// gross-vals
	private ArrayList<ArrayList<String>> gross;
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<String> seasons) {
		this.seasons = seasons;
	}
	public ArrayList<ArrayList<String>> getGross() {
		return gross;
	}
	public void setGross(ArrayList<ArrayList<String>> gross) {
		this.gross = gross;
	}
	
}
