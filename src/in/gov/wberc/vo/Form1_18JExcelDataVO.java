package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_18JExcelDataVO implements TransInterfaceVO {
	
	private String py_id;
	private ArrayList<String> cols;
	private ArrayList<String> miscids;
	private ArrayList<ArrayList<String>> vals;
	private String formid;
	
	
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	
}
