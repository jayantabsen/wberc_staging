package in.gov.wberc.vo;

import java.util.ArrayList;

public class StationListF1_3VO implements TransInterfaceVO {

	private ArrayList<StationF1_3VO> stations = null;

	public ArrayList<StationF1_3VO> getStations() {
		return stations;
	}

	public void setStations(ArrayList<StationF1_3VO> stations) {
		this.stations = stations;
	}
	
	
}
