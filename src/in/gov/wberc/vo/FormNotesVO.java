package in.gov.wberc.vo;

import java.util.ArrayList;

public class FormNotesVO implements TransInterfaceVO {

	private ArrayList<String> notes;

	public ArrayList<String> getNotes() {
		return notes;
	}

	public void setNotes(ArrayList<String> notes) {
		this.notes = notes;
	}
	
}
