package in.gov.wberc.vo;

import java.util.ArrayList;

public class StationNPetitionYearVO implements TransInterfaceVO {

	private String stationid = null;
	private String stationnm = null;
	private String capacity = null;
	private String unit_of_calc = null;
	private ArrayList<YearListVO> labeledYearsAl = null;
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	public String getStationnm() {
		return stationnm;
	}
	public void setStationnm(String stationnm) {
		this.stationnm = stationnm;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getUnit_of_calc() {
		return unit_of_calc;
	}
	public void setUnit_of_calc(String unit_of_calc) {
		this.unit_of_calc = unit_of_calc;
	}
	public ArrayList<YearListVO> getLabeledYearsAl() {
		return labeledYearsAl;
	}
	public void setLabeledYearsAl(ArrayList<YearListVO> labeledYearsAl) {
		this.labeledYearsAl = labeledYearsAl;
	}
	
	
}
