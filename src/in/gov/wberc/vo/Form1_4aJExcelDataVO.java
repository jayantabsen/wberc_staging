package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_4aJExcelDataVO implements TransInterfaceVO {

	// station
	private String stationid;
	// pyid-s
	private ArrayList<String> cols;
	// seasons-s
	private ArrayList<String> seasons;
	// gross-vals
	private ArrayList<ArrayList<String>> vals;
	
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<String> seasons) {
		this.seasons = seasons;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	
}
