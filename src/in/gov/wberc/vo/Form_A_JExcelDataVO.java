/*** @author Najma on 25-Jan-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form_A_JExcelDataVO implements TransInterfaceVO {
	
	//	Station id
	private String stn_id;
	//  Year id
	private String py_id;
	//  Form id
	private String form_id;
	//  User id
	private String user_id;

	//  Values
	private ArrayList<ArrayList<String>> vals;
	
	public String getStn_id() {
		return stn_id;
	}
	public void setStn_id(String stn_id) {
		this.stn_id = stn_id;
	}
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}




}
