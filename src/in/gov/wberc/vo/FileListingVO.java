package in.gov.wberc.vo;

import java.util.ArrayList;

public class FileListingVO implements TransInterfaceVO {

	private ArrayList<DocumentVO> fileList;

	public ArrayList<DocumentVO> getFileList() {
		return fileList;
	}

	public void setFileList(ArrayList<DocumentVO> fileList) {
		this.fileList = fileList;
	}
	
}
