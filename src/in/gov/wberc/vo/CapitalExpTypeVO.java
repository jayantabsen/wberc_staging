//01.02.2018 Priyanka
package in.gov.wberc.vo;

public class CapitalExpTypeVO {
	
	private int capital_exp_type_id;
	private String capital_exp_type_code;
	private String capital_exp_type_desc;
	
	public int getCapital_exp_type_id() {
		return capital_exp_type_id;
	}
	public void setCapital_exp_type_id(int capital_exp_type_id) {
		this.capital_exp_type_id = capital_exp_type_id;
	}
	public String getCapital_exp_type_code() {
		return capital_exp_type_code;
	}
	public void setCapital_exp_type_code(String capital_exp_type_code) {
		this.capital_exp_type_code = capital_exp_type_code;
	}
	public String getCapital_exp_type_desc() {
		return capital_exp_type_desc;
	}
	public void setCapital_exp_type_desc(String capital_exp_type_desc) {
		this.capital_exp_type_desc = capital_exp_type_desc;
	}
	
}
