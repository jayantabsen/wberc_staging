package in.gov.wberc.vo;

import java.util.ArrayList;

public class From1_2JExcelDataVO implements TransInterfaceVO {

	// types-s
	private ArrayList<String> types;
	// stationid-s
	private ArrayList<String> rows;
	// pyid-s
	private ArrayList<String> cols;
	// vals
	private ArrayList<StationRowVO> vals;
	
	public ArrayList<String> getTypes() {
		return types;
	}
	public void setTypes(ArrayList<String> types) {
		this.types = types;
	}
	public ArrayList<String> getRows() {
		return rows;
	}
	public void setRows(ArrayList<String> rows) {
		this.rows = rows;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<StationRowVO> getVals() {
		return vals;
	}
	public void setVals(ArrayList<StationRowVO> vals) {
		this.vals = vals;
	}
		
}
