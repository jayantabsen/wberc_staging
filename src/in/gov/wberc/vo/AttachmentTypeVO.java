package in.gov.wberc.vo;

public class AttachmentTypeVO implements TransInterfaceVO {

	private String attachment_type_desc;

	public String getAttachment_type_desc() {
		return attachment_type_desc;
	}

	public void setAttachment_type_desc(String attachment_type_desc) {
		this.attachment_type_desc = attachment_type_desc;
	}
	
}
