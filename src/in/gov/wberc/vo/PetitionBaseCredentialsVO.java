package in.gov.wberc.vo;

public class PetitionBaseCredentialsVO implements TransInterfaceVO {

	private String req;
	private String ph_id;

	public String getReq() {
		return req;
	}
	public void setReq(String req) {
		this.req = req;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	
	
}
