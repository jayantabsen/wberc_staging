//*** By JBS
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_18c2JExcelDataVO implements TransInterfaceVO {
	
	private String ph_id;
	private String form_id;
	private String user_id;
	private ArrayList<String> cols;
	private ArrayList<ArrayList<String>> vals;
	
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	
}
