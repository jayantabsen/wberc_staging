//*** By JBS
package in.gov.wberc.vo;

public class Form1_17iJExcelDataVO implements TransInterfaceVO {
	
	private String py_id;
	private String form_id;
	private String user_id;
	
	private String exp;
	private String basic_pay;
	private String dear_allowance;
	private String other_allowance;
	private String gratuity;
	private String pf_contrib;
	private String pension_contrib;
	private String bonus;
	private String ltc;
	private String leave_encashment;
	private String welfare_expend;
	private String other;
	private String shortfall_pf;
	private String emp_incentive;
	private String personnel_no;
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}
	public String getBasic_pay() {
		return basic_pay;
	}
	public void setBasic_pay(String basic_pay) {
		this.basic_pay = basic_pay;
	}
	public String getDear_allowance() {
		return dear_allowance;
	}
	public void setDear_allowance(String dear_allowance) {
		this.dear_allowance = dear_allowance;
	}
	public String getOther_allowance() {
		return other_allowance;
	}
	public void setOther_allowance(String other_allowance) {
		this.other_allowance = other_allowance;
	}
	public String getGratuity() {
		return gratuity;
	}
	public void setGratuity(String gratuity) {
		this.gratuity = gratuity;
	}
	public String getPf_contrib() {
		return pf_contrib;
	}
	public void setPf_contrib(String pf_contrib) {
		this.pf_contrib = pf_contrib;
	}
	public String getPension_contrib() {
		return pension_contrib;
	}
	public void setPension_contrib(String pension_contrib) {
		this.pension_contrib = pension_contrib;
	}
	public String getBonus() {
		return bonus;
	}
	public void setBonus(String bonus) {
		this.bonus = bonus;
	}
	public String getLtc() {
		return ltc;
	}
	public void setLtc(String ltc) {
		this.ltc = ltc;
	}
	public String getLeave_encashment() {
		return leave_encashment;
	}
	public void setLeave_encashment(String leave_encashment) {
		this.leave_encashment = leave_encashment;
	}
	public String getWelfare_expend() {
		return welfare_expend;
	}
	public void setWelfare_expend(String welfare_expend) {
		this.welfare_expend = welfare_expend;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public String getShortfall_pf() {
		return shortfall_pf;
	}
	public void setShortfall_pf(String shortfall_pf) {
		this.shortfall_pf = shortfall_pf;
	}
	public String getEmp_incentive() {
		return emp_incentive;
	}
	public void setEmp_incentive(String emp_incentive) {
		this.emp_incentive = emp_incentive;
	}
	public String getPersonnel_no() {
		return personnel_no;
	}
	public void setPersonnel_no(String personnel_no) {
		this.personnel_no = personnel_no;
	}
}
