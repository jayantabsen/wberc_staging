package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_18JExcelDataVO_1 implements TransInterfaceVO {

	private String form_id;
	private String util_id;
	private String ph_id;
	private String user_id;
	private ArrayList<String> cols; // py_id(s)
	private ArrayList<String> rows1; // stn_id(s) + formula
	private ArrayList<String> rows2; // misc_form_label_id(s)
	private Form1_18JExcelDataVO_2 val;
	
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getUtil_id() {
		return util_id;
	}
	public void setUtil_id(String util_id) {
		this.util_id = util_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getRows1() {
		return rows1;
	}
	public void setRows1(ArrayList<String> rows1) {
		this.rows1 = rows1;
	}
	public ArrayList<String> getRows2() {
		return rows2;
	}
	public void setRows2(ArrayList<String> rows2) {
		this.rows2 = rows2;
	}
	public Form1_18JExcelDataVO_2 getVal() {
		return val;
	}
	public void setVal(Form1_18JExcelDataVO_2 val) {
		this.val = val;
	}
	
}
