package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_3JExcelDataVO implements TransInterfaceVO {

	// station
	private String stationid;
	// pyid-s
	private ArrayList<String> cols;
	// seasons-s
	private ArrayList<String> seasons;
	// usage / to_of_day
	private ArrayList<ArrayList<String>> usage;
	// vals
	private ArrayList<F1_3SeasonUsageVO1> vals;
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<String> seasons) {
		this.seasons = seasons;
	}

	public ArrayList<ArrayList<String>> getUsage() {
		return usage;
	}
	public void setUsage(ArrayList<ArrayList<String>> usage) {
		this.usage = usage;
	}
	public ArrayList<F1_3SeasonUsageVO1> getVals() {
		return vals;
	}
	public void setVals(ArrayList<F1_3SeasonUsageVO1> vals) {
		this.vals = vals;
	}
	
}
