//*** By Najma

package in.gov.wberc.vo;

public class SourcePyCredentialVO {

	private String formid = "";
	private String pdid = "";
	private String sourceid = "";
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	public String getPdid() {
		return pdid;
	}
	public void setPdid(String pdid) {
		this.pdid = pdid;
	}
	public String getSourceid() {
		return sourceid;
	}
	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

}
