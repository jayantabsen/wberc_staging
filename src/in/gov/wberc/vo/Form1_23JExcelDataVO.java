/*** @author Najma on 30-Jan-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_23JExcelDataVO implements TransInterfaceVO {
	//	Station id
	private String stn_id;
	//	Form Id
	private String form_id;
	//	Ph Id
	private String ph_id;
	//	User Id
	private String user_id;
	//  Petition year
	private ArrayList<String> cols;

	//  Get Misc ids
	private ArrayList<String> miscid_for_Incentive_for_Generation;
	private ArrayList<String> miscid_for_Incentive_for_Hydropower;
	private ArrayList<String> miscid_for_Incentive_for_Transmission;
	//	Get vals
	private ArrayList<ArrayList<String>> vals_for_Incentive_for_Generation;
	private ArrayList<ArrayList<String>> vals_for_Incentive_for_Hydropower;
	private ArrayList<ArrayList<String>> vals_for_Incentive_for_Transmission;

	
	public String getStn_id() {
		return stn_id;
	}
	public void setStn_id(String stn_id) {
		this.stn_id = stn_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getPh_id() {
		return ph_id;
	}
	public void setPh_id(String ph_id) {
		this.ph_id = ph_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getMiscid_for_Incentive_for_Generation() {
		return miscid_for_Incentive_for_Generation;
	}
	public void setMiscid_for_Incentive_for_Generation(ArrayList<String> miscid_for_Incentive_for_Generation) {
		this.miscid_for_Incentive_for_Generation = miscid_for_Incentive_for_Generation;
	}
	public ArrayList<String> getMiscid_for_Incentive_for_Hydropower() {
		return miscid_for_Incentive_for_Hydropower;
	}
	public void setMiscid_for_Incentive_for_Hydropower(ArrayList<String> miscid_for_Incentive_for_Hydropower) {
		this.miscid_for_Incentive_for_Hydropower = miscid_for_Incentive_for_Hydropower;
	}
	public ArrayList<String> getMiscid_for_Incentive_for_Transmission() {
		return miscid_for_Incentive_for_Transmission;
	}
	public void setMiscid_for_Incentive_for_Transmission(ArrayList<String> miscid_for_Incentive_for_Transmission) {
		this.miscid_for_Incentive_for_Transmission = miscid_for_Incentive_for_Transmission;
	}
	public ArrayList<ArrayList<String>> getVals_for_Incentive_for_Generation() {
		return vals_for_Incentive_for_Generation;
	}
	public void setVals_for_Incentive_for_Generation(ArrayList<ArrayList<String>> vals_for_Incentive_for_Generation) {
		this.vals_for_Incentive_for_Generation = vals_for_Incentive_for_Generation;
	}
	public ArrayList<ArrayList<String>> getVals_for_Incentive_for_Hydropower() {
		return vals_for_Incentive_for_Hydropower;
	}
	public void setVals_for_Incentive_for_Hydropower(ArrayList<ArrayList<String>> vals_for_Incentive_for_Hydropower) {
		this.vals_for_Incentive_for_Hydropower = vals_for_Incentive_for_Hydropower;
	}
	public ArrayList<ArrayList<String>> getVals_for_Incentive_for_Transmission() {
		return vals_for_Incentive_for_Transmission;
	}
	public void setVals_for_Incentive_for_Transmission(ArrayList<ArrayList<String>> vals_for_Incentive_for_Transmission) {
		this.vals_for_Incentive_for_Transmission = vals_for_Incentive_for_Transmission;
	}

	
	
}
