package in.gov.wberc.vo;

public class CreateNewPetition4ARRVO implements TransInterfaceVO {

	private String phid = "";
	private String utilid = "";
	public String getPhid() {
		return phid;
	}
	public void setPhid(String phid) {
		this.phid = phid;
	}
	public String getUtilid() {
		return utilid;
	}
	public void setUtilid(String utilid) {
		this.utilid = utilid;
	}
	
}
