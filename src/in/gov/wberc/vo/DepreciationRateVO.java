//01.02.2018 Priyanka
package in.gov.wberc.vo;

public class DepreciationRateVO {
	
	private int depreciation_rate_id;
	private String  depreciation_rate_desc;
	private String  depreciation_rate_code;
	private boolean isactive;
	private int sequence;
	
	public int getDepreciation_rate_id() {
		return depreciation_rate_id;
	}
	public void setDepreciation_rate_id(int depreciation_rate_id) {
		this.depreciation_rate_id = depreciation_rate_id;
	}
	public String getDepreciation_rate_desc() {
		return depreciation_rate_desc;
	}
	public void setDepreciation_rate_desc(String depreciation_rate_desc) {
		this.depreciation_rate_desc = depreciation_rate_desc;
	}
	public String getDepreciation_rate_code() {
		return depreciation_rate_code;
	}
	public void setDepreciation_rate_code(String depreciation_rate_code) {
		this.depreciation_rate_code = depreciation_rate_code;
	}
	public boolean isIsactive() {
		return isactive;
	}
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

}
