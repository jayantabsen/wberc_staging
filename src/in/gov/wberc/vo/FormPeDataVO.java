package in.gov.wberc.vo;

public class FormPeDataVO {

	private String package_name;
	private String work_scope;
	private String awarded_by;
	private String bid_recv_no;
	private String award_date;
	private String work_start_date;
	private String work_end_date;
	private String award_value;
	private String firm_escalated;
	private String actual_expenditure;
	private String tax;
	private String cost;
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public String getWork_scope() {
		return work_scope;
	}
	public void setWork_scope(String work_scope) {
		this.work_scope = work_scope;
	}
	public String getAwarded_by() {
		return awarded_by;
	}
	public void setAwarded_by(String awarded_by) {
		this.awarded_by = awarded_by;
	}
	public String getBid_recv_no() {
		return bid_recv_no;
	}
	public void setBid_recv_no(String bid_recv_no) {
		this.bid_recv_no = bid_recv_no;
	}
	public String getAward_date() {
		return award_date;
	}
	public void setAward_date(String award_date) {
		this.award_date = award_date;
	}
	public String getWork_start_date() {
		return work_start_date;
	}
	public void setWork_start_date(String work_start_date) {
		this.work_start_date = work_start_date;
	}
	public String getWork_end_date() {
		return work_end_date;
	}
	public void setWork_end_date(String work_end_date) {
		this.work_end_date = work_end_date;
	}
	public String getAward_value() {
		return award_value;
	}
	public void setAward_value(String award_value) {
		this.award_value = award_value;
	}
	public String getFirm_escalated() {
		return firm_escalated;
	}
	public void setFirm_escalated(String firm_escalated) {
		this.firm_escalated = firm_escalated;
	}
	public String getActual_expenditure() {
		return actual_expenditure;
	}
	public void setActual_expenditure(String actual_expenditure) {
		this.actual_expenditure = actual_expenditure;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	
}
