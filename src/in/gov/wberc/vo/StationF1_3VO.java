package in.gov.wberc.vo;

public class StationF1_3VO {
	
	private String id = "";
	private String name = "";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
