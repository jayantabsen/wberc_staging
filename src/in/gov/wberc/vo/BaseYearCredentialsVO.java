package in.gov.wberc.vo;

public class BaseYearCredentialsVO {

	private String phid = "";
	private String formid = "";

	public String getPhid() {
		return phid;
	}

	public void setPhid(String phid) {
		this.phid = phid;
	}

	public String getFormid() {
		return formid;
	}

	public void setFormid(String formid) {
		this.formid = formid;
	}
	
}
