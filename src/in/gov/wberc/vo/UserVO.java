package in.gov.wberc.vo;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class UserVO implements Serializable, TransInterfaceVO {
	
	private String name = "";
	private String email = "";
	private String phone = "";
	private String loginid = "";
	private String lastlogin = "";
	private String utilid = "";
	
	/*Priyanka 11.01.2018 @start*/
	private String userid = ""; 
	private String roleid = ""; 
	private String role_desc = "";
	private String role_code = "";
	private String utilName = "";
	
	/*Priyanka 11.01.2018 @End*/
	
	private String token = "";
	private ArrayList<MenuStatusVO> menu_status;
	
	public String getUtilid() {
		return utilid;
	}
	public void setUtilid(String utilid) {
		this.utilid = utilid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public ArrayList<MenuStatusVO> getMenu_status() {
		return menu_status;
	}
	public void setMenu_status(ArrayList<MenuStatusVO> menu_status) {
		this.menu_status = menu_status;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getRole_desc() {
		return role_desc;
	}
	public void setRole_desc(String role_desc) {
		this.role_desc = role_desc;
	}
	public String getRole_code() {
		return role_code;
	}
	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}
	public String getUtilName() {
		return utilName;
	}
	public void setUtilName(String utilName) {
		this.utilName = utilName;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
}
