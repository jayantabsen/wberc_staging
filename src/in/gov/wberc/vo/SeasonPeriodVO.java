package in.gov.wberc.vo;

public class SeasonPeriodVO  {
	
	private int seasonid;
	private String season_nm;
	private int periodid;
	private String period_nm;

	public int getSeasonid() {
		return seasonid;
	}
	public void setSeasonid(int seasonid) {
		this.seasonid = seasonid;
	}
	public String getSeason_nm() {
		return season_nm;
	}
	public void setSeason_nm(String season_nm) {
		this.season_nm = season_nm;
	}
	public int getPeriodid() {
		return periodid;
	}
	public void setPeriodid(int periodid) {
		this.periodid = periodid;
	}
	public String getPeriod_nm() {
		return period_nm;
	}
	public void setPeriod_nm(String period_nm) {
		this.period_nm = period_nm;
	}

	
}
