package in.gov.wberc.vo;

public class TokenVO implements TransInterfaceVO {
	private String token;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
