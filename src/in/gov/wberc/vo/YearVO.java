package in.gov.wberc.vo;

public class YearVO implements TransInterfaceVO {
	
	private String yearval = "";
	
	public String getYearval() {
		return yearval;
	}
	public void setYearval(String yearval) {
		this.yearval = yearval;
	}
	
}
