package in.gov.wberc.vo;

import java.util.ArrayList;

public class AttachmentTypeListingVO implements TransInterfaceVO {

	private ArrayList<AttachmentTypeVO2> vals;

	public ArrayList<AttachmentTypeVO2> getVals() {
		return vals;
	}

	public void setVals(ArrayList<AttachmentTypeVO2> vals) {
		this.vals = vals;
	}
	
}
