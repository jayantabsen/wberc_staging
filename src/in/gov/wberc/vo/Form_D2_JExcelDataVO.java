/*** @author Najma on 29-Jan-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form_D2_JExcelDataVO implements TransInterfaceVO {
	
	//	Station id
	private String stationid;
	//	Form Id
	private String formid;
	//	Ph Id
	private String phid;
	//  Get Misc ids
	private ArrayList<String> miscids;
	//	Get vals
	private ArrayList<ArrayList<String>> vals;
	
	public String getStationid() {
		return stationid;
	}
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	public String getPhid() {
		return phid;
	}
	public void setPhid(String phid) {
		this.phid = phid;
	}
	
	
}
