
//*** By JBS

package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_17jJExcelDataVO implements TransInterfaceVO {
	
	private String pyid;
	// Get Misc ids for only Top Headers
	private ArrayList<String> cols;
	// Get Misc ids for only Left Headers
	private ArrayList<String> miscids;
	// Get vals
	private ArrayList<ArrayList<String>> vals;
	
	public String getPyid() {
		return pyid;
	}
	public void setPyid(String pyid) {
		this.pyid = pyid;
	}
	public ArrayList<ArrayList<String>> getVals() {
		return vals;
	}
	public void setVals(ArrayList<ArrayList<String>> vals) {
		this.vals = vals;
	}
	public ArrayList<String> getMiscids() {
		return miscids;
	}
	public void setMiscids(ArrayList<String> miscids) {
		this.miscids = miscids;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}

}
