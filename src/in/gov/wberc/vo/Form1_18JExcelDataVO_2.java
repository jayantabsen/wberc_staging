package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_18JExcelDataVO_2 implements TransInterfaceVO {

	private ArrayList<ArrayList<String>> station_vals;
	private ArrayList<ArrayList<String>> misc_vals;
	
	public ArrayList<ArrayList<String>> getStation_vals() {
		return station_vals;
	}
	public void setStation_vals(ArrayList<ArrayList<String>> station_vals) {
		this.station_vals = station_vals;
	}
	public ArrayList<ArrayList<String>> getMisc_vals() {
		return misc_vals;
	}
	public void setMisc_vals(ArrayList<ArrayList<String>> misc_vals) {
		this.misc_vals = misc_vals;
	}
	
}
