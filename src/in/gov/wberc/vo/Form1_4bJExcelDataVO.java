package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_4bJExcelDataVO implements TransInterfaceVO {

	// station
	private String pump_stationid;
	// pyid-s
	private ArrayList<String> cols;
	// seasons-s
	private ArrayList<String> seasons;
	// usage / to_of_day
	private ArrayList<ArrayList<String>> usage;	// vals
	private ArrayList<F1_4bSeasonUsageVO> vals;

	public String getPump_stationid() {
		return pump_stationid;
	}
	public void setPump_stationid(String pump_stationid) {
		this.pump_stationid = pump_stationid;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<String> getSeasons() {
		return seasons;
	}
	public void setSeasons(ArrayList<String> seasons) {
		this.seasons = seasons;
	}

	public ArrayList<ArrayList<String>> getUsage() {
		return usage;
	}
	public void setUsage(ArrayList<ArrayList<String>> usage) {
		this.usage = usage;
	}
	public ArrayList<F1_4bSeasonUsageVO> getVals() {
		return vals;
	}
	public void setVals(ArrayList<F1_4bSeasonUsageVO> vals) {
		this.vals = vals;
	}
	
}
