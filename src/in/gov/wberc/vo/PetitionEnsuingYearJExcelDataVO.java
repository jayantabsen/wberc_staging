//*** By Najma
package in.gov.wberc.vo;

import java.util.ArrayList;

public class PetitionEnsuingYearJExcelDataVO implements TransInterfaceVO {

	private ArrayList<EnsuingYearVO> ensuing_petition_year;

	public ArrayList<EnsuingYearVO> getEnsuing_petition_year() {
		return ensuing_petition_year;
	}

	public void setEnsuing_petition_year(ArrayList<EnsuingYearVO> ensuing_petition_year) {
		this.ensuing_petition_year = ensuing_petition_year;
	}
	
}
