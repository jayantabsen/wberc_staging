package in.gov.wberc.vo;

import java.util.ArrayList;

public class PumpStationListF1_4bVO implements TransInterfaceVO {

	private ArrayList<PumpStationF1_4bVO> pump_stations = null;

	public ArrayList<PumpStationF1_4bVO> getPump_stations() {
		return pump_stations;
	}

	public void setPump_stations(ArrayList<PumpStationF1_4bVO> pump_stations) {
		this.pump_stations = pump_stations;
	}
	
}
