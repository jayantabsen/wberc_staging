package in.gov.wberc.vo;

import java.util.ArrayList;

public class From1_1aJExcelDataVO implements TransInterfaceVO {

	// unitid-s
	private ArrayList<String> rows;
	// pyid-s
	private ArrayList<String> cols;
	// vals
	private ArrayList<StationNUnitValVO> vals;
	private String formid;
	
	public ArrayList<String> getRows() {
		return rows;
	}
	public String getFormid() {
		return formid;
	}
	public void setFormid(String formid) {
		this.formid = formid;
	}
	public void setRows(ArrayList<String> rows) {
		this.rows = rows;
	}
	public ArrayList<String> getCols() {
		return cols;
	}
	public void setCols(ArrayList<String> cols) {
		this.cols = cols;
	}
	public ArrayList<StationNUnitValVO> getVals() {
		return vals;
	}
	public void setVals(ArrayList<StationNUnitValVO> vals) {
		this.vals = vals;
	}
	
}
