package in.gov.wberc.vo;

public class EnsuingYearVO {

	private String py_id;
	private String year_nm;
	public String getPy_id() {
		return py_id;
	}
	public void setPy_id(String py_id) {
		this.py_id = py_id;
	}
	public String getYear_nm() {
		return year_nm;
	}
	public void setYear_nm(String year_nm) {
		this.year_nm = year_nm;
	}
	
}
