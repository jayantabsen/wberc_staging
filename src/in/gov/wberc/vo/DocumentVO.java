/**
 * 
 */
package in.gov.wberc.vo;

/**
 * @author rajat
 *
 */
public class DocumentVO {
	
	private String docName;
	private String docId;
	private String docType;
	private String description;
	private String fileSize;
	private String createdOn;
	private String docAuthor;
	private String docURL;
	private String petitionHeaderId;
	private String formId;
	private String petitionId;
	private String docVersionLabel;
	private boolean isLatestVersion;
	private String path; // implemented by JBS.
	
	/**
	 * @return the docName
	 */
	public String getDocName() {
		return docName;
	}
	/**
	 * @param docName the docName to set
	 */
	public void setDocName(String docName) {
		this.docName = docName;
	}
	/**
	 * @return the docId
	 */
	public String getDocId() {
		return docId;
	}
	/**
	 * @param docId the docId to set
	 */
	public void setDocId(String docId) {
		this.docId = docId;
	}
	/**
	 * @return the docType
	 */
	public String getDocType() {
		return docType;
	}
	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the fileSize
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the docAuthor
	 */
	public String getDocAuthor() {
		return docAuthor;
	}
	/**
	 * @param docAuthor the docAuthor to set
	 */
	public void setDocAuthor(String docAuthor) {
		this.docAuthor = docAuthor;
	}
	/**
	 * @return the docURL
	 */
	public String getDocURL() {
		return docURL;
	}
	/**
	 * @param docURL the docURL to set
	 */
	public void setDocURL(String docURL) {
		this.docURL = docURL;
	}
	/**
	 * @return the petitionHeaderId
	 */
	public String getPetitionHeaderId() {
		return petitionHeaderId;
	}
	/**
	 * @param petitionHeaderId the petitionHeaderId to set
	 */
	public void setPetitionHeaderId(String petitionHeaderId) {
		this.petitionHeaderId = petitionHeaderId;
	}
	/**
	 * @return the formId
	 */
	public String getFormId() {
		return formId;
	}
	/**
	 * @param formId the formId to set
	 */
	public void setFormId(String formId) {
		this.formId = formId;
	}
	/**
	 * @return the petitionId
	 */
	public String getPetitionId() {
		return petitionId;
	}
	/**
	 * @param petitionId the petitionId to set
	 */
	public void setPetitionId(String petitionId) {
		this.petitionId = petitionId;
	}
	/**
	 * @return the docVersionLabel
	 */
	public String getDocVersionLabel() {
		return docVersionLabel;
	}
	/**
	 * @param docVersionLabel the docVersionLabel to set
	 */
	public void setDocVersionLabel(String docVersionLabel) {
		this.docVersionLabel = docVersionLabel;
	}
	/**
	 * @return the isLatestVersion
	 */
	public boolean isLatestVersion() {
		return isLatestVersion;
	}
	/**
	 * @param isLatestVersion the isLatestVersion to set
	 */
	public void setLatestVersion(boolean isLatestVersion) {
		this.isLatestVersion = isLatestVersion;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

}
