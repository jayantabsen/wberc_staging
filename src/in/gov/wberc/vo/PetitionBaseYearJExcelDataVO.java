package in.gov.wberc.vo;

import java.util.ArrayList;

public class PetitionBaseYearJExcelDataVO implements TransInterfaceVO {

	private ArrayList<ArrayList<String>> base_year;

	public ArrayList<ArrayList<String>> getBase_year() {
		return base_year;
	}

	public void setBase_year(ArrayList<ArrayList<String>> base_year) {
		this.base_year = base_year;
	}
	
}
