/*** @author Najma on 01-Feb-2018 */
package in.gov.wberc.vo;

import java.util.ArrayList;

public class Form1_19aObjectVO implements TransInterfaceVO{

	// Get values for all stations and formula
	private ArrayList<ArrayList<String>> vals_for_stations; 
	// Get values for all miscs
	private ArrayList<ArrayList<String>> vals_for_miscs_for_CapitalExpenditure;
	private ArrayList<ArrayList<String>> vals_for_miscs_for_SpecialProjects;
	
	public ArrayList<ArrayList<String>> getVals_for_stations() {
		return vals_for_stations;
	}
	public void setVals_for_stations(ArrayList<ArrayList<String>> vals_for_stations) {
		this.vals_for_stations = vals_for_stations;
	}
	public ArrayList<ArrayList<String>> getVals_for_miscs_for_CapitalExpenditure() {
		return vals_for_miscs_for_CapitalExpenditure;
	}
	public void setVals_for_miscs_for_CapitalExpenditure(
			ArrayList<ArrayList<String>> vals_for_miscs_for_CapitalExpenditure) {
		this.vals_for_miscs_for_CapitalExpenditure = vals_for_miscs_for_CapitalExpenditure;
	}
	public ArrayList<ArrayList<String>> getVals_for_miscs_for_SpecialProjects() {
		return vals_for_miscs_for_SpecialProjects;
	}
	public void setVals_for_miscs_for_SpecialProjects(ArrayList<ArrayList<String>> vals_for_miscs_for_SpecialProjects) {
		this.vals_for_miscs_for_SpecialProjects = vals_for_miscs_for_SpecialProjects;
	}
	
	
}
