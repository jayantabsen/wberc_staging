package in.gov.wberc.dms;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

public class UploadDocumentOnly {
	
	private static final String serverUrl = "http://182.75.177.246:8282/alfresco/cmisatom";
	private static final String username = "admin"; 
	private static final String password = "password";
	private static final String filePath = "/2017-18/WBPDCL/ARR";

	public static void main(String args[]) {
		
		org.apache.log4j.BasicConfigurator.configure();
		
		Session session = getSession();
		Folder parentFolder = getFolderByPath(session, filePath);
		
		String localFilePath = "/home/l33tlpt3/l33tws2/elec_content/WBERC/WBERC_Data_Model.pdf";
		//String localFilePath = "/home/l33tlpt3/l33tws2/elec_content/WBERC/myDocument01.txt";
		String fileName = "WBERC_Data_Model.pdf";
		//String fileName = "myDocument01.txt";
		String mime = "application/pdf";
		//String mime = "text/plain";
		createDocument(parentFolder, localFilePath, fileName, mime);
	}
	
	public static void fileUpload(String serverFilePath, String localFilePath, String fileName, String mime) {
		
		org.apache.log4j.BasicConfigurator.configure();
		
		Session session = getSession();
		Folder parentFolder = getFolderByPath(session, serverFilePath);

		createDocument(parentFolder, localFilePath, fileName, mime);
	}
	
	/**
	 * Finds folder by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Folder getFolderByPath(Session session, String path) {
		try {
		return (Folder) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
	public static Session getSession() {
		
		Map<String, String> sessionParameters = new HashMap<String, String>();
		sessionParameters.put(SessionParameter.USER, username);
		sessionParameters.put(SessionParameter.PASSWORD, password);
		sessionParameters.put(SessionParameter.ATOMPUB_URL, serverUrl);
		sessionParameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		Session session = sessionFactory.getRepositories(sessionParameters).get(0).createSession();

        return session;
	}
	
	public static void createDocument(Folder parentFolder, String localFilePath, String fileName, String mime) {
		// Creating file document
		Map<String, Object> documentProperties = new HashMap<String, Object>();
		String name = fileName;
		documentProperties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		documentProperties.put(PropertyIds.NAME, name);
		
		// Locating the file in local drive and get into File
		File file = new File(localFilePath);
		
		// Reading the file
		byte[] content = new byte[(int) file.length()];
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
		    fileInputStream.read(content);
		} catch (FileNotFoundException e) {
		    System.out.println("File Not Found.");
		    e.printStackTrace();
		} catch (IOException e1) {
		    System.out.println("Error Reading The File.");
		    e1.printStackTrace();
		}
		
		InputStream stream = new ByteArrayInputStream(content);
		ContentStream contentStream = new ContentStreamImpl(name, new BigInteger(content), mime, stream);
		Document newContent1 =  parentFolder.createDocument(documentProperties, contentStream, null);
		System.out.println("Document created: " + newContent1.getId());
	}
	
}
