package in.gov.wberc.dms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

public class CreateFolderOnly {
	
	private static final String serverUrl = "http://182.75.177.246:8282/alfresco/cmisatom";
	private static final String username = "admin"; 
	private static final String password = "password";

	public static void main(String args[]) {
		org.apache.log4j.BasicConfigurator.configure();
		
		Session session = getSession();
		String base_year = "2018-19";
		String entity = "WBPDCL";
		String module = "ARR";
		createSubFolder(session, serverUrl, username, password, "/WBERC/", base_year);
		createSubFolder(session, serverUrl, username, password, "/WBERC/"+base_year+"/", entity);
		createSubFolder(session, serverUrl, username, password, "/WBERC/"+base_year+"/"+entity, module);
	}
	
	public static void createPetitionFolderStructure(String base_year, String entity, String module) {
		org.apache.log4j.BasicConfigurator.configure();
		
		Session session = getSession();
		//creatingFirstLevelFolder(session, base_year);
		createSubFolder(session, serverUrl, username, password, "/WBERC/", base_year);
		createSubFolder(session, serverUrl, username, password, "/WBERC/"+base_year+"/", entity);
		createSubFolder(session, serverUrl, username, password, "/WBERC/"+base_year+"/"+entity, module);
	}
	
	public static void creatingFirstLevelFolder(Session session, String firstLevelFolder) {
		// Getting root folder
		Folder root = session.getRootFolder();
				
		// Creating 1st level of folder
		Map<String, Object> folderProperties = new HashMap<String, Object>();
		folderProperties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
		folderProperties.put(PropertyIds.NAME, firstLevelFolder);
		try {
			Folder newFolder = root.createFolder(folderProperties);
		} catch (Exception ex) { }
	}
	
	public static void createSubFolder(Session session, String serverUrl, String username, 
			String password, String parentFolder, String folderName) {
		Folder root = getFolderByPath(session, parentFolder);
	    Map<String, Object> properties = new HashMap<>();
	    properties.put(PropertyIds.OBJECT_TYPE_ID, BaseTypeId.CMIS_FOLDER.value());
	    properties.put(PropertyIds.NAME, folderName);
	    /*
	    List<Ace> addAces = new LinkedList<>();
	    List<Ace> removeAces = new LinkedList<>();
	    List<Policy> policies = new LinkedList<>();
	    Folder newFolder = root.createFolder(properties, policies, addAces, removeAces, getSession().getDefaultContext());
		*/
	    try {
	    	Folder newFolder = root.createFolder(properties);
	    } catch (Exception ex) { }
	    
	}
	
	public static Session getSession() {
		
		Map<String, String> sessionParameters = new HashMap<String, String>();
		sessionParameters.put(SessionParameter.USER, username);
		sessionParameters.put(SessionParameter.PASSWORD, password);
		sessionParameters.put(SessionParameter.ATOMPUB_URL, serverUrl);
		sessionParameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		Session session = sessionFactory.getRepositories(sessionParameters).get(0).createSession();

        return session;
    }
	
	/**
	 * Finds folder by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Folder getFolderByPath(Session session, String path) {
		try {
		return (Folder) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
}
