package in.gov.wberc.dms;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

public class FileDownloadOnly {
	
	private static final String serverUrl = "http://182.75.177.246:8282/alfresco/cmisatom";
	private static final String username = "admin"; 
	private static final String password = "password";
	private static final String fileName = "WBERC_Data_Model.pdf";
	//private static final String fileName = "myDocument01.txt";
	private static final String destinationFolder = "/home/l33tlpt3/Desktop/";
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		
		org.apache.log4j.BasicConfigurator.configure();
		
		FileDownloadOnly fd = new FileDownloadOnly();
		
		// searching the document id
		
		// downloading the file
		//fd.downloadDocumentByID(serverUrl, uername, password, "workspace://SpacesStore/2c189c93-d12c-43f4-ae64-a5b87965e46d;1.0", fileName, destinationFolder);
		//fd.downloadDocumentByID(serverUrl, username, password, "2c189c93-d12c-43f4-ae64-a5b87965e46d", fileName, destinationFolder);
		
		/*
		Folder folder = findFolderForPath(fd.getSession(), "/folder01/folder02/folder03");
		if (folder != null) {
			System.out.println("Folder found.");
			System.out.println("Name of the folder: "+folder.getName());
			ItemIterable<CmisObject> contentItems = folder.getChildren();
	        for (CmisObject contentItem : contentItems) {
	            if (contentItem instanceof Document) {
	                Document docMetadata = (Document) contentItem;
	                ContentStream docContent = docMetadata.getContentStream();
	                System.out.println(docMetadata.getName() + " [size=" + docContent.getLength() + "][Mimetype=" +
	                        docContent.getMimeType() + "][type=" + docMetadata.getType().getDisplayName() + 
	                        "][ID=" + docMetadata.getId() + "]");
	            } else {
	            	if (contentItem instanceof Folder) {
		            	Folder docMetadata = (Folder) contentItem;
		            	System.out.println(contentItem.getName() + " [type=" + contentItem.getType().getDisplayName() + 
		            			"][ID=" + docMetadata.getId() + "]");
	            	}
	            }
	        }
		}
		*/
		
		//Document document = findDocumentForPath(fd.getSession(), "/folder01/folder02/folder03/WBERC_Data_Model.pdf");
		Document document = findDocumentForPath(fd.getSession(), "/2017-18/WBPDCL/ARR/WBERC_Data_Model.pdf");
		//Document document = findDocumentForPath(fd.getSession(), "/folder01/folder02/folder03/myDocument01.txt");
		if (document != null) {
			System.out.println("Document found.");
			System.out.println("Name of the document: "+document.getName());
			String documentid = document.getId();
			System.out.println("ID of the document: "+document.getId());
			String actualId = documentid.substring(documentid.lastIndexOf('/')+1, documentid.indexOf(';')).trim();
			System.out.println("Actual ID of the document: "+actualId);
			fd.downloadDocumentByID(serverUrl, username, password, actualId, fileName, destinationFolder);
		}
		
	}
	
	public void downloadDocumentByID(String serverUrl, String username, String password ,
			String documentID, String fileName, String destinationFolder) {
		
		FileDownloadOnly fd = new FileDownloadOnly();
		String fullPath= destinationFolder + fileName;
		Document newDocument = (Document)fd.getSession().getObject(documentID);
		System.out.println(newDocument.getId());
		try {
			ContentStream cs = newDocument.getContentStream(null);
			BufferedInputStream in =new BufferedInputStream(cs.getStream());
            FileOutputStream fos = new FileOutputStream(fullPath);
            OutputStream bufferedOutputStream = new BufferedOutputStream(fos);
            byte[] buf = new byte[1024];
            int n=0;
            while ((n=in.read(buf))>0)
            {
                bufferedOutputStream.write(buf,0,n);
            }
            bufferedOutputStream.close();
            fos.close();
            in.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getLocalizedMessage());
		}
		
	}
		
	/**
	 * Finds folder by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Folder findFolderForPath(Session session, String path) {
		try {
		return (Folder) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
	/**
	 * Finds document by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Document findDocumentForPath(Session session, String path) {
		try {
		return (Document) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}

	public Session getSession() {
		
		Map<String, String> sessionParameters = new HashMap<String, String>();
		sessionParameters.put(SessionParameter.USER, username);
		sessionParameters.put(SessionParameter.PASSWORD, password);
		sessionParameters.put(SessionParameter.ATOMPUB_URL, serverUrl);
		sessionParameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		Session session = sessionFactory.getRepositories(sessionParameters).get(0).createSession();

        return session;
    }
	
}
