package in.gov.wberc.dms;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

public class FileListingOnly {

	private static final String serverUrl = "http://182.75.177.246:8282/alfresco/cmisatom";
	private static final String username = "admin"; 
	private static final String password = "password";
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		org.apache.log4j.BasicConfigurator.configure();
		
		Session session = getSession();
		//Folder parentFolder = getFolderByPath(session, "/folder01/folder02/folder03");
		Folder parentFolder = getFolderByPath(session, "/2017-18/WBPDCL/ARR");
		listTopFolder(session, parentFolder);
	}
	
	public static void listTopFolder(Session session, Folder parentFolder) {
        ItemIterable<CmisObject> contentItems = parentFolder.getChildren();
        for (CmisObject contentItem : contentItems) {
            if (contentItem instanceof Document) {
                Document docMetadata = (Document) contentItem;
                ContentStream docContent = docMetadata.getContentStream();
                String documentid = docMetadata.getId();
                System.out.println("[Name=" + docMetadata.getName()  + "]--[Mimetype=" + docContent.getMimeType() 
                	+ "]--[ID=" + documentid.substring(documentid.lastIndexOf('/')+1, documentid.indexOf(';')).trim() 
                	+ "]--[Path=" + docMetadata.getPaths() + "]");
            }
        }
    }
	
	/**
	 * Finds folder by path and if not found returns null.
	 * @param session
	 * @param path
	 * @return
	 */
	public static Folder getFolderByPath(Session session, String path) {
		try {
		return (Folder) session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException e) {
			return null;
		}
	}
	
	public static Session getSession() {
		
		Map<String, String> sessionParameters = new HashMap<String, String>();
		sessionParameters.put(SessionParameter.USER, username);
		sessionParameters.put(SessionParameter.PASSWORD, password);
		sessionParameters.put(SessionParameter.ATOMPUB_URL, serverUrl);
		sessionParameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
		Session session = sessionFactory.getRepositories(sessionParameters).get(0).createSession();

        return session;
	}
	
}
